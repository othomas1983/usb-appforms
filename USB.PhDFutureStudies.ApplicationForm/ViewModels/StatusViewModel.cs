﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PhDFutureStudies.ApplicationForm.ViewModels
{
    public class StatusViewModel
    {
        public bool PersonalDetailsComplete { get; set; }
        public bool AddressDetailsComplete  { get; set; }
        public bool WorkStudiesComplete     { get; set; }
        public bool DocumentationComplete   { get; set; }
        public bool PaymentComplete         { get; set; }
    }
}