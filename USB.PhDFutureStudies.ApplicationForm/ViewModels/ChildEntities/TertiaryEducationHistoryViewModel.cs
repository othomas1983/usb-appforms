﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PhDFutureStudies.ApplicationForm.ViewModels
{
    public class TertiaryEducationHistoryViewModel
    {
        public string Institution { get; set; }
        public string Degree { get; set; }
        public string FieldOfStudy { get; set; }
        public string StudentNumber { get; set; }
        public string Period { get; set; }
        public bool Completed { get; set; }
        public bool First { get; set; }
    }
}