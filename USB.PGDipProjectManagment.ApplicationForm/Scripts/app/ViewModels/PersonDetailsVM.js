﻿/// <reference path="../../knockout-3.3.0.debug.js" />
/// <reference path="../../knockout.validation.js" />

var knockoutValidationSettings = {
    insertMessages: true,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};



ko.validation.rules['simpleDate'] = {
    validator: function (val, validate) {
        return ko.validation.utils.isEmptyVal(val) || moment(val, 'YYYY/MM/DD').isValid();
    },
    message: 'Date required in YYYY/MM/DD format'
};

ko.validation.rules['futureDate'] = {
    validator: function (val, validate) {
        return ko.validation.utils.isEmptyVal(val) || moment(val, 'YYYY/MM/DD').isSame(Date.now(), 'day')  || moment(val, 'YYYY/MM/DD').isAfter(Date.now(), 'day');
    },
    message: 'The Expiry Date you entered is no longer valid'
};

ko.validation.rules['pastDate'] = {
    validator: function (val, validate) {
        return ko.validation.utils.isEmptyVal(val) || moment(val, 'YYYY/MM/DD').isSame(Date.now(), 'day') || moment(val, 'YYYY/MM/DD').isBefore(Date.now(), 'day');
    },
    message: 'The birth date you entered must be in the past'
};

ko.validation.rules['email'] = {
    validator: function (val, validate) {
        var re = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return ko.validation.utils.isEmptyVal(val) || re.test(val);
    },
    message: 'Invalid email address.'
};

ko.validation.rules['mobile'] = {
    validator: function (val, validate) {
        var isValid = isValidCellphoneFormat(val);
        return isValid;
    },
    message: 'Invalid cell phone number. For international numbers, please use the standard international number format.'
};

ko.validation.registerExtenders();
ko.validation.configuration = knockoutValidationSettings;

function isValidCellphoneFormat(number) {

    number = $.trim(number).replace(/ /g, "").replace('(', "").replace(')', "");

    var matches = number.substring(1).match(/[^+0-9]+/g);

    if (matches != null) {
        return false;
    }
    else {
        // local numbers
        if (number.charAt(0) == "0") {
            if (number.length == 10) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            // international numbers
            if (number.charAt(0) == "+") {

                if (number.charAt(1) == "0") {
                    return false;
                }

                if (number.charAt(3) == "0") {
                    return false;
                }

                if ((number.length >= 12) && (number.length <= 15)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }
}

function PersonalDetails(emailAddress, isDisabled, isFirstTimeAppplicant, enrolmentId) {
    
    var self = this;

    var rootUrl = "/PGDipProjectManagment/";

    self.GenderApi_url = rootUrl + "PersonalDropdown/LoadDropdown/";
    self.SubmitPersonalDetails_url = rootUrl + "Page/SubmitPersonalInformation";
    self.AddressDetails_url = rootUrl + "Page/AddressDetails";

    var usNumber = "";
    var emptyObject = "";
    var selectForeginNumber = ""; 
    var selectPassportNumber = "";
    var selectSouthAfrica = "";

    $("option[innerText='South Africa");

    //observable properties

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);
    self.isFirstTimeApplicant = ko.observable((isFirstTimeAppplicant !== null) ? isFirstTimeAppplicant : false);

    self.loading = ko.observableArray();

    // Observable arrays for dropdown options
    self.Genders = ko.observableArray();
    self.CorrespondenceLanguages = ko.observable();
    self.Languages = ko.observableArray();
    self.Nationalities = ko.observableArray();
    self.Titles = ko.observableArray();
    self.Ethnicities = ko.observableArray();
    self.NonSAEthnicities = ko.observableArray();
    self.Disabilities = ko.observableArray();
    self.AddressCountries = ko.observableArray();
    self.CountriesIssued = ko.observableArray();
    self.PersonalDetails = ko.observableArray();
    self.ForeignIndetificationTypes = ko.observableArray();
    self.PermitTypes = ko.observableArray();
    self.MaritalStatus = ko.observableArray();
    self.Offerings = ko.observableArray();
    self.Programmes = ko.observableArray();
    self.USNumber = ko.observableArray();

    self.ShowForeignNumberInput = ko.observable(false);
    self.ShowPassportNumberInput = ko.observable(false);
    self.ShowExpiryDateInput = ko.observable(true);
    self.IsSouthAfrican = ko.observable(false);
    self.IsNonSouthAfrican = ko.observable(false);

    // Observables for selected dropdown option
    self.SelectedGender = ko.observable();
    self.SelectedLanguage = ko.observable();
    self.SelectedNationality = ko.observable();
    self.SelectedTitle = ko.observable();
    self.SelectedEthnicity = ko.observable();
    self.SelectedDisability = ko.observable();
    self.SelectedCountryOfIssue = ko.observable();

    self.SelectedForeignIdentificationType = ko.observable();
    self.SelectedCorrespondanceLanguage = ko.observable();
    self.SelectedPermitType = ko.observable();
    self.SelectedMaritalStatus = ko.observable();
    self.SelectedOffering = ko.observable();

    // Observables for input values
    self.Initials = ko.observable();
    self.Surname = ko.observable();
    self.FirstNames = ko.observable();
    self.GivenName = ko.observable();
    self.IdNumber = ko.observable();
    self.PassportForeignNumber = ko.observable();
    self.DateOfBirth = ko.observable();
    self.MaidenName = ko.observable();
    self.OtherDisability = ko.observable();
    self.eMail = ko.observable((emailAddress !== null) ? emailAddress : '');

    self.UseWheelchair = ko.observable(false);
    self.BusinessPhoneNumber = ko.observable();
    self.HomePhoneNumber = ko.observable();
    self.FaxNumber = ko.observable();
    self.MobileNumber = ko.observable();
    self.ShowForgeinSection = ko.observable(false);
    self.PassportNumber = ko.observable();
    self.ExpiryDate = ko.observable();
    
    self.SouthAfricaIdentificationTypes = ko.observableArray(false);
    self.SouthAfricaIdentificationType = ko.observable();

    self.blendedOptions = ko.observableArray();
    self.SelectedBlendedOption = ko.observable();
    self.isBlendedOffering = ko.observable(false);
    self.AlternativeEmailAddress = ko.observable();

    self.hasOtherDisability = ko.observable(false);

    self.SelectedDisability.subscribe(function (data) {
        self.hasOtherDisability(false);
        self.OtherDisability("");
        if (data != undefined) {
            var matchingDisability = JSLINQ(self.Disabilities()).Where(function (item) {
                return item.Value == data;
            });

            if (matchingDisability.items.length > 0) {
                if (matchingDisability.items[0].Key.indexOf("Other") > -1) {
                    self.hasOtherDisability(true);
                }
            }
        }
    });

    self.SelectedOffering.subscribe(function (data) {
        self.isBlendedOffering(false);
        self.SelectedBlendedOption(null);

        if (data != undefined) {
            var matchingOffering = JSLINQ(self.Offerings()).Where(function (item) {
                return item.Item2 == data;
            });

            if (matchingOffering.items.length > 0) {
                if (matchingOffering.items[0].Item3 == 1) {
                    self.isBlendedOffering(true);
                }
            } else {
                self.SelectedOffering(null);
            }
        }
    });

    self.serverErrorMessage = ko.observable();

    self.ShowSAEthnicitesInput = ko.observable(true);

    self.resources = null;
    var loadDropDownUrl = "";
    self.loadDropDown = function (type) {
        if (enrolmentId == '00000000-0000-0000-0000-000000000000' || enrolmentId == null || enrolmentId == '') {

            loadDropDownUrl = "PersonalDropdown/LoadDropdown?enrolmentId=&type="
        }
        else {

            loadDropDownUrl = "PersonalDropdown/LoadDropdown?enrolmentId=" + enrolmentId + "&type="
        }
        self.loading.push(true);
        $.ajax({
            url: rootUrl + loadDropDownUrl + type,
            type: 'GET',
            contentType: "application/json",
            success: function (j) {
                if (j.dropdownList != undefined) {
                    self.populateDropDownList(j.dropdownList, type);
                } else if (j.dropdownGuidList != undefined) {
                    self.populateDropDownList(j.dropdownGuidList, type);
                } else if (j.tupleList != undefined) {
                    self.populateDropDownList(j.tupleList, type);
                }
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            self.loading.pop();
        });
    };

    self.preloadDropDownLists = function () {
        self.loadDropDown("gender");
        self.loadDropDown("permitTypes");
        self.loadDropDown("titles");
        self.loadDropDown("languages");
        self.loadDropDown("nationalities");
        self.loadDropDown("ethnicites");
        self.loadDropDown("nonSAEthnicities");
        self.loadDropDown("correspondenceLanguages");
        self.loadDropDown("foreignIdentificationTypes");
        self.loadDropDown("disabilities");
        self.loadDropDown("addressCountries");
        self.loadDropDown("maritalStatus");
        self.loadDropDown("SouthAfricaIdentificationTypes");
        self.loadDropDown("offerings");
        self.loadDropDown("programme");
        self.loadDropDown("countriesIssued");
        self.loadDropDown("blendedAttendance");
    };

    self.loadExistingDetails = function () {
        $.ajax({
            url: rootUrl + "Page/PopulatePersonalDetails",
            type: 'POST',
            contentType: "application/json",
            //data: AddAntiForgeryToken({}),
            success: function (data) {
                if (data != undefined && data.USNumber != undefined) {

                    var mapping = {
                        'ignore': ['SelectedPermitType']
                    };
                    ko.mapping.fromJS(data, mapping, self);

                    if (data
                        .SelectedOffering !=
                        null &&
                        data.SelectedOffering == "00000000-0000-0000-0000-000000000000") {
                        self.SelectedOffering(null);
                    }

                    if (data.SelectedPermitType !== null) {
                        self.SelectedPermitType(data.SelectedPermitType);
                    }

                    self.calculateSouthAfrica();
                    self.resolveJsonDates(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(error.responseText);
                waitingDialog.hide();
            }
        }).always(function () {
            waitingDialog.hide();
        });
    };

    self.resolveJsonDates = function (data) {
        self.DateOfBirth(self.formatDate(data.DateOfBirth));
        self.ExpiryDate(self.formatDate(data.ForeignIdExpiryDate));
    }

    self.formatDate = function (jsonDate) {
        try {
            var date = new Date(parseInt(/^\/Date\((.*?)\)\/$/.exec(jsonDate)[1], 10));
            var day = String("00" + date.getDate()).slice(-2);
            var month = String("00" + (date.getMonth() + 1)).slice(-2);
            var year = date.getFullYear();

            var formatedDate = year + "/" + month + "/" + day;

            return formatedDate;
        } catch (e) {
            return jsonDate;
        }
    }

    self.populateDropDownList = function (list, type) {

        switch (type) {
            case "gender":;
                self.Genders(list);
                break;
            case "titles":;
                self.Titles(list);
                break;
            case "ethnicites":;
                self.Ethnicities(list);
                break;
            case "nonSAEthnicities":;
                self.NonSAEthnicities(list);
                break;
            case "nationalities":;
                self.Nationalities(list);
                break;
            case "countriesIssued":;
                self.CountriesIssued(list);
                break;
            case "correspondenceLanguages":;
                self.CorrespondenceLanguages(list);
                break;
            case "languages":;
                self.Languages(list);
                break;
            case "foreignIdentificationTypes":;
                self.ForeignIndetificationTypes(list);
                break;
            case "disabilities":;
                self.Disabilities(list);
                break;
            case "addressCountries":;
                self.AddressCountries(list);
                break;
            case "permitTypes":;
                self.PermitTypes(list);
                break;
            case "maritalStatus":;
                self.MaritalStatus(list);
                break;
            case "SouthAfricaIdentificationTypes":;
                self.SouthAfricaIdentificationTypes(list);
                break;
            case "offerings":;
                self.Offerings(list);
                break;
            case "programme":;
                self.Programmes(list);
                break;
            case "blendedAttendance":
                self.blendedOptions(list);
        };
    };

    self.setRequiredFields = function () {

        self.SelectedOffering.extend({ required: true });
        self.SelectedTitle.extend({ required: true });
        self.Surname.extend({ required: true });
        self.FirstNames.extend({ required: true });
        self.GivenName.extend({ required: true });
        self.SelectedNationality.extend({ required: true });
        self.DateOfBirth.extend({ required: true, simpleDate: true, pastDate: true });
        self.SelectedGender.extend({ required: true });
        self.SelectedEthnicity.extend({ required: true });
        self.SelectedLanguage.extend({ required: true });
        self.SelectedCorrespondanceLanguage.extend({ required: true });
        self.SelectedMaritalStatus.extend({ required: true });
        self.SelectedDisability.extend({ required: true });

        self.SelectedForeignIdentificationType.extend({
            required: { onlyIf: function () { return self.IsNonSouthAfrican() === true; } }
        });
        self.SelectedPermitType.extend({
            required: { onlyIf: function () { return self.IsNonSouthAfrican() === true; } }
        });

        self.IdNumber.extend({
            required: { onlyIf: function () { return self.IsSouthAfrican() === true; } }
        });

        self.PassportNumber.extend({
            required: { onlyIf: function () { return self.ShowForgeinSection() === true; } }
        });

        self.ExpiryDate.extend({
            required: { onlyIf: function () { return self.ShowForgeinSection() === true; } },
            simpleDate: { onlyIf: function () { return self.ShowForgeinSection() === true; } },
            futureDate: { onlyIf: function () { return self.ShowForgeinSection() === true; } }
        });

        self.MobileNumber.extend({
            required: true,
            pattern: {
                message: "Invalid phone number.",
                params: /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
            }
        });

        self.MobileNumber.subscribe(function (data) {

            if (data != undefined) {
                $("#MobileNumberId").intlTelInput("setNumber", data);
                var intlNumber = $("#MobileNumberId").intlTelInput("getNumber");

                self.MobileNumber(intlNumber);

                if ($('#MobileNumberId').attr('placeholder') != undefined && intlNumber !== "") {
                    var numberLength = $('#MobileNumberId').attr('placeholder').replace(/\s/g, "").length;
                    if (!$("#MobileNumberId").intlTelInput("isValidNumber")) {
                        self.MobileNumber.setError("Invalid number for selected country.");
                        self.MobileNumber.isModified(true);
                    } else {
                        self.MobileNumber.clearError();
                    }
                }
            }

        });

        $("#MobileNumberId")
            .intlTelInput({
                allowExtensions: true,
                autoHideDialCode: true,
                autoPlaceholder: true,
                nationalMode: false,
                preferredCountries: [],
                utilsScript: "/PGDipProjectManagment/Scripts/utils.js"
            });

        $("#MobileNumberId")
            .on("countrychange",
                function (e, countryData) {
                    var extension = $("#MobileNumberId").intlTelInput("getExtension");
                    $("#MobileNumberId").intlTelInput("setNumber", "+" + extension);
                });

        self.eMail.extend({
            required: true,
            email: true
        });

        self.SelectedBlendedOption.extend({
            required: { onlyIf: function () { return self.isBlendedOffering() == true } }
        });
    };

    var PersonalContactDetails = {
        SelectedTitle: self.SelectedTitle,
        Initials: self.Initials,
        Surname: self.Surname,
        FirstNames: self.FirstNames,
        GivenName: self.GivenName,
        IdNumber: self.IdNumber,
        SelectedForeignIdentificationType: self.SelectedForeignIdentificationType,
        DateOfBirth: self.DateOfBirth,
        SelectedGender: self.SelectedGender,
        SelectedLanguage: self.SelectedLanguage,
        SelectedNationality: self.SelectedNationality,
        SelectedCountryOfIssue: self.SelectedCountryOfIssue,

        SelectedEthnicity: self.SelectedEthnicity,
        SelectedDisability: self.SelectedDisability,
        BusinessPhoneNumber: self.BusinessPhoneNumber,
        HomePhoneNumber: self.HomePhoneNumber,
        ForeignIdExpiryDate: self.ExpiryDate,
        PassportNumber: self.PassportNumber,
        FaxNumber: self.FaxNumber,
        MobileNumber: self.MobileNumber,
        eMail: self.eMail,
        UseWheelchair: self.UseWheelchair,
        MaritalStatus: self.SelectedMaritalStatus,
        MaidenName: self.MaidenName,
        OtherDisability: self.OtherDisability,
        SelectedPermitType: self.SelectedPermitType,
        SouthAfricaIdentificationTypes: self.SouthAfricaIdentificationTypes,
        SouthAfricaIdentificationType: self.SouthAfricaIdentificationType,
        IsSouthAfrican: self.IsSouthAfrican,
        SelectedOffering: self.SelectedOffering,
        SelectedProgramme: self.SelectedProgramme,
        SelectedCorrespondanceLanguage: self.SelectedCorrespondanceLanguage,
        SelectedMaritalStatus: self.SelectedMaritalStatus,
        USNumber: self.USNumber,
        SelectedBlendedOption: self.SelectedBlendedOption,
        AlternativeEmailAddress: self.AlternativeEmailAddress
    }

    PersonalContactDetails.errors = ko.validation.group(PersonalContactDetails);
    
    self.loading.subscribe(function () {
        if (self.loading().length == 0) {
            // Last dropdown population async ajax call has ended

            if (self.isFirstTimeApplicant()) {
                // Hide dialog if 
                waitingDialog.hide();
            } else {
                // Fill in inputs if existing informantion exists
                self.loadExistingDetails();
            }
        }
    });

    self.calculateSouthAfrica = function () {
        var selectedValue = $("#selectNationality option:selected").val();
        selectSouthAfrica = $('#selectNationality option').filter(function () { return $(this).html() == "South Africa"; }).val();
        if (selectedValue === selectSouthAfrica) {
            self.IsSouthAfrican(true);
            self.IsNonSouthAfrican(false);
            self.ShowForgeinSection(false);
            self.PassportForeignNumber("");
            self.ShowPassportNumberInput(false);
            self.ShowForeignNumberInput(false);
            var SAGuid = self.SouthAfricaIdentificationTypes()[0].Value;
            self.SouthAfricaIdentificationType(SAGuid);
        }
        else if (selectedValue == "") {
            self.ShowForgeinSection(false);
            self.IsNonSouthAfrican(false);
            self.IsSouthAfrican(false);
        }
        else {
            var selectedValue = $("#selectIdentificationType option:selected").val();
            self.resolveSelectedForeignIdentificationType(selectedValue);
            self.ShowForgeinSection(true);
            self.IsNonSouthAfrican(true);
            self.IsSouthAfrican(false);
        }
    };

    self.getDateOfBirthFromIdNumber = function (idNumber) {
        var date = {};
        var year = parseInt(idNumber.toString().substring(0, 2));
        var currentCentury = parseInt(new Date().getFullYear().toString().substring(0, 2) + "00");
        var lastCentury = currentCentury - 100;
        var currentYear = parseInt(new Date().getFullYear().toString().substring(2, 4));

        if (year > currentYear) {
            year += lastCentury;
        }
        else {
            year += currentCentury;
        }

        date.year = year;
        date.month = idNumber.substring(2, 4);
        date.day = idNumber.substring(4, 6);

        return date;
    };


    //events
    self.FirstNames.subscribe(function () {
        if (self.FirstNames()) {
            var names = self.FirstNames().split(" ");
            var result = "";

            if (names.length > 0) {
                for (var i = 0; i < names.length; i++) {
                    result += names[i].substring(0, 1);
                }
            }
        }
        self.Initials(result);
    });

    self.IdNumber.subscribe(function () {
        $("#txtIdNumber").blur(function () {
            var dateOfBirth = self.getDateOfBirthFromIdNumber(self.IdNumber());
            $("#txtDateOfBirth").datepicker();
            $("#txtDateOfBirth").datepicker('setDate', dateOfBirth.year + "-" + dateOfBirth.month + "-" + dateOfBirth.day);
        });
    });

    self.SelectedForeignIdentificationType.subscribe(function () {
        var selectedValue = $("#selectIdentificationType option:selected").val();
        self.resolveSelectedForeignIdentificationType(selectedValue);
    });

    self.SelectedNationality.subscribe(function () {

        var selectedValue = self.SelectedNationality();
        selectSouthAfrica = $('#selectNationality option').filter(function () { return $(this).html() == "South Africa"; }).val();

        if (selectedValue === selectSouthAfrica) {
            self.SelectedForeignIdentificationType('');
            self.IsSouthAfrican(true);
            self.IsNonSouthAfrican(false);
            self.ShowForgeinSection(false);
            self.PassportForeignNumber("");
            self.ShowPassportNumberInput(false);
            self.ShowForeignNumberInput(false);
            self.ShowSAEthnicitesInput(true);

            var SAGuid = self.SouthAfricaIdentificationTypes()[0].Value;

            self.SouthAfricaIdentificationType(SAGuid);
            self.ExpiryDate('');
            self.SelectedPermitType('');
        }
        else if (selectedValue == "") {
            self.IsNonSouthAfrican(false);
            self.IsSouthAfrican(false);
            self.ShowSAEthnicitesInput(false);
        }
        else {
            self.IsNonSouthAfrican(true);
            self.IsSouthAfrican(false);
            self.ShowSAEthnicitesInput(false);
        }
    });

    self.StepOneSaveAndProceed = function () {
        var data = ko.toJSON(PersonalContactDetails);

        usNumber = PersonalContactDetails.USNumber();

        if (PersonalContactDetails.errors().length === 0) {
            waitingDialog.show('Saving Personal Information');
            $.ajax({
                url: self.SubmitPersonalDetails_url,
                contentType: 'application/json; charset=utf-8',
                //data: AddAntiForgeryToken(data),  
                data: data,
                type: "POST",
                success: function (j) {
                    if (j.Success == false) {
                        self.serverErrorMessage(j.Message);
                        $('#serverValidationAlert').show();
                        window.scroll(0, 0);
                    }
                    else {
                        window.location.href = self.AddressDetails_url;
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            }).done(function (response) {
                waitingDialog.hide();
            });

        } else {
            $('#requiredFieldsAlert').show();
            PersonalContactDetails.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
    };

    self.resolveSelectedForeignIdentificationType = function (selectedValue) {
        if (selectedValue === emptyObject) {
            self.ShowForgeinSection(false);
        }

        selectForeginNumber = $("#selectIdentificationType option").filter(function () { return $(this).html() == "Foreign Identification Number" }).val();

        if (selectedValue === selectForeginNumber) {
            self.ShowForgeinSection(true);
            self.PassportForeignNumber("Foreign ID Number");
            self.ShowForeignNumberInput(true);
            self.ShowPassportNumberInput(false);
            
        }

        selectPassportNumber = $("#selectIdentificationType option").filter(function () { return $(this).html() == "Passport Number" }).val();

        if (selectedValue === selectPassportNumber) {
            self.ShowForgeinSection(true);
            self.PassportForeignNumber("Passport Number");
            self.ShowPassportNumberInput(true);
            self.ShowForeignNumberInput(false);
            //self.ShowExpiryDateInput(true);
        }
    };

    //self.mobileNumberLength = ko.computed(function () {
    //    if (self.MobileNumber()) {
    //        return self.MobileNumber().length;
    //    }
    //});

    (function () {
        waitingDialog.show('Preparing personal information page');

        self.setRequiredFields();

        self.preloadDropDownLists();
    })();   
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });

    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);

    var dateOfBirth = $('#txtDateOfBirth').datepicker({
        beforeShowDay: function (date) {
            return date.valueOf() < today.valueOf();
        },
        autoclose: true
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > dateOfBirth.datepicker("getDate").valueOf() || !dateOfBirth.datepicker("getDate").valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            dateOfBirth.datepicker("update", newDate);
        }
    });

    var expiryDate = $('#txtExpiryDate').datepicker({
        beforeShowDay: function (date) {
            return date.valueOf() >= today.valueOf();
        },
        autoclose: true
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > expiryDate.datepicker("getDate").valueOf() || !expiryDate.datepicker("getDate").valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            expiryDate.datepicker("update", newDate);
        }
    });
});