﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PGDipProjectManagment.ApplicationForm.ViewModels
{
    public class EmploymentHistoryViewModel
    {
        public string Employer { get; set; }
        public string JobTitle { get; set; }
        public string WorkArea { get; set; }
        public string Organization { get; set; }
        public string Industry { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string Type { get; set; }
        public string Latest { get; set; }
    }
}