﻿using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.PGDipProjectManagment.ApplicationForm.ViewModels;

namespace USB.PGDipProjectManagment.ApplicationForm.Controllers
{
    public class AddressDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, Guid>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "countries" : ;
                    dropdownList = controller.AddressCountries().ToList();
                    break;
               
                default: ;
                    break;
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
    }
}
