﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PGDipLeadership.ApplicationForm.ViewModels
{
    public class WorkStudiesViewModel
    {
        public string EnglishProficiencySpeaking { get; set; }
        public string EnglishProficiencyWriting { get; set; }
        public string EnglishProficiencyReading { get; set; }
        public string EnglishProficiencyUnderstanding { get; set; }
        public string AfrikaansProficiencyPraat { get; set; }
        public string AfrikaansProficiencySkryf { get; set; }
        public string AfrikaansProficiencyLees { get; set; }
        public string AfrikaansProficiencyVerstaan { get; set; }
        public string MathematicsCompetency { get; set; } 
        public string MathematicsPercentage { get; set; }
        public string PriorInvolvement { get; set; }
        
        public List<TertiaryEducationHistoryViewModel> TertiaryEducationHistoryViewModel { get; set; }

        public List<EmploymentHistoryViewModel> EmploymentHistoryViewModel { get; set; }

        #region Current Employment
        public string YearsOfWorkingExeperience { get; set; }
        public string AnnualGrossSalary { get; set; }
        public string WillEmployerBeAssistingFinancially { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonTelephone { get; set; }
        public string ContactPersonEmailAddress { get; set; }
        #endregion

        public List<RefereeViewModel> RefereeViewModel { get; set; }
    }
}