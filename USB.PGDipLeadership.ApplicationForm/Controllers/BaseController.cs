﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace USB.PGDipLeadership.ApplicationForm.Controllers
{
    public class BaseController : Controller
    {
        public string JsonConverter(object obj)
        {
            if(obj.IsNotNull())
            {
               var result = new JavaScriptSerializer().Serialize(obj);
               return result;
            }
            return string.Empty;
        }

    }
}
