﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using USB.Domain.Models;
using Newtonsoft.Json;
namespace USB.MBA.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class USBMBAService : IUSBMBAService
    {
        public const string SavePersonalDetails_URL = "http://";
        public const string SaveAddressDetails_URL = "http://";
        
        public ApplicationSubmissionResult SavePersonalDetails(string json)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    var addressDetails = JsonConvert.DeserializeObject<PersonalContactDetails>(json);

                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
            }
            return new ApplicationSubmissionResult();
        }

        public ApplicationSubmissionResult SaveAddressDetails(string json)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    var addressDetails = JsonConvert.DeserializeObject<AddressDetails>(json);

                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
            }
            return new ApplicationSubmissionResult();
        }

        public ApplicationSubmissionResult SaveWorkDetails(string json)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    var workDetails = JsonConvert.DeserializeObject<Workstudies>(json);


                   // return webClient.UploadData();
                }
            }
            catch(WebException)
            {

            }
            return new ApplicationSubmissionResult();
        }

        [WebGet(BodyStyle=WebMessageBodyStyle.Wrapped)]
        public ApplicationSubmissionResult SaveDocument(DocumentType documentType, byte[] binary, Guid id)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {

                   // return webClient.UploadData(binary);
                }
            }
            catch(WebException)
            {

            }
            return new ApplicationSubmissionResult();
        }

        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        public ApplicationSubmissionResult DeleteDocument(DocumentType documentType, Guid id)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {

                    // return webClient.UploadData(binary);
                }
            }
            catch (WebException)
            {

            }
            return new ApplicationSubmissionResult();
        }
    }
}
