﻿using System.Web;
using System.Web.Mvc;
using WebForms.API.ActionFilters;

namespace WebForms.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());          
        }
    }
}
