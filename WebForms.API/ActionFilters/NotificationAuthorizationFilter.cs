﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebForms.API.Models;
using WebForms.API.Utilities;

namespace WebForms.API.ActionFilters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class NotificationAuthorizationFilter : AuthorizationFilterAttribute
    {
        bool Active = true;
        public NotificationAuthorizationFilter()
        {

        }
        public NotificationAuthorizationFilter(bool active)
        {
            Active = active;
        }

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (Active)
            {
                var queryString = filterContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

                var systemName = queryString["System"];
                var systemIdentifier = queryString["SystemId"];

                var isAuthorized = this.AuthorizeSystem(systemName, systemIdentifier);
                if (!isAuthorized)
                {
                    this.Challenge(filterContext);
                    return;
                }
                base.OnAuthorization(filterContext);
            }
        }

        private bool AuthorizeSystem(string sysName, string sysId)
        {

            var systems = Config.AllowedSystems;
            foreach (var systemString in systems)
            {
                var system = systemString.Split('|');
                if (sysName == system[0] && sysId == system[1])
                {
                    return true;

                }
            }
            return false;
        }

        private void Challenge(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}