﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForms.API.Models
{
    public class NotificationResult
    {
        public string ReceiptNumber { get; set; }
        public string StudentNumber{ get; set; }
        public string TransactionId { get; set; }
        public string System { get; set; }
        public Guid SystemId { get; set; }

    }
}