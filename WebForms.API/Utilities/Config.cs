﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebForms.API.Utilities
{
    public static class Config
    {
        public static string[] AllowedSystems => ConfigurationManager.AppSettings["AllowedSystems"].Split(',');
    }
}