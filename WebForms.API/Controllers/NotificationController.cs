﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebForms.API.ActionFilters;
using WebForms.API.Models;

namespace WebForms.API.Controllers
{
    public class NotificationController : ApiController
    {
        //Example URL
        //http://localhost:37994/api/Notification?System=USMain&SystemId=671191ce-f20c-42df-9212-9ac94320f8d2&ReceiptNumber=123456789&StudentNumber=13465503&TransactionId=TK123456789

        [NotificationAuthorizationFilter]
        public string Get([FromUri] NotificationResult notification)
        {

            if (ProcessNotification(notification))
            {
                return String.Format("Student No: {0} - Receipt {1}", notification.StudentNumber,notification.ReceiptNumber);
            }
            else
            {
                return String.Format("Error - Update Receipt {0} failed", notification.ReceiptNumber);
            }           
        }

        private bool ProcessNotification(NotificationResult notification)
        {
            return true;
        }
    }

  
}
