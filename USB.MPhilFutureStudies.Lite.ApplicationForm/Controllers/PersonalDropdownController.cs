﻿using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using USB.Domain.Models.Enums;
using SettingsMan = USB.MPhilFutureStudies.Lite.ApplicationForm.Properties.Settings;

namespace USB.MPhilFutureStudies.Lite.ApplicationForm.Controllers
{
    public class PersonalDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string enrolmentId, string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();

            var tupleList = new List<Tuple<string, Guid, ProgrammeOfferingType>>();

            var controller = new ApplicationFormApiController();

            try
            {
                switch (type)
                {
                    case "gender":
                        ;
                        dropdownGuidList = controller.Genders().ToList();
                        break;
                    case "titles":
                        ;
                        dropdownGuidList = controller.Titles().ToList();
                        break;
                    case "ethnicites":
                        ;
                        dropdownGuidList = controller.Ethnicities().ToList();
                        break;
                    case "nonSAEthnicities":
                        ;
                        dropdownGuidList = controller.NonSAEthnicities().ToList();
                        break;
                    case "nationalities":
                        ;
                        dropdownGuidList = controller.Nationalities().ToList();
                        break;
                    case "countriesIssued":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "correspondenceLanguages":
                        ;
                        dropdownGuidList = controller.CorrespondenceLanguages().ToList();
                        break;
                    case "languages":
                        ;
                        dropdownGuidList = controller.Languages().ToList();
                        break;
                    case "foreignIdentificationTypes":
                        ;
                        dropdownGuidList = controller.ForeignIdentificationTypes().ToList();
                        break;
                    case "SouthAfricaIdentificationTypes":
                        ;
                        dropdownGuidList = controller.SouthAfricaIdentificationTypes().ToList();
                        break;
                    case "disabilities":
                        ;
                        dropdownGuidList = controller.Disabilities().ToList();
                        break;
                    case "addressCountries":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "permitTypes":
                        ;
                        dropdownGuidList = controller.PermitTypes().ToList();
                        break;
                    case "maritalStatus":
                        ;
                        dropdownGuidList = controller.MaritalStatus().ToList();
                        break;
                    case "offerings":
                        ;
                        var programmeGuid = SettingsMan.Default.ProgrammeGuid;
                       
                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            tupleList = controller.EnrolmentIdGetOfferings(false, programmeGuid, enrolmentGuid).ToList();
                        }
                        else
                        {
                            tupleList = controller.GetOfferings(false, programmeGuid).ToList();
                        }
                        break;
                    case "programme":
                        ;
                        break;
                    case "blendedAttendance":
                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            dropdownList = controller.EnrolmentIdGetBlendedOptions(enrolmentGuid).ToList();
                        }
                        else
                        {
                            dropdownList = controller.GetBlendedOptions().ToList();

                        }
                        break;
                    case "mbaStreams":
                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            dropdownList = controller.EnrolmentIdGetMBAStreamOptions(enrolmentGuid).ToList();
                        }
                        else
                        {
                            dropdownList = controller.GetMBAStreamOptions().ToList();

                        }
                        break;
                    default:
                        ;
                        break;
                }
            }
            catch (InvalidOperationException)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new { dropdownGuidList }, JsonRequestBehavior.AllowGet);
            }

            if (tupleList.Any())
            {
                return Json(new { tupleList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
    }
}
