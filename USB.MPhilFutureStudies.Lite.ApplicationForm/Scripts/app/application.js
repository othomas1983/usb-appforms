﻿var Application = function () {
    var instance = this;

    instance.personalDetails_Url = "/ShortenedAppFormMPhilFutureStudies/Page/PersonalDetails";
    instance.addressDetails_Url = "/ShortenedAppFormMPhilFutureStudies/Page/AddressDetails";
    instance.work_Study_Url = "/ShortenedAppFormMPhilFutureStudies/Page/WorkStudies";
    instance.documentation_Url = "/ShortenedAppFormMPhilFutureStudies/Page/Documentation";
    //instance.payment_Url = "/ShortenedAppFormMPhilFutureStudies/Page/Payment";
    instance.status_Url = "/ShortenedAppFormMPhilFutureStudies/Page/Status";

    var activeTab = null;

    instance.canClickOnPersonalDetails = ko.observable(false);
    instance.canClickOnAddressDetails = ko.observable(false);
    instance.canClickOnWorkAndStudies = ko.observable(false);
    instance.canClickOnDocumentationDetails = ko.observable(false);
   // instance.canClickOnPaymentDetails = ko.observable(false);
    instance.canClickOnStatusDetails = ko.observable(false);

    this.load = function () {
        instance.registerEvents();
        instance.setActiveTab();
    }

    this.registerEvents = function () {
        $('.navPersonalDetails').on('click',
            function () {
                if (instance.canClickOnPersonalDetails()) {
                    window.location.href = instance.personalDetails_Url;
                }
            });
        $('.navAddressDetails').on('click',
            function () {
                if (instance.canClickOnAddressDetails()) {
                    window.location.href = instance.addressDetails_Url;
                }
            });
        $('.navWorkAndStudyDetails').on('click',
            function () {
                if (instance.canClickOnWorkAndStudies()) {
                    window.location.href = instance.work_Study_Url;
                }
            });
        $('.navDocumentationDetails').on('click',
            function () {
                if (instance.canClickOnPersonalDetails() &&
                    instance.canClickOnAddressDetails() &&
                    instance.canClickOnWorkAndStudies()) {
                    window.location.href = instance.documentation_Url;
                }
            });
      
        $('.navStatusDetails').on('click',
            function () {
                if (instance.canClickOnStatusDetails()) {
                    window.location.href = instance.status_Url;
                }
            });
    }

    this.recreatePopoverDialog = function (elId) {
        $(elId).popover({
            container: "body",
            placement: "bottom",
            trigger: "hover"
        });
    };

    this.setActiveTab = function () {
        $.ajax({
            url: "/ShortenedAppFormMPhilFutureStudies/Page/GetApplicationStatus",
            type: 'POST',
            contentType: "application/json",
            cache: false,
            success: function (data) {
                if (data != undefined) {
                    if (data.PersonalDetailsComplete != undefined) {

                        instance.recreatePopoverDialog("#tab1");

                        if (data.PersonalDetailsComplete) {
                            $("#tab1").popover("destroy");
                            $("#navPersonalDetails").removeClass("glyphicon-remove");
                            $("#navPersonalDetails").addClass("glyphicon-ok");
                            instance.canClickOnPersonalDetails(true);
                        } else {
                            $("#navPersonalDetails").addClass("glyphicon-remove");
                            $("#navPersonalDetails").removeClass("glyphicon-ok");
                            instance.canClickOnPersonalDetails(false);
                        }
                    }
                    if (data.AddressDetailsComplete != undefined) {

                        instance.recreatePopoverDialog("#tab2");

                        if (data.AddressDetailsComplete) {
                            $("#tab2").popover("destroy");
                            $("#navAddressDetails").removeClass("glyphicon-remove");
                            $("#navAddressDetails").addClass("glyphicon-ok");
                            instance.canClickOnAddressDetails(true);
                        } else {
                            $("#navAddressDetails").addClass("glyphicon-remove");
                            $("#navAddressDetails").removeClass("glyphicon-ok");
                            instance.canClickOnAddressDetails(false);
                        }
                    }
                    if (data.WorkStudiesComplete != undefined) {

                        instance.recreatePopoverDialog("#tab3");

                        if (data.WorkStudiesComplete) {
                            $("#tab3").popover("destroy");
                            $("#navWorkAndStudyDetails").removeClass("glyphicon-remove");
                            $("#navWorkAndStudyDetails").addClass("glyphicon-ok");
                            instance.canClickOnWorkAndStudies(true);
                        } else {
                            $("#navWorkAndStudyDetails").addClass("glyphicon-remove");
                            $("#navWorkAndStudyDetails").removeClass("glyphicon-ok");
                            instance.canClickOnWorkAndStudies(false);
                        }
                    }
                    if (data.DocumentationComplete != undefined) {

                        instance.recreatePopoverDialog("#tab5");

                        if (data.DocumentationComplete) {
                            $("#tab5").popover("destroy");
                            $("#navDocumentationDetails").removeClass("glyphicon-remove");
                            $("#navDocumentationDetails").addClass("glyphicon-ok");
                            instance.canClickOnDocumentationDetails(true);
                        } else {
                            $("#navDocumentationDetails").addClass("glyphicon-remove");
                            $("#navDocumentationDetails").removeClass("glyphicon-ok");
                            instance.canClickOnDocumentationDetails(false);
                        }
                    }
             
                }
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });

        var url = window.location.pathname;

        if (url == instance.personalDetails_Url) {
            $("#tab1").addClass('active');
            $('#currentPage').text('Page 1 of 5');
        }
        if (url == instance.addressDetails_Url) {
            $("#tab2").addClass('active');
            $('#currentPage').text('Page 2 of 5');
        }
        if (url == instance.work_Study_Url) {
            $("#tab3").addClass('active');
            $('#currentPage').text('Page 3 of 5');
        }
        if (url == instance.documentation_Url) {
            $("#tab5").addClass('active');
            $('#currentPage').text('Page 4 of 5');
        }
       
        if (url == instance.status_Url) {
            $("#tab7").addClass('active');
            $('#currentPage').text('Page 5 of 5');
        }
    }

    AddAntiForgeryToken = function (data) {
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        return data;
    };
}
