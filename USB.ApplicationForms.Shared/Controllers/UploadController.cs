﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Windows.Documents;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using NLog;
using StellenboschUniversity.Usb.Integration.Crm.Crm;
using Usb.Domain.Sharepoint;
using USB.Domain.Models;
using USB.Domain.Properties;
using USB.Domain.State;

using ApplicationSubmissionResult = StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto.ApplicationSubmissionResult;
using iTextSharp.text.pdf;
using USB.Domain.Models.Enums;

namespace USB.ApplicationForms.Shared.Controllers
{
    public class UploadController : Controller
    {

        private const string Upload = "Upload";
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private const string UsNumberString = "UsNumber";

        [HttpPost]
        public JsonResult SetState(SessionContext context)
        {
            if (!context.IsValid())
                return Json(new {Success = false});
            Session[Upload] = context;
            return Json(new { Success = true });
        }
        
        private void Setflags(MultiFileUploadViewModel model, SessionContext context)
        {
            bool? insideCRM = context.InsideCRM;
          
            if (context.InsideCRM)
            {
                model.DisableView = true;
                model.ReviewEnabled = true;
            }
            else if (context.ApplicantStatusEnumValue != ApplicantStatus.Applicant)
            {
                model.DisableView = true;
            }

            foreach (var document in model.Documents)
            {
                document.CanReview = document.Status == "Received" && document.Level < 3 && model.ReviewEnabled;
            }
        }


        [HttpGet]
        public ActionResult Index(string data, bool includeGMAT, bool isShortForm=true)
        {
            var isShort = HttpContext.Request["isShortForm"];

            if (Session[Upload] == null && string.IsNullOrEmpty(data))
            {
                return Json(new { Success = false, ErrorMessage = "Your session is invalid or has expired. Please refresh the page or login again. " });
            }
            if (data !=  null)
            {
                var json = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(data));
                Session[Upload] = JsonConvert.DeserializeObject<SessionContext>(json);
            }
            var context = CheckSessionContext(false);


           
            context.IncludeGMAT = includeGMAT;
            context.RootUrl = ViewBag.RootUrl;
            if (isShort == null || HttpContext.Request["isShortForm"] == null)
            {
                context.IsShortForm = false;
            }
            else
            {
                context.IsShortForm = true;
            }
            MultiFileUploadViewModel model = context.Sections != null ? RestoreExistingModel(context) : GetNewModel(context);

            Setflags(model, context);
            
            Session[Upload] = context;
            ViewBag.Model = JsonConvert.SerializeObject(model);

            return View();

        }

        private static MultiFileUploadViewModel GetNewModel(SessionContext context)
        {
            var controller = new ApplicationFormApiController();

            var enrolmentId = context.EnrolmentId;

            List<Document> requiredDocs = new List<Document>();
            List<Document> existingDocs;

            if (context.IsShortForm)
            {
                existingDocs = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId) ?? new List<Document>();

                //if (existingDocs.Count > 0)
                //{
                //    existingDocs = existingDocs.Where(x => x.Level == 1).ToList();
                //}
            }
            else
            {
                existingDocs = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId) ??
                                          new List<Document>();
            }

            if (context.InsideCRM)
            {
                requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId, true,
                    context.InsideCRM);
                if (context.IsShortForm)
                {

                    requiredDocs.AddRange(controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId,
                        true));
                }
                
                //Is SHL
                //Is GMat
                //IS RPL
                //Use enrolmentId to check above true or false

                requiredDocs.AddRange(controller.GetRequiredDocumentsInformationDirect(context.EnrolmentId));

                var notUploaded = (from required in requiredDocs
                    join document in existingDocs
                    on required.Description equals document.Description
                    into temp
                    from left in temp.DefaultIfEmpty()
                    select new {Required = required, Existing = left}).Where(i => i.Existing == null);

                existingDocs.AddRange(notUploaded.Select(i => i.Required));

                var hasSHLDocsAlready = existingDocs.Where(x => x.Description.Contains("SHL")).ToList();

                if (hasSHLDocsAlready.Count == 0)
                {
                    List<Document> shlDocumentationResult =
                        controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId, true,
                            context.InsideCRM, 2);
                    var shlDocumentation = shlDocumentationResult.Where(x => x.Description.Contains("SHL")).ToList();

                    existingDocs.AddRange(shlDocumentation);
                }

                existingDocs = (existingDocs ?? new List<Document>()).OrderBy(x => x.Level).ToList();
            }
            else
            {
                var applicantStatus = context.ApplicantStatusEnumValue;
                switch (applicantStatus)
                {
                    case ApplicantStatus.Applicant:
                        if (context.IsShortForm)
                        {
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, true);
                        }
                        else
                        {
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, context.IncludeGMAT, context.InsideCRM);
                            requiredDocs.AddRange(controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, context.IncludeGMAT, context.InsideCRM, 2));
                        }
                        break;
                    case ApplicantStatus.Admitted:
                        if (context.IsShortForm)
                        {
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId,true);
                        }
                        else
                        {
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId,
                            context.IncludeGMAT, context.InsideCRM, 3);
                        }
                        break;
                }

                if (requiredDocs.Count > 0)
                {
                    var notUploaded = (from required in requiredDocs
                        join document in existingDocs
                        on required.Description equals document.Description
                        into temp
                        from left in temp.DefaultIfEmpty()
                        select new {Required = required, Existing = left}).Where(i => i.Existing == null);

                    existingDocs.AddRange(notUploaded.Select(i => i.Required));
                }

                if (!context.IncludeGMAT)
                {
                    if (existingDocs.FirstOrDefault(d => d.Description.Contains("GMAT")).IsNotNull())
                    {
                        existingDocs.RemoveAll(x => x.Description.Contains("GMAT"));
                    }
                }

                if (existingDocs.FirstOrDefault(d => d.Description.Contains("RPL")).IsNotNull())
                {
                    existingDocs.RemoveAll(x => x.Description.Contains("RPL"));
                }

                if (existingDocs.FirstOrDefault(d => d.Description.Contains("SHL")).IsNotNull())
                {
                    existingDocs.RemoveAll(x => x.Description.Contains("SHL"));
                }
            }

            MultiFileUploadViewModel model =
                new MultiFileUploadViewModel(existingDocs.Where(d => !d.IsExcluded).ToList());
            context.Sections =
                model.Sections.Select(s => new FileUploadSection(s.Name, s.Level, s.DocumentType, null)).ToList();
            model.Sections = model.Sections.OrderBy(s => s.Name).ToList();
            model.EnrolmentId = context.EnrolmentId;

            var declarationDoc = model.Sections.FirstOrDefault(x => x.Name.Contains("Signed Declaration Form"));

            if (declarationDoc.IsNotNull())
            {
                model.Sections.Remove(declarationDoc);
                model.Sections.Insert(0, declarationDoc);
            }

            return model;
        }

        private static MultiFileUploadViewModel RestoreExistingModel(SessionContext context)
        {
            var controller = new ApplicationFormApiController();
            var enrolmentId = context.EnrolmentId;
            List<Document> existingDocs = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId) ?? new List<Document>();
            
            var model = new MultiFileUploadViewModel(existingDocs.Where(d => !d.IsExcluded).ToList());
            var emptysections = (from contextSection in context.Sections
                join dbSection in model.Sections
                    on contextSection.Name equals dbSection.Name
                    into temp
                from left in temp.DefaultIfEmpty()
                select new {Existing = contextSection, Document = left})
                .Where(i => i.Document == null)
                .ToList();

            model.Sections.AddRange(emptysections.Select(s => s.Existing).ToList());
            model.Sections = model.Sections.OrderBy(s => s.Name).ToList();
            model.EnrolmentId = context.EnrolmentId;

            return model;
        }

        [HttpGet]
        public FileResult DownloadFile(int documentId)
        {

            var context = CheckSessionContext();

            var controller = new ApplicationFormApiController();

            Tuple<string, byte[]> documentData = controller.GetDocument(documentId,context.EnrolmentId);

            return File(documentData.Item2, System.Net.Mime.MediaTypeNames.Application.Octet, documentData.Item1);
        }


        private string GetBaseUrl(HttpRequestBase request)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
            return baseUrl;
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult UploadFiles(IEnumerable<string> data, IEnumerable<int> documentIdsToDelete)
        {
            var context = CheckSessionContext();

           

            List<Document> documents = new List<Document>();
            foreach (var largeDocumentString in data ?? new string[] { })
            {
                documents.Add(JsonConvert.DeserializeObject<Document>(largeDocumentString));
            }

            var model = new MultiFileUploadViewModel(documents);

            var tempModel = context.Sections != null ? RestoreExistingModel(context) : GetNewModel(context);

            try
            {

                var controller = new ApplicationFormApiController();
                var isShlTestRequired = !context.IncludeGMAT;
                var receivedDocuments = model.Documents.Where(d => d.DocumentId == 0).ToList();
                var isShortForm = context.IsShortForm;

                var applicationSubmission = new ApplicationSubmission()
                {
                    ApplicationSource = Domain.Models.ApplicationSource.Documentation,
                    DocumentExclusionKeys = new List<int>(),
                    DocumentUploadKeys = receivedDocuments.Select(d => d.DocumentId).ToList(),
                    DocumentUploadNames = receivedDocuments.Select(d => d.FileName ?? string.Empty).ToList(),
                };

                applicationSubmission.ApplicationSource = ApplicationSource.Documentation;              


                var result = SubmitDocumentation(applicationSubmission,
                    receivedDocuments,
                    documentIdsToDelete,
                    context.ProgrammeId,
                    context.EnrolmentId,
                    context.UsNumber,
                    context.ApplicantStatusEnumValue.Value,
                    "",
                    isShlTestRequired,
                    context.Sections,
                    context.ProgrammeType,
                    isShortForm);

                foreach (var document in model.Documents.Where(d => d.ReviewStateChanged))
                {
                    controller.SetDocumentAsReviewed(document.DocumentId, context.EnrolmentId, document.Reviewed);
                }

                if (!result.Successful)
                {
                    return Json(new { Success = false, ErrorMessage = result.Message, ErrorId = result.ErrorId });
                }             

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { Success = false, ErrorMessage = ex.Message });

            }

            return Json(new { Success = true });
        }

        private SessionContext CheckSessionContext(bool checkSections = true)
        {
            SessionContext context = (SessionContext) Session[Upload] ?? new SessionContext();

            if (context == null || !context.IsValid() || (checkSections && (context.Sections == null || !context.Sections.Any())))
            {
                throw new Exception("Your session has expired or is invalid. Please refresh the page or login again");
            }
            return context;
        }


        public ApplicationSubmissionResult SubmitDocumentation(ApplicationSubmission applicationSubmission, IEnumerable<Document> documentsReceived, IEnumerable<int> documentIdsToDelete,
            Guid ProgrammeId, Guid enrolmentId, string usNumber,
            ApplicantStatus status, string programmeName, bool? SHLTestRequired, IList<FileUploadSection> sections, ProgrammeType programmeType, bool? isShortForm = false)
        {
            applicationSubmission.SHLTestRequiredTwoOptions = SHLTestRequired;
            applicationSubmission.UploadedDocumentTypes = new List<DocumentType>();

            if (!CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrolmentId))
            {
                CrmFileUpload.CreateDocumentLocationForEnrollment(enrolmentId);
            }


            //Add
            var validDocs = (from d in documentsReceived
                             join s in sections
                                 on d.Description equals s.Name
                             select d).ToList();

            foreach (var document in validDocs)
            {
                var bytes = Convert.FromBase64String(document.Data.Substring(document.Data.LastIndexOf(',') + 1));
                SharepointActions.UploadDocument(document.FileName, usNumber, document.Level, programmeName, document.Description, document.DocumentId, enrolmentId, bytes, "No");
                applicationSubmission.UploadedDocumentTypes.Add(document.DocumentType);
                applicationSubmission.DocumentUploadKeys.Add(document.DocumentId);

            }
            //Delete
            foreach (var documentId in (documentIdsToDelete ?? new int[] { }).Distinct().ToList())
            {
                SharepointActions.DeleteDocument(documentId, enrolmentId);
                applicationSubmission.DocumentUploadKeys.Remove(documentId);
            }
            
            applicationSubmission.EnrolmentGuid = enrolmentId;

            var controller = new ApplicationFormApiController();
            var uploadedDocuments = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId);

            if (uploadedDocuments.IsNull())
            {
                applicationSubmission.DocumentsOutstandingTwoOptions = true;
                applicationSubmission.DocumentsCompleteTwoOptions = false;
            }
            else
            {
                applicationSubmission.DocumentsOutstandingTwoOptions = uploadedDocuments.IsNotNull() && sections.Count > uploadedDocuments.Count;
                applicationSubmission.DocumentsCompleteTwoOptions = uploadedDocuments.IsNotNull() && sections.Count <= uploadedDocuments.Count;
            }

            applicationSubmission.ProgrammeType = programmeType;

            CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission, isShortForm.GetValueOrDefault());

            return new ApplicationSubmissionResult() { Successful = true };
        }

        [HttpGet]
        public FileResult DownloadDeclarationFile()
        {
            var context = CheckSessionContext();
            string pdfTemplate = HttpContext.Server.MapPath(@"~/Content/documents/e-application_postgrad_contract.pdf");

            PdfReader reader = new PdfReader(pdfTemplate);

            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamper = new PdfStamper(reader, ms, '\0', false))
                {
                    AcroFields formFields = stamper.AcroFields;
                    formFields.SetField("UsNumber", context.UsNumber);
                    stamper.FormFlattening = true;
                }

                return File(ms.ToArray(), MediaTypeNames.Application.Octet, "e-application_postgrad_contract.pdf");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            //filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
            
            
        }
    }
}