﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using USB.Domain.Models;
using System;

namespace USB.ApplicationForms.Shared.Controllers
{
    public class MultiFileUploadViewModel
    {
        private IEnumerable<Document> _documents;
        private IList<FileUploadSection> _sections;

        public MultiFileUploadViewModel(IEnumerable<Document> documents)
        {
            Documents = documents;
        }

        public MultiFileUploadViewModel()
        {
        }


        [JsonIgnore]
        public IEnumerable<Document> Documents
        {
            get
            {
                if (_documents == null && _sections != null && _sections.Any())
                {
                    _documents = _sections.SelectMany(s => (s.Documents ?? new List<Document>()).ToList()).ToList();
                }
                return _documents;
            }
            set
            {
                _documents = value;
                _sections =
                    Documents.GroupBy(d => d.Description)
                        .Select(
                            d =>
                                new FileUploadSection(d.Key, d.FirstOrDefault(x => x.Description == d.Key).Level,
                                    d.FirstOrDefault(x => x.Description == d.Key).DocumentType,
                                    Documents.Where(x => x.FileName != null && x.Description == d.Key).ToList()))
                        .ToList();
            }
        }



        public IList<FileUploadSection> Sections
        {
            get { return _sections; }
            set { _sections = value; }
        }

        public Guid EnrolmentId { get; set; }
        public bool DisableView { get; internal set; }
        public bool ReviewEnabled { get; internal set; }

    }
}