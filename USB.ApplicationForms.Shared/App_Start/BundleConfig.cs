﻿using System.Web.Optimization;

namespace USB.ApplicationForms.Shared
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                ("~/Scripts/jquery-{version}.js"),
                ("~/Scripts/jquery-ui-{version}.js"),
                ("~/Scripts/knockout-{version}.js"),
                ("~/Scripts/knockout.mapping.js"),
                ("~/Scripts/jquery.validate.js")
             ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                      "~/Scripts/validate.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/shared.js",
                      "~/Scripts/index.js",
                      "~/Scripts/multifileupload.js",
                      "~/Scripts/WaitingDialog.js"
                     
            ));

        }
    }
}
