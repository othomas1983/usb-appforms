﻿using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class Extensions
    {
        private static TextInfo textInfo = new CultureInfo("en-ZA", false).TextInfo;

        //public static string ToTitleCase(this string input)
        //{
        //    return textInfo.ToTitleCase(input.ToLower());
        //}

        public static string ConvertToSisDate(this DateTime date)
        {
            return date.ToString("yyyyMMdd");
        }

        public static DateTime? ConvertFromSisDate(this string date)
        {
            DateTime? result;

            try
            {
                result = DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture);
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public static string DecimalToString(this decimal number)
        {
            return Convert.ToInt32(number).ToString();
        }

        public static DateTime ToUtcWithoutConversion(this DateTime dateTime)
        {
            return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
        }

        public static DateTime? ToUtcWithoutConversion(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return DateTime.SpecifyKind(dateTime.Value, DateTimeKind.Utc);
            }
            else
            {
                return dateTime;
            }
        }
    }
}
