﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PhDBMA.ApplicationForm.ViewModels
{
    public class AddressDetailsViewModel
    {
        public Guid ResidentialCountry { get; set; }
        public string ResidentialStreet1 { get; set; }
        public string ResidentialStreet2 { get; set; }
        public string ResidentialStreet3 { get; set; }
        public string ResidentialCity { get; set; }
        public string ResidentialSuburb { get; set; }
        public string ResidentialPostalCode { get; set; }
        public Guid PostalCountry { get; set; }
        public string PostalStreet1 { get; set; }
        public string PostalStreet2 { get; set; }
        public string PostalStreet3 { get; set; }
        public string PostalCity { get; set; }
        public string PostalSuburb { get; set; }
        public string PostalPostalCode { get; set; }
        public Guid BusinessCountry { get; set; }
        public string BusinessStreet1 { get; set; }
        public string BusinessStreet2 { get; set; }
        public string BusinessStreet3 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessStateProvince { get; set; }
        public string BusinessPostalCode { get; set; }
        public Guid BillingCountry { get; set; }
        public string BillingStreet1 { get; set; }
        public string BillingStreet2 { get; set; }
        public string BillingStreet3 { get; set; }
        public string BillingCity { get; set; }
        public string BillingSuburb { get; set; }
        public string BillingPostalCode { get; set; }
    }
}