﻿/// <reference path="../../knockout-3.3.0.debug.js" />
/// <reference path="../../knockout.validation.js" />
/// <reference path="../../knockout.mapping-latest.js" />

//var knockoutValidationSettings = {
//    insertMessages: false,
//    decorateElement: true,
//    errorElementClass: 'input-validation-error',
//    messagesOnModified: true,
//    decorateElementOnModified: true,
//    decorateInputElement: true
//};

//ko.validation.configuration = knockoutValidationSettings;

var Marketing = function (isDisabled) {
    var self = this;
    var rootUrl = "/MPhilFutureStudies/";
     
    $('#infoSessionAttended').hide();

    self.loading = ko.observableArray();

    //self.loading.subscribe(function () {
    //    if (self.loading().length == 0) {
    //        // Last dropdown population async ajax call has ended

    //        // Fill in inputs if existing informantion exists
    //        self.loadExistingData();
    //    }
    //});

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    self.checkboxAdvertisement = ko.observable(false);
    self.checkboxNewsArticle = ko.observable(false);
    self.checkboxUsbPromotionalEmails = ko.observable(false);
    self.checkboxConferenceOrExpo = ko.observable(false);
    self.checkboxReferralByMyEmployer = ko.observable(false);
    self.checkboxStellenboschUniversity = ko.observable(false);
    self.checkboxReferralByAnAlumnus = ko.observable(false);
    self.checkboxUsbWebsite = ko.observable(false);
    self.checkboxReferralByACurrentStudent = ko.observable(false);
    self.checkboxUsbEd = ko.observable(false);
    self.checkboxReferralByAFamilyMember = ko.observable(false);
    self.checkboxSocialMedia = ko.observable(false);
    self.checkboxBrochure = ko.observable(false);
    self.checkboxOnCampusEvent = ko.observable(false);

    self.checkboxEarnMoreMoney = ko.observable(false);
    self.checkboxGiveMyChildABetterFuture = ko.observable(false);
    self.checkboxIncreaseMyStatus = ko.observable(false);
    self.checkboxMakeMyParentsProud = ko.observable(false);
    self.checkboxProvideStability = ko.observable(false);
    self.checkboxGetAPromotion = ko.observable(false);
    self.checkboxQualifyForOportunities = ko.observable(false);
    self.checkboxAdvanceInCareer = ko.observable(false);
    self.checkboxHaveMoreControl = ko.observable(false);
    self.checkboxHaveSatisfyingCareer = ko.observable(false);
    self.checkboxBetterNetworkingOportunities = ko.observable(false);
    self.checkboxImproveSkills = ko.observable(false);
    self.checkboxImproveLeadershipSkills = ko.observable(false);
    self.checkboxQualifyToWorkAtOtherCompanies = ko.observable(false);
    self.checkboxForeignWorkOportunities = ko.observable(false);
    self.checkboxCatchUpWithPeers = ko.observable(false);
    self.checkboxStartOwnBusiness = ko.observable(false);
    self.checkboxLearnSomethingDifferent = ko.observable(false);
    self.checkboxGetRespect = ko.observable(false);
    self.checkboxRoleModel = ko.observable(false);
    self.checkboxStandOut = ko.observable(false);
    self.checkboxImproveSocioEconomicStatus = ko.observable(false);
    self.checkboxDevelopSkills = ko.observable(false);
    self.checkboxKeepUpWithChangingWorld = ko.observable(false);
    self.checkboxHaveMoreInfluence = ko.observable(false);
    self.checkboxGainInternationalExposure = ko.observable(false);
    self.checkboxManagementJob = ko.observable(false);
    self.checkboxBecomeAnExpert = ko.observable(false);
    self.checkboxReinventMyself = ko.observable(false);
    self.checkboxIncreaseConfidence = ko.observable(false);
    self.checkboxOvercomeSocialBarriers = ko.observable(false);

    self.checkboxCommunicateFromHome = ko.observable(false);
    self.checkboxLocatedInCurrentCountry = ko.observable(false);
    self.checkboxLocationThatIWouldLike = ko.observable(false);
    self.checkboxExcellentAcademic = ko.observable(false);
    self.checkboxGoodReputationForBusiness = ko.observable(false);
    self.checkboxGraduatesAreMoreSuccessful = ko.observable(false);
    self.checkboxHasTheSpecificProgram = ko.observable(false);
    self.checkboxHighlyRankedSchool = ko.observable(false);
    self.checkboxParentsGraduated = ko.observable(false);
    self.checkboxWellKnownInternationally = ko.observable(false);
    self.checkboxItsAlumniIncludeMany = ko.observable(false);
    self.checkboxHighQualityInstructors = ko.observable(false);
    self.checkboxGraduatesFromThisSchool = ko.observable(false);
    self.checkboxRecommendedByEmployer = ko.observable(false);
    self.checkboxRecommenedByFriends = ko.observable(false);
    self.checkboxEaseOfFittingIn = ko.observable(false);
    self.checkboxOnlyTheBestStudents = ko.observable(false);
    self.checkboxLowerTuitionCosts = ko.observable(false);
    self.checkboxOfferGenerousScholarships = ko.observable(false);
    self.checkboxADegreeFromThisSchool = ko.observable(false);
    self.checkboxOffersGoodStudentExperience = ko.observable(false);
    self.checkboxGoodOnCampusCareer = ko.observable(false);
    self.checkboxModernFacilities = ko.observable(false);
    self.checkboxOffersOnlineClasses = ko.observable(false);
    self.checkboxHasAStrongAlumniNetwork = ko.observable(false);

    self.serverErrorMessage = ko.observable();

    var Marketing = {
        checkboxAdvertisement: self.checkboxAdvertisement,
        checkboxNewsArticle: self.checkboxNewsArticle,
        checkboxUsbPromotionalEmails: self.checkboxUsbPromotionalEmails,
        checkboxConferenceOrExpo: self.checkboxConferenceOrExpo,
        checkboxReferralByMyEmployer: self.checkboxReferralByMyEmployer,
        checkboxStellenboschUniversity: self.checkboxStellenboschUniversity,
        checkboxReferralByAnAlumnus: self.checkboxReferralByAnAlumnus,
        checkboxUsbWebsite: self.checkboxUsbWebsite,
        checkboxReferralByACurrentStudent: self.checkboxReferralByACurrentStudent,
        checkboxUsbEd: self.checkboxUsbEd,
        checkboxReferralByAFamilyMember: self.checkboxReferralByAFamilyMember,
        checkboxSocialMedia: self.checkboxSocialMedia,
        checkboxBrochure: self.checkboxBrochure,
        checkboxOnCampusEvent: self.checkboxOnCampusEvent,


        checkboxEarnMoreMoney: self.checkboxEarnMoreMoney,
        checkboxGiveMyChildABetterFuture: self.checkboxGiveMyChildABetterFuture,
        checkboxIncreaseMyStatus: self.checkboxIncreaseMyStatus,
        checkboxMakeMyParentsProud: self.checkboxMakeMyParentsProud,
        checkboxProvideStability: self.checkboxProvideStability,
        checkboxGetAPromotion: self.checkboxGetAPromotion,
        checkboxQualifyForOportunities: self.checkboxQualifyForOportunities,
        checkboxAdvanceInCareer: self.checkboxAdvanceInCareer,
        checkboxHaveMoreControl: self.checkboxHaveMoreControl,
        checkboxHaveSatisfyingCareer: self.checkboxHaveSatisfyingCareer,
        checkboxBetterNetworkingOportunities: self.checkboxBetterNetworkingOportunities,
        checkboxImproveSkills: self.checkboxImproveSkills,
        checkboxImproveLeadershipSkills: self.checkboxImproveLeadershipSkills,
        checkboxQualifyToWorkAtOtherCompanies: self.checkboxQualifyToWorkAtOtherCompanies,
        checkboxForeignWorkOportunities: self.checkboxForeignWorkOportunities,
        checkboxCatchUpWithPeers: self.checkboxCatchUpWithPeers,
        checkboxStartOwnBusiness: self.checkboxStartOwnBusiness,
        checkboxLearnSomethingDifferent: self.checkboxLearnSomethingDifferent,
        checkboxGetRespect: self.checkboxGetRespect,
        checkboxRoleModel: self.checkboxRoleModel,
        checkboxStandOut: self.checkboxStandOut,
        checkboxImproveSocioEconomicStatus: self.checkboxImproveSocioEconomicStatus,
        checkboxDevelopSkills: self.checkboxDevelopSkills,
        checkboxKeepUpWithChangingWorld: self.checkboxKeepUpWithChangingWorld,
        checkboxHaveMoreInfluence: self.checkboxHaveMoreInfluence,
        checkboxGainInternationalExposure: self.checkboxGainInternationalExposure,
        checkboxManagementJob: self.checkboxManagementJob,
        checkboxBecomeAnExpert: self.checkboxBecomeAnExpert,
        checkboxReinventMyself: self.checkboxReinventMyself,
        checkboxIncreaseConfidence: self.checkboxIncreaseConfidence,
        checkboxOvercomeSocialBarriers: self.checkboxOvercomeSocialBarriers,


        checkboxCommunicateFromHome: self.checkboxCommunicateFromHome,
        checkboxLocatedInCurrentCountry: self.checkboxLocatedInCurrentCountry,
        checkboxLocationThatIWouldLike: self.checkboxLocationThatIWouldLike,
        checkboxExcellentAcademic: self.checkboxExcellentAcademic,
        checkboxGoodReputationForBusiness: self.checkboxGoodReputationForBusiness,
        checkboxGraduatesAreMoreSuccessful: self.checkboxGraduatesAreMoreSuccessful,
        checkboxHasTheSpecificProgram: self.checkboxHasTheSpecificProgram,
        checkboxHighlyRankedSchool: self.checkboxHighlyRankedSchool,
        checkboxParentsGraduated: self.checkboxParentsGraduated,
        checkboxWellKnownInternationally: self.checkboxWellKnownInternationally,
        checkboxItsAlumniIncludeMany: self.checkboxItsAlumniIncludeMany,
        checkboxHighQualityInstructors: self.checkboxHighQualityInstructors,
        checkboxGraduatesFromThisSchool: self.checkboxGraduatesFromThisSchool,
        checkboxRecommendedByEmployer: self.checkboxRecommendedByEmployer,
        checkboxRecommenedByFriends: self.checkboxRecommenedByFriends,
        checkboxEaseOfFittingIn: self.checkboxEaseOfFittingIn,
        checkboxOnlyTheBestStudents: self.checkboxOnlyTheBestStudents,
        checkboxLowerTuitionCosts: self.checkboxLowerTuitionCosts,
        checkboxOfferGenerousScholarships: self.checkboxOfferGenerousScholarships,
        checkboxADegreeFromThisSchool: self.checkboxADegreeFromThisSchool,
        checkboxOffersGoodStudentExperience: self.checkboxOffersGoodStudentExperience,
        checkboxGoodOnCampusCareer: self.checkboxGoodOnCampusCareer,
        checkboxModernFacilities: self.checkboxModernFacilities,
        checkboxOffersOnlineClasses: self.checkboxOffersOnlineClasses,
        checkboxHasAStrongAlumniNetwork: self.checkboxHasAStrongAlumniNetwork
    };

    //Marketing.errors = ko.validation.group(Marketing);

    //self.loadDropDown = function (type) {
    //    self.loading.push(true);
    //    $.ajax({
    //        url: rootUrl + "MarketingDropdown/LoadDropdown?type=" + type,
    //        type: 'GET',
    //        contentType: "application/json",
    //        cache: false,
    //        success: function (j) {
    //            self.populateDropDownList(j.dropdownList, type);
    //        },
    //        error: function (error) {
    //            console.log(error.responseText);
    //        }
    //    }).always(function () {
    //        self.loading.pop();
    //    });
    //};

    //self.populateDropDownList = function (list, type) {
    //    switch (type) {
    //        case "infoSessions":
    //            ;
    //            self.infoSessions(list);
    //            break;
    //    };
    //};

    //self.preloadDropDownLists = function () {
    //    self.loadDropDown("infoSessions");
    //};

    self.loadExistingData = function () {
        $.ajax({
            url: rootUrl + "Page/PopulateMarketing",
            type: 'POST',
            contentType: "application/json",
            //data: AddAntiForgeryToken({}),
            cache: false,
            success: function (data) {
                ko.mapping.fromJS(data, {}, self);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            waitingDialog.hide();
        });
    };


    (function () {
        waitingDialog.show('Preparing Tell Us More Details Form');
        self.loadExistingData();
    })();


    self.StepFourSaveAndProceed = function () {
        //if (Marketing.errors().length === 0) {
        $.ajax({
            url: rootUrl + "Page/SubmitMarketing",
            contentType: 'application/json; charset=utf-8',
            data: ko.mapping.toJSON(Marketing),
            type: "POST",
            success: function (data) {
                console.log(data);
                if (data.Success == false) {
                    self.serverErrorMessage(data.Message);
                    $('#serverValidationAlert').show();
                    window.scroll(0, 0);
                } else {
                    window.location.href = rootUrl + "Page/Documentation";
                }
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).done(function (response) {
            console.log(response);
        });
        //} else {
        //    $('#requiredFieldsAlert').show();
        //    Marketing.errors.showAllMessages();
        //}
    }
};


$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});