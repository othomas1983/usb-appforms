﻿var knockoutValidationSettings = {
    insertMessages: true,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};

ko.validation.configuration = knockoutValidationSettings;
ko.bindingHandlers.radioChecked = (function () {
    var groupObservables = {};
    return {
        init: function (element, valueAccessor) {
            var name = element.name,
                groupObservable = groupObservables[name] || (groupObservables[name] = ko.observable());

            ko.utils.registerEventHandler(element, 'click', function () {
                valueAccessor()(element.checked);
            });
            var s1 = ko.computed(function () {
                if (valueAccessor()()) {
                    element.checked = true;
                    groupObservable(element);
                }
            });
            var s2 = groupObservable.subscribe(function () {
                valueAccessor()(element.checked);
            });
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                s1.dispose();
                s2.dispose();
            });
        }
    };
})();
function WorkAndStudies(isDisabled) {
    var self = this;

    var rootUrl = "/PhDDevFin/";

    var yearList = [""];
    var start = new Date().getFullYear() - 80;
    var end = new Date().getFullYear();
    var options = "";

    for (var year = start ; year <= end; year++) {
        options += year;
        yearList.push(year);
    }

    self.redirect_url = rootUrl + "Page/Marketing";

    self.loading = ko.observableArray();

    self.loading.subscribe(function () {
        if (self.loading().length == 0) {
            // Last dropdown population async ajax call has ended

            // Fill in inputs if existing informantion exists
            self.loadExistingWorkStudies();
        }
    });

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    self.IsAssistingFinancially = ko.observable(false);

    self.englishProficiencySpeakingList = ko.observableArray();
    self.englishProficiencyWritingList = ko.observableArray();
    self.englishProficiencyReadingList = ko.observableArray();
    self.englishProficiencyUnderstandingList = ko.observableArray();
    self.mathematicsCompetencyList = ko.observableArray();
    self.mathematicsPercentageList = ko.observableArray();
    self.priorInvolvementList = ko.observableArray();
    self.employmentHistoryList = ko.observableArray();
    self.institutionsList = ko.observableArray();
    self.fieldOfStudyList = ko.observableArray();
    self.industryList = ko.observableArray();
    self.workAreaList = ko.observableArray();
    self.organizationList = ko.observableArray();
    self.grossSalaryList = ko.observableArray();
    self.employerAssistFinanciallyList = ko.observableArray();
    self.employmentTypeList = ko.observableArray();

    //self.IsRplCandidate = ko.observable(false);

    self.EnglishProficiencySpeaking = ko.observable();
    self.EnglishProficiencyWriting = ko.observable();
    self.EnglishProficiencyReading = ko.observable();
    self.EnglishProficiencyUnderstanding = ko.observable();
    self.MathematicsCompetency = ko.observable();
    self.MathematicsPercentage = ko.observable();
    self.PriorInvolvement = ko.observable();

    self.EmploymentType = ko.observable();

    self.YearsOfWorkingExperience = ko.observable().extend({ min: 0, max: 99, digit: true });
    self.AnnualGrossSalary = ko.observable();

    self.WillEmployerBeAssistingFinancially = ko.observable();
    self.ContactPersonName = ko.observable();
    self.ContactPersonTelephone = ko.observable();
    self.ContactPersonEmailAddress = ko.observable();
    self.EmployerName = ko.observable();
    self.EmployerJobTitle = ko.observable();
    self.employerWorkAreaList = ko.observableArray();
    self.EmployerWorkAreas = ko.observable();

    self.displayAlert = ko.observable(false);
    //self.isOtherInstitution = ko.observable(false);
    self.availableYears = ko.observableArray(yearList);
    self.serverErrorMessage = ko.observable();
    self.validationSummary = ko.observableArray();
    

    //Tertiary Education
    var ObservableTertiaryEducationHistory = function (historyItem) {
        var te_self = this;

        te_self.Id = historyItem.Id;

        te_self.Institution = ko.observable(historyItem.Institution).extend({ required: true });
        te_self.OtherInstitution = ko.observable(historyItem.OtherInstitution);
        te_self.Degree = ko.observable(historyItem.Degree).extend({ required: true });
        te_self.FieldOfStudy = ko.observable(historyItem.FieldOfStudy).extend({ required: true });
        te_self.StudentNumber = ko.observable(historyItem.StudentNumber).extend({ required: true });
        te_self.StartDate = ko.observable(historyItem.StartDate).extend({ required: true });
        te_self.EndDate = ko.observable(historyItem.EndDate).extend({ required: true, min: te_self.StartDate });
        te_self.Completed = ko.observable(historyItem.Completed).extend({ required: false });
        te_self.First = ko.observable(historyItem.First);
        te_self.Highest = ko.observable(historyItem.Highest);
        te_self.isOtherInstitution = ko.observable();


        if (!te_self.OtherInstitution()) {
            te_self.isOtherInstitution(false);
        }
        else {
            te_self.isOtherInstitution(te_self.OtherInstitution().length > 0);
        }
    };

    self.TertiaryEducationHistory = ko.observableArray().extend({
        validation: [{
            validator: function (sources) {
                var anySelectedRadio = false;

                $(sources).each(function () {
                    var thisselectedRadio = this.First();
                    if (anySelectedRadio !== true && thisselectedRadio === true) {
                        anySelectedRadio = thisselectedRadio;
                        return anySelectedRadio;
                    }

                });
                return anySelectedRadio;
            },
            message: 'Tertiary Registrations - One academic record should be set to First.'
        },
        {
            validator: function (sources) {
                var anySelectedRadio = false;

                $(sources).each(function () {
                    var thisselectedRadio = this.Highest();
                    if (anySelectedRadio !== true && thisselectedRadio === true) {
                        anySelectedRadio = thisselectedRadio;
                        return anySelectedRadio;
                    }
                });
                return anySelectedRadio;
            },
            message: 'Tertiary Registrations - One academic record should be set to Highest.'
        }]
    });

    function AddTertiaryEducation() {
        self.TertiaryEducationHistory.push(new ObservableTertiaryEducationHistory({
            'Institution': '',
            'OtherInstitution': '',
            'Degree': '',
            'FieldOfStudy': '',
            'StudentNumber': '',
            'StartDate': '',
            'EndDate': '',
            'Completed': '',
            'First': false,
            'Highest': false,
            'isOtherInstitution': ''
        }));
    };

    //Employement History

    var ObservableEmploymentHistory = function (historyItem) {
        var oe_self = this;

        oe_self.Id = historyItem.Id;

        oe_self.Employer = ko.observable(historyItem.Employer).extend({ required: true });
        oe_self.JobTitle = ko.observable(historyItem.JobTitle).extend({ required: true });
        oe_self.WorkArea = ko.observable(historyItem.WorkArea).extend({ required: true });
        oe_self.Organization = ko.observable(historyItem.Organization).extend({ required: true });
        oe_self.Industry = ko.observable(historyItem.Industry).extend({ required: true });
        oe_self.StartDate = ko.observable(historyItem.StartDate).extend({ required: true });
        oe_self.EndDate = ko.observable(historyItem.EndDate).extend({ required: true, min: oe_self.StartDate });
        oe_self.Type = ko.observable(historyItem.Type).extend({ required: true });
        oe_self.Latest = ko.observable(historyItem.Latest);
        //oe_self.OccupationalCategory = ko.observable(historyItem.OccupationalCategory).extend({ required: true });
    };

    self.EmploymentHistory = ko.observableArray().extend({
        validation: {
            validator: function (sources) {
                var anySelectedRadio = false;

                $(sources).each(function () {
                    var thisselectedRadio = this.Latest();
                    if (anySelectedRadio !== true && thisselectedRadio === true) {
                        anySelectedRadio = thisselectedRadio;
                        return anySelectedRadio;
                    }
                });
                return anySelectedRadio;
            },
            message: 'Employment History - One employment record should be set to Latest.'
        }
    });


    function AddEmploymentHistory() {
        self.EmploymentHistory.push(new ObservableEmploymentHistory({
            'Employer': '',
            'JobTitle': '',
            'WorkArea': '',
            'Organization': '',
            'Industry': '',
            'StartDate': '',
            'EndDate': '',
            'Type': '',
            'Latest': false,
            'OccupationalCategory': ''
        }));
    }

    // Referee

    var ObservableReferee = function (referee) {
        var r_self = this;

        r_self.RefereeName = ko.observable(referee.RefereeName).extend({ required: true });
        r_self.RefereePosition = ko.observable(referee.RefereePosition).extend({ required: true });
        r_self.RefereeTelephone = ko.observable(referee.RefereeTelephone).extend({ required: true });
        r_self.RefereeCellPhone = ko.observable(referee.RefereeCellPhone).extend({ required: true });
        r_self.RefereeEmail = ko.observable(referee.RefereeEmail).extend({
            required: true, pattern: {
                params: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                message: "Please enter a valid email address."
            }
        });
    };

    self.Referee = ko.observableArray();

    self.Referee.push(new ObservableReferee({
        'RefereeName': '',
        'RefereePosition': '',
        'RefereeTelephone': '',
        'RefereeCellPhone': '',
        'RefereeEmail': ''
    }));

    self.Referee.push(new ObservableReferee({
        'RefereeName': '',
        'RefereePosition': '',
        'RefereeTelephone': '',
        'RefereeCellPhone': '',
        'RefereeEmail': ''
    }));

    self.addTertiaryEducationHistory = function () {
        AddTertiaryEducation();
    }.bind(self);

    self.addEmploymentHistory = function () {
        AddEmploymentHistory();
    }.bind(self);

    self.setRequiredFields = function () {
        //self.IsRplCandidate.extend({ required: true });

        self.EnglishProficiencySpeaking.extend({ required: true });
        self.EnglishProficiencyWriting.extend({ required: true });
        self.EnglishProficiencyReading.extend({ required: true });
        self.EnglishProficiencyUnderstanding.extend({ required: true });
        self.MathematicsCompetency.extend({ required: true });
        self.MathematicsPercentage.extend({ required: true });

        self.YearsOfWorkingExperience.extend({ required: true });
        self.AnnualGrossSalary.extend({ required: true });
        self.WillEmployerBeAssistingFinancially.extend({ required: true });
        self.PriorInvolvement.extend({ required: true });

        self.ContactPersonName.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() === true;
                }
            }
        });
        self.ContactPersonTelephone.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() === true;
                }
            }
        });
        self.ContactPersonEmailAddress.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() === true;
                }
            }
        });
        self.EmployerName.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() == true;
                }
            }
        });
        self.EmployerJobTitle.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() == true;
                }
            }
        });
        self.EmployerWorkAreas.extend({
            required: {
                onlyIf: function () {
                    return self.IsAssistingFinancially() == true;
                }
            }
        });
    };

    var WorkAndStudies = {
        IsRplCandidate: self.IsRplCandidate,

        EnglishProficiencySpeaking: self.EnglishProficiencySpeaking,
        EnglishProficiencyWriting: self.EnglishProficiencyWriting,
        EnglishProficiencyReading: self.EnglishProficiencyReading,
        EnglishProficiencyUnderstanding: self.EnglishProficiencyUnderstanding,

        MathematicsCompetency: self.MathematicsCompetency,
        MathematicsPercentage: self.MathematicsPercentage,
        PriorInvolvement: self.PriorInvolvement,

        YearsOfWorkingExperience: self.YearsOfWorkingExperience,
        AnnualGrossSalary: self.AnnualGrossSalary,
        WillEmployerBeAssistingFinancially: self.WillEmployerBeAssistingFinancially,
        ContactPersonName: self.ContactPersonName,
        ContactPersonTelephone: self.ContactPersonTelephone,
        ContactPersonEmailAddress: self.ContactPersonEmailAddress,
        EmployerName: self.EmployerName,
        EmployerJobTitle: self.EmployerJobTitle,
        EmployerWorkAreas: self.EmployerWorkAreas,
        TertiaryEducationHistory: self.TertiaryEducationHistory,
        EmploymentHistory: self.EmploymentHistory,
        Referee: self.Referee
    };

    WorkAndStudies.errors = ko.validation.group(WorkAndStudies, { deep: true, observable: true, live: true });

    self.loadDropDowns = function () {

        self.loadDropDown("talkEnglish");
        self.loadDropDown("writeEnglish");
        self.loadDropDown("readEnglish");
        self.loadDropDown("understandEnglish");
        self.loadDropDown("mathematicsCompetency");
        self.loadDropDown("mathematicsPercentage");
        self.loadDropDown("priorInvolvement");
        self.loadDropDown("workArea");
        self.loadDropDown("industry");
        self.loadDropDown("organization");
        self.loadDropDown("institution");
        self.loadDropDown("fieldOfStudy");
        self.loadDropDown("industry");
        self.loadDropDown("fieldOfStudy");
        self.loadDropDown("workArea");
        self.loadDropDown("organization");
        self.loadDropDown("grossSalary");
        self.loadDropDown("employerAssistFinancially");
        self.loadDropDown("employmentType");
        self.loadDropDown("employerWorkAreas");
    };

    self.loadDropDown = function (type) {
        self.loading.push(true)
        $.ajax({
            url: rootUrl + "WorkStudiesDropdown/LoadDropdown?type=" + type,
            type: 'GET',
            contentType: "application/json",
            success: function (j) {
                self.populateDropDownList(j.list, type);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            self.loading.pop();
        });
    };

    self.populateDropDownList = function (list, type) {

        switch (type) {
            case "readEnglish":;
                self.englishProficiencyReadingList(list);
                break;
            case "writeEnglish":;
                self.englishProficiencyWritingList(list);
                break;
            case "talkEnglish":;
                self.englishProficiencySpeakingList(list);
                break;
            case "understandEnglish":;
                self.englishProficiencyUnderstandingList(list);
                break;
            case "mathematicsCompetency":;
                self.mathematicsCompetencyList(list);
                break;
            case "mathematicsPercentage":;
                self.mathematicsPercentageList(list);
                break;
            case "priorInvolvement":;
                self.priorInvolvementList(list);
                break;
            case "institution":;
                self.institutionsList(list);
                break;
            case "organization":;
                self.organizationList(list);
                break;
            case "workArea":;
                self.workAreaList(list);
                break;
            case "fieldOfStudy":;
                self.fieldOfStudyList(list)
                break;
            case "industry":;
                self.industryList(list)
                break;
            case "grossSalary":;
                self.grossSalaryList(list);
                break;
            case "employerAssistFinancially":;
                self.employerAssistFinanciallyList(list);
                break;
            case "employmentType":;
                self.employmentTypeList(list);
            case "employerWorkAreas":
                ;
                self.employerWorkAreaList(list);
            default:
                ;


        }
    }

    $(function () {
        waitingDialog.show("Preparing Work Studies Form");

        self.setRequiredFields();
        self.loadDropDowns();
    });

    self.loadExistingWorkStudies = function () {
        $.ajax({
            url: rootUrl + "Page/PopulateWorkStudies",
            type: 'POST',
            contentType: "application/json",
            //data: AddAntiForgeryToken({}),
            success: function (data) {
                if (data.EnglishProficiencyReading !== null) {

                    var mapping = {
                        'ignore': ["TertiaryEducationHistory", "EmploymentHistory"]
                    }

                    ko.mapping.fromJS(data, mapping, self);

                    self.TertiaryEducationHistory(ko.utils.arrayMap(data.TertiaryEducationHistory,
                        function (item) {
                            return new ObservableTertiaryEducationHistory(item);
                        }));

                    self.EmploymentHistory(ko.utils.arrayMap(data.EmploymentHistory,
                        function (item) {
                            return new ObservableEmploymentHistory(item);
                        }));

                } else {
                    AddTertiaryEducation();
                    AddEmploymentHistory();
                }
            }
        }).always(function () {
            waitingDialog.hide();
            $("#degreeTooltip").popover({
                container: "body",
                placement: "top",
                trigger: "hover"
            });
        });
    };

    self.StepThreeSaveAndProceed = function () {
        var data = ko.mapping.toJSON(WorkAndStudies);
        if (WorkAndStudies.errors().length === 0) {
            waitingDialog.show('Saving Work and Studies Details');
            $.ajax({
                url: rootUrl + "Page/SubmitWorkStudies",
                contentType: 'application/json; charset=utf-8',
                data: ko.mapping.toJSON(WorkAndStudies),
                type: "POST",
                success: function (data) {
                    console.log(data);
                    if (data.Success == false) {
                        self.serverErrorMessage(data.Message);
                        $('#serverValidationAlert').show();
                        window.scroll(0, 0);
                    } else { 
                        window.location.href = rootUrl + "Page/Documentation";
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            }).done(function (response) {
                console.log(response);
            });
        } else {
            self.validationSummary(WorkAndStudies.errors());
            self.validationSummary().forEach(function (item) {
                if (item === "This field is required.") {
                    self.validationSummary.remove(item);
                }
            });
            WorkAndStudies.errors.showAllMessages(true);
            this.displayAlert(true);
            $('#requiredFieldsAlert').show();
            window.scrollTo(0, 0);
        }
    }

    self.WillEmployerBeAssistingFinancially.subscribe(function () {
        var selectedValue = self.WillEmployerBeAssistingFinancially();
        var selectNo = $('#assistFinancially option').filter(function () { return $(this).html() == "No"; }).val();

        if (selectedValue == parseInt(selectNo)) {
            self.IsAssistingFinancially(false);
            self.ContactPersonName("");
            self.ContactPersonTelephone("");
            self.ContactPersonEmailAddress("");
            self.EmployerName("");
            self.EmployerJobTitle("");
            self.EmployerWorkAreas("");
        }
        else {
            self.IsAssistingFinancially(true);
        }

    });

    self.removeEducationHistoryItem = function (item) {
        self.TertiaryEducationHistory.remove(item);
    };

    self.removeEmploymentHistory = function (item) {
        self.EmploymentHistory.remove(item);
    };

    self.institutionChange = function (data) {
        var selectedValue = data.Institution;
        var selectOther = $('#otherUniversity option').filter(function () { return $(this).html() == "Other University"; }).val();
        if (selectedValue() === selectOther) {
            data.isOtherInstitution(true);
        }
        else {
            data.isOtherInstitution(false);
            data.OtherInstitution('');
        }
    };
}

function LoadKeyValuePairJSONObject(data, observableArray) {
    var jsonResult = data;
    var list = observableArray;
    for (key in data) {
        var item = {
            name: key,         // Push the key on the array
            value: jsonResult[key] // Push the key's value on the array
        };
        list.push(item);
    };
    return list;
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });

});
