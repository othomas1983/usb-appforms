﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using USB.Domain.Models;

namespace USB.PhDDevFin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PhDDevFinService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PhDDevFinService.svc or PhDDevFinService.svc.cs at the Solution Explorer and start debugging.
    public class PhDDevFinService : IPhDDevFinService
    {
        public const string SavePersonalDetails_URL = "http://";
        public const string SaveAddressDetails_URL = "http://";
        public ApplicationSubmissionResult SavePersonalDetails(string json)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    webClient.UploadString(SavePersonalDetails_URL, json);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                
            }
            return new ApplicationSubmissionResult();

        }

        public ApplicationSubmissionResult SaveAddressDetails(string json)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    webClient.UploadString(SavePersonalDetails_URL, json);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                
            }
            return new ApplicationSubmissionResult();
        }

        public ApplicationSubmissionResult SaveWorkDetails(string workDetails)
        {
            return new ApplicationSubmissionResult();
        }

        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        public ApplicationSubmissionResult SaveDocument(DocumentType documentType, byte[] binary)
        {
            return new ApplicationSubmissionResult();
        }
    }
}
