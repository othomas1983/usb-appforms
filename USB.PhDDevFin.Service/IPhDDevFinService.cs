﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using USB.Domain;
using USB.Domain.Models;

namespace USB.PhDDevFin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPhDDevFinService" in both code and config file together.
    [ServiceContract]
    public interface IPhDDevFinService
    {
        [OperationContract]
        ApplicationSubmissionResult SavePersonalDetails(string personalDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveAddressDetails(string addressDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveWorkDetails(string workDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveDocument(DocumentType documentType, byte[] binary);
    }
}
