﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace USB.MPhilDevFin.ApplicationForm.Controllers
{
    public class AfrigisController : Controller
    {
        public JsonResult GetDistributionAreas()
        {
            var query = HttpContext.Request["query"];

            var postalDistributionArea = new Afrigis.PostalDistributionArea();

            var areas = postalDistributionArea.LoadAreas(query);
            //Session["PostalAreas"] = areas;
            return Json(postalDistributionArea.Areas, JsonRequestBehavior.AllowGet);
        }
	}
}