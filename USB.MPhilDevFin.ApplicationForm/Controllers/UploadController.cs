﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using USB.Domain.Tasks;
using USB.MPhilDevFin.ApplicationForm.ViewModels;

namespace USB.MPhilDevFin.ApplicationForm.Controllers
{
    public class UploadController : Controller
    {
        // Enable both Get and Post so that our jquery call can send data, and get a status

        private const string applicationFormDocumentTypeName = "Application_x0020_Form_x0020_Document_x0020_Type";

        [HttpPost]
        public HttpResponseMessage UploadSignedDeclaration()
        {
            //Take this line out
            Session["enrollmentGuid"] = new Guid();

            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                var fileStream = file.InputStream;

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.ContentType = "text/plain";

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());

                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }

                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadGMATSelectionTest()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadApplicationFee()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadCertifiedAcademicRecord()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadCertifiedDegree()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadCV()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadMarriageCertificate()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadMatricCertificate()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadMotivationalEssay()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public HttpResponseMessage UploadCopyOfId()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                var filename = file.FileName.Substring(0, file.FileName.Length - file.FileName.IndexOf('.'));

                //ar fileStream = file.OpenWrite("C:\\upload\\" + file.FileName);
                var fileStream = file.InputStream;

                //file.InputStream.Seek(0, SeekOrigin.Begin);
                //file.InputStream.CopyTo(fileStream);

                //fileStream.Close();

                HttpContext.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = file.FileName };

                HttpContext.Response.Write(serializer.Serialize(result));
                HttpContext.Response.StatusCode = 200;

                byte[] bytes;
                var id = Guid.NewGuid();
                using (var byteReader = new BinaryReader(fileStream))
                {
                    bytes = byteReader.ReadBytes((int)file.InputStream.Length);
                }

                var enrolmentGuid = Guid.Parse(Session["enrollmentGuid"].ToString());
                var message = SharepointTask.UploadFile(file.FileName, applicationFormDocumentTypeName, id, enrolmentGuid, bytes);
                //For compatibility with IE's "done" event we need to return a result as well as setting the context.response

                if (message.IsNotNullOrEmpty())
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                else
                {
                    HttpContext.Response.StatusCode = 200;
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }
    }
}
