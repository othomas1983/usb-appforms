﻿using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.MPhilDevFin.ApplicationForm.ViewModels;

namespace USB.MPhilDevFin.ApplicationForm.Controllers
{
    public class MarketingDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "infoSessions": ;
                    dropdownList = controller.InfoSessions().ToList();
                    break;
        
                default: ;
                    break;
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
    }
}
