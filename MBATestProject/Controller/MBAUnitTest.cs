﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using USB.MBA.ApplicationForm.Controllers;
using FrontEndTestProject.dto;

namespace FrontEndTestProject.Controller
{
    /// <summary>
    /// Summary description for MBAUnitTest
    /// </summary>
    [TestClass]
    public class MBAUnitTest
    {
        #region StartUp Page Tests

        [TestMethod]
        public void EnsureEmailAddressIsEntered()
        {
            //Arrange

            //Act

            //Assert
        }

        [TestMethod]
        public void CheckForInvalidEmailAddress()
        {
            //Arrange

            //Act

            //Assert
        }

        [TestMethod]
        public void EnsureEmailAddressIsValid()
        {
            //Arrange

            //Act

            //Assert
        }

        #endregion

        #region PersonalInformationTests
        
        [TestMethod]
        public void ValidateAndSubmitStepOne()
        {
            //Arrange
            var controller = new PageController();
            var personalDetails = new PersonalContactInformationData();
            var model = personalDetails.CreatePersonalContactDetails();
          
            var load = new PersonalDropdownController();
            var type = "nationalities";
            var loadDropdown = load.LoadDropdown("13465503", type);

            //Act
            controller.SubmitPersonalInformation(model);

            //Assert
            Assert.IsNotNull(controller);
        }

        #endregion

        #region Address Details Tests

        #endregion

        #region Work & Study Tests

        #endregion

        #region Tell Us More Tests

        #endregion

        #region Documentation Tests

        #endregion

        #region Payment Tests

        #endregion

        #region Status Tests

        #endregion






    }
}
