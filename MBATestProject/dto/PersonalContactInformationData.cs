﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.MBA.ApplicationForm.Controllers;

namespace FrontEndTestProject.dto
{
    public class PersonalContactInformationData
    {
        public StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto.PersonalContactDetails CreatePersonalContactDetails()
        {
            var model = new StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto.PersonalContactDetails();

            model.BusinessPhoneNumber = "02133300332";
            model.DateOfBirth = DateTime.Now.AddYears(-24);
            model.eMail = "sandisiwev@eohmc.co.za";
            model.FaxNumber = "02133300203";
            model.FirstNames = "John";
            model.ForeignIdExpiryDate = DateTime.Now.AddYears(+2);
            model.ForeignNumber = "155222333";
            model.GivenName = "Saida";
            model.HomePhoneNumber = "0215555555";
            model.IdNumber = "96012109983084";
            model.Initials = "J";
            model.IsSouthAfrican = true;
            model.MaidenName = "Andy";
            model.MobileNumber = "0714443333";
            model.OtherDisability = "none";
            model.PassportNumber = "02133300332";
            model.Surname = "Vutula";
            model.SelectedNationality = Guid.NewGuid();
            model.SelectedForeignIdentificationType = Guid.NewGuid();
            model.SelectedPermitType = Guid.NewGuid();
            model.SelectedOffering = Guid.NewGuid();
            model.SelectedTitle = Guid.NewGuid();
            model.SelectedGender = Guid.NewGuid();
            model.SelectedEthnicity = Guid.NewGuid();
            model.SelectedLanguage = Guid.NewGuid();
            model.SelectedCorrespondanceLanguage = new Guid();
            model.SelectedMaritalStatus = new Guid();
            model.SelectedDisability = new Guid();
            model.SouthAfricaIdentificationType = new Guid(); 
            model.Status = 1;
            model.UseWheelchair = false;

            return model;
        }
    }
}
