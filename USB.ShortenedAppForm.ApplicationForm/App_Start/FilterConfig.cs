﻿using System.Web;
using System.Web.Mvc;

namespace USB.MBA.Lite.ApplicationForm
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}