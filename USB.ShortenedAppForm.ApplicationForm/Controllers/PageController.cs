﻿using iTextSharp.text.pdf;
using NLog;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Airborne.Notifications;
using USB.MBA.Lite.ApplicationForm.Session;
using USB.MBA.Lite.ApplicationForm.ViewModels;
using SettingsMan = USB.MBA.Lite.ApplicationForm.Properties.Settings;
using Newtonsoft.Json;
using USB.Domain.Models;
using USB.Domain.Models.Enums;
using USB.Domain.State;

namespace USB.MBA.Lite.ApplicationForm.Controllers
{

    public class PageController : BaseController
    {
        //
        // Righardt add domain side validation
        private const string EnrolmentGuidString = "EnrollmentGuid";
        private const string programmeType = "ProgrammeType";
        private const string EmailString = "email";
        private const string UsNumberString = "UsNumber";
        private const string ContactIdString = "ContactId";
        private const string idParameter = "idParameter";
        private const string reviewIdParameter = "reviewIdParameter";
        private const string programmeGuid = "programmeGuid";
        private const string webTokenGuid = "webTokenId";
        private const string applicantStatusString = "applicantStatus";
        private const string insideCRMString = "insideCRM";
        private const string appFormSubmittedString = "appFormSubmitted";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Home

        [HttpGet]
        public ActionResult Index()
        {
            Session.Clear();
            Session[programmeType] = ProgrammeType.ShortenedAppFormMBA;

            var iID = string.Empty;

            var id = HttpContext.Request["id"];
            Session[idParameter] = id;

            if (id.IsNotNullOrEmpty())
            {
                var controller = new ApplicationFormApiController();

                var webTokenId = HttpContext.Request[webTokenGuid];
                Session[webTokenGuid] = webTokenId;
                Session[EnrolmentGuidString] = new Guid(id);

                // Comment validation out for testing purposes
                if (webTokenId.IsNotNullOrEmpty()/* && controller.IsWebTokenValid(new Guid(webTokenId))*/)
                {
                    Session[insideCRMString] = true;
                }

                try
                {
                    var status = controller.GetApplicantStatus(new Guid(id));

                    if (status.IsNull())
                    {
                        var msg = "Application status not found." + Environment.NewLine + "Please contact the System Administrator";
                        logger.Error(string.Format(msg));
                        Session[EnrolmentGuidString] = null;
                        ViewBag.Message = msg;

                        return View("Error");
                    }
                    else
                    {
                        Session[applicantStatusString] = status;
                    }

                    Session[appFormSubmittedString] = controller.IsApplicationFormSubmitted(new Guid(id));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);

                    var msg = ex.Message;
                    ViewBag.Message = msg;

                    return View("Error");
                }

                SetViewBagVariables();
                ViewBag.EnrolmentId = new Guid(HttpContext.Request["id"]);
                return View("PersonalDetails");
            }


            Session[applicantStatusString] = ApplicantStatus.Applicant;
            return View();
        }

        #endregion

        #region Send Mail

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SendMail(string email)
        {
            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var usNumber = applicationFormApiController.GetUsNumber(email);

                applicationFormApiController.ExecuteApplicationAcknowledgmentWorkFlow(usNumber);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidOperationException)
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Submit
        
        [HttpPost]
        public ActionResult SubmitStudentDetails(string email, string studentNumber)
        {
            if (!ModelState.IsValid)
            {
                return Json(new {Success = false, Message = "Email is a required field."});
            }

            try
            {
                var controller = new ApplicationFormApiController();
                var studentHistory = new List<StudentAcademicRecordHistory>();

                Session[EnrolmentGuidString] = null;
                Session[EmailString] = email;

                if (studentNumber.IsNotNullOrEmpty())
                    studentHistory = controller.GetStudentAcademicRecordHistory(email, studentNumber);

                return Json(studentHistory);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Trace(ex.StackTrace);
                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]



        public ActionResult SubmitIndex(string email, string studentNumber)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            if (email == "undefined")
            {
                ModelState.AddModelError("email", "eMail is a required field");
                return View("Index");
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                Session[EnrolmentGuidString] = null;
                Session[EmailString] = email;

                PersonalContactDetails personDetails = new PersonalContactDetails()
                {
                    eMail = email,
                    Status = null
                };

                var usNumber = applicationFormApiController.GetUsNumber(email);

                bool HasSarLatest = applicationFormApiController.ContactHasSARWithApplicantStatus(usNumber);
                if (HasSarLatest == false)
                {
                    //open empty personal details page
                    Session[programmeType] = ProgrammeType.ShortenedAppFormMBA;
                    Session[UsNumberString] = usNumber;


                    personDetails.HasSARWithApplicantStatus = false;
                    return Json(personDetails);
                }
                else
                {
                    Session[UsNumberString] = usNumber;
                    Session[programmeType] = ProgrammeType.ShortenedAppFormMBA;
                    if (usNumber.IsNotNullOrEmpty())
                    {
                        string programmeType = "ShortenedAppFormMBA";
                        personDetails = applicationFormApiController.GetPersonalDetails(usNumber, programmeType);
                        Session[EnrolmentGuidString] = personDetails.SARId;
                    }
                }

                return Json(personDetails);

            }
            catch (InvalidOperationException invalid)
            {
                logger.Error(invalid.Message);
                logger.Trace(invalid.StackTrace);
                return View("Error");
            }
        }
        
        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitPersonalInformation(PersonalContactDetails model)
        {
            bool isShortForm = SettingsMan.Default.IsShortForm;

            //if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            //{
            //    return Json(new { Success = false, Message = "" });
            //}

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }
            try
            {
                var result = new USB.Domain.Validation.ApplicationRules.IdNumberValidation().IsValid(model);

                if (result.IsNotNull())
                {
                    if (result.ErrorMessage.IsNotNullOrEmpty())
                    {
                        ModelState.AddModelError("personalDetails", result.ErrorMessage);
                        return Json(new { Success = false, Message = result.ErrorMessage });
                    }
                }

                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);

                //applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                var usNumber = model.USNumber ?? GetUSNumber(applicationSubmission);
                if (usNumber.IsNotNullOrEmpty())
                    Session[UsNumberString] = usNumber;

                applicationSubmission.UsNumber = usNumber;

                if (model.SelectedOffering.IsNotNull() && Guid.Parse(model.SelectedOffering.GetValueOrDefault().ToString()) != Guid.Empty)
                {
                    var enrolment = GetEnrolment(applicationFormApiController, model.SelectedOffering);

                    if (enrolment != Guid.Empty)
                    {
                        Session[EnrolmentGuidString] = enrolment;
                        applicationSubmission.EnrolmentGuid = enrolment;
                    }
                }
                //Setting the Programtype for documentvisibility functionlaity on shortforms
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.ContactDetails;
                var submission = applicationFormApiController.SubmitPersonalInformation(applicationSubmission, isShortForm);

                if (!submission.Successful)
                {
                    return Json(new { Success = false, Message = submission.Message });
                }
                else
                {
                    Session[UsNumberString] = submission.UsNumber ?? applicationSubmission.UsNumber;
                    Session[EnrolmentGuidString] = submission.EnrolmentId;
                    Session[ContactIdString] = submission.ContactId;

                    //return Json(new { Success = true, Message = "" });
                  
                    return View("AddressDetails");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitAddressDetails(AddressDetails model)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.UsNumber = Session[UsNumberString].ToString();

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.AddressDetails;

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                var submission = applicationFormApiController.SubmitAddressDetails(applicationSubmission);

                if (!submission.Successful)
                {
                    return Json(new { Success = true, Message = submission.Message });
                }
                else
                {
                    return Json(new { Success = true, Message = "" });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitWorkStudies(Workstudies model)
        {
            bool isShortForm = SettingsMan.Default.IsShortForm;

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);

                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.UsNumber = Session[UsNumberString].ToString();

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.WorkStudies;
                var submission = applicationFormApiController.SubmitWorkDetails(applicationSubmission, isShortForm);

                if (!submission.Successful)
                {
                    return Json(new { Success = false, Message = submission.Message });
                }
                else
                {
                    return Json(new { Success = true, Message = "" });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitMarketing(MarketingModel model)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);

                var usNumber = Session[UsNumberString].AsString();
                var enrollmentGuid = Session[EnrolmentGuidString].AsGuid();

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.TellUsMore;

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                var submission = applicationFormApiController.SubmitMarketing(applicationSubmission, enrollmentGuid, usNumber);

                if (!submission.Successful)
                {
                    return Json(new { Success = false, Message = submission.Message });
                }
                else
                {
                    return Json(new { Success = true, Message = "" });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitDocumentation(Documentation model, bool SHLTestRequired, string reviewed)
        {
             bool isShortForm = SettingsMan.Default.IsShortForm;

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];
                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Documentation;

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                var result = applicationFormApiController.SubmitDocumentation(applicationSubmission,
                                                                 SettingsMan.Default.ProgrammeGuid,
                                                                 (Guid)Session[EnrolmentGuidString],
                                                                 (string)Session[UsNumberString],
                                                                 (ApplicantStatus)Session[applicantStatusString],
                                                                 SettingsMan.Default.ProgrammeName, SHLTestRequired, isShortForm);

                if (!result.Successful)
                {
                    //Set outstanding docs to received for MVS
                    //var programmetype = HttpContext.Request["programmeType"];
                    //if (applicationSubmission.ProgrammeType == ProgrammeType.ShortenedAppFormMBA)
                    //{
                        applicationSubmission.DocumentsCompleteTwoOptions = true;
                        applicationSubmission.DocumentsOutstandingTwoOptions = false;
                        applicationSubmission.ApplicationSubmittedTwoOptions = true;
                    
                    //}

                    return Json(new { Success = false, Message = result.Message, ErrorId = result.ErrorId });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            return Json(new { Success = true, Message = "" });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitPayment(PaymentDetails model)
        {
            bool isShortForm = SettingsMan.Default.IsShortForm;

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];
                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Payment;

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                applicationFormApiController.SubmitPaymentDetails(applicationSubmission,
                                                                  (Guid)Session[EnrolmentGuidString],
                                                                  (string)Session[UsNumberString], isShortForm);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            return Json(new { Success = true, Message = "" });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitStatus()
        {
             bool isShortForm = SettingsMan.Default.IsShortForm;

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                var applicationSubmission = new ApplicationSubmission();
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];
                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Status;

                applicationSubmission.ProgrammeType = ProgrammeType.ShortenedAppFormMBA;
                applicationFormApiController.SubmitStatus(applicationSubmission,
                                                          (Guid)Session[EnrolmentGuidString],
                                                          (string)Session[UsNumberString], isShortForm);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            ///Redirect to thank you page
            ///

            var enrolmentId = (Guid)Session[EnrolmentGuidString];
            var thankyoupageURL = ApplicationFormApiController.GetThankYouPageURL(enrolmentId);
            if (thankyoupageURL.IsNullOrEmpty())
            {
                thankyoupageURL = "empty";
            }
            return Json(new { Success = true, Message = thankyoupageURL });
        }

        #endregion

        #region Populate Details

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulatePersonalDetails(PersonalContactDetails model)
        {
            var applicationFormApiController = new ApplicationFormApiController();
      

            if (Session[idParameter].IsNotNull())
            {
                var id = Session[idParameter];

                try
                {
                    model = applicationFormApiController.GetPersonalDetails(id.AsGuid().GetValueOrDefault());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }

                Session[EnrolmentGuidString] = id.AsGuid();
                Session[UsNumberString] = model.USNumber;
                Session.Remove(idParameter);
            }
            else
            {
                var usNumber = Session[UsNumberString];
                model.eMail = Session[EmailString].AsString();

                bool HasSarLatest = applicationFormApiController.ContactHasSARWithApplicantStatus(usNumber.ToString());
                if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == false)
                {
                    try
                    {
                        string programmeType = "ShortenedAppFormMBA";

                        if (Session[EnrolmentGuidString].IsNotNull() &&
                            Guid.Parse(Session[EnrolmentGuidString].ToString()) != Guid.Empty)
                        {
                            model = applicationFormApiController.GetPersonalDetails(Guid.Parse(Session[EnrolmentGuidString].ToString()));
                        }
                        else
                        {
                            model = applicationFormApiController.GetPersonalDetails(usNumber.ToString(), programmeType);
                        }
                        model.ProgramType = ProgrammeType.ShortenedAppFormMBA;
                        Session[programmeType] = model.ProgramType;
                       
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                        logger.Trace(ex.StackTrace);
                    }
                }
                else if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty())
                {
                    model.ProgramType = ProgrammeType.ShortenedAppFormMBA;
                    Session[programmeType] = model.ProgramType;
                    model.USNumber = usNumber.ToString();
                }
                if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == true && (Guid)Session[EnrolmentGuidString] != null)
                {
                    string programmeType = "ShortenedAppFormMBA";

                    if (Session[EnrolmentGuidString].IsNotNull() &&
                        Guid.Parse(Session[EnrolmentGuidString].ToString()) != Guid.Empty)
                    {
                        model = applicationFormApiController.GetPersonalDetails(Guid.Parse(Session[EnrolmentGuidString].ToString()));
                    }
                    else
                    {
                        model = applicationFormApiController.GetPersonalDetails(usNumber.ToString(), programmeType);
                    }
                    model.ProgramType = ProgrammeType.ShortenedAppFormMBA;
                    Session[programmeType] = model.ProgramType;
                }
            }

            return Json(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulateAddressDetails(AddressDetails model)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            var usNumber = Session[UsNumberString];
            bool HasSarLatest = applicationFormApiController.ContactHasSARWithApplicantStatus(usNumber.ToString());
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == true)
            {
                try
                {
                    model = applicationFormApiController.GetAddressDetails(usNumber.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == false && (Guid)Session[EnrolmentGuidString] != null)
            {
                try
                {
                    model = applicationFormApiController.GetAddressDetails(usNumber.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }

                ViewBag.email = Session["email"];

            return Json(model);

            //return Json(new { AddressDetails = model, USNumber = usNumber }, JsonRequestBehavior.AllowGet);             
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulateWorkStudies(Workstudies model)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            
            var usNumber = Session[UsNumberString];

            bool HasSarLatest = applicationFormApiController.ContactHasSARWithApplicantStatus(usNumber.ToString());
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == true)
            {
                try
                {
                    model = applicationFormApiController.GetWorkStudies(usNumber.ToString(), (Guid)Session[EnrolmentGuidString]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == false && (Guid)Session[EnrolmentGuidString] != null)
            {
                try
                {
                    model = applicationFormApiController.GetWorkStudies(usNumber.ToString(), (Guid)Session[EnrolmentGuidString]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }

                ViewBag.email = Session["email"];

            return Json(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulateMarketing(MarketingModel model)
        {
            var applicationFormApiController = new ApplicationFormApiController();

            var usNumber = Session[UsNumberString];

            bool HasSarLatest = applicationFormApiController.ContactHasSARWithApplicantStatus(usNumber.ToString());
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == true)
            {
                try
                {
                    model = applicationFormApiController.GetMarketing(usNumber.ToString(), (Guid)Session[EnrolmentGuidString]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }
            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty() && HasSarLatest == false && (Guid)Session[EnrolmentGuidString] != null)
            {
                try
                {
                    model = applicationFormApiController.GetMarketing(usNumber.ToString(), (Guid)Session[EnrolmentGuidString]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }

            ViewBag.email = Session["email"];
            return Json(model);
        }

        #endregion

        #region Pages
        [HttpGet]
        [SessionExpire]
        public ActionResult AddressDetails()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }
            
            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Documentation()
        {
            if (Session[UsNumberString].IsNull() ||
              Session[UsNumberString].ToString().IsNullOrEmpty() && Session[EnrolmentGuidString].IsNull() ||
              Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
            {
                return HttpNotFound();
            }


            if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false) &&
                 ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted))
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
                ViewBag.ReviewEnabled = true;
                ViewBag.IsAdmittedApplicant = true;
            }
            else if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false))
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
                ViewBag.ReviewEnabled = true;
            }
            else if (Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false) &&
                      ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Admitted))
            {
                ViewBag.IsDeleteEnabled = false;
            }
            else if ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted)
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.IsAdmittedApplicant = true;
            }
            else if ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant)
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
            }
            Session["programmeGuid"] = SettingsMan.Default.ProgrammeGuid;
       
            var sessionContext = SessionContext.FromSession(Session);
            ViewBag.Context = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sessionContext)));
            
            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Payment()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Status()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            var controller = new ApplicationFormApiController();

            ApplicationStatus appStatus = controller.GetApplicationStatus((Guid)Session[EnrolmentGuidString]);

            StatusViewModel model = null;

            if (appStatus.IsNotNull())
            {
                model = new StatusViewModel()
                {
                    PersonalDetailsComplete = appStatus.PersonalDetailsComplete,
                    AddressDetailsComplete = appStatus.AddressDetailsComplete,
                    WorkStudiesComplete = appStatus.WorkStudiesComplete,
                    MarketingComplete = appStatus.MarketingComplete,
                    DocumentationComplete = appStatus.DocumentationComplete,
                    PaymentComplete = appStatus.PaymentComplete
                };
            }

            //if (((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant) ||
            //     (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false)))
            //{
            //    ViewBag.DisableView = true;
            //}

            SetViewBagVariables();

            return View(model);
        }

        [HttpPost]
        public JsonResult GetApplicationStatus()
        {
            if (Session[EnrolmentGuidString].IsNull() || Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
            {
                return
                    new JsonResult().GenericError(
                        NotificationCollection.Create(Notification.Create("Not found", NotificationSeverity.Error)));
            }

            var controller = new ApplicationFormApiController();

            ApplicationStatus appStatus = controller.GetApplicationStatus((Guid)Session[EnrolmentGuidString]);

            StatusViewModel model = null;

            if (appStatus.IsNotNull())
            {
                model = new StatusViewModel()
                {
                    PersonalDetailsComplete = appStatus.PersonalDetailsComplete,
                    AddressDetailsComplete = appStatus.AddressDetailsComplete,
                    WorkStudiesComplete = appStatus.WorkStudiesComplete,
                    MarketingComplete = appStatus.MarketingComplete,
                    DocumentationComplete = appStatus.DocumentationComplete,
                    PaymentComplete = appStatus.PaymentComplete
                };
            }



            //if (((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant) ||
            //     (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false)))
            //{
            //    ViewBag.DisableView = true;
            //}

            SetViewBagVariables();

            return Json(model);
        }


        [HttpGet]
        [SessionExpire]
        public ActionResult WorkStudies()
        {
            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Marketing()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }
            
            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult PersonalDetails(string email)
        {
            if (Session[idParameter].IsNull() &&
                (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty()))
            {
                if (email.IsNullOrEmpty())
                {
                    return HttpNotFound();
                }

                ViewBag.IsFirstTimeApplicant = true;
            }

            SetViewBagVariables();

            ViewBag.EmailAddress = email;
            Session["ProgrammeName"] = SettingsMan.Default.ProgrammeName;
            var id = HttpContext.Request["id"];
            if (id != null || Session[EnrolmentGuidString].IsNotNull() && Guid.Parse(Session[EnrolmentGuidString].ToString()) != Guid.Empty)
            {
                if (id != null && new Guid(id) != Guid.Empty)
                {
                    ViewBag.EnrolmentId = id;
                }
                else
                {
                    ViewBag.EnrolmentId = Guid.Parse(Session[EnrolmentGuidString].ToString());
                }


            }
            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Success()
        {
            Session.Clear();

            return View();
        }

        [HttpGet]
        public ActionResult Error()
        {
            if(Request.QueryString["message"].IsNotNullOrEmpty())
            {
                ViewBag.Message = Request.QueryString["message"].ToString();
            }

            return View();
        }

        [HttpGet]
        public ActionResult Timeout()
        {
            return View();
        }

        [HttpGet]
        public FileResult DownloadDeclarationFile()
        {
            var usNumber = Session[UsNumberString];
            string pdfTemplate = HttpContext.Server.MapPath(@"~/Content/documents/e-application_postgrad_contract.pdf");

            PdfReader reader = new PdfReader(pdfTemplate);

            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamper = new PdfStamper(reader, ms, '\0', false))
                {
                    AcroFields formFields = stamper.AcroFields;
                    formFields.SetField("UsNumber", usNumber.IsNotNull() ? usNumber.ToString() : string.Empty);
                    stamper.FormFlattening = true;
                }

                return File(ms.ToArray(), MediaTypeNames.Application.Octet, "e-application_postgrad_contract.pdf");
            }
        }

        #endregion

        #region Private Methods
        // Enable both Get and Post so that our jquery call can send data, and get a status
        private string GetBaseUrl(HttpRequestBase request)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            //if (appUrl != "/") appUrl += "/";

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        private void SetViewBagVariables()
        {
            if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      !(Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // All Views Editable
                ViewBag.DisableView = false;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      (Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      (ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted)
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
        }

        private Guid GetEnrolment(ApplicationFormApiController applicationFormApiController, Guid? offeringId)
        {
            var enrolmentId = applicationFormApiController.GetEnrolmentGuid((string)Session[UsNumberString], offeringId);
            Session[EnrolmentGuidString] = enrolmentId;
            return enrolmentId;
        }

        private string GetUSNumber(ApplicationSubmission applicationSubmission)
        {
            string usNumber = null;

            string sisCorrespondenceLanguageCode =
                CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                usNumber = PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission, sisCorrespondenceLanguageCode);
            }

            return usNumber;
        }

        #endregion


        protected override void OnException(ExceptionContext filterContext)
        {
            logger.Error(filterContext.Exception.Message);

            ViewBag.Message = filterContext.Exception.Message;

            filterContext.Result = new RedirectResult("~/Error/Index");
            filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
        }
    }
}
