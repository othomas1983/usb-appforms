﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MBA.Lite.ApplicationForm.ViewModels
{
    public class DocumentationViewModel
    {
        public string SignedDeclaration { get; set; }
        public string GMATSelectionTest{ get; set; }
        public string ApplicationFee { get; set; }
        public string CertifiedAcademicRecords { get; set; }
        public string CertifiedDegrees { get; set; }
        public string CV { get; set; }
        public string MarriageCertificate { get; set; }
        public string MotivationalEssays { get; set; } 
        public string CopyOfId { get; set; }
        public string AcceptanceForm { get; set; }
    }
}