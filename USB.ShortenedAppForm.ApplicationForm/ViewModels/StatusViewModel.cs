﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MBA.Lite.ApplicationForm.ViewModels
{
    public class StatusViewModel
    {
        public StatusViewModel()
        {
            PersonalDetailsComplete = false;
            AddressDetailsComplete = false;
            WorkStudiesComplete = false;
            MarketingComplete = false;
            DocumentationComplete = false;
            PaymentComplete = false;
        }

        public bool PersonalDetailsComplete { get; set; }
        public bool AddressDetailsComplete  { get; set; }
        public bool WorkStudiesComplete     { get; set; }
        public bool MarketingComplete { get; set; }
        public bool DocumentationComplete   { get; set; }
        public bool PaymentComplete         { get; set; }
    }
}