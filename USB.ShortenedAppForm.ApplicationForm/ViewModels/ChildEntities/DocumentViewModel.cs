﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MBA.Lite.ApplicationForm.ViewModels
{
    public class DocumentViewModel
    {
        public string Id { get; set; }

        public string DocumentType { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string UploadedDate { get; set; }
        public string Status { get; set; }
        public bool Reviewed { get; set; }
    }
}