﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MBA.Lite.ApplicationForm.ViewModels
{
    public class PersonalContactDetailsViewModel
    {
        public Guid Title { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string FirstNames { get; set; }
        public string GivenName { get; set; }
        public string IdNumber { get; set; }
        public string BusinessPhoneNumber { get; set; }
        public string HomePhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MobileNumber { get; set; }
        public Guid Nationality { get; set; }
        public Guid IdentificationType { get; set; }
        public Guid PermitType { get; set; }
        public string DateofBirth { get; set; }
        public Guid Gender { get; set; }
        public Guid Ethnicity { get; set; }
        public Guid HomeLanguage { get; set; }
        public Guid CorrespondenceLanguage { get; set; }
        public Guid MaritalStatus { get; set; }
        public Guid Disabilities { get; set; }
        public bool UseWheelchair { get; set; }
    }

}