﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PGDipDevFinance.ApplicationForm.ViewModels
{
    public class PaymentDetailsViewModel
    {
        public string Bank { get; set; }
        public string TypeOfAccount { get; set; }
        public string AccountNumber { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BeneficiaryName { get; set; }
        public string ChequesMadeOutTo { get; set; }
    }
}