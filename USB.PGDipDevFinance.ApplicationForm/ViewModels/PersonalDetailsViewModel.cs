﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.PGDipDevFinance.ApplicationForm.ViewModels
{
    public class PersonalContactDetailsViewModel
    {
        public string BusinessPhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string eMail { get; set; }
        public string FaxNumber { get; set; }
        public string FirstNames { get; set; }
        public DateTime? ForeignIdExpiryDate { get; set; }
        public string ForeignNumber { get; set; }
        public string GivenName { get; set; }
        public string HomePhoneNumber { get; set; }

        public string IdNumber { get; set; }
        public string Initials { get; set; }
        public bool? IsSouthAfrican { get; set; }
        public string MaidenName { get; set; }
        public string MobileNumber { get; set; }
        public string OtherDisability { get; set; }
        public string PassportNumber { get; set; }
        public string Surname { get; set; }
        public Guid SelectedNationality { get; set; }
        public Guid SelectedForeignIdentificationType { get; set; }
        public Guid? SelectedPermitType { get; set; }
        public Guid SelectedProgramme { get; set; }
        public Guid SelectedOffering { get; set; }
        public Guid SelectedTitle { get; set; }
        public Guid SelectedGender { get; set; }
        public Guid SelectedEthnicity { get; set; }
        public Guid SelectedLanguage { get; set; }
        public Guid SelectedCorrespondanceLanguage { get; set; }
        public Guid SelectedMaritalStatus { get; set; }
        public Guid SelectedDisability { get; set; }
        public Guid SouthAfricaIdentificationType { get; set; }
        public int? Status { get; set; }
        public bool UseWheelchair { get; set; }
        public string USNumber { get; set; }
    }

}