﻿using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.Usb.Integration.Crm.Crm;
using System;
using System.Linq;
using SettingsMan = USB.Domain.Properties.Settings;
using Usb.Domain.Sharepoint;
using USB.Domain.Controller.Dto;
using System.Collections.Generic;
using StellenboschUniversity.Utilities.NLogGelfExtensions;
using System.ServiceModel;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        private readonly string duplicateForeignerMessage = SettingsMan.Default.DuplicateForeignerMessage;

        public PersonalContactDetails GetPersonalDetails(string usNumber, string programmeType)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_studentacademicrecord sar = null;

                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.usb_USNumber == usNumber);

                if (contact.IsNotNull())
                {
                    sar =
                        crmServiceContext.usb_studentacademicrecordSet
                            .FirstOrDefault(x => x.usb_USNumber == usNumber);
                }

                return ContactMapper.MapToPersonalContactDetails(sar, contact, programmeType);
            }
        }

        public PersonalContactDetails GetPersonalDetails(Guid Id)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar =
                    crmServiceContext.usb_studentacademicrecordSet.FirstOrDefault(
                        p => p.usb_studentacademicrecordId == Id);

                PersonalContactDetails personalDetails = null;

                if (sar.IsNotNull())
                {
                    personalDetails = new PersonalContactDetails();
                    var contact = sar.usb_studentacademicrecord_ContactLookup;

                    personalDetails = ContactMapper.MapToPersonalContactDetails(sar, contact, null);
                }

                return personalDetails;
            }
        }

        public PersonalContactDetails GetPersonalDetailsByEmail(string email)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.FirstOrDefault(p => p.EMailAddress2 == email);

                //return contact;
                //var sar = contact.usb_studentacademicrecord_ContactLookup;

                return ContactMapper.MapToPersonalContactDetails(null, contact, null);
            }
        }

        public AddressDetails GetAddressDetails(string usNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.
                    Where(p => p.usb_USNumber == usNumber).FirstOrDefault();

                return ContactMapper.MapToAddressDetails(contact);
            }
        }

        public Workstudies GetWorkStudies(string usNumber, Guid? enrolmentId = null)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                if (!enrolmentId.HasValue)
                    return new Workstudies();

                var studentAcademicRecords = new List<usb_studentacademicrecord>();
                var employmentHistory = new List<EmploymentHistory>();
                var tertiaryEducationHistory = new List<TertiaryEducationHistory>();
                var workStudies = new Workstudies();

                bool hasEmploymentHistory = crmServiceContext.usb_employmenthistorySet.Where(
                    x => x.usb_StudentAcademicRecordLookup.Id == enrolmentId.GetValueOrDefault()).ToList().Any();

                bool hasTertiaryEducationHistory = crmServiceContext.usb_qualificationsSet.Where(
                    x => x.usb_StudentAcadenicRecordLookup.Id == enrolmentId).ToList().Any();

                if (!hasEmploymentHistory || !hasTertiaryEducationHistory)
                {
                    studentAcademicRecords =
                        crmServiceContext.usb_studentacademicrecordSet.Where(
                            x => x.usb_USNumber == usNumber).ToList();
                }
                else
                {
                    studentAcademicRecords =
                        crmServiceContext.usb_studentacademicrecordSet.Where(
                            x => x.usb_studentacademicrecordId == enrolmentId).ToList();
                }

                foreach (var studentAcademicRecord in studentAcademicRecords)
                {
                    var employmentHistoryResult =
                        crmServiceContext.usb_employmenthistorySet.Where(
                                x =>
                                    x.usb_StudentAcademicRecordLookup.Id ==
                                    studentAcademicRecord.usb_studentacademicrecordId)
                            .ToList();

                    var tertiaryEducationHistoryResult =
                        crmServiceContext.usb_qualificationsSet.Where(
                                x =>
                                    x.usb_StudentAcadenicRecordLookup.Id ==
                                    studentAcademicRecord.usb_studentacademicrecordId)
                            .ToList();

                    if (!employmentHistoryResult.Any() && !tertiaryEducationHistoryResult.Any()) continue;

                    employmentHistory.AddRange(employmentHistoryResult.Select(CRMEntityMapper.MapToEmploymentHistory));

                    tertiaryEducationHistory.AddRange(
                        tertiaryEducationHistoryResult.Select(CRMEntityMapper.MapToTertiaryEducationHistory));

                    workStudies = EnrolmentMapper.MapToWorkStudies(studentAcademicRecord);
                    workStudies.EmploymentHistory = employmentHistory;
                    workStudies.TertiaryEducationHistory = tertiaryEducationHistory;

                    break;
                }

                return workStudies;
            }
        }

        public MarketingModel GetMarketing(string usNumber, Guid? enrolmentId = null)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var enrolmentGuid = new Guid();

                if (enrolmentId.IsNull())
                {
                    enrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
                }
                else
                {
                    //enrolmentGuid = GetEnrolmentGuid(usNumber);
                    if (enrolmentId != null) enrolmentGuid = (Guid) enrolmentId;
                }

                var studentAcademicRecord = crmServiceContext.usb_studentacademicrecordSet.
                    Where(p => p.Id == enrolmentGuid).FirstOrDefault();

                return EnrolmentMapper.MapToMarketingModel(studentAcademicRecord);
            }
        }

        public string GetUsNumber(string email)
        {
            try
            {
                using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                {
                    var contact = (from c in crmServiceContext.ContactSet
                                   where c.EMailAddress2 == email
                                   select c).FirstOrDefault();

                    if (contact.IsNotNull())
                    {
                        return contact.usb_USNumber;
                    }
                }

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                return ex.Detail.Message;
            }
            


            return string.Empty;
        }

      

        private static IEnumerable<Contact> GetContacts(CrmServiceContext crmServiceContext)
        {
            var contact = (from c in crmServiceContext.ContactSet
                where c.ContactId == Guid.Parse("03c0dd79-8279-e111-86d6-00155d62cf10")
                select c).ToList();

            // foreach (var c  in contact) 
            //  {
            //    if (CrmActions.GetEnrolmentGuid(c.usb_USNumber) != Guid.Empty)
            // yield return c;
            //  }
            return contact;
        }

        public usb_studentacademicrecord_statuscode GetStatus(string usNumber)
        {
            var mapper = new SARStatusCode();
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                var studentAcademic = (from c in crmServiceContext.usb_studentacademicrecordSet
                    where c.usb_USNumber == usNumber
                    select c).FirstOrDefault();
                if (studentAcademic.IsNotNull() && studentAcademic.statecode == 0)
                {
                    mapper.GetStatusCode(studentAcademic.statuscode.GetValueOrDefault(2));
                }
            }

            return usb_studentacademicrecord_statuscode.Deactivated;
        }

        //public bool ContactHasSARWithApplicantStatus(string usNumber)
        //{
        //    var match = false;

        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {

        //        var studentAcademic = (from c in crmServiceContext.usb_studentacademicrecordSet
        //            where c.usb_USNumber == usNumber
        //            select c).ToList();


        //        if(studentAcademic.IsNull())
        //        {
        //            match = false;
        //        }
        //        else
        //        {
        //            foreach (var sar in studentAcademic)
        //            {
        //                var offering = sar.usb_ProgrammeOfferingLookup;
        //                if (offering.IsNotNull())
        //                {
        //                    var isOfferingCurrent = IsOfferingDateCurrent(offering.Id);

        //                    if(isOfferingCurrent == false)
        //                    {
        //                        match = false;
        //                    }
        //                    else
        //                    {
        //                        match = true;
        //                    }

        //                    //if (match)
        //                    //{
        //                    //    break;
        //                    //}
        //                }
        //            }
        //        }               
        //    }

        //    return match;
        //}

        public bool ContactHasSARWithApplicantStatus(string usNumber)
        {
            var match = false;

           

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

             var    studentAcademicRecords = (from c in crmServiceContext.usb_studentacademicrecordSet
                                       where c.usb_USNumber == usNumber
                                       select c).ToList();

                foreach (var sar in studentAcademicRecords)
                {
                    var offering = sar.usb_ProgrammeOfferingLookup;
                    if (offering.IsNotNull())
                    {
                        var isOfferingCurrent = IsOfferingDateCurrent(offering.Id);
                        match = isOfferingCurrent;
                        if (match)
                        {
                            break;
                        }
                    }
                }
            }

            return match;
        }

        public string GetContactApplicationLink(string usNumber)
        {
            var appLink = "";



            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                var studentAcademicRecords = (from c in crmServiceContext.usb_studentacademicrecordSet
                                              where c.usb_USNumber == usNumber
                                              select c).ToList();

                foreach (var sar in studentAcademicRecords)
                {
                    var offering = sar.usb_ProgrammeOfferingLookup;
                    if (offering.IsNotNull())
                    {
                        var isOfferingCurrent = IsOfferingDateCurrent(offering.Id);

                        appLink = sar.usb_ApplicationFormURL;
                    }
                }
            }

            return appLink;
        }

        public string ValidateStudentDetails(string email, string usNumber, string idNumber)
        {
            string result = "";
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                var contact = (from c in crmServiceContext.ContactSet
                               where c.usb_USNumber == usNumber &&
                               c.EMailAddress2 == email                               
                               select c).ToList();
                if (contact.IsNotNull())
                {
                    foreach (var student in contact)
                    {
                        //Check identification
                        var idType = student.usb_IndentificationTypeLookup.Name;
                        if (idType.IsNotNullOrEmpty())
                        {
                            if (idType == "ID Number")
                            {
                                //check if idnumber == idnumber supplied
                                if (student.GovernmentId == idNumber)
                                {
                                    result= "true";
                                }
                                else
                                {
                                    result = "false";
                                }

                            }
                            else
                            {
                                //check if foreign/passport number = idnumber supplied
                                if (student.usb_PassportNumber == idNumber)
                                {
                                    result = "true";
                                }
                                else
                                {
                                    result = "false";
                                }
                            }
                        }
                    }
                }else
                {
                    result = "false";
                }
            }
            return result;
        }


        public bool IsOfferingDateCurrent(Guid offeringId)
        {
            var isOfferingCurrent = true;

            var offering = CrmQueries.GetOffering(offeringId);
            int yearOffered = 0;

            if (offering.usb_YearOffered.IsNotNullOrEmpty())
            {
                var year = new string(offering.usb_YearOffered.Where(c => !char.IsWhiteSpace(c)).ToArray());
                yearOffered = int.Parse(year);
            }

            var yearForApplication = DateTime.Now.AddYears(1).Year;

            if (yearOffered <= 0) return true;
            if (yearOffered < yearForApplication)
            {
                isOfferingCurrent = false;
            }

           return isOfferingCurrent;
        }

        public ApplicationSubmissionResult SubmitPersonalInformation(ApplicationSubmission applicationSubmission,
            bool? isShortForm = false)
        {
            Guid contactId = Guid.Empty;
            Guid enrolmentGuid = Guid.Empty;
            string sisCorrespondenceLanguageCode =
                CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());
            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            if (usNumber.IsNullOrEmpty())
            {
                // foreigner duplication check
                if (CrmQueries.GetNationalityName(applicationSubmission.Nationality.GetValueOrDefault()) !=
                    "South Africa")
                {
                    usNumber = CrmActions.FindStrongForeignerMatch(
                        applicationSubmission.Nationality.GetValueOrDefault(),
                        applicationSubmission.DateOfBirth.GetValueOrDefault(),
                        applicationSubmission.FirstNames,
                        applicationSubmission.Surname,
                        applicationSubmission.Language.GetValueOrDefault(),
                        applicationSubmission.Gender.GetValueOrDefault(),
                        applicationSubmission.Ethnicity.GetValueOrDefault(),
                        applicationSubmission.ForeignIdNumber,
                        applicationSubmission.PassportNumber);

                    if (usNumber == null)
                    {
                        if (CrmActions.FindWeakForeignerMatches(applicationSubmission.Nationality.GetValueOrDefault(),
                                applicationSubmission.DateOfBirth.GetValueOrDefault(),
                                applicationSubmission.FirstNames, applicationSubmission.Surname) == true)
                        {
                            return new ApplicationSubmissionResult()
                            {
                                Successful = false,
                                Message = duplicateForeignerMessage
                            };
                        }
                    }
                }

                // get US number
                if (usNumber.IsNullOrEmpty())
                {
                    try
                    {
                        if (usNumber.IsNullOrEmpty())
                        {
                            usNumber =
                                PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission,
                                    sisCorrespondenceLanguageCode);
                        }
                    }
                    catch (ApplicationException ae)
                    {
                        ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error,
                            "Could not get applicant US number from SIS", ae, applicationSubmission);

                        return new ApplicationSubmissionResult() {Successful = false, Message = ae.Message};
                    }
                    catch (Exception exception)
                    {
                        ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error,
                            "Could not get applicant US number from SIS", exception, applicationSubmission);

                        return new ApplicationSubmissionResult()
                        {
                            Successful = false,
                            Message = "Could not get US number for applicant."
                        };
                    }
                }

                logger.LogWithContext(NLog.LogLevel.Info,
                    String.Format("Applicant was assigned US number: {0}", usNumber), applicationSubmission);
            }
            // create contact
            try
            {
                contactId = CrmActions.GetContactId(usNumber);

                CrmActions.CreateOrUpdateContact(usNumber, applicationSubmission, contactId);

            }
            catch (Exception exception)
            {
                logger.Error("Could not create CRM contact", exception, applicationSubmission);
                return new ApplicationSubmissionResult() {Successful = false, Message = "Could not create contact."};
            }

            try
            {
                enrolmentGuid = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission,
                    isShortForm.GetValueOrDefault());
            }
            catch (Exception exception)
            {
                string innerExceptionMessage = "";

                if (exception.InnerException != null)
                {
                    if (exception.InnerException.Message != null)
                    {
                        innerExceptionMessage = exception.InnerException.Message;
                    }
                }

                logger.LogWithContext(NLog.LogLevel.Error,
                    "Could not create CRM enrollment - inner exception: " + innerExceptionMessage, exception,
                    applicationSubmission);
                return new ApplicationSubmissionResult() {Successful = false, Message = "Could not create enrollment."};
            }

            // handle uploaded documents

            logger.LogWithContext(NLog.LogLevel.Info, "Successful submit", applicationSubmission);

            return new ApplicationSubmissionResult()
            {
                Successful = true,
                Message = "",
                UsNumber = usNumber,
                EnrolmentId = enrolmentGuid,
                ContactId = contactId
            };
        }

        public ApplicationSubmissionResult SubmitWorkDetails(ApplicationSubmission applicationSubmission,
            bool? isShortForm = false)
        {
            Guid contactId = Guid.Empty;
            string sisCorrespondenceLanguageCode =
                CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            // create contact
            try
            {
                //contactId = GetContactId(usNumber);
                //var enrolmentGuid = CrmActions.GetEnrolmentGuid(applicationSubmission.UsNumber);

                CrmActions.CreateOrUpdateEnrollment(applicationSubmission.UsNumber, applicationSubmission,
                    isShortForm.GetValueOrDefault());
            }
            catch (Exception exception)
            {
                logger.LogWithContext(NLog.LogLevel.Error, "Could not create CRM contact", exception,
                    applicationSubmission);
                return new ApplicationSubmissionResult() {Successful = false, Message = "Could not create contact."};
            }

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitAddressDetails(ApplicationSubmission applicationSubmission,
            bool? isShortForm = false)
        {
            Guid contactId = Guid.Empty;

            string sisCorrespondenceLanguageCode =
                CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            string usNumber = applicationSubmission.UsNumber;

            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            if (String.IsNullOrEmpty(usNumber))
            {
                // foreigner duplication check
                if (CrmQueries.GetNationalityName(applicationSubmission.Nationality.GetValueOrDefault()) !=
                    "South Africa")
                {
                    usNumber = CrmActions.FindStrongForeignerMatch(
                        applicationSubmission.Nationality.GetValueOrDefault(),
                        applicationSubmission.DateOfBirth.GetValueOrDefault(),
                        applicationSubmission.FirstNames,
                        applicationSubmission.Surname,
                        applicationSubmission.Language.GetValueOrDefault(),
                        applicationSubmission.Gender.GetValueOrDefault(),
                        applicationSubmission.Ethnicity.GetValueOrDefault(),
                        applicationSubmission.ForeignIdNumber,
                        applicationSubmission.PassportNumber);

                    if (usNumber == null)
                    {
                        if (
                            CrmActions.FindWeakForeignerMatches(applicationSubmission.Nationality.GetValueOrDefault(),
                                applicationSubmission.DateOfBirth.GetValueOrDefault(), applicationSubmission.FirstNames,
                                applicationSubmission.Surname) == true)
                        {
                            return new ApplicationSubmissionResult()
                            {
                                Successful = false,
                                Message = duplicateForeignerMessage
                            };
                        }
                    }
                }
            }

            // get US number
            if (String.IsNullOrEmpty(usNumber))
            {
                try
                {
                    usNumber = PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission,
                        sisCorrespondenceLanguageCode);
                }
                catch (Exception exception)
                {
                    ApplicationFormApiController.logger.LogWithContext(NLog.LogLevel.Error,
                        "Could not get applicant US number from SIS", exception, applicationSubmission);

                    return new ApplicationSubmissionResult()
                    {
                        Successful = false,
                        Message = "Could not get US number for applicant."
                    };
                }
            }

            logger.LogWithContext(NLog.LogLevel.Info, String.Format("Applicant was assigned US number: {0}", usNumber),
                applicationSubmission);

            // create contact
            try
            {
                contactId = CrmActions.GetContactId(usNumber);
                var contactCreation = CrmActions.CreateOrUpdateContact(usNumber, applicationSubmission, contactId);
                var enrolmentCreation = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission,
                    isShortForm.GetValueOrDefault());
            }
            catch (Exception exception)
            {
                logger.LogWithContext(NLog.LogLevel.Error, "Could not create CRM contact", exception,
                    applicationSubmission);
                return new ApplicationSubmissionResult() {Successful = false, Message = "Could not create contact."};
            }

            logger.LogWithContext(NLog.LogLevel.Info, "Successful submit", applicationSubmission);

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitMarketing(ApplicationSubmission applicationSubmission,
            Guid? enrollmentId, string usNumber)
        {
            applicationSubmission.EnrolmentGuid = enrollmentId;
            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            var contactCreation = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission, false);

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitDocumentation(ApplicationSubmission applicationSubmission,
            Guid programmeId, Guid enrolmentId, string usNumber,
            ApplicantStatus status, string programmeName, bool? SHLTestRequired = false, bool? isShortForm = false)
        {
             var isPermit = IsPermitReq(usNumber);
            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            var isRPLCandidate = IsRPLCandidate(enrolmentId);

            applicationSubmission.SHLTestRequiredTwoOptions = SHLTestRequired;
            applicationSubmission.EnrolmentGuid = enrolmentId;

            if (!applicationSubmission.DocumentUploadNames.Contains(SettingsMan.Default.DeclarationDocumentName))
            {
                return new ApplicationSubmissionResult()
                {
                    Successful = false,
                    Message = "Student Declaration form has to be upload and submitted",
                    ErrorId = SettingsMan.Default.DeclarationDocumentName
                };
            }

            List<Document> requiredDocumentList = null;

            if (status == ApplicantStatus.Applicant)
            {
                if (isShortForm.Value)
                {
                    // Required Documents
                    requiredDocumentList =
                        CrmQueries.GetRequiredDocuments(programmeId,true)
                            .Where(x => x.Description == SettingsMan.Default.DeclarationDocumentName)
                            .ToList();
                    // Dependent Documents
                   // requiredDocumentList.AddRange(CrmQueries.GetRequiredDocuments(programmeId, 3));
                }
                else
                {
                    // Required Documents
                    requiredDocumentList = CrmQueries.GetRequiredDocuments(programmeId);

                    // Dependent Documents
                    requiredDocumentList.AddRange(CrmQueries.GetRequiredDocuments(programmeId, 2));
                }
            }
            else if (status == ApplicantStatus.Admitted)
            {
                requiredDocumentList = CrmQueries.GetRequiredDocuments(programmeId, 3);
            }
            requiredDocumentList = ResolveIsPermitRequired(isPermit, requiredDocumentList);
            requiredDocumentList = ResolveMarriageCertRequired(isMarriedFemale, requiredDocumentList);
            requiredDocumentList = ResolveRPLCandidate(isRPLCandidate, requiredDocumentList);
            requiredDocumentList = ResolveSHLGMATDocs(SHLTestRequired.Value, requiredDocumentList);

            applicationSubmission.DocumentsOutstandingTwoOptions = false;
            applicationSubmission.DocumentsCompleteTwoOptions = true;

            foreach (Document document in requiredDocumentList)
            {
                if (!applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId) &&
                    !applicationSubmission.DocumentExclusionKeys.Contains(document.DocumentId))
                {
                    applicationSubmission.DocumentsOutstandingTwoOptions = true;
                }
            }

            if (applicationSubmission.DocumentsOutstandingTwoOptions == true)
            {
                applicationSubmission.DocumentsCompleteTwoOptions = false;
            }

            if (CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrolmentId) == false)
            {
                CrmFileUpload.CreateDocumentLocationForEnrollment(enrolmentId);
            }

            Tuple<string, byte[]> documentData;
            string fileName;
            byte[] fileBytes;



            foreach (Document document in requiredDocumentList)
            {
                var reviewed = "No";
                if (Cache.ContainsItem(enrolmentId.ToString(), document.DocumentTypeId.ToString()) &&
                    applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId))
                {
                    documentData = Cache.GetCachedItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                    fileName = documentData.Item1;
                    fileBytes = documentData.Item2;

                    //Get document info from Sharepoint
                    var fileMetaData = SharepointActions.GetItemMetaData(document.DocumentTypeId.GetValueOrDefault(),
                        enrolmentId);

                    if (fileMetaData.IsNotNull())
                    {
                        reviewed = (string) fileMetaData["Reviewed"];
                    }

                    SharepointActions.UploadDocument(fileName, usNumber, document.Level, programmeName,
                        document.Description,
                        document.DocumentId, enrolmentId, fileBytes, reviewed);

                    Cache.RemoveItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                }
                else if (Cache.ContainsItem(enrolmentId.ToString(), document.DocumentTypeId.ToString()) &&
                         !applicationSubmission.DocumentUploadKeys.Contains(document.DocumentId))
                {
                    Cache.RemoveItem(enrolmentId.ToString(), document.DocumentTypeId.ToString());
                    // Cleanup share point if necessary
                    SharepointActions.DeleteDocument(document.DocumentTypeId ?? Guid.Empty, enrolmentId);
                }
                else
                {
                    // Cleanup share point if necessary
                    SharepointActions.DeleteDocument(document.DocumentTypeId ?? Guid.Empty, enrolmentId);
                }
            }

            CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission, isShortForm.GetValueOrDefault());  
              return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitPaymentDetails(ApplicationSubmission applicationSubmission,
            Guid? enrollmentId, string usNumber, bool? isShortForm = false)
        {
            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            var contactCreation = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission,
                isShortForm.GetValueOrDefault());

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public ApplicationSubmissionResult SubmitStatus(ApplicationSubmission applicationSubmission,
            Guid offeringGuidstring, string usNumber, bool? isShortForm = false)
        {
            if (applicationSubmission.EnrolmentGuid.IsNull() || applicationSubmission.EnrolmentGuid == Guid.Empty)
            {
                applicationSubmission.EnrolmentGuid = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }

            var contactCreation = CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission,
                isShortForm.GetValueOrDefault());

            return new ApplicationSubmissionResult() {Successful = true, Message = ""};
        }

        public void ExecuteApplicationAcknowledgmentWorkFlow(string usNumber, Guid? enrolmentId = null)
        {
            var sarId = new Guid();
            //var contactId = GetContactId(usNumber);
            if (enrolmentId.IsNull())
            {
                sarId = CrmActions.GetEnrolmentGuid(usNumber, Guid.Empty);
            }
            else
            {
                sarId = enrolmentId.GetValueOrDefault();
            }

            //TEMP
            var workflowId = SettingsMan.Default.WorkFlowGUID;
            CrmActions.ExecuteApplicationAcknowledgmentWorkFlow(workflowId, sarId);
        }

        #region Private Methods

        public List<Document> ResolveIsPermitRequired(string isPermit, List<Document> requiredDocumentList)
        {
            if (isPermit == "false")
            {
                var permit =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.CopyOfPermit)
                        .FirstOrDefault();
                requiredDocumentList.Remove(permit);
            }

            return requiredDocumentList;
        }

        public List<Document> ResolveMarriageCertRequired(bool isMarriedFemale, List<Document> requiredDocumentList)
        {
            if (!isMarriedFemale)
            {
                var marriageCertificate =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.MarriageCertificateName)
                        .FirstOrDefault();
                requiredDocumentList.Remove(marriageCertificate);
            }

            return requiredDocumentList;
        }

        public List<Document> ResolveRPLCandidate(int? isRPLCandidate, List<Document> requiredDocumentList)
        {
            if (isRPLCandidate == 3 || isRPLCandidate == 2)
            {
                var rplRef1 =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.RPLRef1Name).FirstOrDefault();
                requiredDocumentList.Remove(rplRef1);

                var rplRef2 =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.RPLRef2Name).FirstOrDefault();
                requiredDocumentList.Remove(rplRef2);

                var rplCandidate =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.RPLCandidateName)
                        .FirstOrDefault();
                requiredDocumentList.Remove(rplCandidate);
            }

            return requiredDocumentList;
        }

        public List<Document> ResolveSHLGMATDocs(bool isSHLTestRequired, List<Document> requiredDocumentList)
        {
            if (isSHLTestRequired)
            {
                var GMATSHLResults =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.GMAT_SHLResultsName)
                        .FirstOrDefault();
                requiredDocumentList.Remove(GMATSHLResults);

                var GMATSelectionTest =
                    requiredDocumentList.Where(d => d.Description == SettingsMan.Default.GMATSelectionTestName)
                        .FirstOrDefault();
                requiredDocumentList.Remove(GMATSelectionTest);
            }

            return requiredDocumentList;
        }

        #endregion

    }
}
