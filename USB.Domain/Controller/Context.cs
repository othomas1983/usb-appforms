﻿using System;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers
{
    public class Context
    {

        private const string EnrolmentGuidString = "EnrollmentGuid";
        private const string EmailString = "email";
        private const string UsNumberString = "UsNumber";
        private const string ContactIdString = "ContactId";
        private const string idParameter = "idParameter";
        private const string reviewIdParameter = "reviewIdParameter";
        private const string programmeGuid = "programmeGuid";
        private const string webTokenGuid = "webTokenId";
        private const string applicantStatusString = "applicantStatus";
        private const string insideCRMString = "insideCRM";
        private const string appFormSubmittedString = "appFormSubmitted";


        public static void GetContext(string id, System.Web.HttpContextBase httpContext)
        {

            httpContext.Session[idParameter] = id;

            if (id.IsNotNullOrEmpty())
            {
                var controller = new ApplicationFormApiController();

                var webTokenId = httpContext.Request[webTokenGuid];
                httpContext.Session[webTokenGuid] = webTokenId;

                httpContext.Session[EnrolmentGuidString] = new Guid(id);

                // Comment validation out for testing purposes
                if (webTokenId.IsNotNullOrEmpty() /* && controller.IsWebTokenValid(new Guid(webTokenId))*/)
                {
                    httpContext.Session[insideCRMString] = true;
                }

                var status = controller.GetApplicantStatus(new Guid(id));

                if (status.IsNull())
                {
                    var msg = "Application status not found." + Environment.NewLine + "Please contact the System Administrator";
                    httpContext.Session[EnrolmentGuidString] = null;
                    throw new Exception(string.Format(msg));
                }
                else
                {
                    httpContext.Session[applicantStatusString] = status;
                }

                httpContext.Session[appFormSubmittedString] = controller.IsApplicationFormSubmitted(new Guid(id));
                httpContext.Session[applicantStatusString] = ApplicantStatus.Applicant;

                return;
            }

            throw new Exception("Invalid enrolment id specified");
           

        }
    }
}