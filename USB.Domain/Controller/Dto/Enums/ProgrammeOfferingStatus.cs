﻿namespace USB.Domain.Controller.Dto.Enums
{
    public enum ProgrammeOfferingStatus
    {
        OfferingProposed = 1,
        Inactive = 2,
        OfferingLaunched = 864480000,
        OfferingClosed = 864480003
    }
}
