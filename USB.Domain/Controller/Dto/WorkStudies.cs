﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class Workstudies
    {
        public int? IsRplCandidate { get; set; }

        public int? EnglishProficiencySpeaking { get; set; }
        public int? EnglishProficiencyWriting { get; set; }
        public int? EnglishProficiencyReading { get; set; }
        public int? EnglishProficiencyUnderstanding { get; set; }
        public int? MathematicsCompetency { get; set; }
        public string MathematicsPercentage { get; set; }
        public int? PriorInvolvement { get; set; }

        public List<TertiaryEducationHistory> TertiaryEducationHistory { get; set; }

        public List<EmploymentHistory> EmploymentHistory { get; set; }

        #region Current Employment

        public string YearsOfWorkingExperience { get; set; }
        public int? AnnualGrossSalary { get; set; }
        public int? WillEmployerBeAssistingFinancially { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonTelephone { get; set; }
        public string ContactPersonEmailAddress { get; set; }

        public string EmployerName { get; set; }
        public string EmployerJobTitle { get; set; }
        public int? EmployerWorkAreas { get; set; }
        #endregion

        public List<Referee> Referee { get; set; }
    }
}
