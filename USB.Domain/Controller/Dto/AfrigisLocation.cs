﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class AfrigisLocation
    {
        public Guid Id
        {
            get;
            set;
        }

        public string DisplayText
        {
            get;
            set;
        }
    }
}
