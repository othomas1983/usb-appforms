﻿namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class MarketingModel
    {
        public bool checkboxAdvertisement { get; set; }
        public bool checkboxNewsArticle { get; set; }
        public bool checkboxUsbPromotionalEmails { get; set; }
        public bool checkboxReferralByMyEmployer { get; set; }
        public bool checkboxReferralByAnAlumnus { get; set; }
        public bool checkboxReferralByACurrentStudent { get; set; }
        public bool checkboxReferralByAFamilyMember { get; set; }
        public bool checkboxSocialMedia { get; set; }
        public bool checkboxConferenceOrExpo { get; set; }
        public bool checkboxStellenboschUniversity { get; set; }
        public bool checkboxUsbWebsite { get; set; }
        public bool checkboxUsbEd { get; set; }
        public bool checkboxBrochure { get; set; }
        public bool checkboxOnCampusEvent { get; set; }



        public bool checkboxEarnMoreMoney { get; set; }

        public bool checkboxGiveMyChildABetterFuture { get; set; }

        public bool checkboxIncreaseMyStatus { get; set; }

        public bool checkboxMakeMyParentsProud { get; set; }

        public bool checkboxProvideStability { get; set; }

        public bool checkboxGetAPromotion { get; set; }

        public bool checkboxQualifyForOportunities { get; set; }

        public bool checkboxAdvanceInCareer { get; set; }

        public bool checkboxHaveMoreControl { get; set; }

        public bool checkboxHaveSatisfyingCareer { get; set; }

        public bool checkboxBetterNetworkingOportunities { get; set; }

        public bool checkboxImproveSkills { get; set; }

        public bool checkboxImproveLeadershipSkills { get; set; }

        public bool checkboxQualifyToWorkAtOtherCompanies { get; set; }

        public bool checkboxForeignWorkOportunities { get; set; }

        public bool checkboxCatchUpWithPeers { get; set; }

        public bool checkboxStartOwnBusiness { get; set; }

        public bool checkboxLearnSomethingDifferent { get; set; }

        public bool checkboxGetRespect { get; set; }

        public bool checkboxRoleModel { get; set; }

        public bool checkboxStandOut { get; set; }

        public bool checkboxImproveSocioEconomicStatus { get; set; }

        public bool checkboxDevelopSkills { get; set; }

        public bool checkboxKeepUpWithChangingWorld { get; set; }

        public bool checkboxHaveMoreInfluence { get; set; }

        public bool checkboxGainInternationalExposure { get; set; }

        public bool checkboxManagementJob { get; set; }

        public bool checkboxBecomeAnExpert { get; set; }

        public bool checkboxReinventMyself { get; set; }

        public bool checkboxIncreaseConfidence { get; set; }

        public bool checkboxOvercomeSocialBarriers { get; set; }


        public bool checkboxCommunicateFromHome { get; set; }

        public bool checkboxLocatedInCurrentCountry { get; set; }

        public bool checkboxLocationThatIWouldLike { get; set; }

        public bool checkboxExcellentAcademic { get; set; }

        public bool checkboxGoodReputationForBusiness { get; set; }

        public bool checkboxGraduatesAreMoreSuccessful { get; set; }

        public bool checkboxHasTheSpecificProgram { get; set; }

        public bool checkboxHighlyRankedSchool { get; set; }

        public bool checkboxParentsGraduated { get; set; }

        public bool checkboxWellKnownInternationally { get; set; }

        public bool checkboxItsAlumniIncludeMany { get; set; }

        public bool checkboxHighQualityInstructors { get; set; }

        public bool checkboxGraduatesFromThisSchool { get; set; }

        public bool checkboxRecommendedByEmployer { get; set; }

        public bool checkboxRecommenedByFriends { get; set; }

        public bool checkboxEaseOfFittingIn { get; set; }

        public bool checkboxOnlyTheBestStudents { get; set; }

        public bool checkboxLowerTuitionCosts { get; set; }

        public bool checkboxOfferGenerousScholarships { get; set; }

        public bool checkboxADegreeFromThisSchool { get; set; }

        public bool checkboxOffersGoodStudentExperience { get; set; }

        public bool checkboxGoodOnCampusCareer { get; set; }

        public bool checkboxModernFacilities { get; set; }

        public bool checkboxOffersOnlineClasses { get; set; }

        public bool checkboxHasAStrongAlumniNetwork { get; set; }
    }
}