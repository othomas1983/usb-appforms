﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class Referee
    {
        public string RefereeName { get; set; }
        public string RefereePosition { get; set; }
        public string RefereeTelephone { get; set; }
        public string RefereeCellPhone { get; set; }
        public string RefereeEmail { get; set; }
    }
}
