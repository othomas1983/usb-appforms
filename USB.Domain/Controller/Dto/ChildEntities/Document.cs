﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using USB.Domain.Models.Enums;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class Document
    {
        private bool _canReview = true;

        public Document()
        {
        }

        public Guid? DocumentTypeId { get; set; }

        public bool IsExcluded { get; set; }
        public bool IsDeleted{ get; set; }

        public DocumentType DocumentType { get; set; }
       
        public string Description { get; set; }
        public string FileName { get; set; }
        public string UploadedDate { get; set; }
        public string Status { get; set; }
        public bool Reviewed { get; set; }
        public int Level { get; set; }
        public string Data { get; set; }
        public int DocumentId { get; set; }
        public int Size { get; set; }

        public bool ReviewStateChanged { get; set; }

        public bool CanReview
        {
            get { return _canReview; }
            set { _canReview = value; }
        }
    }
}