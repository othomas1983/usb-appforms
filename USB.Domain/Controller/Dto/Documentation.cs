﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class Documentation
    {
        public List<Document> RequiredDocuments { get; set; }
        public List<int> ExcludedDocumentIds { get; set; }
    }
}
