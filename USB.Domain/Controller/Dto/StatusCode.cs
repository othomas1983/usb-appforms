﻿using StellenboschUniversity.Usb.Integration.Crm;
using System;

namespace USB.Domain.Controller.Dto
{
    public class SARStatusCode
    {
        public usb_studentacademicrecord_statuscode GetStatusCode(int code)
        {
            Type type = typeof(usb_studentacademicrecord_statuscode);
            //var value = Enum.GetName(type, (object)code);
            return (usb_studentacademicrecord_statuscode)Enum.Parse(type, ""+code);
        }
    }

}
