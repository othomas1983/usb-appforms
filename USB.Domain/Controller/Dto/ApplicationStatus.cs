﻿namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class ApplicationStatus
    {
        public bool PersonalDetailsComplete { get; set; }
        public bool AddressDetailsComplete  { get; set; }
        public bool WorkStudiesComplete     { get; set; }
        public bool MarketingComplete       { get; set; }
        public bool DocumentationComplete   { get; set; }
        public bool PaymentComplete         { get; set; }
    }
}