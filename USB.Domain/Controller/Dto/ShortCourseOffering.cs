﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto
{
    public class ShortCourseOffering
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string ShortCourseName
        {
            get;
            set;
        }

        public Guid ShortCourseId
        {
            get;
            set;
        }

        public int ShortCourseNumber
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public bool ProfessionalMembership
        {
            get;
            set;
        }

        public bool OccupationalCategory
        {
            get;
            set;
        }

        public string AdministratorName
        {
            get;
            set;
        }

        public string AdministratorTelephone
        {
            get;
            set;
        }

        public string AdministratorEmail
        {
            get;
            set;
        }
    }
}
