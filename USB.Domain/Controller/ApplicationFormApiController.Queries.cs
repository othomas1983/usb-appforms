﻿using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using Usb.Domain.Sharepoint;
using USB.Domain.Models;
using SettingsMan = USB.Domain.Properties.Settings;

namespace StellenboschUniversity.Usb.Integration.Crm.Controllers
{
    public partial class ApplicationFormApiController
    {
        public Guid GetIdOfShortCourseWithNumber(string courseNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var crmShortCourse =
                    crmServiceContext.usb_programmeofferingSet.FirstOrDefault(
                        p => p.usb_ProgrammeOfferingCode == courseNumber);

                if (crmShortCourse == null)
                {
                    throw new Exception();
                }

                return crmShortCourse.Id;
            }
        }

        public List<AfrigisLocation> AfrigisSearch(string query)
        {
            var results = new List<AfrigisLocation>();

            query = query.Trim();

            IQueryable<usb_afrigis> crmResults = null;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmResults = crmServiceContext.usb_afrigisSet.Where(p =>
                                            p.usb_CityTown.Contains(query) ||
                                            p.usb_CityTownAlt.Contains(query) ||
                                            p.usb_Suburb.Contains(query) ||
                                            p.usb_SuburbAlt.Contains(query) ||
                                            p.usb_PostalCode.Contains(query));
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            foreach (var crmResult in crmResults)
            {
                results.Add(new AfrigisLocation()
                {
                    Id = crmResult.Id,
                    DisplayText = crmResult.usb_Suburb + ", " + crmResult.usb_CityTown + ", " + crmResult.usb_PostalCode
                });
            }

            return results;
        }

        public List<Document> GetSharePointUpoadedDocumentsInformation(Guid enrolmentId)
        {
            return SharepointActions.GetSharePointUpoadedDocuments(enrolmentId);
        }

        public List<Document> GetRequiredDocumentsInformation(Guid programmeId, Guid enrolmentId, int level=1)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(programmeId, level);
            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            var isRPLCandidate = IsRPLCandidate(enrolmentId);
            var isPermit = IsPermitReq(GetUsNumber(enrolmentId));
            

            foreach (Document doc in documentList)
            {

                if (Cache.ContainsItem(enrolmentId.ToString(), doc.DocumentTypeId.ToString()))
                {
                    Tuple<string, byte[]> documentData = Cache.GetCachedItem(enrolmentId.ToString(), doc.DocumentTypeId.ToString());

                    doc.FileName = documentData.Item1;
                    doc.UploadedDate = DateTime.Now.ToString("dd/MM/yyyy");
                    doc.Status = "Received";
                    doc.Level = level;

                    IDictionary<string, object> metadata =
                        GetSharePointItemMetaData(enrolmentId, doc.DocumentTypeId ?? Guid.Empty);

                    if (metadata.IsNotNull())
                    {
                        doc.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                    }
                    else
                    {
                        doc.Reviewed = false;
                    }
                }
                else 
                {
                    IDictionary<string, object> metadata =
                        GetSharePointItemMetaData(enrolmentId, doc.DocumentTypeId ?? Guid.Empty);

                    if (metadata.IsNotNull())
                    {
                        doc.FileName = metadata["Title"].AsString();
                        doc.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                        doc.Status = "Received";
                        doc.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                        doc.Level = level;

                        Cache.AddOrReplace(enrolmentId.ToString(), doc.DocumentId.ToString(),
                                       SharepointActions.DownloadDocument(doc.DocumentId, enrolmentId));
                    }
                } 
            }
            if (isPermit == "false")
            {
                var permit = documentList.Where(d => d.Description == SettingsMan.Default.CopyOfPermit).FirstOrDefault();
                documentList.Remove(permit);
            }
            if (!isMarriedFemale)
             {
                 var marriageCertificate = documentList.Where(d => d.Description == SettingsMan.Default.MarriageCertificateName).FirstOrDefault();
                 documentList.Remove(marriageCertificate);
             }

             if (isRPLCandidate == 3 ||  isRPLCandidate == 2)
            {
                var rplRef1 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef1Name).FirstOrDefault();
                documentList.Remove(rplRef1);

                var rplRef2 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef2Name).FirstOrDefault();
                documentList.Remove(rplRef2);

                var rplCandidate = documentList.Where(d => d.Description == SettingsMan.Default.RPLCandidateName).FirstOrDefault();
                documentList.Remove(rplCandidate);
            }

             var declatationForm = documentList.Where(d => d.Description == SettingsMan.Default.DeclarationDocumentName).FirstOrDefault();
            if (declatationForm.IsNotNull())
            {
                documentList.Remove(declatationForm);
                documentList.Insert(0, declatationForm);
            }

            return documentList;
        }
        public List<Document> GetRequiredDocumentsInformationDirect(Guid enrolmentId)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(enrolmentId, 2,false);
            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            var isRPLCandidate = IsRPLCandidate(enrolmentId);
            var isPermit = IsPermitReq(GetUsNumber(enrolmentId));
            //Check shl and gmat
            var isShl = IsSHLTestRequired(enrolmentId);


            if (isPermit == "false")
            {
                var permit = documentList.Where(d => d.Description == SettingsMan.Default.CopyOfPermit).FirstOrDefault();
                documentList.Remove(permit);
            }

            if (!isMarriedFemale)
            {
                var marriageCertificate = documentList.Where(d => d.Description == SettingsMan.Default.MarriageCertificateName).FirstOrDefault();
                documentList.Remove(marriageCertificate);
            }
            if(!isShl)
            {
                //remove shl
                var gmatShlDocuments = documentList.Where(d => d.Description == SettingsMan.Default.SHLSelectionName).FirstOrDefault();
                documentList.Remove(gmatShlDocuments);
            }
            else
            {
                //remove gmat
                var gmatShlDocuments = documentList.Where(d => d.Description == SettingsMan.Default.GMATSelectionTestName).FirstOrDefault();
                documentList.Remove(gmatShlDocuments);
                
            }

            if (isRPLCandidate == 3 || isRPLCandidate == 2)
            {
                var rplRef1 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef1Name).FirstOrDefault();
                documentList.Remove(rplRef1);

                var rplRef2 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef2Name).FirstOrDefault();
                documentList.Remove(rplRef2);
            }

            //if (!insideCRM)
            //{
            //    var gmatShlDocuments = documentList.Where(d => d.Description == SettingsMan.Default.SHLSelectionName).FirstOrDefault();
            //    documentList.Remove(gmatShlDocuments);

            //    var rplRef1 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef1Name).FirstOrDefault();
            //    documentList.Remove(rplRef1);

            //    var rplRef2 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef2Name).FirstOrDefault();
            //    documentList.Remove(rplRef2);

            //    var rplCandidate = documentList.Where(d => d.Description == SettingsMan.Default.RPLCandidateName).FirstOrDefault();
            //    documentList.Remove(rplCandidate);
            //}

            var declatationForm = documentList.Where(d => d.Description == SettingsMan.Default.DeclarationDocumentName).FirstOrDefault();
            if (declatationForm.IsNotNull())
            {
                documentList.Remove(declatationForm);
                documentList.Insert(0, declatationForm);
            }

            return documentList;
        }

        public List<Document> GetRequiredDocumentsInformationDirect(Guid programmeId, Guid enrolmentId, bool includeGMAT, bool insideCRM, int level = 1)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(programmeId, level);
            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            var isRPLCandidate = IsRPLCandidate(enrolmentId);
            var isPermit = IsPermitReq(GetUsNumber(enrolmentId));

            if (isPermit == "false")
            {
                var permit = documentList.Where(d => d.Description == SettingsMan.Default.CopyOfPermit).FirstOrDefault();
                documentList.Remove(permit);
            }

            if (!isMarriedFemale)
            {
                var marriageCertificate = documentList.Where(d => d.Description == SettingsMan.Default.MarriageCertificateName).FirstOrDefault();
                documentList.Remove(marriageCertificate);
            }

            if (!insideCRM)
            {
                var gmatShlDocuments = documentList.Where(d => d.Description == SettingsMan.Default.SHLSelectionName).FirstOrDefault();
                documentList.Remove(gmatShlDocuments);

                var rplRef1 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef1Name).FirstOrDefault();
                documentList.Remove(rplRef1);

                var rplRef2 = documentList.Where(d => d.Description == SettingsMan.Default.RPLRef2Name).FirstOrDefault();
                documentList.Remove(rplRef2);

                var rplCandidate = documentList.Where(d => d.Description == SettingsMan.Default.RPLCandidateName).FirstOrDefault();
                documentList.Remove(rplCandidate);
            }

            var declatationForm = documentList.Where(d => d.Description == SettingsMan.Default.DeclarationDocumentName).FirstOrDefault();
            if (declatationForm.IsNotNull())
            {
                documentList.Remove(declatationForm);
                documentList.Insert(0, declatationForm);
            }

            return documentList;
        }

        public List<Document> GetRequiredDocumentsInformationDirect(Guid programmeId, Guid enrolmentId,  bool isShortCourse=true)
        {
            List<Document> documentList = CrmQueries.GetRequiredDocuments(programmeId, isShortCourse);
            var isMarriedFemale = IsApplicantFemaleAndMarried(enrolmentId);
            var isRPLCandidate = IsRPLCandidate(enrolmentId);
            var isPermit = IsPermitReq(GetUsNumber(enrolmentId));

            if (isPermit == "false")
            {
                var permit = documentList.Where(d => d.Description == SettingsMan.Default.CopyOfPermit).FirstOrDefault();
                documentList.Remove(permit);
            }
            if (!isMarriedFemale)
            {
                var marriageCertificate = documentList.Where(d => d.Description == SettingsMan.Default.MarriageCertificateName).FirstOrDefault();
                documentList.Remove(marriageCertificate);
            }


            var declatationForm = documentList.Where(d => d.Description == SettingsMan.Default.DeclarationDocumentName).FirstOrDefault();
            if (declatationForm.IsNotNull())
            {
                documentList.Remove(declatationForm);
                documentList.Insert(0, declatationForm);
            }

            return documentList;
        }

        public Document GetRequiredDocumentInformation(Guid documentId, Guid enrolmentId)
        {
            Document document = CrmQueries.GetRequiredDocument(documentId);

            if (Cache.ContainsItem(enrolmentId.ToString(), documentId.ToString()))
            {
                Tuple<string, byte[]> documentData = Cache.GetCachedItem(enrolmentId.ToString(), documentId.ToString());

                document.FileName = documentData.Item1;
                document.UploadedDate = DateTime.Now.ToString("dd/MM/yyyy");
                document.Status = "Received";
            }
            else 
            {
                IDictionary<string, object> metadata =
                   GetSharePointItemMetaData(enrolmentId, documentId);

                if (metadata.IsNotNull())
                {
                    document.FileName = metadata["Title"].AsString();
                    document.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                    document.Status = "Received";
                    document.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                }
            }

            return document;
        }

        public Document GetRequiredDocumentInformationDirect(Guid documentId, Guid enrolmentId)
        {
            Document document = CrmQueries.GetRequiredDocument(documentId);
         
            IDictionary<string, object> metadata =
                GetSharePointItemMetaData(enrolmentId, documentId);

            if (metadata.IsNotNull())
            {
                document.FileName = metadata["Title"].AsString();
                document.UploadedDate = ((DateTime)metadata["Modified"]).ToString("dd/MM/yyyy");
                document.Status = "Received";
                document.Reviewed = (metadata["Reviewed"].AsString().ToLower() == "yes");
                document.DocumentTypeId = Guid.Empty;
            }
    
            return document;
        }


        public void ValidateDocumentsReviewed(Guid enrolmentId)
        {
            var docs = GetSharePointUpoadedDocumentsInformation(enrolmentId);
            var allReviewed = true;
            var applicationFeeReviewedChanged = false;
            var acceptenceFormReviewedChanged = false;
            var applicationFeeReviewed = false;
            var acceptenceFormReviewed = false;


            foreach (var document in docs)
            {
                if (!document.Reviewed)
                {
                    allReviewed = false;
                }

                if (document.FileName.Contains("Application Fee"))
                {
                    applicationFeeReviewedChanged = true;
                    applicationFeeReviewed = document.Reviewed;
                }
                if (document.FileName.Contains("Acceptance Form"))
                {
                    acceptenceFormReviewedChanged = true;
                    acceptenceFormReviewed = document.Reviewed;
                }

            }

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var sar = new usb_studentacademicrecord()
                    {
                        Id = enrolmentId,
                        usb_ApplicationSubmittedTwoOptions = allReviewed
                    };
                    if (applicationFeeReviewedChanged)
                    {
                        sar.usb_ApplicationFeeTwoOptions = applicationFeeReviewed;
                    }
                    if (applicationFeeReviewedChanged)
                    {
                        sar.usb_AcceptanceFormTwoOptions = acceptenceFormReviewed;
                    }

                    crmServiceContext.Update(sar);
                }
                finally
                {
                    crmServiceContext.Dispose();
                } 
            }
        }

        public Document DeleteDocument(Guid documentId, Guid enrolmentId)
        {
            if (Cache.ContainsItem(enrolmentId.ToString(), documentId.ToString()))
            {
                Cache.RemoveItem(enrolmentId.ToString(), documentId.ToString());
            }

            return CrmQueries.GetRequiredDocument(documentId);
        }

        public void SetDocumentAsReviewed(int documentId, Guid enrollmentId, bool reviewed)
        {
            SharepointActions.SetFileReviewedMetdataTag(documentId, enrollmentId, reviewed);

            ValidateDocumentsReviewed(enrollmentId);

           // return GetRequiredDocumentInformation(documentId, enrollmentId);
        }

        public ApplicationStatus GetApplicationStatus(Guid enrolmentId)
        {
            return CrmQueries.GetApplicationStatus(enrolmentId);
        }

        public string GetSharePointFileName(Guid enrolmentId, Guid documentTypeId)
        {
            return SharepointActions.GetFileName(documentTypeId, enrolmentId);
        }

        public IDictionary<string, object> GetSharePointItemMetaData(Guid enrolmentId, Guid documentTypeId)
        {
            return SharepointActions.GetItemMetaData(documentTypeId, enrolmentId);
        }
        public IDictionary<string, object> GetSharePointItemMetaDataByID(Guid enrolmentId, int documentId)
        {
            return SharepointActions.GetItemMetaDataByID(documentId, enrolmentId);
        }

        public Guid GetEnrolmentGuid(string usNumber, Guid? offeringId)
        {
            return CrmActions.GetEnrolmentGuid(usNumber, offeringId);
        }

        public bool IsWebTokenValid(Guid webTokenId)
        {
            return CrmQueries.IsWebTokenValid(webTokenId);
        }
    
        public ApplicantStatus? GetApplicantStatus(Guid enrolmentId)
        {
            return CrmQueries.GetApplicantStatus(enrolmentId);
        }

        public string GetUsNumber(Guid enrolmentId)
        {
            return CrmQueries.GetUsNumber(enrolmentId);
        }

        public bool IsApplicationFormSubmitted(Guid enrolmentId)
        {
            return CrmQueries.IsApplicationFormSubmitted(enrolmentId);
        }

        public bool IsApplicantFemaleAndMarried(Guid enrolmentId)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            var personalDetials = applicationFormApiController.GetPersonalDetails(enrolmentId);

            if (personalDetials.IsNotNull())
            {
                return personalDetials.SelectedGenderName.Contains(SettingsMan.Default.FemaleName) &&
                            personalDetials.SelectedMaritalStatusName.Contains(SettingsMan.Default.MarriedName);
            }
            return false;
        }

        public int? IsRPLCandidate(Guid enrolmentId)
        {
            var studentRecordDetails = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);

            if (studentRecordDetails.IsNotNull())
            {
                if (studentRecordDetails.usb_RPLCandidateOptionset.IsNotNull())
                {
                    return studentRecordDetails.usb_RPLCandidateOptionset;
                }
               else
                {
                    return null;
                }
            }

            return null;
        }

        public string IsPermitReq(string UsNumber)
        {
            var contact = CrmActions.GetContact(UsNumber);
            var Permit = "";

            if (contact.IsNotNull())
            {
                if (contact.usb_PermitTypeLookup == null)
                {
                    Permit = "false";
                }
                else
                {
                    if (contact.usb_PermitTypeLookup.Name.Contains("No Permit"))
                    {
                        //No permit doc
                        Permit = "false";
                    }
                    else
                    {
                        //Show permit doc
                        Permit = "true";
                    }
                }
            }

            return Permit;
        }


        public bool IsSHLTestRequired(Guid enrolmentId)
        {
            var studentRecordDetails = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentId);

            if (studentRecordDetails.IsNotNull())
            {
                      return studentRecordDetails.usb_SHLTestRequiredTwoOptions.HasValue ? studentRecordDetails.usb_SHLTestRequiredTwoOptions.Value : false;
            }

            return false;
        }

        public static string GetThankYouPageURL(Guid enrolmentID)
        {
            usb_studentacademicrecord studentAcademicRecord = null;
            studentAcademicRecord = CrmActions.LoadStudentAcademicRecordByEnrolrmentId(enrolmentID);

            int? MBAStream = null;
            Guid poID = Guid.Empty;
            Guid poFormat = Guid.Empty;

            if (studentAcademicRecord.IsNotNull())
            {
                //MBA Stream ProgrammeOffering and ProgrammeOfferingID
                poID = studentAcademicRecord.usb_ProgrammeOfferingLookup.Id;
                MBAStream = studentAcademicRecord.usb_MBAStream;
                //Programme Format Guid for selected offering
                poFormat = CrmQueries.GetProgrammeOfferingFormat(poID);

            }
            string thankyoupgurl = CrmQueries.GetThankYouPageURL(MBAStream, poID, poFormat);
            



            return thankyoupgurl;
        }


        public List<StudentAcademicRecordHistory> GetStudentAcademicRecordHistory(string email, string usNumber)
        {
            return CrmQueries.GetStudentAcademicRecordHistory(email, usNumber);
        }
    }
}
