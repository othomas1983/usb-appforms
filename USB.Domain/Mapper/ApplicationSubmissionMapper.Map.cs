﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {
        public static ApplicationSubmission Map(PersonalContactDetails personalContactDetails)
        {
            var application = new ApplicationSubmission()
            {
                ContactNumber = personalContactDetails.HomePhoneNumber,
                CorrespondanceLanguage = personalContactDetails.SelectedCorrespondanceLanguage,
                DateOfBirth = personalContactDetails.DateOfBirth,
                Disability = personalContactDetails.SelectedDisability,
                OtherDisability = personalContactDetails.OtherDisability,
                Email = personalContactDetails.eMail,
                Ethnicity = personalContactDetails.SelectedEthnicity,
                FaxNumber = personalContactDetails.FaxNumber,
                FirstNames = personalContactDetails.FirstNames,
                ForeignIdExpiryDate = personalContactDetails.ForeignIdExpiryDate,
                ForeignIdNumber = personalContactDetails.ForeignNumber,
                Gender = personalContactDetails.SelectedGender,
                GivenName = personalContactDetails.GivenName,
                IdNumber = personalContactDetails.IdNumber,
                Initials = personalContactDetails.Initials,
                Language = personalContactDetails.SelectedLanguage,
                MaidenName = personalContactDetails.MaidenName,
                MaritalStatus = personalContactDetails.SelectedMaritalStatus,
                MobileNumber = personalContactDetails.MobileNumber.Trim().Replace(" ","").Replace("(","").Replace(")",""),
                Nationality = personalContactDetails.SelectedNationality,
                CountryOfIssue = personalContactDetails.SelectedCountryOfIssue,
                Offering = personalContactDetails.SelectedOffering.GetValueOrDefault(),
                PermitType = (personalContactDetails.SelectedPermitType != Guid.Empty) ? personalContactDetails.SelectedPermitType : null,
                PassportExpiryDate = personalContactDetails.ForeignIdExpiryDate,
                PassportNumber = personalContactDetails.PassportNumber,
                Surname = personalContactDetails.Surname,
                Title = personalContactDetails.SelectedTitle,
                UsesWheelchair = personalContactDetails.UseWheelchair,
                WorkNumber = personalContactDetails.BusinessPhoneNumber,
                SelectedBlendedOption = personalContactDetails.SelectedBlendedOption,
                SelectedMbaStream = personalContactDetails.SelectedMbaStream,
                AlternativeEmailAddress = personalContactDetails.AlternativeEmailAddress,
                ProgrammeType = personalContactDetails.ProgramType
            };

            if (personalContactDetails.IsSouthAfrican.GetValueOrDefault(true))
            {
                application.IsSouthAfrican = personalContactDetails.IsSouthAfrican;
                application.GovernmentIdType = personalContactDetails.SouthAfricaIdentificationType;

            }
            else
            {
                application.ForeignIdType = personalContactDetails.SelectedForeignIdentificationType;
            }

            return application;
        }

        public static ApplicationSubmission Map(AddressDetails addressDetails)
        {
            return new ApplicationSubmission()
            {
                ResidentialCity = addressDetails.ResidentialCity,
                ResidentialCountry = addressDetails.ResidentialCountry,
                ResidentialPostalCode = addressDetails.ResidentialPostalCode,
                ResidentialStreet1 = addressDetails.ResidentialStreet1,
                ResidentialStreet2 = addressDetails.ResidentialStreet2,
                ResidentialStreet3 = addressDetails.ResidentialStreet3,
                ResidentialSuburb = addressDetails.ResidentialSuburb,
                ResidentialStateOrProvince = addressDetails.ResidentialStateProvince,
                ResidentialAfrigis = addressDetails.ResidentialGisId,

                //CourierAfrigis=,
                PostalPostalCode = addressDetails.PostalPostalCode,
                PostalCountry = addressDetails.PostalCountry,
                PostalStreet1 = addressDetails.PostalStreet1,
                PostalStreet2 = addressDetails.PostalStreet2,
                PostalStreet3 = addressDetails.PostalStreet3,
                PostalSuburb = addressDetails.PostalSuburb,
                PostalCity = addressDetails.PostalCity,
                PostalStateOrProvince = addressDetails.PostalStateProvince,
                PostalAfrigis = addressDetails.PostalGisId,

                BillingAddressedTo = addressDetails.BillingTo,
                BillingPostalCode = addressDetails.BillingPostalCode,
                BillingCountry = addressDetails.BillingCountry,
                BillingStreet1 = addressDetails.BillingStreet1,
                BillingStreet2 = addressDetails.BillingStreet2,
                BillingStreet3 = addressDetails.BillingStreet3,
                BillingSuburb = addressDetails.BillingSuburb,
                BillingCity = addressDetails.BillingCity,
                BillingAfrigis = addressDetails.BillingGisId,

                BusinessPostalCode = addressDetails.BusinessPostalCode,
                BusinessCountry = addressDetails.BusinessCountry,
                BusinessStreet1 = addressDetails.BusinessStreet1,
                BusinessStreet2 = addressDetails.BusinessStreet2,
                BusinessStreet3 = addressDetails.BusinessStreet3,
                BusinessSuburb = addressDetails.BusinessSuburb,
                BusinessCity = addressDetails.BusinessCity,
                BusinessAfrigis = addressDetails.BusinessGisId
            };
        }

        public static ApplicationSubmission Map(Workstudies workStudies)
        {
            var applicationSubmission = new ApplicationSubmission()
            {
                IsRplCandidate = workStudies.IsRplCandidate,

                ApplicationSource = USB.Domain.Models.ApplicationSource.WorkStudies,
                AnnualSalary = workStudies.AnnualGrossSalary,

                EnglishProficiencyRead = workStudies.EnglishProficiencyReading,
                EnglishProficiencyTalk = workStudies.EnglishProficiencySpeaking,
                EnglishProficiencyUnderstand = workStudies.EnglishProficiencyUnderstanding,
                EnglishProficiencyWrite = workStudies.EnglishProficiencyWriting,

                MathematicsCompetency = workStudies.MathematicsCompetency,
                MathematicsPercentage = workStudies.MathematicsPercentage,
                usb_WhatDidYouDoLastYearOptionset = workStudies.PriorInvolvement,

                Qualification = workStudies.TertiaryEducationHistory,
                EmploymentHistory = workStudies.EmploymentHistory,

                TotalYearsWorkingExperience = workStudies.YearsOfWorkingExperience,
                EmployerAssistFinancially = workStudies.WillEmployerBeAssistingFinancially,
                PaymentContactPerson = workStudies.ContactPersonName,
                PaymentContactNumber = workStudies.ContactPersonTelephone,
                PaymentContactEmail = workStudies.ContactPersonEmailAddress,
                PaymentCompanyName = workStudies.EmployerName,
                PaymentEmployerJobTitle = workStudies.EmployerJobTitle,
                EmployerWorkAreas = workStudies.EmployerWorkAreas
                //application.usb_WorkAreaOptionset.IsNotNull() ? application.usb_WorkAreaOptionset : 0,

                //WorkContactTelephone = workStudies.ContactPersonTelephone
                //WorkEmail = workStudies.ContactPersonEmailAddress,
                //WorkStudiesPersonName = workStudies.ContactPersonName,
                //WorkContactTelephone = workStudies.ContactPersonTelephone,


                //workStudies.EmploymentHistoryViewModel,



                //workStudies.PriorInvolvement =,
                //workStudies.RefereeViewModel =,
                //workStudies.TertiaryEducationHistoryViewModel =,
                //workStudies.WillEmployerBeAssistingFinancially =,
                //workStudies.YearsOfWorkingExeperience
            };

            if (workStudies.Referee[0].IsNotNull())
            {
                applicationSubmission.usb_Referee1_Name = workStudies.Referee[0].RefereeName;
                applicationSubmission.usb_Referee1_Position = workStudies.Referee[0].RefereePosition;
                applicationSubmission.usb_Referee1_Telephone = workStudies.Referee[0].RefereeTelephone;
                applicationSubmission.usb_Referee1_Email = workStudies.Referee[0].RefereeEmail;
                applicationSubmission.usb_Referee1_Cellphone = workStudies.Referee[0].RefereeCellPhone;
            }
            if (workStudies.Referee[1].IsNotNull())
            {
                applicationSubmission.usb_Referee2_Name = workStudies.Referee[1].RefereeName;
                applicationSubmission.usb_Referee2_Position = workStudies.Referee[1].RefereePosition;
                applicationSubmission.usb_Referee2_Telephone = workStudies.Referee[1].RefereeTelephone;
                applicationSubmission.usb_Referee2_Email = workStudies.Referee[1].RefereeEmail;
                applicationSubmission.usb_Referee2_Cellphone = workStudies.Referee[1].RefereeCellPhone;
            }

            return applicationSubmission;

        }

        public static ApplicationSubmission Map(MarketingModel marketing)
        {
            return new ApplicationSubmission
            {
                checkboxAdvertisement = marketing.checkboxAdvertisement,
                checkboxNewsArticle = marketing.checkboxNewsArticle,
                checkboxUsbPromotionalEmails = marketing.checkboxUsbPromotionalEmails,
                checkboxReferralByMyEmployer = marketing.checkboxReferralByMyEmployer,
                checkboxStellenboschUniversity = marketing.checkboxStellenboschUniversity,
                checkboxReferralByAnAlumnus = marketing.checkboxReferralByAnAlumnus,
                checkboxUsbWebsite = marketing.checkboxUsbWebsite,
                checkboxReferralByACurrentStudent = marketing.checkboxReferralByACurrentStudent,
                checkboxUsbEd = marketing.checkboxUsbEd,
                checkboxReferralByAFamilyMember = marketing.checkboxReferralByAFamilyMember,
                checkboxBrochure = marketing.checkboxBrochure,
                checkboxSocialMedia = marketing.checkboxSocialMedia,
                checkboxConferenceOrExpo = marketing.checkboxConferenceOrExpo,
                checkboxOnCampusEvent = marketing.checkboxOnCampusEvent,

                checkboxEarnMoreMoney = marketing.checkboxEarnMoreMoney,
                checkboxGiveMyChildABetterFuture = marketing.checkboxGiveMyChildABetterFuture,
                checkboxIncreaseMyStatus = marketing.checkboxIncreaseMyStatus,
                checkboxMakeMyParentsProud = marketing.checkboxMakeMyParentsProud,
                checkboxProvideStability = marketing.checkboxProvideStability,
                checkboxGetAPromotion = marketing.checkboxGetAPromotion,
                checkboxQualifyForOportunities = marketing.checkboxQualifyForOportunities,
                checkboxAdvanceInCareer = marketing.checkboxAdvanceInCareer,
                checkboxHaveMoreControl = marketing.checkboxHaveMoreControl,
                checkboxHaveSatisfyingCareer = marketing.checkboxHaveSatisfyingCareer,
                checkboxBetterNetworkingOportunities = marketing.checkboxBetterNetworkingOportunities,
                checkboxImproveSkills = marketing.checkboxImproveSkills,
                checkboxImproveLeadershipSkills = marketing.checkboxImproveLeadershipSkills,
                checkboxQualifyToWorkAtOtherCompanies = marketing.checkboxQualifyToWorkAtOtherCompanies,
                checkboxForeignWorkOportunities = marketing.checkboxForeignWorkOportunities,
                checkboxCatchUpWithPeers = marketing.checkboxCatchUpWithPeers,
                checkboxStartOwnBusiness = marketing.checkboxStartOwnBusiness,
                checkboxLearnSomethingDifferent = marketing.checkboxLearnSomethingDifferent,
                checkboxGetRespect = marketing.checkboxGetRespect,
                checkboxRoleModel = marketing.checkboxRoleModel,
                checkboxStandOut = marketing.checkboxStandOut,
                checkboxImproveSocioEconomicStatus = marketing.checkboxImproveSocioEconomicStatus,
                checkboxDevelopSkills = marketing.checkboxDevelopSkills,
                checkboxKeepUpWithChangingWorld = marketing.checkboxKeepUpWithChangingWorld,
                checkboxHaveMoreInfluence = marketing.checkboxHaveMoreInfluence,
                checkboxGainInternationalExposure = marketing.checkboxGainInternationalExposure,
                checkboxManagementJob = marketing.checkboxManagementJob,
                checkboxBecomeAnExpert = marketing.checkboxBecomeAnExpert,
                checkboxReinventMyself = marketing.checkboxReinventMyself,
                checkboxIncreaseConfidence = marketing.checkboxIncreaseConfidence,
                checkboxOvercomeSocialBarriers = marketing.checkboxOvercomeSocialBarriers,

                checkboxCommunicateFromHome = marketing.checkboxCommunicateFromHome,
                checkboxLocatedInCurrentCountry = marketing.checkboxLocatedInCurrentCountry,
                checkboxLocationThatIWouldLike = marketing.checkboxLocationThatIWouldLike,
                checkboxExcellentAcademic = marketing.checkboxExcellentAcademic,
                checkboxGoodReputationForBusiness = marketing.checkboxGoodReputationForBusiness,
                checkboxGraduatesAreMoreSuccessful = marketing.checkboxGraduatesAreMoreSuccessful,
                checkboxHasTheSpecificProgram = marketing.checkboxHasTheSpecificProgram,
                checkboxHighlyRankedSchool = marketing.checkboxHighlyRankedSchool,
                checkboxParentsGraduated = marketing.checkboxParentsGraduated,
                checkboxWellKnownInternationally = marketing.checkboxWellKnownInternationally,
                checkboxItsAlumniIncludeMany = marketing.checkboxItsAlumniIncludeMany,
                checkboxHighQualityInstructors = marketing.checkboxHighQualityInstructors,
                checkboxGraduatesFromThisSchool = marketing.checkboxGraduatesFromThisSchool,
                checkboxRecommendedByEmployer = marketing.checkboxRecommendedByEmployer,
                checkboxRecommenedByFriends = marketing.checkboxRecommenedByFriends,
                checkboxEaseOfFittingIn = marketing.checkboxEaseOfFittingIn,
                checkboxOnlyTheBestStudents = marketing.checkboxOnlyTheBestStudents,
                checkboxLowerTuitionCosts = marketing.checkboxLowerTuitionCosts,
                checkboxOfferGenerousScholarships = marketing.checkboxOfferGenerousScholarships,
                checkboxADegreeFromThisSchool = marketing.checkboxADegreeFromThisSchool,
                checkboxOffersGoodStudentExperience = marketing.checkboxOffersGoodStudentExperience,
                checkboxGoodOnCampusCareer = marketing.checkboxGoodOnCampusCareer,
                checkboxModernFacilities = marketing.checkboxModernFacilities,
                checkboxOffersOnlineClasses = marketing.checkboxOffersOnlineClasses,
                checkboxHasAStrongAlumniNetwork = marketing.checkboxHasAStrongAlumniNetwork
            };
        }

        public static ApplicationSubmission Map(Documentation documentation)
        {
            var docs = documentation.RequiredDocuments.Where(d => d.Status == "Received");

            return new ApplicationSubmission()
            {
                ApplicationSource = USB.Domain.Models.ApplicationSource.Documentation,
                DocumentExclusionKeys = documentation.ExcludedDocumentIds ?? new List<int>(),
                DocumentUploadKeys = docs.Select(d=> d.DocumentId).ToList(),
                DocumentUploadNames = docs.Select(d => d.Description ?? string.Empty).ToList(),
            };
        }

        public static ApplicationSubmission Map(PaymentDetails paymentDetails)
        {
            return new ApplicationSubmission() { };
        }
      
    }
}
