﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public class CRMEntityMapper
    {
        public static EmploymentHistory MapToEmploymentHistory(usb_employmenthistory application)
        {
            var employmentHistory = new EmploymentHistory()
            {
                Employer = application.usb_name,
                EndDate = application.usb_EndDate == null ? 0 : application.usb_EndDate.Value.AddYears(1).Year,
                Industry = application.usb_IndustryOptionset.IsNotNull() ?application.usb_IndustryOptionset : 0 ,
                StartDate = application.usb_StartDate == null ? 0 : application.usb_StartDate.Value.AddYears(1).Year,
                WorkArea = application.usb_WorkAreaOptionset.IsNotNull() ? application.usb_WorkAreaOptionset : 0,
                Type = application.usb_EmploymentTypeOptionset.IsNotNull() ? application.usb_EmploymentTypeOptionset : 0,
                OccupationalCategory = application.usb_IndustryOptionset.IsNotNull() ? application.usb_IndustryOptionset : 0,
                Organization = application.usb_TypeofOrganizationOptionset.IsNotNull() ? application.usb_TypeofOrganizationOptionset : 0,
                JobTitle = application.usb_JobTitle,
                Latest = application.usb_LatestTwoOptions.IsNotNull() ? application.usb_LatestTwoOptions : false
            };

            return employmentHistory;
        }

        public static TertiaryEducationHistory MapToTertiaryEducationHistory(usb_qualifications application)
        {
            var employmentHistory = new TertiaryEducationHistory()
            {
                Id = application.Id,
                Completed = application.usb_CompleteTwoOptions.GetValueOrDefault(),
                Degree = application.usb_name,
                DegreeLookup = application.usb_DegreeLookup == null ? Guid.Empty : application.usb_DegreeLookup.Id,
                EndDate = application.usb_EndDate == null ? 0 : application.usb_EndDate.Value.AddYears(1).Year,
                FieldOfStudy = application.usb_FieldOfStudyOptionset.IsNotNull() ? application.usb_FieldOfStudyOptionset.Value : 0,
                First = application.usb_FirstTwoOptions.IsNotNull() ? application.usb_FirstTwoOptions:false,
                Highest= application.usb_HighestQualification.IsNotNull() ? application.usb_HighestQualification :false,
                Institution = application.usb_AcademicInstitutionLookup == null ? Guid.Empty : application.usb_AcademicInstitutionLookup.Id,
                OtherInstitution = application.usb_InstitutionOther,
                StartDate = application.usb_StartDate == null ? 0 : application.usb_StartDate.Value.AddYears(1).Year,
                StudentNumber = application.usb_StudentNumber
            };

            return employmentHistory;
        }

    }
}
