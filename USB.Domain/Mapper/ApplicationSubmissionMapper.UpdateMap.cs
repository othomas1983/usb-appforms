﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.Usb.Integration.Crm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {
        //Update existing app
        public static ApplicationSubmission MapUpdate(PersonalContactDetails personalContactDetails, 
                                               ApplicationSubmission existingApp)
        {
            existingApp.ContactNumber = personalContactDetails.HomePhoneNumber;
            existingApp.CorrespondanceLanguage = personalContactDetails.SelectedCorrespondanceLanguage;
            existingApp.DateOfBirth = personalContactDetails.DateOfBirth;
            existingApp.Disability = personalContactDetails.SelectedDisability;
            existingApp.Ethnicity = personalContactDetails.SelectedEthnicity;
            existingApp.FirstNames = personalContactDetails.FirstNames;
            existingApp.FaxNumber = personalContactDetails.FaxNumber;
            existingApp.ForeignIdExpiryDate = personalContactDetails.ForeignIdExpiryDate;
            existingApp.ForeignIdNumber = personalContactDetails.ForeignNumber;
            existingApp.ForeignIdType = personalContactDetails.SelectedForeignIdentificationType;
            existingApp.Gender = personalContactDetails.SelectedGender;
            existingApp.IdNumber = personalContactDetails.IdNumber;
            existingApp.Initials = personalContactDetails.Initials;
            existingApp.Language = personalContactDetails.SelectedLanguage;
            existingApp.MaritalStatus = personalContactDetails.SelectedMaritalStatus;
            existingApp.MobileNumber = personalContactDetails.MobileNumber;
            existingApp.Nationality = personalContactDetails.SelectedNationality;
            existingApp.CountryOfIssue = personalContactDetails.SelectedCountryOfIssue;
            existingApp.PassportExpiryDate = personalContactDetails.ForeignIdExpiryDate;
            existingApp.PassportNumber = personalContactDetails.PassportNumber;
            existingApp.PermitType = personalContactDetails.SelectedPermitType;
            existingApp.Surname = personalContactDetails.Surname;
            
            existingApp.Title = personalContactDetails.SelectedTitle;
            existingApp.WorkNumber = personalContactDetails.BusinessPhoneNumber;
            
            //var dummyDate = new DateTime();

            //if (DateTime.TryParse(personalContactDetails.DateOfBirth, out dummyDate))
            //{
            //    existingApp.DateOfBirth = dummyDate;
            //}
            //if (DateTime.TryParse(personalContactDetails.ForeignIdExpiryDate, out dummyDate))
            //{
            //    existingApp.ForeignIdExpiryDate = dummyDate;
            //    existingApp.PassportExpiryDate = dummyDate;
            //}

            return existingApp;
        }

        public static ApplicationSubmission MapUpdate(AddressDetails addressDetails, 
                                               ApplicationSubmission existingApp)
        {
                existingApp.ResidentialCity = addressDetails.ResidentialCity;
                existingApp.ResidentialCountry = addressDetails.ResidentialCountry;
                existingApp.ResidentialPostalCode = addressDetails.ResidentialPostalCode;
                existingApp.ResidentialStreet1 = addressDetails.ResidentialStreet1;
                existingApp.ResidentialStreet2 = addressDetails.ResidentialStreet2;
                existingApp.ResidentialStreet3 = addressDetails.ResidentialStreet3;
                existingApp.ResidentialSuburb = addressDetails.ResidentialSuburb;
                //CourierAfrigis=;
                existingApp.PostalPostalCode = addressDetails.PostalPostalCode;
                existingApp.PostalCountry = addressDetails.PostalCountry;
                existingApp.PostalStreet1 = addressDetails.PostalStreet1;
                existingApp.PostalStreet2 = addressDetails.PostalStreet2;
                existingApp.PostalStreet3 = addressDetails.PostalStreet3;
                existingApp.PostalSuburb = addressDetails.PostalSuburb;
                existingApp.PostalCity = addressDetails.PostalCity;
                //PostalAfrigis = 
                return existingApp;

        }

        public static ApplicationSubmission MapUpdate(Workstudies workStudies, 
                                               ApplicationSubmission existingApp)
        {
                existingApp.AnnualSalary = workStudies.AnnualGrossSalary;
                existingApp.WorkEmail = workStudies.ContactPersonEmailAddress;
                existingApp.WorkStudiesPersonName = workStudies.ContactPersonName;
                existingApp.WorkContactTelephone = workStudies.ContactPersonTelephone;
                //workStudies.EmploymentHistoryViewModel;
                existingApp.EnglishProficiencyRead = workStudies.EnglishProficiencyReading;
                existingApp.EnglishProficiencyTalk = workStudies.EnglishProficiencySpeaking;
                existingApp.EnglishProficiencyUnderstand = workStudies.EnglishProficiencyUnderstanding;
                existingApp.EnglishProficiencyWrite = workStudies.EnglishProficiencyWriting;
                existingApp.MathematicsCompetency = workStudies.MathematicsCompetency;
                existingApp.MathematicsPercentage = workStudies.MathematicsPercentage;
                //workStudies.PriorInvolvement =,
                //workStudies.RefereeViewModel =,
                //workStudies.TertiaryEducationHistoryViewModel =,
                //workStudies.WillEmployerBeAssistingFinancially =,
                //workStudies.YearsOfWorkingExeperience
                return existingApp;
        }

        public static ApplicationSubmission MapUpdate(MarketingModel marketing, 
                                               ApplicationSubmission existingApp)
        {   
               existingApp.checkboxAdvertisement = marketing.checkboxAdvertisement;
               existingApp.checkboxNewsArticle = marketing.checkboxNewsArticle;
               existingApp.checkboxUsbPromotionalEmails = marketing.checkboxUsbPromotionalEmails;
               existingApp.checkboxConferenceOrExpo = marketing.checkboxConferenceOrExpo;
               existingApp.checkboxReferralByMyEmployer = marketing.checkboxReferralByMyEmployer;
               existingApp.checkboxStellenboschUniversity = marketing.checkboxStellenboschUniversity;
               existingApp.checkboxReferralByAnAlumnus = marketing.checkboxReferralByAnAlumnus;
               existingApp.checkboxUsbWebsite = marketing.checkboxUsbWebsite;
               existingApp.checkboxReferralByACurrentStudent = marketing.checkboxReferralByACurrentStudent;
               existingApp.checkboxUsbEd = marketing.checkboxUsbEd;
               existingApp.checkboxReferralByAFamilyMember = marketing.checkboxReferralByAFamilyMember;
               existingApp.checkboxBrochure = marketing.checkboxBrochure;
               existingApp.checkboxSocialMedia = marketing.checkboxSocialMedia;
               existingApp.checkboxOnCampusEvent = marketing.checkboxOnCampusEvent;

                
                return existingApp;
        }
        
    }
}