﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {

        #region Enrollment Mapping Region

        public static usb_studentacademicrecord MapToEnrollment(ApplicationSubmission application)
        {
            switch (application.ApplicationSource)
            {
                case USB.Domain.Models.ApplicationSource.ContactDetails:
                    return MapPersonalDetailsToEnrollment(application);

                case USB.Domain.Models.ApplicationSource.AddressDetails:
                    return new usb_studentacademicrecord();

                case USB.Domain.Models.ApplicationSource.TellUsMore:
                    return MapMarketingToEnrollment(application);

                case USB.Domain.Models.ApplicationSource.WorkStudies:
                    return MapWorkStudiesToEnrollment(application);

                case USB.Domain.Models.ApplicationSource.Documentation:
                    return MapDocumentsToEnrollment(application);

                case USB.Domain.Models.ApplicationSource.Payment:
                    return new usb_studentacademicrecord();

                case USB.Domain.Models.ApplicationSource.Status:
                    return new usb_studentacademicrecord();

                default:
                    return null;
            }
        }

        #endregion

        #region Personal Detail Mapping

        private static usb_studentacademicrecord MapPersonalDetailsToEnrollment(ApplicationSubmission application)
        {
            return new usb_studentacademicrecord()
            {
                usb_name = application.GivenName + " " + application.Surname,
                usb_FirstName = application.FirstNames,
                usb_LastName = application.Surname
            };
        }

        #endregion

        #region Work Studies Mapping

        private static usb_studentacademicrecord MapWorkStudiesToEnrollment(ApplicationSubmission application)
        {
            return new usb_studentacademicrecord()
            {
                usb_RPLCandidateOptionset = application.IsRplCandidate,

                usb_EnglishReadOptionset = application.EnglishProficiencyRead,
                usb_EnglishSpeakOptionset = application.EnglishProficiencyTalk,
                usb_EnglishUnderstandOptionset = application.EnglishProficiencyUnderstand,
                usb_EnglishWriteOptionset = application.EnglishProficiencyWrite,

                //Financial Assistance
                usb_EmployerassistfinanciallyOptionset = application.EmployerAssistFinancially,
                usb_EmployerName = application.PaymentContactPerson,
                usb_EmployerTelephone = application.PaymentContactNumber,
                usb_EmployerEmail = application.PaymentContactEmail,

                usb_EmployerCompany = application.PaymentCompanyName,
                usb_EmployerJobTitle = application.PaymentEmployerJobTitle,
                usb_EmployerWorkArea= application.EmployerWorkAreas,                

                usb_SalaryOptionset = application.AnnualSalary,
                usb_WhatDidYouDoLastYearOptionset = application.usb_WhatDidYouDoLastYearOptionset,
                usb_TotalYearsWorkingExperience = application.TotalYearsWorkingExperience,

                usb_HighestLevelMathematicsCompetedOptionset = application.MathematicsCompetency,
                usb_SymbolAchieved = application.MathematicsPercentage,

                usb_Referee1_Cellphone = application.usb_Referee1_Cellphone,
                usb_Referee1_Email = application.usb_Referee1_Email,
                usb_Referee1_Name = application.usb_Referee1_Name,
                usb_Referee1_Position = application.usb_Referee1_Position,
                usb_Referee1_Telephone = application.usb_Referee1_Telephone,

                usb_Referee2_Cellphone = application.usb_Referee2_Cellphone,
                usb_Referee2_Email = application.usb_Referee2_Email,
                usb_Referee2_Name = application.usb_Referee2_Name,
                usb_Referee2_Position = application.usb_Referee2_Position,
                usb_Referee2_Telephone = application.usb_Referee2_Telephone,
            };
        }

        #endregion

        #region Documents Mapping

        private static usb_studentacademicrecord MapDocumentsToEnrollment(ApplicationSubmission application)
        {
            return new usb_studentacademicrecord()
            {
                usb_DocumentsCompletedTwoOptions = application.DocumentsCompleteTwoOptions
            };
        }

        #endregion

        #region Marketing Mapping Region

        public static MarketingModel MapToMarketingModel(ApplicationSubmission existingApp)
        {
            var marketing = new MarketingModel();

            marketing.checkboxAdvertisement = existingApp.usb_advertisementTwoOptions.GetValueOrDefault();
            marketing.checkboxNewsArticle = existingApp.usb_NewsArticleTwoOptions.GetValueOrDefault();
            marketing.checkboxUsbPromotionalEmails = existingApp.usb_PromotionalEmailsTwoOptions.GetValueOrDefault();
            marketing.checkboxReferralByMyEmployer = existingApp.usb_ReferralByEmployerTwoOptions.GetValueOrDefault();
            marketing.checkboxStellenboschUniversity =
                existingApp.usb_StellenboschUniversityTwoOptions.GetValueOrDefault();
            marketing.checkboxReferralByAnAlumnus = existingApp.usb_ReferralByAlumnusTwoOptions.GetValueOrDefault();
            marketing.checkboxUsbWebsite = existingApp.usb_USBWebsiteTwoOptions.GetValueOrDefault();
            marketing.checkboxReferralByACurrentStudent =
                existingApp.usb_ReferralByCurrentStudentTwoOptions.GetValueOrDefault();
            marketing.checkboxUsbEd = existingApp.usb_USBEdTwoOptions.GetValueOrDefault();
            marketing.checkboxReferralByAFamilyMember =
                existingApp.usb_ReferralByFamilyFriendTwoOptions.GetValueOrDefault();
            marketing.checkboxBrochure = existingApp.usb_BrochureTwoOptions.GetValueOrDefault();
            marketing.checkboxSocialMedia = existingApp.usb_SocialMediaTwoOptions.GetValueOrDefault();
            marketing.checkboxConferenceOrExpo = existingApp.usb_ConferenceExpoTwoOptions.GetValueOrDefault();
            marketing.checkboxOnCampusEvent = existingApp.usb_OnCampusEventTwoOptions.GetValueOrDefault();

            marketing.checkboxEarnMoreMoney = existingApp.checkboxEarnMoreMoney;
            marketing.checkboxGiveMyChildABetterFuture = existingApp.checkboxGiveMyChildABetterFuture;
            marketing.checkboxIncreaseMyStatus = existingApp.checkboxIncreaseMyStatus;
            marketing.checkboxMakeMyParentsProud = existingApp.checkboxMakeMyParentsProud;
            marketing.checkboxProvideStability = existingApp.checkboxProvideStability;
            marketing.checkboxGetAPromotion = existingApp.checkboxGetAPromotion;
            marketing.checkboxQualifyForOportunities = existingApp.checkboxQualifyForOportunities;
            marketing.checkboxAdvanceInCareer = existingApp.checkboxAdvanceInCareer;
            marketing.checkboxHaveMoreControl = existingApp.checkboxHaveMoreControl;
            marketing.checkboxHaveSatisfyingCareer = existingApp.checkboxHaveSatisfyingCareer;
            marketing.checkboxBetterNetworkingOportunities = existingApp.checkboxBetterNetworkingOportunities;
            marketing.checkboxImproveSkills = existingApp.checkboxImproveSkills;
            marketing.checkboxImproveLeadershipSkills = existingApp.checkboxImproveLeadershipSkills;
            marketing.checkboxQualifyToWorkAtOtherCompanies = existingApp.checkboxQualifyToWorkAtOtherCompanies;
            marketing.checkboxForeignWorkOportunities = existingApp.checkboxForeignWorkOportunities;
            marketing.checkboxCatchUpWithPeers = existingApp.checkboxCatchUpWithPeers;
            marketing.checkboxStartOwnBusiness = existingApp.checkboxStartOwnBusiness;
            marketing.checkboxLearnSomethingDifferent = existingApp.checkboxLearnSomethingDifferent;
            marketing.checkboxGetRespect = existingApp.checkboxGetRespect;
            marketing.checkboxRoleModel = existingApp.checkboxRoleModel;
            marketing.checkboxStandOut = existingApp.checkboxStandOut;
            marketing.checkboxImproveSocioEconomicStatus = existingApp.checkboxImproveSocioEconomicStatus;
            marketing.checkboxDevelopSkills = existingApp.checkboxDevelopSkills;
            marketing.checkboxKeepUpWithChangingWorld = existingApp.checkboxKeepUpWithChangingWorld;
            marketing.checkboxHaveMoreInfluence = existingApp.checkboxHaveMoreInfluence;
            marketing.checkboxGainInternationalExposure = existingApp.checkboxGainInternationalExposure;
            marketing.checkboxManagementJob = existingApp.checkboxManagementJob;
            marketing.checkboxBecomeAnExpert = existingApp.checkboxBecomeAnExpert;
            marketing.checkboxReinventMyself = existingApp.checkboxReinventMyself;
            marketing.checkboxIncreaseConfidence = existingApp.checkboxIncreaseConfidence;
            marketing.checkboxOvercomeSocialBarriers = existingApp.checkboxOvercomeSocialBarriers;

            marketing.checkboxCommunicateFromHome = existingApp.checkboxCommunicateFromHome;
            marketing.checkboxLocatedInCurrentCountry = existingApp.checkboxLocatedInCurrentCountry;
            marketing.checkboxLocationThatIWouldLike = existingApp.checkboxLocationThatIWouldLike;
            marketing.checkboxExcellentAcademic = existingApp.checkboxExcellentAcademic;
            marketing.checkboxGoodReputationForBusiness = existingApp.checkboxGoodReputationForBusiness;
            marketing.checkboxGraduatesAreMoreSuccessful = existingApp.checkboxGraduatesAreMoreSuccessful;
            marketing.checkboxHasTheSpecificProgram = existingApp.checkboxHasTheSpecificProgram;
            marketing.checkboxHighlyRankedSchool = existingApp.checkboxHighlyRankedSchool;
            marketing.checkboxParentsGraduated = existingApp.checkboxParentsGraduated;
            marketing.checkboxWellKnownInternationally = existingApp.checkboxWellKnownInternationally;
            marketing.checkboxItsAlumniIncludeMany = existingApp.checkboxItsAlumniIncludeMany;
            marketing.checkboxHighQualityInstructors = existingApp.checkboxHighQualityInstructors;
            marketing.checkboxGraduatesFromThisSchool = existingApp.checkboxGraduatesFromThisSchool;
            marketing.checkboxRecommendedByEmployer = existingApp.checkboxRecommendedByEmployer;
            marketing.checkboxRecommenedByFriends = existingApp.checkboxRecommenedByFriends;
            marketing.checkboxEaseOfFittingIn = existingApp.checkboxEaseOfFittingIn;
            marketing.checkboxOnlyTheBestStudents = existingApp.checkboxOnlyTheBestStudents;
            marketing.checkboxLowerTuitionCosts = existingApp.checkboxLowerTuitionCosts;
            marketing.checkboxOfferGenerousScholarships = existingApp.checkboxOfferGenerousScholarships;
            marketing.checkboxADegreeFromThisSchool = existingApp.checkboxADegreeFromThisSchool;
            marketing.checkboxOffersGoodStudentExperience = existingApp.checkboxOffersGoodStudentExperience;
            marketing.checkboxGoodOnCampusCareer = existingApp.checkboxGoodOnCampusCareer;
            marketing.checkboxModernFacilities = existingApp.checkboxModernFacilities;
            marketing.checkboxOffersOnlineClasses = existingApp.checkboxOffersOnlineClasses;
            marketing.checkboxHasAStrongAlumniNetwork = existingApp.checkboxHasAStrongAlumniNetwork;

            return marketing;
        }

        private static usb_studentacademicrecord MapMarketingToEnrollment(ApplicationSubmission application)
        {
            return new usb_studentacademicrecord()
            {
                usb_advertisementTwoOptions = application.usb_advertisementTwoOptions.GetValueOrDefault(),
                usb_NewsArticleTwoOptions = application.usb_NewsArticleTwoOptions.GetValueOrDefault(),
                usb_PromotionalEmailsTwoOptions = application.usb_PromotionalEmailsTwoOptions.GetValueOrDefault(),
                usb_ReferralByEmployerTwoOptions = application.usb_ReferralByEmployerTwoOptions.GetValueOrDefault(),
                usb_StellenboschUniversityTwoOptions =
                    application.usb_StellenboschUniversityTwoOptions.GetValueOrDefault(),
                usb_ReferralByAlumnusTwoOptions = application.usb_ReferralByAlumnusTwoOptions.GetValueOrDefault(),
                usb_USBWebsiteTwoOptions = application.usb_USBWebsiteTwoOptions.GetValueOrDefault(),
                usb_usb_USBEdTwoOptions = application.usb_USBEdTwoOptions.GetValueOrDefault(),
                usb_ReferralByCurrentStudentTwoOptions =
                    application.usb_ReferralByCurrentStudentTwoOptions.GetValueOrDefault(),
                usb_ReferralByFamilyFriendTwoOptions =
                    application.usb_ReferralByFamilyFriendTwoOptions.GetValueOrDefault(),
                usb_SocialMediaTwoOptions = application.usb_SocialMediaTwoOptions.GetValueOrDefault(),
                usb_ConferenceExpoTwoOptions = application.usb_ConferenceExpoTwoOptions.GetValueOrDefault(),
                usb_BrochureTwoOptions = application.usb_BrochureTwoOptions.GetValueOrDefault(),
                usb_OnCampusEventTwoOptions = application.usb_OnCampusEventTwoOptions.GetValueOrDefault(),

                usb_EarnMoreMoneyTwoOptions = application.checkboxEarnMoreMoney,
                usb_GiveChildrenBetterFutureTwoOptions = application.checkboxGiveMyChildABetterFuture,
                usb_IncreaseStatusAmongTwoOptions = application.checkboxIncreaseMyStatus,
                usb_MakeMyParentsProudTwoOptions = application.checkboxMakeMyParentsProud,
                usb_ProvideStabilityCareeTwoOptions = application.checkboxProvideStability,
                usb_GetAPromotionTwoOptions = application.checkboxGetAPromotion,
                usb_QualifyForOpportunityTwoOptions = application.checkboxQualifyForOportunities,
                usb_AdvanceCareerTwoOptions = application.checkboxAdvanceInCareer,
                usb_HaveMoreControlTwoOptions = application.checkboxHaveMoreControl,
                usb_HaveFulfilingCareerTwoOptions = application.checkboxHaveSatisfyingCareer,
                usb_BetterNetworkingTwoOptions = application.checkboxBetterNetworkingOportunities,
                usb_ImproveSpecificSkillsTwoOptions = application.checkboxImproveSkills,
                usb_ImproveLeadershipSkillsTwoOptions = application.checkboxImproveLeadershipSkills,
                usb_QualifyAtOtherCompaniesTwoOptions = application.checkboxQualifyToWorkAtOtherCompanies,
                usb_AccessEmploymentTwoOptions = application.checkboxForeignWorkOportunities,
                usb_HelpKeepUpToPeersTwoOptions = application.checkboxCatchUpWithPeers,
                usb_StartRunOwnBusinessTwoOptions = application.checkboxStartOwnBusiness,
                usb_LearnSomethingdifferentTwoOptions = application.checkboxLearnSomethingDifferent,
                usb_GetMoreRespectTwoOptions = application.checkboxGetRespect,
                usb_BeARoleModelTwoOptions = application.checkboxRoleModel,
                usb_StandOutFromOthersTwoOptions = application.checkboxStandOut,
                usb_ImproveSocioEconomicTwoOptions = application.checkboxImproveSocioEconomicStatus,
                usb_DevelopSkillsToHaveTwoOptions = application.checkboxDevelopSkills,
                usb_KeepUpWithTheFastTwoOptions = application.checkboxKeepUpWithChangingWorld,
                usb_HaveMoreInflueneceTwoOptions = application.checkboxHaveMoreInfluence,
                usb_GainInternationalExposureTwoOptions = application.checkboxGainInternationalExposure,
                usb_MoveIntoManagementTwoOptions = application.checkboxManagementJob,
                usb_BecomeExpertTwoOptions = application.checkboxBecomeAnExpert,
                usb_ReinventMyselfTwoOptions = application.checkboxReinventMyself,
                usb_IncreaseConfidenceTwoOptions = application.checkboxIncreaseConfidence,
                usb_OvercomeSocialTwoOptions = application.checkboxOvercomeSocialBarriers,


                usb_NearbyCanCommuteTwoOptions = application.checkboxCommunicateFromHome,
                usb_LocatedInMyTwoOptions = application.checkboxLocatedInCurrentCountry,
                usb_LocationThatTWouldTwoOptions = application.checkboxLocationThatIWouldLike,
                usb_ExcellentAcademicTwoOptions = application.checkboxExcellentAcademic,
                usb_GoodReputationForTwoOptions = application.checkboxGoodReputationForBusiness,
                usb_GraduateAreMoreTwoOptions = application.checkboxGraduatesAreMoreSuccessful,
                usb_HasSpecificProgramTwoOptions = application.checkboxHasTheSpecificProgram,
                usb_HighRankedSchoolTwoOptions = application.checkboxHighlyRankedSchool,
                usb_ParentGraduatedTwoOptions = application.checkboxParentsGraduated,
                usb_WellKnownTwoOptions = application.checkboxWellKnownInternationally,
                usb_AlumniIncludeManyTwoOptions = application.checkboxItsAlumniIncludeMany,
                usb_HighQualityFacultyTwoOptions = application.checkboxHighQualityInstructors,
                usb_GraduatesFromSchoolTwoOptions = application.checkboxGraduatesFromThisSchool,
                usb_RecommendedByEmployerTwoOptions = application.checkboxRecommendedByEmployer,
                usb_RecommendedByColleguesTwoOptions = application.checkboxRecommenedByFriends,
                usb_EaseOfFittingInTwoOptions = application.checkboxEaseOfFittingIn,
                usb_OnlyBestStudentsTwoOptions = application.checkboxOnlyTheBestStudents,
                usb_LowerTuitionCostTwoOptions = application.checkboxLowerTuitionCosts,
                usb_usb_OffersGenerousScholarTwoOptions = application.checkboxOfferGenerousScholarships,
                usb_ADegreeFromThisSchoolTwoOptions = application.checkboxADegreeFromThisSchool,
                usb_OfferAGoodStudentTwoOptions = application.checkboxOffersGoodStudentExperience,
                usb_GoodOnCampusCareerTwoOptions = application.checkboxGoodOnCampusCareer,
                usb_StateOfTheArtFacilitiesTwoOptions = application.checkboxModernFacilities,
                usb_OffersOnlineClassesTwoOptoins = application.checkboxOffersOnlineClasses,
                usb_HasAStrongAlumniNetTwoOptions = application.checkboxHasAStrongAlumniNetwork


            };
        }

        #endregion
    }
}
