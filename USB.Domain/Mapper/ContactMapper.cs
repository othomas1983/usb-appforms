﻿using System;
using System.Linq;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public class ContactMapper
    {
        public static PersonalContactDetails MapToPersonalContactDetails(usb_studentacademicrecord sar, Contact application, string programmeType)
        {
            var contact = new PersonalContactDetails()
            {
                BusinessPhoneNumber = application.Telephone1,
                DateOfBirth = application.BirthDate.GetValueOrDefault(),
                OtherDisability = application.usb_DisabilityOther,
                GivenName = application.NickName,
                eMail = application.EMailAddress2,
                AlternativeEmailAddress = application.EMailAddress3,
                FaxNumber = application.Fax,
                FirstNames = application.FirstName,
                ForeignNumber = application.usb_PassportNumber,
                HomePhoneNumber = application.Telephone2,
                IdNumber = application.GovernmentId,
                Initials = application.usb_Initials,
                MaidenName = application.usb_MaidenName,
                MobileNumber = application.MobilePhone,
                PassportNumber = application.usb_PassportNumber,
                SelectedGender = application.usb_GenderLookup.Id,
                SelectedGenderName = application.usb_GenderLookup.Name,
                SelectedNationality =
                    application.usb_contact_NationalityLookup.IsNotNull()
                        ? application.usb_contact_NationalityLookup.Id
                        : new Guid(),
                SelectedCountryOfIssue =
                    application.usb_CountryIssued.IsNotNull() ? application.usb_CountryIssued.Id : new Guid(),
                SelectedTitle =
                    application.usb_contact_TitleLookup.IsNotNull()
                        ? application.usb_contact_TitleLookup.Id
                        : new Guid(),
                SelectedLanguage =
                    application.usb_contact_LanguageLookup.IsNotNull()
                        ? application.usb_contact_LanguageLookup.Id
                        : new Guid(),
                SelectedMaritalStatus =
                    application.usb_MaritalStatusLookup.IsNotNull()
                        ? application.usb_MaritalStatusLookup.Id
                        : new Guid(),
                SelectedMaritalStatusName =
                    application.usb_MaritalStatusLookup.IsNotNull() ? application.usb_MaritalStatusLookup.Name : "",
                SelectedEthnicity =
                    application.usb_EthnicityLookup.IsNotNull() ? application.usb_EthnicityLookup.Id : new Guid(),
                SelectedCorrespondanceLanguage =
                    application.usb_CorrespondenceLanguageLookup.IsNotNull()
                        ? application.usb_CorrespondenceLanguageLookup.Id
                        : new Guid(),
                SelectedDisability =
                    application.usb_contact_DisabilityLookup.IsNotNull()
                        ? application.usb_contact_DisabilityLookup.Id
                        : new Guid(),
                Status = application.StatusCode,
                Surname = application.LastName,
                UseWheelchair = application.usb_DoYouUseAWheelchairTwoOptions.GetValueOrDefault(),
                USNumber = application.usb_USNumber,
                SelectedOffering = sar.usb_ProgrammeOfferingLookup.Id,
                SARId = sar.usb_studentacademicrecordId.GetValueOrDefault()
            };

            var applicationFormApiController = new ApplicationFormApiController();

            contact.HasSARWithApplicantStatus =
                applicationFormApiController.ContactHasSARWithApplicantStatus(application.usb_USNumber);

            if (application.usb_PermitTypeLookup.IsNotNull())
            {
                contact.SelectedPermitType = application.usb_PermitTypeLookup.Id;
            }

            if (application.usb_PassportExpiryDate.HasValue)
            {
                contact.ForeignIdExpiryDate = application.usb_PassportExpiryDate.GetValueOrDefault();
            }
            else if (application.usb_PassportExpiryDate.HasValue)
            {
                contact.ForeignIdExpiryDate = application.usb_PassportExpiryDate.GetValueOrDefault();
            }

            if (application.usb_IndentificationTypeLookup.IsNotNull())
            {
                contact.SelectedForeignIdentificationType = application.usb_IndentificationTypeLookup.Id;
            }

            contact.SelectedBlendedOption = sar.usb_BlendedAttendanceOptionset.GetValueOrDefault();
            contact.SelectedMbaStream = sar.usb_MBAStream.GetValueOrDefault();

            return contact;
        }

        public static AddressDetails MapToAddressDetails(Contact application)
        {
            var contact = new StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto.AddressDetails()
            {
                BillingTo = application.usb_AddresseeBillTo,
                BillingCity = application.usb_CityBillTo,
                BillingPostalCode = application.usb_PostalCodeBillTo,
                BillingStreet1 = application.usb_usb_Address1BillTo,
                BillingStreet2 = application.usb_usb_Adress2BillTo,
                BillingSuburb = application.usb_SuburbBillTo,

                ResidentialCity = application.Address1_City,
                ResidentialStreet1 = application.Address1_Line1,
                ResidentialStreet2 = application.Address1_Line2,
                ResidentialStreet3 = application.Address1_Line3,
                ResidentialPostalCode = application.Address1_PostalCode,
                ResidentialSuburb = application.usb_Address1_Suburb,
                ResidentialStateProvince = application.Address1_StateOrProvince,

                BusinessCity = application.usb_CityWork,
                BusinessStreet1 = application.usb_Address1Work,
                BusinessStreet2 = application.usb_Address2Work,
                BusinessStreet3 = application.usb_Address3Work,
                BusinessSuburb = application.usb_SuburbWork,
                BusinessPostalCode = application.usb_PostalCodeWork,

                PostalCity = application.Address2_City,
                PostalStreet1 = application.Address2_Line1,
                PostalStreet2 = application.Address2_Line2,
                PostalStreet3 = application.Address2_Line3,
                PostalPostalCode = application.Address2_PostalCode,
                PostalSuburb = application.usb_Address2_Suburb,
                PostalStateProvince = application.Address2_StateOrProvince
            };

            if (application.usb_Address2_CountryLookup.IsNotNull())
            {
                contact.PostalCountry = application.usb_Address2_CountryLookup.Id;
            }

            if (application.usb_CountryWorkLookup.IsNotNull())
            {
                contact.BusinessCountry = application.usb_CountryWorkLookup.Id;
            }

            if (application.usb_CountryBillTo_Lookup.IsNotNull())
            {
                contact.BillingCountry = application.usb_CountryBillTo_Lookup.Id;
            }

            if (application.usb_Address1Country_Lookup.IsNotNull())
            {
                contact.ResidentialCountry = application.usb_Address1Country_Lookup.Id;
            }

            if (application.usb_AfriGISLookup.IsNotNull())
            {
                contact.ResidentialGisId = application.usb_AfriGISLookup.Id;
            }

            if (application.usb_AfriGISPostalLookup.IsNotNull())
            {
                contact.PostalGisId = application.usb_AfriGISPostalLookup.Id;
            }

            if (application.usb_AfriGISWorkLookup.IsNotNull())
            {
                contact.BusinessGisId = application.usb_AfriGISWorkLookup.Id;
            }

            if (application.usb_AfriGISBillToLookup.IsNotNull())
            {
                contact.BillingGisId = application.usb_AfriGISBillToLookup.Id;
            }

            return contact;
        }

        #region Private Methods

        private static int ResolveBlendedAttendance(Guid studentAcademicRecord)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar =
                    crmServiceContext.usb_studentacademicrecordSet.FirstOrDefault(
                        x => x.usb_studentacademicrecordId == studentAcademicRecord);

                if (sar.IsNotNull())
                {
                    return sar.usb_BlendedAttendanceOptionset.GetValueOrDefault();
                }
            }

            return 0;
        }

        #endregion

    }
}
