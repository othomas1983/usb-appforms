﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using USB.Domain.Controller.Dto;
using USB.Domain.Models;
using USB.Domain.Models.Enums;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public static class CrmActions
    {
        public static object ApplicationFormApiControllerSubmit { get; private set; }

        public static ApplicationSubmission CreateOrUpdateContact(string usNumber, ApplicationSubmission application, Guid? contactId)
        {
            var contact = new Contact();
            contact.Id = contactId.Value;
            contact.usb_USNumber = usNumber.Trim();

            #region application.ApplicationSource == ApplicationSource.ContactDetails

            if (application.ApplicationSource == USB.Domain.Models.ApplicationSource.ContactDetails)
            {
                contact.BirthDate = application.DateOfBirth.ToUtcWithoutConversion();
                contact.LastName = application.Surname.Trim();
                contact.FirstName = application.FirstNames.Trim();
                contact.usb_Initials = application.Initials.Trim();
                contact.NickName = (String.IsNullOrEmpty(application.GivenName) == false) ? application.GivenName.Trim() : "";
                contact.usb_MaidenName = application.MaidenName ?? string.Empty;
                contact.usb_PassportNumber = application.PassportNumber;
                contact.usb_PassportExpiryDate = application.PassportExpiryDate.ToUtcWithoutConversion();

                if (application.Gender.HasValue)
                {
                    contact.usb_GenderLookup = new CrmEntityReference(usb_gender.EntityLogicalName, application.Gender.GetValueOrDefault());
                }
                if (application.Title.HasValue)
                {
                    contact.usb_TitleLookup = new CrmEntityReference(usb_title.EntityLogicalName, application.Title.GetValueOrDefault());
                }
                if (application.Ethnicity.HasValue)
                {
                    contact.usb_EthnicityLookup = new CrmEntityReference(usb_ethnicity.EntityLogicalName, application.Ethnicity.GetValueOrDefault());
                }
                if (application.MaritalStatus.HasValue)
                {
                    contact.usb_MaritalStatusLookup = new CrmEntityReference(usb_maritalstatus.EntityLogicalName, application.MaritalStatus.GetValueOrDefault());
                }
                if (application.Nationality.HasValue)
                {
                    contact.usb_NationalityLookup = new CrmEntityReference(usb_nationality.EntityLogicalName, application.Nationality.GetValueOrDefault());
                }
                if (application.IsSouthAfrican.GetValueOrDefault())
                {
                    contact.GovernmentId = application.IdNumber;
                    contact.usb_IndentificationTypeLookup = new CrmEntityReference(usb_idtype.EntityLogicalName, application.GovernmentIdType.GetValueOrDefault());
                }
                else
                {
                    contact.usb_IndentificationTypeLookup = new CrmEntityReference(usb_idtype.EntityLogicalName, application.ForeignIdType.GetValueOrDefault());
                    if (application.CountryOfIssue != Guid.Empty)
                    {
                        contact.usb_CountryIssued = new CrmEntityReference(usb_country.EntityLogicalName, application.CountryOfIssue.GetValueOrDefault());
                    }
                }

                if (application.PermitType.HasValue)
                {
                    contact.usb_PermitTypeLookup = new CrmEntityReference(usb_idtype.EntityLogicalName, application.PermitType.GetValueOrDefault());
                }

                if (application.Language.HasValue)
                {
                    Guid languageId = CrmQueries.GetHomeLanguage(application.Language.Value);
                    contact.usb_LanguageLookup = new CrmEntityReference(usb_language.EntityLogicalName, languageId);
                }

                if (application.CorrespondanceLanguage.HasValue)
                {
                    Guid correspondenceLanguageId = CrmQueries.GetCrmCorrespondenceLanguage(application.CorrespondanceLanguage.GetValueOrDefault());
                    contact.usb_CorrespondenceLanguageLookup = new CrmEntityReference(usb_correspondencelanguage.EntityLogicalName, correspondenceLanguageId);
                }

                if (application.Disability.HasValue)
                {
                    contact.usb_DisabilityLookup = new CrmEntityReference(usb_disability.EntityLogicalName, application.Disability.GetValueOrDefault());
                }
                contact.usb_DisabilityOther = application.OtherDisability == null ? string.Empty : application.OtherDisability;

                contact.usb_DoYouUseAWheelchairTwoOptions = application.UsesWheelchair;
                contact.EMailAddress2 = application.Email;
                contact.EMailAddress3 = application.AlternativeEmailAddress;

                // contact.Address1_Telephone1 = application.ContactNumber;
                contact.MobilePhone = application.MobileNumber;
                contact.Telephone1 = application.WorkNumber;
                contact.Telephone2 = application.ContactNumber;
                contact.Fax = application.FaxNumber;

                if (application.ForeignIdType.HasValue)
                {
                    contact.usb_IndentificationTypeLookup = new CrmEntityReference(usb_idtype.EntityLogicalName, application.ForeignIdType.GetValueOrDefault());
                }
            }

            #endregion

            #region application.ApplicationSource == USB.Domain.Models.ApplicationSource.AddressDetails

            if (application.ApplicationSource == USB.Domain.Models.ApplicationSource.AddressDetails)
            {
                #region ResidentialCity

                if (application.ResidentialCity.IsNotNull())
                {
                    // Residential Address
                    contact.Address1_Line1 = application.ResidentialStreet1;
                    contact.Address1_Line2 = application.ResidentialStreet2;
                    contact.Address1_Line3 = application.ResidentialStreet3;
                    contact.usb_Address1_Suburb = application.ResidentialSuburb;
                    contact.Address1_City = application.ResidentialCity;
                    contact.Address1_PostalCode = application.ResidentialPostalCode;

                    contact.Address1_StateOrProvince = application.ResidentialStateOrProvince;

                    if (application.ResidentialCountry.IsNotNull())
                    {
                        contact.usb_Address1Country_Lookup = new CrmEntityReference(usb_country.EntityLogicalName, application.ResidentialCountry.GetValueOrDefault());
                    }
                }

                #endregion

                #region PostalCountry

                if (application.PostalCountry.IsNotNull())
                {
                    // Postal Address
                    contact.Address2_Line1 = application.PostalStreet1;
                    contact.Address2_Line2 = application.PostalStreet2;
                    contact.Address2_Line3 = application.PostalStreet3;
                    contact.usb_Address2_Suburb = application.PostalSuburb;
                    contact.Address2_City = application.PostalCity;
                    contact.Address2_PostalCode = application.PostalPostalCode;

                    contact.Address2_StateOrProvince = application.PostalStateOrProvince;
                    contact.Address2_PrimaryContactName = application.PostalAddressedTo;

                    if (application.PostalCountry.HasValue)
                    {
                        contact.usb_Address2_CountryLookup = new CrmEntityReference(usb_country.EntityLogicalName, application.PostalCountry.GetValueOrDefault());
                    }
                }

                #endregion


                if (application.BusinessCountry.IsNotNull())
                {
                    // Business Address
                    contact.usb_Address1Work = application.BusinessStreet1;
                    contact.usb_Address2Work = application.BusinessStreet2;
                    contact.usb_Address3Work = application.BusinessStreet3;
                    contact.usb_SuburbWork = application.BusinessSuburb;
                    contact.usb_CityWork = application.BusinessCity;
                    contact.usb_PostalCodeWork = application.BusinessPostalCode;

                    //need country lookup
                    if (application.BusinessCountry.HasValue)
                    {
                        contact.usb_CountryWorkLookup = new CrmEntityReference(usb_country.EntityLogicalName, application.BusinessCountry.GetValueOrDefault());
                    }
                }

                if (application.BillingStreet1.IsNotNull())
                {
                    //Billing
                    contact.usb_usb_Address1BillTo = application.BillingStreet1;
                    contact.usb_usb_Adress2BillTo = application.BillingStreet2;
                    contact.usb_Address3BillTo = application.BillingStreet3;
                    contact.usb_AddresseeBillTo = application.BillingAddressedTo;
                    contact.usb_CityBillTo = application.BillingCity;
                    contact.usb_PostalCodeBillTo = application.BillingPostalCode;
                    contact.usb_SuburbBillTo = application.BillingSuburb;
                    

                    //need country lookup
                    if (application.BillingCountry.HasValue)
                    {
                        contact.usb_CountryBillTo_Lookup = new CrmEntityReference(usb_country.EntityLogicalName, application.BillingCountry.GetValueOrDefault());
                    }
                }

                if ( application.ResidentialAfrigis.IsNotNull() && (application.ResidentialAfrigis != Guid.Empty) )
                {
                    contact.usb_AfriGISLookup = new CrmEntityReference(usb_afrigis.EntityLogicalName, application.ResidentialAfrigis.GetValueOrDefault());
                }

                if ( application.PostalAfrigis.IsNotNull() && (application.PostalAfrigis != Guid.Empty) )
                {
                    contact.usb_AfriGISPostalLookup = new CrmEntityReference(usb_afrigis.EntityLogicalName, application.PostalAfrigis.GetValueOrDefault());
                }

                if ( application.BusinessAfrigis.IsNotNull() && (application.BusinessAfrigis != Guid.Empty) )
                {
                    contact.usb_AfriGISWorkLookup = new CrmEntityReference(usb_afrigis.EntityLogicalName, application.BusinessAfrigis.GetValueOrDefault());
                }

                if ( application.BillingAfrigis.IsNotNull() && (application.BillingAfrigis != Guid.Empty) )
                {
                    contact.usb_AfriGISBillToLookup = new CrmEntityReference(usb_afrigis.EntityLogicalName, application.BillingAfrigis.GetValueOrDefault());
                }
            }

            #endregion

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmServiceContext.Update(contact);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return application;
        }

        public static Guid CreateOrUpdateEnrollment(string usNumber, ApplicationSubmission application, bool isShortForm)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid? enrolmentId = new Guid();

                enrolmentId = application.EnrolmentGuid ?? GetEnrolmentGuid(usNumber, application.Offering);

                var enrolment = ApplicationSubmissionMapper.MapToEnrollment(application);
                enrolment.usb_USNumber = usNumber.Trim();
                enrolment.Id = enrolmentId.GetValueOrDefault();
                enrolmentId = enrolment.usb_studentacademicrecordId;
                
                if (!HasApplicationFormURL(enrolmentId.GetValueOrDefault()))
                {
                    enrolment.usb_ApplicationFormURL = application.ApplicationFormsBaseUrl + "?id=" + enrolmentId.Value;
                }

                switch (application.ApplicationSource)
                {
                    case ApplicationSource.ContactDetails:
                        enrolment.usb_PersonalInformationTwoOptions = true;
                        enrolment.usb_BlendedAttendanceOptionset = application.SelectedBlendedOption;
                        enrolment.usb_MBAStream = application.SelectedMbaStream;
                        break;

                    case ApplicationSource.AddressDetails:
                        enrolment.usb_AddressDetailsTwoOptions = true;
                        break;

                    case ApplicationSource.TellUsMore:

                        enrolment.usb_advertisementTwoOptions = application.checkboxAdvertisement;
                        enrolment.usb_NewsArticleTwoOptions = application.checkboxNewsArticle;
                        enrolment.usb_PromotionalEmailsTwoOptions = application.checkboxUsbPromotionalEmails;
                        enrolment.usb_ReferralByEmployerTwoOptions = application.checkboxReferralByMyEmployer;
                        enrolment.usb_ReferralByAlumnusTwoOptions = application.checkboxReferralByAnAlumnus;
                        enrolment.usb_ReferralByCurrentStudentTwoOptions = application.checkboxReferralByACurrentStudent;
                        enrolment.usb_ReferralByFamilyFriendTwoOptions = application.checkboxReferralByAFamilyMember;
                        enrolment.usb_SocialMediaTwoOptions = application.checkboxSocialMedia;
                        enrolment.usb_ConferenceExpoTwoOptions = application.checkboxConferenceOrExpo;
                        enrolment.usb_StellenboschUniversityTwoOptions = application.checkboxStellenboschUniversity;
                        enrolment.usb_USBWebsiteTwoOptions = application.checkboxUsbWebsite;
                        enrolment.usb_usb_USBEdTwoOptions = application.checkboxUsbEd;
                        enrolment.usb_BrochureTwoOptions = application.checkboxBrochure;
                        enrolment.usb_OnCampusEventTwoOptions = application.checkboxOnCampusEvent;

                        enrolment.usb_EarnMoreMoneyTwoOptions = application.checkboxEarnMoreMoney;
                        enrolment.usb_GiveChildrenBetterFutureTwoOptions = application.checkboxGiveMyChildABetterFuture;
                        enrolment.usb_IncreaseStatusAmongTwoOptions = application.checkboxIncreaseMyStatus;
                        enrolment.usb_MakeMyParentsProudTwoOptions = application.checkboxMakeMyParentsProud;
                        enrolment.usb_ProvideStabilityCareeTwoOptions = application.checkboxProvideStability;
                        enrolment.usb_GetAPromotionTwoOptions = application.checkboxGetAPromotion;
                        enrolment.usb_QualifyForOpportunityTwoOptions = application.checkboxQualifyForOportunities;
                        enrolment.usb_AdvanceCareerTwoOptions = application.checkboxAdvanceInCareer;
                        enrolment.usb_HaveMoreControlTwoOptions = application.checkboxHaveMoreControl;
                        enrolment.usb_HaveFulfilingCareerTwoOptions = application.checkboxHaveSatisfyingCareer;
                        enrolment.usb_BetterNetworkingTwoOptions = application.checkboxBetterNetworkingOportunities;
                        enrolment.usb_ImproveSpecificSkillsTwoOptions = application.checkboxImproveSkills;
                        enrolment.usb_ImproveLeadershipSkillsTwoOptions = application.checkboxImproveLeadershipSkills;
                        enrolment.usb_QualifyAtOtherCompaniesTwoOptions =
                            application.checkboxQualifyToWorkAtOtherCompanies;
                        enrolment.usb_AccessEmploymentTwoOptions = application.checkboxForeignWorkOportunities;
                        enrolment.usb_HelpKeepUpToPeersTwoOptions = application.checkboxCatchUpWithPeers;
                        enrolment.usb_StartRunOwnBusinessTwoOptions = application.checkboxStartOwnBusiness;
                        enrolment.usb_LearnSomethingdifferentTwoOptions = application.checkboxLearnSomethingDifferent;
                        enrolment.usb_GetMoreRespectTwoOptions = application.checkboxGetRespect;
                        enrolment.usb_BeARoleModelTwoOptions = application.checkboxRoleModel;
                        enrolment.usb_StandOutFromOthersTwoOptions = application.checkboxStandOut;
                        enrolment.usb_ImproveSocioEconomicTwoOptions = application.checkboxImproveSocioEconomicStatus;
                        enrolment.usb_DevelopSkillsToHaveTwoOptions = application.checkboxDevelopSkills;
                        enrolment.usb_KeepUpWithTheFastTwoOptions = application.checkboxKeepUpWithChangingWorld;
                        enrolment.usb_HaveMoreInflueneceTwoOptions = application.checkboxHaveMoreInfluence;
                        enrolment.usb_GainInternationalExposureTwoOptions =
                            application.checkboxGainInternationalExposure;
                        enrolment.usb_MoveIntoManagementTwoOptions = application.checkboxManagementJob;
                        enrolment.usb_BecomeExpertTwoOptions = application.checkboxBecomeAnExpert;
                        enrolment.usb_ReinventMyselfTwoOptions = application.checkboxReinventMyself;
                        enrolment.usb_IncreaseConfidenceTwoOptions = application.checkboxIncreaseConfidence;
                        enrolment.usb_OvercomeSocialTwoOptions = application.checkboxOvercomeSocialBarriers;

                        enrolment.usb_CloseToHomeTwoOptions = application.checkboxCommunicateFromHome;
                        enrolment.usb_LocatedInMyTwoOptions = application.checkboxLocatedInCurrentCountry;
                        enrolment.usb_LocationThatTWouldTwoOptions = application.checkboxLocationThatIWouldLike;
                        enrolment.usb_ExcellentAcademicTwoOptions = application.checkboxExcellentAcademic;
                        enrolment.usb_GoodReputationForTwoOptions = application.checkboxGoodReputationForBusiness;
                        enrolment.usb_GraduateAreMoreTwoOptions = application.checkboxGraduatesAreMoreSuccessful;
                        enrolment.usb_HasSpecificProgramTwoOptions = application.checkboxHasTheSpecificProgram;
                        enrolment.usb_HighRankedSchoolTwoOptions = application.checkboxHighlyRankedSchool;
                        enrolment.usb_ParentGraduatedTwoOptions = application.checkboxParentsGraduated;
                        enrolment.usb_WellKnownTwoOptions = application.checkboxWellKnownInternationally;
                        enrolment.usb_AlumniIncludeManyTwoOptions = application.checkboxItsAlumniIncludeMany;
                        enrolment.usb_HighQualityFacultyTwoOptions = application.checkboxHighQualityInstructors;
                        enrolment.usb_GraduatesFromSchoolTwoOptions = application.checkboxGraduatesFromThisSchool;
                        enrolment.usb_RecommendedByEmployerTwoOptions = application.checkboxRecommendedByEmployer;
                        enrolment.usb_RecommendedByColleguesTwoOptions = application.checkboxRecommenedByFriends;
                        enrolment.usb_EaseOfFittingInTwoOptions = application.checkboxEaseOfFittingIn;
                        enrolment.usb_OnlyBestStudentsTwoOptions = application.checkboxOnlyTheBestStudents;
                        enrolment.usb_LowerTuitionCostTwoOptions = application.checkboxLowerTuitionCosts;
                        enrolment.usb_usb_OffersGenerousScholarTwoOptions = application.checkboxOfferGenerousScholarships;
                        enrolment.usb_ADegreeFromThisSchoolTwoOptions = application.checkboxADegreeFromThisSchool;
                        enrolment.usb_OfferAGoodStudentTwoOptions = application.checkboxOffersGoodStudentExperience;
                        enrolment.usb_GoodOnCampusCareerTwoOptions = application.checkboxGoodOnCampusCareer;
                        enrolment.usb_StateOfTheArtFacilitiesTwoOptions = application.checkboxModernFacilities;
                        enrolment.usb_OffersOnlineClassesTwoOptoins = application.checkboxOffersOnlineClasses;
                        enrolment.usb_HasAStrongAlumniNetTwoOptions = application.checkboxHasAStrongAlumniNetwork;
                        
                        enrolment.usb_TellUsMoreTwoOptions = true;
                        break;

                    case ApplicationSource.WorkStudies:

                        try
                        {
                            ModifyQualificationHistory(enrolment.Id, application, crmServiceContext);
                            ModifyEmploymentHistory(enrolment.Id, application, crmServiceContext);
                        }
                        finally
                        {
                            crmServiceContext.Dispose();
                        }

                        enrolment.usb_WorkStudiesTwoOptions = true;

                        if(!isShortForm)
                        enrolment.usb_RPLCandidateOptionset = application.IsRplCandidate;
                        break;

                      

                    case ApplicationSource.Documentation:
                        enrolment.usb_SHLTestRequiredTwoOptions = application.SHLTestRequiredTwoOptions;
                        if (application.UploadedDocumentTypes.Any())
                        {
                            Guid g = new Guid();
                            Guid.TryParse(application.EnrolmentGuid.ToString(), out g);

                            ApplicationFormApiController app = new ApplicationFormApiController();

                            //Get required docs for Usnumber
                            List<Document> DependentDocumentList = app.GetRequiredDocumentsInformationDirect(g);
                            List<Document> ReqDocumentList = app.GetRequiredDocumentsInformation(GetProgrammeID(g), g, 1);
                            ReqDocumentList.AddRange(DependentDocumentList);

                            //Uploaded In Sharepoint
                            var documentUploadedList = application.UploadedDocumentTypes;

                            //List<Document> ProgrammeRequiredDocumentList = CrmQueries.GetRequiredDocuments(GetProgrammeID(g));
                            //// Dependent Documents
                            //ProgrammeRequiredDocumentList.AddRange(CrmQueries.GetRequiredDocuments(GetProgrammeID(g), 2));


                            //// Set N/A documentation comparing StudentDoc List and Programme doc List

                            //foreach (Document item in ProgrammeRequiredDocumentList)
                            //{
                            //    var doc = item.DocumentType.ToString();
                            //    if (ReqDocumentList.Find(i => i.Equals(item)) == null)
                            //    {


                            //        if (doc == "AcceptanceForm")
                            //        {
                            //            // enrolment.usb_AcceptanceFormTwoOptions = true;
                            //        }
                            //        else if (doc == "CopyOfPermit")
                            //        {
                            //            enrolment.usb_CopyofPermit = 0;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (doc == "AcceptanceForm")
                            //        {
                            //            // enrolment.usb_AcceptanceFormTwoOptions = true;
                            //        }
                            //        else if (doc == "CopyOfPermit")
                            //        {
                            //            enrolment.usb_CopyofPermit = 0;
                            //        }
                            //    }
                            //}

                           

                            enrolment.usb_SHLTestRequiredTwoOptions = application.SHLTestRequiredTwoOptions;

                            foreach (DocumentType item in documentUploadedList)
                            {
                                if (ReqDocumentList.Find(i => i.Equals(item)) == null)
                                {
                                    switch (item)
                                    {
                                        case DocumentType.AcceptanceForm:
                                            enrolment.usb_AcceptanceFormTwoOptions = true;
                                            break;
                                        case DocumentType.ApplicationFee:
                                            enrolment.usb_ApplicationFeeTwoOptions = true;
                                            break;
                                        case DocumentType.CV:
                                            enrolment.usb_CVTwoOptions = true;
                                            break;
                                        case DocumentType.CertifiedCopiesOfAcademicRecords:
                                            enrolment.usb_CertifiedCopiesAcademicTwoOptions = true;
                                            break;
                                        case DocumentType.CertifiedCopiesOfDegree:
                                            enrolment.usb_CertifiedCopiesDegreeTwoOptions = true;
                                            break;
                                        case DocumentType.CopyOfID:
                                            enrolment.usb_CopyOfIDPassportTwoOptions = true;
                                            break;
                                        case DocumentType.DescriptionOfOrganisationalUnit:
                                            enrolment.usb_DescriptionOrganisationalTwoOptions = true;
                                            break;
                                        case DocumentType.GmatSelectionTest:
                                            enrolment.usb_GMATTestTwoOptions = true;
                                            break;
                                        case DocumentType.InitialResearchProposal:
                                            enrolment.usb_InitialResearchProposalTwoOptions = true;
                                            break;
                                        case DocumentType.MarriageCertificate:
                                            enrolment.usb_MarriageCertificateTwoOptions = true;
                                            break;
                                        case DocumentType.MatricCertificate:
                                            enrolment.usb_MatricCertificateTwoOptions = true;
                                            break;
                                        case DocumentType.MotivationForProgrammeAcceptance:
                                            enrolment.usb_MotivationForProgrammeTwoOptions = true;
                                            break;
                                        case DocumentType.MotivationalEssays:
                                            enrolment.usb_MotivationalEssaysTwoOptions = true;
                                            break;
                                        case DocumentType.RplCandidate:
                                            enrolment.usb_RPLCandidateTwoOptions = true;
                                            break;
                                        case DocumentType.RplRef1:
                                            enrolment.usb_RPLRef1TwoOptions = true;
                                            break;
                                        case DocumentType.RplRef2:
                                            enrolment.usb_RPLRef2TwoOptions = true;
                                            break;
                                        case DocumentType.ShlResults:
                                            enrolment.usb_SHLTestRequiredTwoOptions = true;
                                            break;
                                        case DocumentType.ShlSelectionTest:
                                            enrolment.usb_SHLGMATSelectionTestTwoOptions = true;

                                            break;
                                        case DocumentType.SignedDeclarationForm:
                                            enrolment.usb_SignedDeclarationFormTwoOptions = true;
                                            break;
                                        case DocumentType.SummaryOfJobDescription:
                                            enrolment.usb_SummaryJobDescriptionTwoOptions = true;
                                            break;
                                        case DocumentType.ConceptNote:
                                            enrolment.usb_ConceptNoteTwoOptions = true;
                                            break;
                                        case DocumentType.ListPublication:
                                            enrolment.usb_ListofPublicationsTwoOptions = true;
                                            break;
                                        case DocumentType.CopyOfPermit:
                                            enrolment.usb_CopyofPermit = 1;
                                            break;
                                    }
                                }
                                else
                                {
                                    //true
                                    switch (item)
                                    {
                                        case DocumentType.AcceptanceForm:
                                            enrolment.usb_AcceptanceFormTwoOptions = false;
                                            break;
                                        case DocumentType.ApplicationFee:
                                            enrolment.usb_ApplicationFeeTwoOptions = false;
                                            break;
                                        case DocumentType.CV:
                                            enrolment.usb_CVTwoOptions = false;
                                            break;
                                        case DocumentType.CertifiedCopiesOfAcademicRecords:
                                            enrolment.usb_CertifiedCopiesAcademicTwoOptions = false;
                                            break;
                                        case DocumentType.CertifiedCopiesOfDegree:
                                            enrolment.usb_CertifiedCopiesDegreeTwoOptions = false;
                                            break;
                                        case DocumentType.CopyOfID:
                                            enrolment.usb_CopyOfIDPassportTwoOptions = false;
                                            break;
                                        case DocumentType.DescriptionOfOrganisationalUnit:
                                            enrolment.usb_DescriptionOrganisationalTwoOptions = false;
                                            break;
                                        case DocumentType.GmatSelectionTest:
                                            enrolment.usb_GMATTestTwoOptions = false;
                                            break;
                                        case DocumentType.InitialResearchProposal:
                                            enrolment.usb_InitialResearchProposalTwoOptions = false;
                                            break;
                                        case DocumentType.MarriageCertificate:
                                            enrolment.usb_MarriageCertificateTwoOptions = false;
                                            break;
                                        case DocumentType.MatricCertificate:
                                            enrolment.usb_MatricCertificateTwoOptions = false;
                                            break;
                                        case DocumentType.MotivationForProgrammeAcceptance:
                                            enrolment.usb_MotivationForProgrammeTwoOptions = false;
                                            break;
                                        case DocumentType.MotivationalEssays:
                                            enrolment.usb_MotivationalEssaysTwoOptions = false;
                                            break;
                                        case DocumentType.RplCandidate:
                                            enrolment.usb_RPLCandidateTwoOptions = false;
                                            break;
                                        case DocumentType.RplRef1:
                                            enrolment.usb_RPLRef1TwoOptions = false;
                                            break;
                                        case DocumentType.RplRef2:
                                            enrolment.usb_RPLRef2TwoOptions = false;
                                            break;
                                        case DocumentType.ShlResults:
                                            enrolment.usb_SHLTestRequiredTwoOptions = false;
                                            break;
                                        case DocumentType.ShlSelectionTest:
                                            enrolment.usb_SHLGMATSelectionTestTwoOptions = false;
                                            break;
                                        case DocumentType.SignedDeclarationForm:
                                            enrolment.usb_SignedDeclarationFormTwoOptions = false;
                                            break;
                                        case DocumentType.SummaryOfJobDescription:
                                            enrolment.usb_SummaryJobDescriptionTwoOptions = false;
                                            break;
                                        case DocumentType.ConceptNote:
                                            enrolment.usb_ConceptNoteTwoOptions = false;
                                            break;
                                        case DocumentType.ListPublication:
                                            enrolment.usb_ListofPublicationsTwoOptions = false;
                                            break;
                                        case DocumentType.CopyOfPermit:
                                            enrolment.usb_CopyofPermit = 2;
                                            break;
                                    }
                                }

                            }

                        }
                        //Moving code in order to see where Optionset is being set for this
                        enrolment.usb_DocumentVisibilityOptionset = (int) application.ProgrammeType;

                        enrolment.usb_DocumentationTwoOptions = isShortForm ? true : application.DocumentsCompleteTwoOptions;
                        enrolment.usb_DocumentsCompletedTwoOptions = application.DocumentsCompleteTwoOptions;
                        enrolment.usb_DocumentsOutstandingTwoOptions = application.DocumentsOutstandingTwoOptions;
                        break;

                    case ApplicationSource.Payment:
                        enrolment.usb_PaymentTwoOptions = true;
                        break;

                    case ApplicationSource.Status:
                        enrolment.usb_StatusTwoOptions = true;
                        break;

                    default: // This label should not be reached
                        break;
                }

                if(application.ProgrammeType == ProgrammeType.ShortenedAppFormMBA || application.ProgrammeType == ProgrammeType.ShortenedAppFormMPhilFutureStudies)
                {
                    enrolment.usb_DocumentVisibilityOptionset = (int)application.ProgrammeType;
                    enrolment.usb_ApplicationFeeTwoOptions = true;
                }
               

                enrolment.usb_USNumber = usNumber.Trim();
                //enrolment.Id = enrolmentId.GetValueOrDefault();



                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    enrolment.usb_ProgrammeOfferingLookup =
                        new CrmEntityReference(usb_programmeoffering.EntityLogicalName, application.Offering);

                }

                var contactId = GetContactId(usNumber);

                if (contactId.IsNotNull())
                {
                    enrolment.usb_ContactLookup = new CrmEntityReference(Contact.EntityLogicalName, contactId);
                }

                try
                {
                    crmServiceContext.Update(enrolment);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    Guid offeringOwner = CrmQueries.GetCourseOfferingOwner(application.Offering);
                    CrmActions.SetOwnershipOfEnrollment(enrolment.Id, offeringOwner);
                }

               

                return enrolmentId.Value;
            }
        }

        private static bool IsSHLTestRequired(Guid? enrolmentId)
        {
            throw new NotImplementedException();
        }

        public static Guid CreateOrUpdateShortEnrollment(string usNumber, ApplicationSubmission application)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid? enrolmentId = new Guid();

                enrolmentId = application.EnrolmentGuid ?? GetEnrolmentGuid(usNumber, application.Offering);

                if (enrolmentId.IsNull() || enrolmentId == Guid.Empty)
                {
                    enrolmentId = crmServiceContext.Create(new usb_studentacademicrecord());
                }

                var enrolment = ApplicationSubmissionMapper.MapToEnrollment(application);

                enrolment.usb_USNumber = usNumber.Trim();
                enrolment.Id = enrolmentId.GetValueOrDefault();
                enrolmentId = enrolment.usb_studentacademicrecordId;
                
                if (!HasApplicationFormURL(enrolmentId.GetValueOrDefault()))
                {
                    enrolment.usb_ApplicationFormURL = application.ApplicationFormsBaseUrl + "?id=" + enrolmentId.Value;
                }
                
                switch (application.ApplicationSource)
                {
                    case ApplicationSource.ContactDetails:
                        enrolment.usb_PersonalInformationTwoOptions = true;
                        enrolment.usb_AddressDetailsTwoOptions = true;
                        enrolment.usb_MBAStream = application.SelectedMbaStream;
                        break;
                    case ApplicationSource.Documentation:
                        enrolment.usb_DocumentationTwoOptions = true;
                        enrolment.usb_PaymentTwoOptions = true;
                        enrolment.usb_SHLTestRequiredTwoOptions = application.SHLTestRequiredTwoOptions;
                        enrolment.usb_DocumentsCompletedTwoOptions = application.DocumentsCompleteTwoOptions;
                        enrolment.usb_DocumentsOutstandingTwoOptions = application.DocumentsOutstandingTwoOptions;
                        break;
                    case ApplicationSource.Status:
                        enrolment.usb_StatusTwoOptions = true;
                        break;
                    case ApplicationSource.AddressDetails:
                        break;
                    case ApplicationSource.WorkStudies:
                        try
                        {
                            ModifyQualificationHistory(enrolment.Id, application, crmServiceContext);
                            ModifyEmploymentHistory(enrolment.Id, application, crmServiceContext);
                        }
                        finally
                        {
                            crmServiceContext.Dispose();
                        }

                        enrolment.usb_WorkStudiesTwoOptions = true;
                        enrolment.usb_TellUsMoreTwoOptions = true;

                        break;
                    case ApplicationSource.TellUsMore:
                        break;
                    case ApplicationSource.Payment:
                        break;
                    default: // This label should not be reached
                        break;
                }

                enrolment.usb_USNumber = usNumber.Trim();

                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    enrolment.usb_ProgrammeOfferingLookup =
                        new CrmEntityReference(usb_programmeoffering.EntityLogicalName, application.Offering);

                }

                var contactId = GetContactId(usNumber);

                if (contactId.IsNotNull())
                {
                    enrolment.usb_ContactLookup = new CrmEntityReference(Contact.EntityLogicalName, contactId);
                }

                try
                {
                    crmServiceContext.Update(enrolment);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                
                if (application.Offering.IsNotNull() && (application.Offering != Guid.Empty))
                {
                    Guid offeringOwner = CrmQueries.GetCourseOfferingOwner(application.Offering);
                    CrmActions.SetOwnershipOfEnrollment(enrolment.Id, offeringOwner);
                }

                return enrolmentId.Value;
            }
        }

        public static usb_studentacademicrecord LoadStudentAcademicRecordByEnrolrmentId(Guid enrolmentID)
        {
            Guid enrolmentId;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_studentacademicrecord studentAcademicRecord = null;
                try
                {
                    studentAcademicRecord = (from c in crmServiceContext.usb_studentacademicrecordSet
                                             where c.Id == enrolmentID
                                             select c).FirstOrDefault();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (studentAcademicRecord.IsNull())
                {
                    enrolmentId = crmServiceContext.Create(new usb_studentacademicrecord());
                    studentAcademicRecord = new usb_studentacademicrecord();
                    studentAcademicRecord.usb_studentacademicrecordId = enrolmentId;
                }


                return studentAcademicRecord;
            }
        }

        public static usb_studentacademicrecord LoadStudentAcademicRecordByOfferingId(Guid contactId, Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {
                    crmServiceContext.ContactSet.
                        Where(p => p.ContactId == contactId).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                var sar = contact.usb_studentacademicrecord_ContactLookup;
                var offeringSar = sar.Where(s => s.usb_ProgrammeOfferingLookup.Id == offeringId).FirstOrDefault();

                return offeringSar;
            }
        }

        private static void ModifyEmploymentHistory(Guid enrolmentGuid, 
                                                    ApplicationSubmission applicationSubmission,
                                                    CrmServiceContext crmServiceContext)
        {
            var employmentHistoryCRM = crmServiceContext.usb_employmenthistorySet.
                                       Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentGuid);

            if ( (applicationSubmission.EmploymentHistory.IsNull() || (applicationSubmission.EmploymentHistory.Count == 0)) &&
                  employmentHistoryCRM.IsNotNull())
            {
                foreach (var item in employmentHistoryCRM)
                {
                    crmServiceContext.Delete(usb_employmenthistory.EntityLogicalName, item.Id);
                }
            }
            else if (applicationSubmission.EmploymentHistory.IsNotNull() && 
                (applicationSubmission.EmploymentHistory.Count > 0) )
            {
                if (employmentHistoryCRM.IsNotNull())
                {
                    foreach (var item in employmentHistoryCRM)
                    {
                        bool exists = applicationSubmission.EmploymentHistory.Exists(p => p.Id == item.Id);
                        if (!exists)
                        {
                            crmServiceContext.Delete(usb_employmenthistory.EntityLogicalName, item.Id);
                        }
                    }
                }

                foreach (var employmentHistoryItem in applicationSubmission.EmploymentHistory)
                {
                    var employmenthistory = new usb_employmenthistory();

                    employmenthistory.usb_EmploymentTypeOptionset = employmentHistoryItem.Type;
                    employmenthistory.usb_name = employmentHistoryItem.Employer;
                    employmenthistory.usb_EndDate = new DateTime(employmentHistoryItem.EndDate.GetValueOrDefault(), 1, 1);
                    employmenthistory.usb_IndustryOptionset = employmentHistoryItem.Industry;
                    employmenthistory.usb_IndustryNameOther = employmenthistory.usb_IndustryNameOther;
                    employmenthistory.usb_JobTitle = employmentHistoryItem.JobTitle;
                    employmenthistory.usb_LatestTwoOptions = employmentHistoryItem.Latest;
                    employmenthistory.usb_StartDate = new DateTime(employmentHistoryItem.StartDate.GetValueOrDefault(), 1, 1);
                    
                    employmenthistory.usb_StudentAcademicRecordLookup = new CrmEntityReference(usb_studentacademicrecord.EntityLogicalName, enrolmentGuid);
                    
                    employmenthistory.usb_TypeofOrganizationOptionset = employmentHistoryItem.Organization;

                    if (employmentHistoryItem.WorkArea > 0)
                    {
                        employmenthistory.usb_WorkAreaOptionset = employmentHistoryItem.WorkArea;
                    }

                    if (employmentHistoryItem.Id.IsNull() || employmentHistoryItem.Id == Guid.Empty)
                    {
                        crmServiceContext.Create(employmenthistory);
                    }
                    else 
                    {
                        employmenthistory.Id = employmentHistoryItem.Id.GetValueOrDefault();
                        crmServiceContext.Update(employmenthistory);
                    }
                }
            }
        }

        private static void ModifyQualificationHistory(Guid enrolmentGuid, 
                                                       ApplicationSubmission applicationSubmission,
                                                       CrmServiceContext crmServiceContext)
        {

            var tertiaryEducationHistoryCRM = crmServiceContext.usb_qualificationsSet.
                                              Where(p => p.usb_StudentAcadenicRecordLookup.Id == enrolmentGuid);

            if ( (applicationSubmission.Qualification.IsNull() || (applicationSubmission.Qualification.Count == 0) ) &&
                  tertiaryEducationHistoryCRM.IsNotNull()) 
            {
                foreach (var item in tertiaryEducationHistoryCRM)
                {
                    crmServiceContext.Delete(usb_qualifications.EntityLogicalName, item.Id);
                }
            }
            else if (applicationSubmission.Qualification.IsNotNull() && 
                     (applicationSubmission.Qualification.Count > 0))
            {
                if ( tertiaryEducationHistoryCRM.IsNotNull() )
                {
                    foreach (var item in tertiaryEducationHistoryCRM)
                    {
                        bool exists = applicationSubmission.Qualification.Exists(p => p.Id == item.Id);
                        if (!exists)
                        {
                            crmServiceContext.Delete(usb_qualifications.EntityLogicalName, item.Id);
                        }
                    }
                }

                foreach (var item in applicationSubmission.Qualification)
                {
                    var studies = new usb_qualifications();

                    studies.usb_StudentAcadenicRecordLookup =
                        new CrmEntityReference(usb_studentacademicrecord.EntityLogicalName, enrolmentGuid);

                    if (item.Institution.IsNotNull())
                    {
                        studies.usb_AcademicInstitutionLookup =
                            new CrmEntityReference(usb_academicinstitution.EntityLogicalName,
                                                   item.Institution.GetValueOrDefault());

                    }
                    
                    studies.usb_InstitutionOther = item.OtherInstitution.IsNull() ? "" : item.OtherInstitution;

                    studies.usb_StudentNumber = item.StudentNumber;
                    studies.usb_CompleteTwoOptions = item.Completed;
                    studies.usb_EndDate = new DateTime(item.EndDate.GetValueOrDefault(), 1, 1);
                    studies.usb_StartDate = new DateTime(item.StartDate.GetValueOrDefault(), 1, 1);
                    studies.usb_FirstTwoOptions = item.First;
                    studies.usb_HighestQualification = item.Highest;
                    studies.usb_name = item.Degree;
                    studies.usb_FieldOfStudyOptionset = item.FieldOfStudy;

                    if (item.Id.IsNull() || item.Id == Guid.Empty)
                    {
                        crmServiceContext.Create(studies);
                    }
                    else 
                    {
                        studies.Id = item.Id.GetValueOrDefault();
                        crmServiceContext.Update(studies);
                    }
                }
            }
        }

        public static void SetOwnershipOfEnrollment(Guid enrollmentId, Guid ownerId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest()
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, ownerId),
                    Target = new EntityReference(usb_studentacademicrecord.EntityLogicalName, enrollmentId)
                };

                try
                {
                    organizationService.Execute(assignRequest);
                }
                finally 
                {
                    organizationService.Dispose();
                }
            }
        }

        public static void ExecuteApplicationAcknowledgmentWorkFlow(Guid workflowId, Guid sarId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {                
                ExecuteWorkflowRequest request = new ExecuteWorkflowRequest()
                {
                    WorkflowId = workflowId,
                    EntityId = sarId
                };

                try
                {
                    ExecuteWorkflowResponse response = (ExecuteWorkflowResponse)organizationService.Execute(request);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }

        public static Guid GetQualificationGuid(Guid enrolmentId)
        {
            Guid qualificationId;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_qualifications qualification = null;
                try
                {
                    qualification = (from c in crmServiceContext.usb_qualificationsSet
                                     where c.usb_StudentAcadenicRecordLookup.Id == enrolmentId
                                     select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (qualification != null && qualification.Id != null)
                {
                    qualificationId = qualification.Id;
                }
                else
                {
                    qualificationId = crmServiceContext.Create(new usb_qualifications());
                }

                return qualificationId;
            }
        }

        public static Guid GetEmploymenthistoryGuid(Guid enrolmentId)
        {
            Guid employmenthistoryGuid;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_employmenthistory employmenthistory = null;
                try
                {
                    employmenthistory = (from c in crmServiceContext.usb_employmenthistorySet
                                         where c.usb_employmenthistory_StudentAcademicRecord.Id == enrolmentId
                                         select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (employmenthistory != null && employmenthistory.Id != null)
                {
                    employmenthistoryGuid = employmenthistory.Id;
                }
                else
                {
                    employmenthistoryGuid = crmServiceContext.Create(new usb_employmenthistory());
                }

                return employmenthistoryGuid;
            }
        }

        public static Contact  GetContact(string usNumber)
        {
          
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {

                    contact = (from c in crmServiceContext.ContactSet
                               where c.usb_USNumber == usNumber
                               select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (contact == null)
                {
                    // create new contact
                    return null;
                }
                else
                {

                    return contact;
                }              
            }
        }


        public static Guid GetContactId(string usNumber)
        {
            Guid contactNumber;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {

                    contact = (from c in crmServiceContext.ContactSet
                               where c.usb_USNumber == usNumber
                               select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                    
                if (contact == null)
                {
                    // create new contact
                    contactNumber = crmServiceContext.Create(new Contact());
                }
                else
                {
                    // get ID of existing contact
                    contactNumber = contact.Id;
                }

                return contactNumber;
            }
        }

        public static usb_studentacademicrecord LoadStudentAcademicRecordByUsNumber(string usNumber)
        {
            Guid enrolmentId;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_studentacademicrecord studentAcademicRecord = null;
                try
                {
                    studentAcademicRecord = (from c in crmServiceContext.usb_studentacademicrecordSet
                                             where c.usb_USNumber == usNumber
                                             select c).FirstOrDefault();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (studentAcademicRecord.IsNull())
                {
                    enrolmentId = crmServiceContext.Create(new usb_studentacademicrecord());
                    studentAcademicRecord = new usb_studentacademicrecord();
                    studentAcademicRecord.usb_studentacademicrecordId = enrolmentId;
                }
                

                return studentAcademicRecord;
            }
        }
        public static Guid GetProgrammeID(Guid enrolmentId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid pId = Guid.Empty;
                Guid poId = Guid.Empty;
                try
                {
                    poId = (from sar in crmServiceContext.usb_studentacademicrecordSet
                            where sar.usb_studentacademicrecordId == enrolmentId
                            select sar.usb_ProgrammeOfferingLookup.Id).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }


                if (poId != null || poId != Guid.Empty)
                {

                    pId = (from sar in crmServiceContext.usb_programmeofferingSet
                           where sar.usb_programmeofferingId == poId
                           select sar.usb_ProgrammeLookup.Id).FirstOrDefault();

                }
                if (pId != null || pId != Guid.Empty)
                {
                    return pId;
                }
                return pId;
            }

        }

        //public static Guid GetEnrolmentGuid(string usNumber)
        //{
        //    Guid enrolmentId = Guid.Empty;
        //    //if (enrolmentId.IsNotNull() && enrolmentId != Guid.Empty)
        //    //{
        //    //    return enrolmentId;
        //    //}

        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {
        //        var SARStatusCode = new SARStatusCode();

        //        usb_studentacademicrecord sar = null;
        //        try
        //        {
        //            var studentAcademicRecords = (from c in crmServiceContext.usb_studentacademicrecordSet
        //                where c.usb_USNumber == usNumber &&
        //                      c.usb_ProgrammeOfferingLookup != null
        //                select c).ToList();

        //            if (studentAcademicRecords.IsNotNull())
        //            {
        //                var records = studentAcademicRecords.Where(x => x.usb_USNumber == usNumber);

        //                var usbStudentacademicrecords = records as usb_studentacademicrecord[] ?? records.ToArray();

        //                if (usbStudentacademicrecords.Any())
        //                {
        //                    foreach (var record in usbStudentacademicrecords)
        //                    {
        //                        var statusCode = SARStatusCode.GetStatusCode(record.statuscode.GetValueOrDefault());

        //                        if (statusCode != usb_studentacademicrecord_statuscode.Applicant) continue;
        //                        var offering = record.usb_ProgrammeOfferingLookup.IsNotNull()
        //                            ? CrmQueries.GetOffering(record.usb_ProgrammeOfferingLookup.Id)
        //                            : null;

        //                        sar = offering.statuscode == (int) usb_programmeoffering_statuscode.OfferingLaunched ? record : null;
        //                    }
        //                }
        //            }
        //        }

        //        finally
        //        {
        //            crmServiceContext.Dispose();
        //        }

        //        enrolmentId = sar?.usb_studentacademicrecordId ?? Guid.Empty;

        //        return enrolmentId;
        //    }
        //}
        

        public static Guid GetEnrolmentGuidWorkFlow(string usNumber, Guid? offeringId)
        {
            Guid enrolmentId;
            var applicationFormApiController = new ApplicationFormApiController();
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var SARStatusCode = new SARStatusCode();

                usb_studentacademicrecord sar = null;
                try
                {
                    var studentAcademicRecord = (from c in crmServiceContext.usb_studentacademicrecordSet
                                                 where c.usb_USNumber == usNumber && c.usb_ProgrammeOfferingLookup != null
                                                 select c).ToList();

                    foreach (var item in studentAcademicRecord)
                    {
                        var statusCode = SARStatusCode.GetStatusCode(item.statuscode.GetValueOrDefault());

                        if (statusCode == usb_studentacademicrecord_statuscode.Applicant)
                        {
                            var offering = item.usb_ProgrammeOfferingLookup;
                            if (offering.IsNotNull())
                            {
                                var isOfferingCurrent = applicationFormApiController.IsOfferingDateCurrent(offering.Id);
                                //if (!isOfferingCurrent) continue;
                                //Do a check to see whether the student has registered twice for this course year before...
                                var enrolledOfferingID = item.usb_Studentacademicrecord_ProgrammeOfferingLookup.Id;
                                //Enrollment exists already for this USnumber on this specific offering
                                if (enrolledOfferingID == offering.Id)
                                {
                                    sar = item;
                                    //continue;
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            }
                        }
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }

                if (sar?.usb_studentacademicrecordId != null)
                {
                    enrolmentId = sar.usb_studentacademicrecordId.Value;
                }
                else
                {
                    sar = new usb_studentacademicrecord
                    {
                        usb_USNumber = usNumber,
                        usb_ProgrammeOfferingLookup = new CrmEntityReference(usb_programmeoffering.EntityLogicalName, offeringId.GetValueOrDefault())
                    };

                    enrolmentId = crmServiceContext.Create(sar);
                }

                return enrolmentId;
            }
        }

        public static Guid GetEnrolmentGuid(string usNumber, Guid? offeringId)
        {
            Guid enrolmentId;
            var applicationFormApiController = new ApplicationFormApiController();
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var SARStatusCode = new SARStatusCode();

                usb_studentacademicrecord sar = null;
                try
                {
                    var studentAcademicRecord = (from c in crmServiceContext.usb_studentacademicrecordSet
                        where c.usb_USNumber == usNumber && c.usb_ProgrammeOfferingLookup != null
                        select c).ToList();

                    foreach (var item in studentAcademicRecord)
                    {
                        var statusCode = SARStatusCode.GetStatusCode(item.statuscode.GetValueOrDefault());

                        if (statusCode == usb_studentacademicrecord_statuscode.Applicant)
                        {
                            var offering = item.usb_ProgrammeOfferingLookup;
                            if (offering.IsNotNull())
                            {
                                var isOfferingCurrent = applicationFormApiController.IsOfferingDateCurrent(offering.Id);
                                //if (!isOfferingCurrent) continue;
                                //Do a check to see whether the student has registered twice for this course year before...
                                var enrolledOfferingID = item.usb_Studentacademicrecord_ProgrammeOfferingLookup.Id;
                                //Enrollment exists already for this USnumber on this specific offering
                                if (enrolledOfferingID == offeringId)
                                {
                                    sar = item;
                                    //continue;
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            }
                        }
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }

                if (sar?.usb_studentacademicrecordId != null)
                {
                    enrolmentId = sar.usb_studentacademicrecordId.Value;
                }
                else
                {
                    sar = new usb_studentacademicrecord
                    {
                        usb_USNumber = usNumber,
                        usb_ProgrammeOfferingLookup = new CrmEntityReference(usb_programmeoffering.EntityLogicalName, offeringId.GetValueOrDefault())
                    };

                    enrolmentId = crmServiceContext.Create(sar);
                }

                return enrolmentId;
            }
        }

        public static bool DoesEnrollmentAlreadyExist(Guid contactId, Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_studentacademicrecord sar = null;
                try
                {
                    sar = crmServiceContext.usb_studentacademicrecordSet
                        .Where(p => p.usb_studentacademicrecord_ContactLookup.Id == contactId)
                        .Where(p => p.usb_ProgrammeOfferingLookup.Id == offeringId)
                        .Where(p => p.statuscode == (int)usb_studentacademicrecord_statuscode.Applicant)
                        .FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                    
                if (sar == null)
                {
                        return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static string FindStrongForeignerMatch(Guid nationalityId, DateTime dateOfBirth, string firstNames, string surname, Guid languageId, Guid genderId, Guid ethnicityId, string foreignIDNumber, string passportNumber)
        {
            if (foreignIDNumber == null)
            {
                foreignIDNumber = "";
            }

            if (passportNumber == null)
            {
                passportNumber = "";
            }

            List<Contact> foreignerMatches = null;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    foreignerMatches = crmServiceContext.ContactSet.
                                       Where(p => p.usb_NationalityLookup.Id == nationalityId).
                                       Where(p => p.BirthDate.GetValueOrDefault() == dateOfBirth).
                                       Where(p => p.FirstName == firstNames.Trim()).
                                       Where(p => p.LastName == surname.Trim()).
                                       Where(p => p.usb_contact_LanguageLookup.Id == languageId).
                                       Where(p => p.usb_GenderLookup.Id == genderId).
                                       Where(p => p.usb_EthnicityLookup.Id == ethnicityId).
                                       Where(p => p.usb_PassportNumber == foreignIDNumber.Trim() || p.usb_PassportNumber == passportNumber.Trim()).ToList<Contact>();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            if (foreignerMatches.Count == 1)
            {
                return foreignerMatches[0].usb_USNumber;
            }
            else
            {
                return null;
            }
        }

        public static bool FindWeakForeignerMatches(Guid nationalityId, DateTime dateOfBirth, string firstNames, string surname)
        {
            List<Contact> foreignerMatches = null;
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    foreignerMatches = crmServiceContext.ContactSet.
                                       Where(p => p.usb_NationalityLookup.Id == nationalityId).
                                       Where(p => p.BirthDate == dateOfBirth).
                                       Where(p => p.FirstName == firstNames.Trim()).
                                       Where(p => p.LastName == surname.Trim()).ToList<Contact>();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
            
            if (foreignerMatches.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region Private Methods

        private static bool HasApplicationFormURL(Guid enrolmentId)
        {
            bool hasUrl = false;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar = crmServiceContext.usb_studentacademicrecordSet.Where(p => p.Id == enrolmentId).FirstOrDefault();

                if (sar.IsNotNull())
                {
                    crmServiceContext.Dispose();

                    hasUrl = sar.usb_ApplicationFormURL.IsNotNullOrEmpty();
                }
            }

            return hasUrl;
        }

        private static bool HasApplicationFormURLOfficeUse(Guid enrolmentId)
        {
            bool hasUrl = false;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sar = crmServiceContext.usb_studentacademicrecordSet.Where(p => p.Id == enrolmentId).FirstOrDefault();

                if (sar.IsNotNull())
                {
                    crmServiceContext.Dispose();

                    hasUrl = sar.usb_ApplicationFormURLOfficeUse.IsNotNullOrEmpty();
                }
            }

            return hasUrl;
        }

        #endregion


    }
}
