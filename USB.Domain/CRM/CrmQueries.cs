﻿using Microsoft.Xrm.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using USB.Domain.Models;
using USB.Domain.Models.Enums;

namespace StellenboschUniversity.Usb.Integration.Crm
{
    public class CrmQueries
    {
        public static Dictionary<string, int> GetOptionSetValues(string entityName, string fieldName)
        {
            var result = new Dictionary<string, int>();

            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                RetrieveAttributeRequest newAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = fieldName.ToLower(),
                    RetrieveAsIfPublished = true
                };

                RetrieveAttributeResponse picklistResponse = null;
                try
                {
                    picklistResponse = (organizationService.Execute(newAttributeRequest) as RetrieveAttributeResponse);
                }
                finally 
                {
                    organizationService.Dispose();
                }

                if (picklistResponse != null)
                {
                    var picklistMetaDetails = (picklistResponse.AttributeMetadata as PicklistAttributeMetadata);
                    
                    if (picklistMetaDetails != null)
                    {
                        var optionsList = picklistMetaDetails.OptionSet.Options.ToArray(); 
                        //.OrderByDescending(c => c).ToArray()
                        if (optionsList != null)                            
                        {
                            foreach (var option in optionsList)
                            {
                                result.Add(option.Label.UserLocalizedLabel.Label, option.Value.Value);
                            }
                        }
                    }
                }
            }
         
            return result;
        }

        public static Guid GetCurrencyId(string currencyName)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                TransactionCurrency crmCurrency = null;

                try
                {
                    crmCurrency = (from c in crmServiceContext.TransactionCurrencySet
                                   where c.CurrencyName == currencyName select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCurrency == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCurrency.Id;
                }
            }
        }

        public static string GetHomeLanguageName(Guid languageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_language crmLanguage = null;
                try
                {
                    crmLanguage = (from c in crmServiceContext.usb_languageSet
                                   where c.usb_languageId.GetValueOrDefault() == languageId
                                   select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmLanguage.usb_name;
                }
            }
        }

        public static Guid GetHomeLanguage(Guid languageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_language crmLanguage = null;
                try
                {
                    crmLanguage = (from c in crmServiceContext.usb_languageSet
                                   where c.usb_languageId.GetValueOrDefault() == languageId
                                   select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmLanguage.Id;
                }
            }
        }

        public static string GetNationalityName(Guid nationalityId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_nationality crmNationality = null;
                try
                {
                    crmNationality = (from c in crmServiceContext.usb_nationalitySet
                                      where c.Id == nationalityId
                                      select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmNationality == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmNationality.usb_name;
                }
            }
        }

        public static string GetSisCorrespondenceLanguageCode(Guid? crmHomeLanguageId)
        {
            if(crmHomeLanguageId != null && crmHomeLanguageId != Guid.Empty)
            {
                if (GetHomeLanguageName(crmHomeLanguageId.Value).Equals("Afrikaans", StringComparison.InvariantCultureIgnoreCase))
                {
                    return "9";
                }
            }
            
            return "1";
        }

        public static Guid GetCrmCorrespondenceLanguage(Guid correspondenceLanguageId)
        {
            //string sisCode = GetSisCorrespondenceLanguageCode(correspondenceLanguage);

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_correspondencelanguage crmCorrespondenceLanguage = null;
                try
                {
                    crmCorrespondenceLanguage = (from c in crmServiceContext.usb_correspondencelanguageSet
                                                 //where c.usb_SISCode == sisCode
                                                 where c.Id == correspondenceLanguageId
                                                 select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCorrespondenceLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCorrespondenceLanguage.Id;
                }
                
            }
        }

        public static Guid GetCrmCorrespondenceLanguageFromHomeLanguage(Guid crmHomeLanguageId)
        {
            string sisCode = GetSisCorrespondenceLanguageCode(crmHomeLanguageId);

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_correspondencelanguage crmCorrespondenceLanguage = null;
                try
                {
                    crmCorrespondenceLanguage = (from c in crmServiceContext.usb_correspondencelanguageSet
                                                     where c.usb_SISCode == sisCode
                                                     select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCorrespondenceLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCorrespondenceLanguage.Id;
                }
            }
        }

        public static List<EmploymentHistory> GetStudentEmploymentHistory(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_employmenthistory> employmentHistoryCRM = null;
                try
                {
                    employmentHistoryCRM = crmServiceContext.usb_employmenthistorySet.
                                           Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentId);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                var listEmploymentHistory = new List<EmploymentHistory>();
                foreach (var item in employmentHistoryCRM)
                {
                    listEmploymentHistory.Add(CRMEntityMapper.MapToEmploymentHistory(item));
                }

                return listEmploymentHistory;
            }
        }

        public static List<TertiaryEducationHistory> GetStudentEducationHistory(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_qualifications> tertiaryEducationHistoryCRM = null;
                try
                {
                    tertiaryEducationHistoryCRM = crmServiceContext.usb_qualificationsSet.
                                                  Where(p => p.usb_StudentAcadenicRecordLookup.Id == enrolmentId);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }


                var listEducationHistory = new List<TertiaryEducationHistory>();
                foreach (var item in tertiaryEducationHistoryCRM)
                {
                    listEducationHistory.Add(CRMEntityMapper.MapToTertiaryEducationHistory(item));
                }

                return listEducationHistory;
            }
        }

        public static bool IsWebTokenValid(Guid webTokenId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_webtoken token = null;
                try
                {
                    token = (from t in crmServiceContext.usb_webtokenSet
                             where t.Id == webTokenId
                             select t).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return token.IsNotNull();
            }
        }

        public static Guid GetCourseOfferingOwner(Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_programmeoffering crmOffering = null;
                try
                {
                    crmOffering = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == offeringId
                                   select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmOffering.OwnerId.Id;
                }
            }
        }

        public static Tuple<string, string, string> GetCourseAdministratorContactDetails(Guid adminId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                SystemUser adminUser = null;

                try
                {
                    adminUser = (from q in crmServiceContext.SystemUserSet where q.Id == adminId select q).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (adminUser == null)
                {
                    return new Tuple<string, string, string>("", "", "");
                }
                else
                {
                    string name = adminUser.FirstName + " " + adminUser.LastName;

                    return new Tuple<string, string, string>(name, adminUser.InternalEMailAddress, adminUser.Address1_Telephone1);
                }
            }
        }
        
        public static List<Document> GetRequiredDocuments(Guid? enrolmentId, int level, bool submit)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                List<Document> requiredDocs = null;

                //Get ProgrammeId using OfferingID
                Guid programmeId = Guid.Empty;
                Guid offeringId = Guid.Empty;
                try
                {
                    offeringId = (from x in crmServiceContext.usb_studentacademicrecordSet
                                   where x.Id == enrolmentId
                                   select x.usb_ProgrammeOfferingLookup.Id).FirstOrDefault();

                    programmeId = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == offeringId
                                   select x.usb_ProgrammeLookup.Id).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                try
                {

                    requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
                                    join docType in crmServiceContext.usb_documenttypeSet
                                    on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId                                  
                                    where requiredDoc.usb_ProgrammeLookup.Id == programmeId &&
                                          requiredDoc.usb_RequirementLevelOptionset == level

                                    select new Document
                                    {
                                        DocumentTypeId = docType.usb_documenttypeId,
                                        Description = docType.usb_name,
                                        DocumentType = (DocumentType)docType.usb_DocumentCode.GetValueOrDefault(),
                                        Level = requiredDoc.usb_RequirementLevelOptionset.GetValueOrDefault()
                                    }).ToList();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return requiredDocs;
            }
        }
        public static List<Document> GetRequiredDocuments(Guid programmeId, int level = 1)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                List<Document> requiredDocs = null;

                try
                {

                    requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
                                    join docType in crmServiceContext.usb_documenttypeSet
                                    on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId
                                    where requiredDoc.usb_ProgrammeLookup.Id == programmeId &&
                                          requiredDoc.usb_RequirementLevelOptionset == level

                                    select new Document
                                    {
                                        DocumentTypeId = docType.usb_documenttypeId,
                                        Description = docType.usb_name,
                                        DocumentType = (DocumentType)docType.usb_DocumentCode.GetValueOrDefault(),
                                        Level = requiredDoc.usb_RequirementLevelOptionset.GetValueOrDefault()
                                    }).ToList();

                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return requiredDocs;
            }
        }

        //public static List<Document> GetRequiredDocuments(Guid programmeId)
        //{
        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {
        //        List<Document> requiredDocs = null;

        //        try
        //        {

        //            requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
        //                            join docType in crmServiceContext.usb_documenttypeSet
        //                            on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId
        //                            where requiredDoc.usb_ProgrammeLookup.Id == programmeId 
                                         

        //                            select new Document
        //                            {
        //                                DocumentTypeId = docType.usb_documenttypeId,
        //                                Description = docType.usb_name,
        //                                DocumentType = (DocumentType)docType.usb_DocumentCode.GetValueOrDefault()
                                      
        //                            }).ToList();

        //        }
        //        finally
        //        {
        //            crmServiceContext.Dispose();
        //        }

        //        return requiredDocs;
        //    }
        //}
        public static List<Document> GetRequiredDocuments(Guid programmeId, bool isShortCourse)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                List<Document> requiredDocs = null;
                var programUidMBA = new Guid();
                var programUidPhil = new Guid();

                try
                {
                    //Check on Programme Id for Shortforms, because the are not using their own Programme Types
                    // ShortForm MBA and Short Form PhilFS

                    //PROD
                    Guid.TryParse("fcff9309-d802-e611-80d2-005056b8008e", out programUidMBA);
                    //PROD
                    Guid.TryParse("13009409-d802-e611-80d2-005056b8008e", out programUidPhil);
                    //PROD MPHIL 13009409-d802-e611-80d2-005056b8008e
                    if (programmeId == programUidMBA)
                    {

                        requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
                                        join docType in crmServiceContext.usb_documenttypeSet
                                        on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId
                                        where requiredDoc.usb_ProgrammeLookup.Id == programUidMBA &&
                                                requiredDoc.usb_name.Like("%Declaration%")


                                        select new Document
                                        {
                                            DocumentTypeId = docType.usb_documenttypeId,
                                            Description = docType.usb_name,
                                            DocumentType = (DocumentType)docType.usb_DocumentCode.GetValueOrDefault(),
                                            Level = requiredDoc.usb_RequirementLevelOptionset.GetValueOrDefault()
                                        }).ToList();
                    }
                    else if (programmeId == programUidPhil)
                    {

                        requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
                                        join docType in crmServiceContext.usb_documenttypeSet
                                        on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId
                                        where requiredDoc.usb_ProgrammeLookup.Id == programUidPhil &&
                                            requiredDoc.usb_name.Like("%Declaration%")

                                        select new Document
                                        {
                                            DocumentTypeId = docType.usb_documenttypeId,
                                            Description = docType.usb_name,
                                            DocumentType = (DocumentType)docType.usb_DocumentCode.GetValueOrDefault(),
                                            Level = requiredDoc.usb_RequirementLevelOptionset.GetValueOrDefault()
                                        }).ToList();
                    }

                   
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return requiredDocs;
            }
        }

        public static Document GetRequiredDocument(Guid documentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Document document = null;
                try
                {
                    document = (from requiredDoc in crmServiceContext.usb_requireddocumentsSet
                        join docType in crmServiceContext.usb_documenttypeSet
                        on requiredDoc.usb_DocumentTypeLookup.Id equals docType.usb_documenttypeId
                        where requiredDoc.Id == documentId
                        select new Document()
                        {
                            DocumentTypeId = docType.usb_documenttypeId,
                            Description = docType.usb_name,
                            DocumentType = (DocumentType) docType.usb_DocumentCode.GetValueOrDefault(),
                            Level = requiredDoc.usb_RequirementLevelOptionset.GetValueOrDefault()
                        }).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (document == null)
                {
                    throw new Exception();
                }

                return document;
            }
        }

        public static ApplicationStatus GetApplicationStatus(Guid enrolmentId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                ApplicationStatus appStatus = null;
                IQueryable<usb_programmeoffering> crmProgrammeOffering = null;
                usb_studentacademicrecord crmEnrolmentPOId = null; //Curent Programme Offering ID for specific enrolmentId to populate PO's eventhough they are not Launced


                try
                {
                    crmEnrolmentPOId = crmServiceContext.usb_studentacademicrecordSet.Where(x => x.usb_studentacademicrecordId == enrolmentId).FirstOrDefault();

                    //sar and programmes where the sar enrolment = enrolment and the offering is launced or the offering == 
                    crmProgrammeOffering = crmServiceContext.usb_programmeofferingSet.Where(x => 
                        x.usb_programmeofferingId == crmEnrolmentPOId.usb_ProgrammeOfferingLookup.Id);

                    Guid POId= Guid.Empty;
                    
                        foreach (var PO in crmProgrammeOffering)
                        {
                            POId = PO.Id;
                        }
                   
                    //Closed PO's vs Open PO's

                    var result = (from sar in crmServiceContext.usb_studentacademicrecordSet
                                  join pos in crmServiceContext.usb_programmeofferingSet
                                  on sar.usb_ProgrammeOfferingLookup.Id equals pos.usb_programmeofferingId
                                  where sar.usb_studentacademicrecordId == enrolmentId
                                   && pos.usb_programmeofferingId == POId
                                 // || pos.statuscode == (int)usb_programmeoffering_statuscode.OfferingLaunched                                                                         
                                  select sar).FirstOrDefault();




                    if (result.IsNotNull())
                    {
                        appStatus = new ApplicationStatus
                        {
                            PersonalDetailsComplete = result.usb_PersonalInformationTwoOptions.GetValueOrDefault(),
                            AddressDetailsComplete = result.usb_AddressDetailsTwoOptions.GetValueOrDefault(),
                            WorkStudiesComplete = result.usb_WorkStudiesTwoOptions.GetValueOrDefault(),
                            MarketingComplete = result.usb_TellUsMoreTwoOptions.GetValueOrDefault(),
                            DocumentationComplete = !result.usb_DocumentsOutstandingTwoOptions.GetValueOrDefault(),
                            PaymentComplete = result.usb_PaymentTwoOptions.GetValueOrDefault()
                        };
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                
                return appStatus;
            }
        }

        public static ApplicantStatus? GetApplicantStatus(Guid enrolmentId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                int? applicantStatus = null;
                try
                {
                    applicantStatus = (from sar in crmServiceContext.usb_studentacademicrecordSet
                                           where sar.usb_studentacademicrecordId == enrolmentId
                                           select sar.statuscode).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }


                if (applicantStatus == null)
                {
                    return null;
                }

                return (ApplicantStatus)applicantStatus;
            }

        }

        public static string GetUsNumber(Guid enrolmentId)
        {
            string UsNumber;
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                UsNumber = (from sar in crmServiceContext.usb_studentacademicrecordSet
                                       where sar.usb_studentacademicrecordId == enrolmentId
                                       select sar.usb_USNumber).FirstOrDefault();

                if (UsNumber.IsNotNull())
                {
                    return UsNumber;
                }
            }


            return string.Empty;
        }

        public static bool IsApplicationFormSubmitted(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                bool? submitted = null;
                try
                {
                    submitted = (from sar in crmServiceContext.usb_studentacademicrecordSet
                                 where sar.usb_studentacademicrecordId == enrolmentId
                                 select sar.usb_StatusTwoOptions.GetValueOrDefault()).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (submitted == null)
                {
                    throw new Exception();
                }

                return submitted ?? false;
            }
        }
        public static Guid GetProgrammeID(Guid enrolmentId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid pId = Guid.Empty;
                Guid poId = Guid.Empty;
                try
                {
                    poId = (from sar in crmServiceContext.usb_studentacademicrecordSet
                                       where sar.usb_studentacademicrecordId == enrolmentId
                                       select sar.usb_ProgrammeOfferingLookup.Id).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }


                if (poId != null || poId != Guid.Empty) 
                {
                   
                    pId = (from sar in crmServiceContext.usb_programmeofferingSet
                            where sar.usb_programmeofferingId == poId
                            select sar.usb_ProgrammeLookup.Id).FirstOrDefault();

                }
                if (pId != null || pId != Guid.Empty)
                {
                    return pId;
                }
                return pId;
            }

        }
        public static usb_programmeoffering GetOffering(Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_programmeoffering crmOffering = null;
                try
                {
                    crmOffering = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == offeringId
                                   select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmOffering;
                }
            }
        }

        public static  Guid GetProgrammeOfferingFormat(Guid pofferingId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid ProgrammeID = Guid.Empty;
                Guid ProgrammeFormatID = Guid.Empty;
                try
                {
                    ProgrammeID = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == pofferingId
                                   select x.usb_programmeoffering_ProgrammeLookup.Id).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (ProgrammeID == null || ProgrammeID == Guid.Empty)
                {
                    throw new Exception();
                }
                else
                {
                   
                    try
                    {
                        ProgrammeFormatID = (from x in crmServiceContext.usb_programmeofferingSet
                                           where x.Id == pofferingId
                                           select x.usb_programmeoffering_ProgrammeFormatLookup.Id).FirstOrDefault();
                    }
                    finally
                    {
                        crmServiceContext.Dispose();
                    }

                }
                if (ProgrammeFormatID == null || ProgrammeFormatID == Guid.Empty)
                {
                    throw new Exception();
                }
                else
                {
                    return ProgrammeFormatID;
                }
            }
        }
        public static string GetThankYouPageURL(int? MBAStream, Guid poID, Guid poFormatID)
        {

          
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                string thankyoupageUrl = "";
                try
                {
                    if (MBAStream == null)
                    {
                        thankyoupageUrl = (from x in crmServiceContext.usb_thankyoupageSet
                                           where x.usb_ProgrammeOffering.Id == poID &&
                                          // x.usb_MBAStream == MBAStream ||
                                           x.usb_Programmeformat.Id == poFormatID
                                           select x.usb_URL).FirstOrDefault();
                    }
                    else
                    {
                        thankyoupageUrl = (from x in crmServiceContext.usb_thankyoupageSet
                                           where x.usb_ProgrammeOffering.Id == poID &&
                                           x.usb_MBAStream == MBAStream &&
                                           x.usb_Programmeformat.Id == poFormatID
                                           select x.usb_URL).FirstOrDefault();
                    }
                    
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (thankyoupageUrl == null)
                {
                    return null;
                }
                else
                {
                    return thankyoupageUrl;
                }
            }
        }


        public static List<StudentAcademicRecordHistory> GetStudentAcademicRecordHistory(string email, string usNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var studentHistory = new List<StudentAcademicRecordHistory>();

                try
                {
                    var result = (from x in crmServiceContext.usb_studentacademicrecordSet
                        join y in crmServiceContext.ContactSet
                        on x.usb_ContactLookup.Id equals y.ContactId
                        join z in crmServiceContext.usb_programmeofferingSet
                        on x.usb_ProgrammeOfferingLookup.Id equals z.usb_programmeofferingId
                        
                        where y.EMailAddress2 == email && y.usb_USNumber == usNumber
                              &&
                              (x.statuscode == (int) ApplicantStatus.Registered ||
                               x.statuscode == (int) ApplicantStatus.Graduated)
                        select new
                        {
                            z.usb_programmeofferingId,
                            z.usb_name,
                          
                            x.usb_studentacademicrecordId,
                            x.usb_USNumber,
                            y.EMailAddress2,
                            z.usb_YearOffered
                        }).ToList();

                    if (result.Any())
                    {
                        //*TO DO Technical Debt*//
                        result = (from a in result
                            where (a.usb_name.Contains("BMA")) ||
                                  (a.usb_name.Contains("Post Graduate Diploma")|| a.usb_name.Contains("PGDip in Futures Studies") )
                            select a).ToList();

                        foreach (var item in result)
                        {
                            int year;
                            int.TryParse(item.usb_YearOffered, out year);

                            studentHistory.Add(new StudentAcademicRecordHistory
                            {
                                ProgrammeOfferingId = item.usb_programmeofferingId.GetValueOrDefault(),
                                ProgrammeOfferingName = item.usb_name,
                                StudentAcademicRecordId = item.usb_studentacademicrecordId.GetValueOrDefault(),
                                EmailAddress = item.EMailAddress2,
                                UsNumber = item.usb_USNumber,
                                Year = year
                            });
                        }
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return studentHistory;
            }
        }
    }
}
