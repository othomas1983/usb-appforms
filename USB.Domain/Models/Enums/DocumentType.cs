﻿namespace USB.Domain.Models.Enums
{
    public enum DocumentType
    {
        AcceptanceForm = 100,
        ApplicationFee = 200,
        CertifiedCopiesOfAcademicRecords = 300,
        CertifiedCopiesOfDegree = 400,
        CopyOfID = 500,
        CV = 600,
        DescriptionOfOrganisationalUnit = 700,
        GmatSelectionTest = 800,
        ShlResults = 900,
        InitialResearchProposal = 1000,
        MarriageCertificate = 1100,
        MatricCertificate = 1200,
        MotivationForProgrammeAcceptance = 1300,
        MotivationalEssays = 1400,
        RplCandidate = 1500,
        RplRef1 = 1600,
        RplRef2 = 1700,
        ShlSelectionTest = 1800,
        SignedDeclarationForm = 1900,
        SummaryOfJobDescription = 2000,
        ConceptNote = 2100,
        ListPublication = 2200,
        CopyOfPermit = 2300,
        None = 0
    }
}
