﻿using System.ComponentModel;

namespace USB.Domain.Models.Enums
{
    public enum ProgrammeType
    {
        [Description("MBA")]
        MBA = 1,
        [Description("MPhil Development Finance")]
        MPhilDevFin = 2,
        [Description("MPhil in Future Studies")]
        MPhilFutureStudies = 3,
        [Description("MPhil in Management Coaching")]
        MPhilManagementCoaching = 4,
        [Description("Postgraduate Diploma in Business Management and Administration")]
        PGDipBMA = 5,
        [Description("Post Graduate Diploma in Development Finance")]
        PGDipDevFinance = 6,
        [Description("Post Graduate Diploma in Financial Planning")]
        PGDipFinPlanning = 7,
        [Description("Post Graduate Diploma in Future Studies")]
        PGDipFutureStudies = 8,
        [Description("Postgraduate Diploma (Leadership Development)")]
        PGDipLeadership = 9,
        [Description("Post Graduate Diploma in Project Management")]
        PGDipProjectManagment = 10,
        [Description("PhD Business Management and Administration")]
        PhDBMA = 11,
        [Description("PhD Development Finance")]
        PhDDevFin = 12,
        [Description("Phd Future Studies")]
        PhDFS = 13,
        [Description("MBA Shortened Application Form")]
        ShortenedAppFormMBA = 14,
        [Description("MPhil Future Studies Shortened Application Form")]
        ShortenedAppFormMPhilFutureStudies = 15
    }
}
