﻿using System.ComponentModel;

namespace USB.Domain.Models.Enums
{
    public enum BlendedAttendance
    {
        [Description("On Campus")]
        Oncampus = 1,
        [Description("Online")]
        Online = 2,
        [Description("Mixed")]
        Mixed = 3
    }
}
