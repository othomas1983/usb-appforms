﻿using System.ComponentModel;

namespace USB.Domain.Models.Enums
{
    public enum MBAStream
    {
        [Description("MBA General")]
        General = 1,
        [Description("MBA Health Care Leadership")]
        ManagingInternationalOrganisation = 2,
        [Description("MBA Managing International Organisations")]
        HealthManagement = 3
    }
}
