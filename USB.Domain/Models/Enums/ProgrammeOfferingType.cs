﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Domain.Models.Enums
{
    public enum ProgrammeOfferingType
    {
        Attendance = 0,
        Blended = 1,
        Online = 2
    }
}
