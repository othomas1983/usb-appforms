﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.Usb.Integration.Sis.Dto
{
    public class Application
    {
        public string Year
        {
            get;
            set;
        }

        public string Programme
        {
            get;
            set;
        }

        public string UsNumber
        {
            get;
            set;
        }
    }
}
