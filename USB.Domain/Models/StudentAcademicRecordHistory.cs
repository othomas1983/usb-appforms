﻿using System;

namespace USB.Domain.Models
{
    public class StudentAcademicRecordHistory
    {
        public Guid ProgrammeOfferingId { get; set; }

        public string ProgrammeOfferingName { get; set; }

        public Guid StudentAcademicRecordId { get; set; }

        public string UsNumber { get; set; }

        public string EmailAddress { get; set; }

        public int? Year { get; set; }
    }
}
