using System.Collections.Generic;
using System.Linq;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using USB.Domain.Models.Enums;

namespace USB.Domain.Models
{
    public class FileUploadSection
    {
        private IEnumerable<Document> _documents = new List<Document>();

        private IEnumerable<int> _documentIdsToDelete = new List<int>();

        public FileUploadSection(string name,int level, DocumentType documentType, IEnumerable<Document> documents)
        {
            Name = name;
            Level = level;
            DocumentType = documentType;
            Documents = documents ?? _documents;
        }

        public FileUploadSection()
        {
        }

        public IEnumerable<int> DocumentIdsToDelete
        {
            get { return _documentIdsToDelete; }
            set { _documentIdsToDelete = value; }
        }

        public string Name { get; set; } 
        public int Level { get; set; } 
        public DocumentType DocumentType { get; set; } 

        public Document Dummy { get { return new Document(); } }

        public IEnumerable<Document> Documents
        {
            get { return _documents.OrderBy(d=>d.FileName).ToList(); }
            set { _documents = value; }
        }
    }
}