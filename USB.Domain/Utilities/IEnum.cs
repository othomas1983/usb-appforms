﻿

using System.Collections.Generic;

namespace USB.Domain.Utilities.Enum
{
    public interface IEnum<T>
    {
        T Value { get; }
        string Name { get; }
        int Code { get; }
        bool HasFlag(T flag);
        bool HasFlag(int flag);
        bool HasAnyFlag(params T[] flag);
        bool HasAnyFlags(IEnumerable<T> flag);
        IEnumerable<int> Indexes();
        T this[int code] { get; }
    }
}