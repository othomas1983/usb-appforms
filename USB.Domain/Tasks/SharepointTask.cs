﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Usb.Domain.Sharepoint;

namespace USB.Domain.Tasks
{
    public static class SharepointTask
    {
        public static string UploadFile(string fileName, string documentTypeName, int level,
                                        int documentId, Guid enrollmentGuid, 
                                        string usNumber, byte[] bytes, string reviewed)
        {
            try
            {
                SharepointActions.UploadDocument(fileName, usNumber, level,  "MBA", documentTypeName, documentId, enrollmentGuid, bytes, reviewed);
            }
            catch(Exception exception)
            {
                return exception.Message;
            }
            return string.Empty;
        }

    }
}