﻿using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace USB.Domain.Validation
{
    public class ApplicationRules
    {
        #region IdNumberValidation

    public class IdNumberValidation
    {       
        private const string IdPattern = @"(?<Year>[0-9][0-9])(?<Month>([0][1-9])|([1][0-2]))(?<Day>([0-2][0-9])|([3][0-1]))(?<Gender>[0-9])(?<Series>[0-9]{3})(?<Citizenship>[0-9])(?<Uniform>[0-9])(?<Control>[0-9])";


        #region Enums

        private enum PersonGender
        {
            female = 0,
            male = 5,
            nonbinary =6
        }

        public enum GenderType
        {
            male,
            female,
            nonbinary
        }

        #endregion

        #region Properties

        private Match IdNumberMatch { get; set; }

        private GenderType? Gender { get; set; }

        private DateTime? DateOfBirth { get; set; }

        #endregion

        public ValidationResult IsValid(PersonalContactDetails model)
        {           
            var nationalities = Dropdowns.Nationalities();
            var genders = Dropdowns.Genders();

            var country = nationalities.FirstOrDefault(c => c.Value == model.SelectedNationality);

            if (country.IsNotNull())
            {
                if (country.Key == "South Africa")
                {
                    if (model.IdNumber.IsNotNull())
                    {
                        var idNumber = model.IdNumber.ToString();
                        //var dateOfBirth = Convert.ToDateTime(context.DateOfBirth);
                        //var dateOfBirth = DateTime.ParseExact(context.DateOfBirth, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                        var dateOfBirth = model.DateOfBirth;

                        var selectedGender = genders.FirstOrDefault(g => g.Value == model.SelectedGender);
                        
                        var gender = new object();
                        if (selectedGender.IsNotNull())
                        {
                            gender = Enum.Parse(typeof(GenderType), selectedGender.Key.ToLower().RemoveSpaces());
                            Gender = (GenderType)Enum.Parse(typeof(GenderType), gender.ToString());
                        }

                        if (idNumber.IsNotNull() && idNumber.Length != 13)
                        {
                            FormatErrorMessage("Id number must be 13 characers long.");
                        }

                        IdNumberMatch = Regex.Match(idNumber, IdPattern, RegexOptions.Compiled | RegexOptions.Singleline);

                        DateOfBirth = dateOfBirth;

                        if (!IsValid())
                        {
                            return new ValidationResult(FormatErrorMessage("Id number format is invalid"));
                        }
                        else if (dateOfBirth.IsNotNull()
                            && !IsValidForDateOfBirth())
                        {
                            return new ValidationResult(FormatErrorMessage("Date of birth, '{0:dd-MM-yyyy}', does not match with Id Number '{1}'".FormatInvariantCulture(dateOfBirth, idNumber)));
                        }
                        else if (Gender != GenderType.nonbinary)
                            {
                                if (Gender.HasValue && !IsValidForGender())
                                {
                                    return new ValidationResult(FormatErrorMessage("The gender '{0}'  does not match the gender of the ID Number provided".FormatInvariantCulture(gender.ToString(), idNumber)));
                                }
                            }
                        }
                        else 
                    {
                        return new ValidationResult(FormatErrorMessage("Id Number field is required"));
                    }
                }
            }
            
            return ValidationResult.Success;
        }

        public string FormatErrorMessage(string errorMessage)
        {
            return string.Format(errorMessage);
        }

        #region Private Methods

        private bool IsValid()
        {
            if (IdNumberMatch.Success)
            {
                // Calculate total A by adding the figures in the odd positions i.e. the first, third, fifth,
                // seventh, ninth and eleventh digits.
                int a = int.Parse(IdNumberMatch.Value.Substring(0, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(2, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(4, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(6, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(8, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(10, 1));

                // Calculate total B by taking the even figures of the number as a whole number, and then
                // multiplying that number by 2, and then add the individual figures together.
                int b = int.Parse(IdNumberMatch.Value.Substring(1, 1) +
                    IdNumberMatch.Value.Substring(3, 1) +
                    IdNumberMatch.Value.Substring(5, 1) +
                    IdNumberMatch.Value.Substring(7, 1) +
                    IdNumberMatch.Value.Substring(9, 1) +
                    IdNumberMatch.Value.Substring(11, 1));

                b *= 2;
                string bString = b.ToString();
                b = 0;
                for (int index = 0; index < bString.Length; index++)
                {
                    b += int.Parse(bString.Substring(index, 1));
                }

                // Calculate total C by adding total A to total B.
                int c = a + b;

                // The control-figure can now be determined by subtracting the ones in figure C from 10.
                string cString = c.ToString();
                cString = cString.Substring(cString.Length - 1, 1);
                int control = 0;

                // Where the total C is a multiple of 10, the control figure will be 0.
                if (cString != "0")
                {
                    control = 10 - int.Parse(cString.Substring(cString.Length - 1, 1));
                }

                if (IdNumberMatch.Groups["Control"].Value == control.ToString())
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsValidForDateOfBirth()
        {
            int year = Convert.ToInt16(IdNumberMatch.Groups["Year"].Value);
                        
            int currentCentury = Convert.ToInt16(DateTime.Now.Year.ToString().Substring(0, 2) + "00");
            int lastCentury = currentCentury - 100;
            int currentYear = Convert.ToInt16(DateTime.Now.Year.ToString().Substring(2, 2));

            // If the year is after or at the current YY, then add last century to it, otherwise add
            // this century.
            // TODO: YY -> YYYY logic needs thinking about
            if (year > currentYear)
            {
                year += lastCentury;
            }
            else
            {
                year += currentCentury;
            }

            var date = new DateTime(year, Convert.ToInt16(IdNumberMatch.Groups["Month"].Value), Convert.ToInt16(IdNumberMatch.Groups["Day"].Value));

            return DateOfBirth.Value.Date == date;
        }

        public bool IsValidForGender()
        {
            int gender = Convert.ToInt16(IdNumberMatch.Groups["Gender"].Value);

            if (gender < (int)PersonGender.male)
            {
                return Gender.Value == GenderType.female;
            }

            return Gender.Value == GenderType.male;
        }

        #endregion
    }

    #endregion
    }
}

