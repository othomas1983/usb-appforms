﻿using System.IO;

namespace Airborne.Utilities
{
    /// <summary>
    /// Defines an interface for generating PDFs.
    /// </summary>
    public interface IPdfGenerator
    {
        /// <summary>
        /// Defines the contractual obligation to generate a .pdf from an html string.
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <returns></returns>
        MemoryStream GeneratePDFFromHtml(string htmlContent);
    }
}
