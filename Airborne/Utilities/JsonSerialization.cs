﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Airborne.Utilities
{
    /// <summary>
    /// Provides utility operations to serialize and deserialize data in Json format
    /// </summary>
    public static class JsonSerialization
    {
        /// <summary>
        /// Serialize data to Json format
        /// </summary>
        public static string JsonSerializer<T>(T instance) where T : class
        {
            using (var ms = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                ser.WriteObject(ms, instance);

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Deserialize data from Json format
        /// </summary>
        public static T JsonDeserialize<T>(string value) where T : class
        {
            //TODO what if the value is empty or null ...

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(value)))
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                T obj = (T)ser.ReadObject(ms);
                return obj;
            }
        }
    }
}
