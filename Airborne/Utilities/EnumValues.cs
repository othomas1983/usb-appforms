﻿using System;
using System.Collections.Generic; 
using System.Linq;

namespace Airborne.Utilities
{
    /// <summary>
    /// Provides utility operations for enum types
    /// </summary>
    public static class EnumValues
    {
        /// <summary>
        /// Returns the enumeration as an array of type T.
        /// </summary>
        public static T[] GetFor<T>() 
        {
            var type = typeof(T);

            if (type.IsEnum.Equals(false))
            {
                throw new InvalidOperationException("The specified type T was not an enum");
            }

            var fields = type.GetFields().Where(f => f.IsLiteral);

            var values = new List<T>();

            foreach (var field in fields)
            {
                object value = field.GetValue(type);
                values.Add((T)value);
            }

            values.Sort();

            return values.ToArray<T>();
        }
    }
}