﻿using Airborne.Notifications;

namespace System.Web.Mvc
{
    public static class JsonExtensions
    {
        /// <summary>
        /// Returns generic success result
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static JsonResult GenericSuccess(this JsonResult json)
        {
            json.Data = new { State = "Successful" };

            return json;
        }

        public static JsonResult GetJsonResult(this JsonResult json)
        {
            json.Data = new { Data = json.Data, State = "Successful" };

            return json;
        }

        /// <summary>
        /// Returns generic error result with error notification messages
        /// </summary>
        /// <param name="json"></param>
        /// <param name="notifications"></param>
        /// <returns></returns>
        public static JsonResult GenericError(this JsonResult json, NotificationCollection notifications)
        {
            //TODO : add the notifications to the JSON Result

            json.Data = new { State = "Error", Notifications = notifications };

            return json;
        }
    }
}
