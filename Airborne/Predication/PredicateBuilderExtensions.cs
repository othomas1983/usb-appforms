﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Airborne.Predication
{
    /// <summary>
    /// Builds predicate for the given filter against given entity.
    /// Property names should be matched. But target property names can be decorated on any of other filter property names.
    /// </summary>
    public static class PredicateBuilderExtensions
    {
        /// <summary>
        /// Build predicate
        /// </summary>
        /// <typeparam name="TTarget">Target entity</typeparam>
        /// <param name="filter">Filter properties to be built query against target entity</param>
        /// <param name="isTrue">If True , then it still works even if all parameters are null. Default value is false</param>
        /// <param name="operatorValue">Operator to combine predicates. Default is And  </param>
        /// <returns></returns>
        public static Expression<Func<TTarget, bool>> BuildPredicate<TTarget>(this object filter, bool isTrue = false, Operator operatorValue = Operator.And)
        {
            var predicate = isTrue ? UniversalPredicateBuilder.True<TTarget>() : UniversalPredicateBuilder.False<TTarget>();

            var filterProperties = filter.GetType().GetProperties();
            foreach (var propertyInfo in filterProperties.Where(pInfo => GetAttribute<FilterMemberAttribute>(pInfo).IsNotNull()))
            {
                var attribute = (FilterMemberAttribute)GetAttribute<FilterMemberAttribute>(propertyInfo);

                //TODO: Not ideal , do a proper type check
                if (IsList(propertyInfo))
                {
                    var valuesObject = filter.GetValue(propertyInfo.Name);
                    var values = (IList)valuesObject;
                    foreach (var value in values)
                    {
                        BuildExpression(value, operatorValue, ref predicate, propertyInfo, attribute);
                    }
                }
                else
                {
                    var value = GetValueOfProperty(filter, propertyInfo);

                    BuildExpression(value, operatorValue, ref predicate, propertyInfo, attribute);
                }
            }
            return predicate;
        }

        private static object GetValueOfProperty(object filter, PropertyInfo propertyInfo)
        {
            var value = filter.GetValue(propertyInfo.Name);
            if (value.IsNotNull()) return value;

            // if value is null, then try find in its base type
            if (propertyInfo.DeclaringType == filter.GetType().BaseType)
            {
                var baseProperty = filter.GetType().BaseType.GetProperty(propertyInfo.Name);
                value = baseProperty.GetValue(filter, null);
            }

            return value;
        }

        private static void BuildExpression<TTarget>(object value, Operator oOperator, ref Expression<Func<TTarget, bool>> predicate,
            PropertyInfo propertyInfo, FilterMemberAttribute attribute)
        {
            if (attribute.IsNull() || value.IsNull())
                return;

            if (attribute.FilterNames.IsNotNull() && attribute.FilterNames.Any())
            {
                var fNames = Split(attribute.FilterNames);
                foreach (var fName in fNames)
                {
                    var filterPropertyName = fName;
                    BuildPredicate(value, filterPropertyName, ref predicate, oOperator, attribute);
                }
                // return predicate;
            }
            else
            {
                BuildPredicate(value, propertyInfo.Name, ref predicate, oOperator, attribute);
            }
        }

        /// <summary>
        /// isTrue is false by default
        /// </summary>
        /// <typeparam name="TTarget">Target entity</typeparam>
        /// <param name="filter">Filter properties to be built query against target entity</param>
        /// <param name="operatorValue">Operator to combine predicates. Default is And  </param>
        /// <returns></returns>
        public static Expression<Func<TTarget, bool>> BuildPredicate<TTarget>(this object filter, Operator operatorValue)
        {
            return filter.BuildPredicate<TTarget>(false, operatorValue);
        }

        /// <summary>
        /// Build predicate if member found at target
        /// </summary>
        private static void BuildPredicate<TTarget>(object value, string filterMemberName, ref Expression<Func<TTarget, bool>> predicate, Operator oOperator, FilterMemberAttribute attribute)
        {
            var targetPropertyInfo = typeof(TTarget).GetProperty(filterMemberName);

            if (targetPropertyInfo.IsNull())
                throw new ArgumentNullException("Property '{0}' not found in the target class '{1}' ".FormatInvariantCulture(filterMemberName, typeof(TTarget).Name));

            var param = Expression.Parameter(typeof(TTarget), String.Empty);

            //skip if the parameter is null
            if (value.IsNull())
                return;

            var left = Expression.PropertyOrField(param, filterMemberName);
            var lambda = BuildLambda<TTarget>(param, value, left, attribute, targetPropertyInfo);
            predicate = (oOperator == Operator.And) ? predicate.And(lambda) : predicate.Or(lambda);
        }

        private static Expression<Func<TTarget, bool>> BuildLambda<TTarget>(ParameterExpression param, object value, MemberExpression left, FilterMemberAttribute attribute, PropertyInfo targetPropertyInfo)
        {
            var subFilter = UniversalPredicateBuilder.False<TTarget>();
            if (attribute.Operator == ComparisonOperator.Contains)
            {
                if (IsList(targetPropertyInfo))
                {
                    var method = typeof(List<int>).GetMethod("Contains", new[] { typeof(int) });
                    var subBody = Expression.Call(left, method, Expression.Constant(value));
                    return Expression.Lambda<Func<TTarget, bool>>(subBody, param);
                    // return subBody;
                }

                subFilter = BuildKeywordsExpression(param, value, left, subFilter);
                return subFilter;
            }

            var body = MethodCallExpression<TTarget>(value, left, attribute);

            return Expression.Lambda<Func<TTarget, bool>>(body, param);
        }

        private static Expression MethodCallExpression<TTarget>(object value, MemberExpression left, FilterMemberAttribute attribute)
        {
            Expression body;
            if (value is string)
            {
                body = attribute.Operator == ComparisonOperator.NotEqual
                         ? Expression.Call(typeof(StringExtensions).GetMethod("IsNotSameAs", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public), left, Expression.Constant(value, left.Type))
                         : Expression.Call(typeof(StringExtensions).GetMethod("IsSameAsViaReflection", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public), left, Expression.Constant(value, left.Type));
                return body;
            }
            body = attribute.Operator == ComparisonOperator.NotEqual
            ? Expression.NotEqual(left, Expression.Constant(value, left.Type), false, typeof(string).GetMethod("ToLower", new[] { typeof(string) }))
            : Expression.Equal(left, Expression.Constant(value, left.Type), false, typeof(string).GetMethod("ToLower", new[] { typeof(string) }));
            return body;
        }

        private static Expression<Func<TTarget, bool>> BuildKeywordsExpression<TTarget>(ParameterExpression param, object value,
                                                                   MemberExpression left, Expression<Func<TTarget, bool>> subFilter)
        {
            var keywords = Split((string)value);
            foreach (var keyword in keywords)
            {
                var copy = keyword;

                //Using Like to do case insensitive search

                var body = Expression.Call(typeof(StringExtensions).GetMethod("Like",
                BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public), left, Expression.Constant(copy, typeof(string)));

                //Not null check is required for strings while using contains
                var notnull = Expression.NotEqual(left, Expression.Constant(null, left.Type), false,
                                                  typeof(string).GetMethod("ToLower", new[] { typeof(string) }));

                var notNullSubFilter = UniversalPredicateBuilder.Create(Expression.Lambda<Func<TTarget, bool>>(notnull, param));

                notNullSubFilter = notNullSubFilter.And(Expression.Lambda<Func<TTarget, bool>>(body, param));

                subFilter = subFilter.Or(notNullSubFilter);
            }
            return subFilter;
        }

        /// <summary>
        /// Check whether the property is a List
        /// </summary>
        private static bool IsList(PropertyInfo targetPropertyInfo)
        {
            return targetPropertyInfo.PropertyType.Name.Contains("IList") || targetPropertyInfo.PropertyType.Name.Contains("List");
        }

        /// <summary>
        /// Gets first or default custom attribute for given PropertyInfo
        /// </summary>
        public static object GetAttribute<TAttribute>(this PropertyInfo propertyInfo, bool inherit = true)
        {
            var attributes = propertyInfo.GetCustomAttributes(inherit);

            var filterMember = attributes.FirstOrDefault(a => a.GetType().Name == typeof(TAttribute).Name);
            return filterMember;
        }

        private static IEnumerable<string> Split(this string keywords)
        {
            return keywords.Split(new[] { ',', '+', '&' }).Select(k => k.Trim()); ;
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        public static string GetPropertyName<TClass>(Expression<Func<TClass>> expression)
        {
            var body = (MemberExpression)expression.Body;
            return body.Member.Name;
        }
    }
}