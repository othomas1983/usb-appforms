using System;
namespace Airborne.Predication
{
    /// <summary>
    /// When applied to a property of a type, specifies that the property will be filtered within the context of a search. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class FilterMemberAttribute : Attribute
    {
        /// <summary>
        /// Gets the filter names
        /// </summary>
        public string FilterNames { get; private set; }
       
        /// <summary>
        /// Gets the comparison operator
        /// </summary>
        public ComparisonOperator? Operator { get; private set; }

        /// <summary>
        ///  Initializes a new instance of the FilterMemberAttribute class.
        /// </summary>
        public FilterMemberAttribute(string targetPropertyNames, ComparisonOperator operatorValue)
        {
            this.Operator = operatorValue;
            FilterNames = targetPropertyNames;
        }

        /// <summary>
        ///  Initializes a new instance of the FilterMemberAttribute class.
        /// </summary>
        public FilterMemberAttribute(string targetPropertyNames)
        {
            this.Operator = ComparisonOperator.Equal;
            FilterNames = targetPropertyNames;
        }

        /// <summary>
        ///  Initializes a new instance of the FilterMemberAttribute class.
        /// </summary>
        public FilterMemberAttribute(ComparisonOperator operatorValue)
        {
            Operator = operatorValue;
        }

        /// <summary>
        ///  Initializes a new instance of the FilterMemberAttribute class.
        /// </summary>
        public FilterMemberAttribute()
        {
            Operator = ComparisonOperator.Equal;
        }
    }
}