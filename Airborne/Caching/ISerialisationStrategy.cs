namespace Airborne.Caching
{
    /// <summary>
    /// Strategy used by the cache to serialise objects
    /// </summary>
    public interface ISerialisationStrategy
    {
        /// <summary>
        /// Takes an Object and serialises it to a byte[].
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        byte[] SerialiseObject(object value);

        /// <summary>
        /// Takes a byte[] and deserialises it to a generic Object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="buffer"></param>
        /// <returns></returns>
        T DeserialiseObject<T>(byte[] buffer);
    }
}