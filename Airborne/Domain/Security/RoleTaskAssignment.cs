﻿using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{
    /// <summary>
    /// Assignment that is associated to a <see cref="Role"/>
    /// </summary>
    [DataContract]
    public class RoleTaskAssignment : TaskAssignment
    {
        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        public Role Role { get; set; }


    }
}
