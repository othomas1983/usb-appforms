﻿namespace Airborne.Domain
{
    /// <summary>
    /// An aggregate is a group of objects that belong together (a group of individual objects that represents a unit). 
    /// Each aggregate has a aggregate root. The client-objects only "talk" to the aggregate root.
    /// <remarks>http://domaindrivendesign.org/node/88</remarks>
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1040:AvoidEmptyInterfaces", Justification="Trying to achieve adherence to good practice")]
    public interface IAggregate
    {
    }
}
