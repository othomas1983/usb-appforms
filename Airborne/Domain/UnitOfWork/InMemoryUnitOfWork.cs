﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Airborne.Domain.UnitOfWork
{
    /// <summary>
    /// This is an in memory implementation of the <see cref="UnitOfWorkBase"/>.
    /// This will only affect instances in memory.
    /// </summary>
    public class InMemoryUnitOfWork : UnitOfWorkBase, IDisposable
    {
        #region Fields
        private IDictionary<IAggregate, IDictionary<PropertyInfo, object>> valueRegister = new Dictionary<IAggregate, IDictionary<PropertyInfo, object>>();
        private bool listenForChages = true;
        #endregion

       
        #region Methods

        private void ObjectPropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (listenForChages)
            {
                IAggregate senderEntity = sender as IAggregate;
                if (senderEntity != null)
                {
                    RegisterValue(senderEntity, e.PropertyName);
                }
            }
        }

        private void RegisterValue(IAggregate instance, string propertyName)
        {
            Type instanceType = instance.GetType();
            PropertyInfo propertyInfo = instanceType.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            if (!valueRegister.ContainsKey(instance))
            {
                valueRegister.Add(instance, new Dictionary<PropertyInfo, object>());
            }
            if (!valueRegister[instance].ContainsKey(propertyInfo))
            {
                object value = propertyInfo.GetValue(instance, null);
                IList list = value as IList;
                if (list != null)
                {
                    value = CopyCollection(list);
                }
                valueRegister[instance][propertyInfo] = value;
            }
        }

        /// <summary>
        /// Set the list with the original values
        /// </summary>
        private static void SetCollection(IList list, IList original)
        {
            list.Clear();
            foreach (var item in original)
            {
                list.Add(item);
            }
        }

        /// <summary>
        /// Makes a copy of items in an IList
        /// </summary>
        private static IList CopyCollection(IList list)
        {
            IList items = new ArrayList();
            foreach (var item in list)
            {
                items.Add(item);
            }

            return items;
        }

        #endregion

        #region UnitOfWorkBase Implementation

        /// <summary>
        /// Objects the registered.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="type">The type.</param>
        protected override void ObjectRegistered(IAggregate instance, RegistrationType type)
        {
            Guard.ArgumentNotNull(instance, "instance");

            INotifyPropertyChanging notifyPropertyChanging = instance as INotifyPropertyChanging;
            if (notifyPropertyChanging.IsNotNull())
            {
                notifyPropertyChanging.PropertyChanging += ObjectPropertyChanging;
            }

        }


        /// <summary>
        /// Commits the unit of work
        /// </summary>
        public override void Commit()
        {
            ObjectRegister.Clear();
        }

        /// <summary>
        /// Rolls back the unit of work
        /// </summary>
        public override void Rollback()
        {
            foreach (IAggregate instance in valueRegister.Keys)
            {
                INotifyPropertyChanging notifyPropertyChanging = instance as INotifyPropertyChanging;
                if (notifyPropertyChanging.IsNotNull())
                {
                    notifyPropertyChanging.PropertyChanging -= ObjectPropertyChanging;
                }


                IDictionary<PropertyInfo, object> values = valueRegister[instance];
                foreach (PropertyInfo propertyInfo in values.Keys)
                {
                    object value = values[propertyInfo];

                    IList original = value as IList;
                    if (original != null)
                    {
                        IList list = propertyInfo.GetValue(instance, null) as IList;
                        SetCollection(list, original);
                    }
                    else
                    {
                        propertyInfo.SetValue(instance, value, null);
                    }
                }
            }

            this.ObjectRegister.Clear();
        }


        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Rollback();
                valueRegister.Clear();
            }
        }
        #endregion
			

        #endregion
    }
}
