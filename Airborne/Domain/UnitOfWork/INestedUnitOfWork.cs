
namespace Airborne.Domain.UnitOfWork
{
    /// <summary>
    /// Contract for a nested unit of work.
    /// </summary>
    public interface INestedUnitOfWork
    {

        /// <summary>
        /// Gets the parent that this nested unit of work is defined in.
        /// </summary>
        UnitOfWorkBase Parent { get; }
    }
}
