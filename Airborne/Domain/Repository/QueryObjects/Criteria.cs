﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// A Query Object is an interpreter [Gang of Four], 
    /// that is, a structure of objects that can form itself into a SQL query. 
    /// You can create this query by refer-ring to classes and fields 
    /// rather than tables and columns. In this way those who write the 
    /// queries can do so independently of the database schema and changes 
    /// to the schema can be localized in a single place.
    /// http://martinfowler.com/eaaCatalog/queryObject.html
    /// </summary>
    public class Criteria
    {
        private IList<Sort> sorting = new List<Sort>();
        private bool distinct;

        /// <summary>
        /// Returns the list of <see cref="Sort"/> that has been added.
        /// </summary>
        public IEnumerable<Sort> Sorting
        {
            get
            {
                return sorting.AsEnumerable();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Criteria"/> is to filter out duplicates.
        /// </summary>
        /// <value><c>true</c> if distinct; otherwise, <c>false</c>.</value>
        public bool Distinct
        {
            get
            {
                return distinct;
            }
            set
            {
                distinct = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the amount of results returned by the query
        /// </summary>
        public int? FetchSize { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        /// <value>The filter.</value>
        public Filter Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Criteria"/> class.
        /// </summary>
        public Criteria()
        { }


        #region Methods

        /// <summary>
        /// Adds the a <see cref="Sort"/> to the sorting of the query definition.
        /// </summary>
        public Criteria Sort(Sort sortBy)
        {
            Guard.ArgumentNotNull(sortBy, "sort");
            sorting.Add(sortBy);
            return this;
        }


        /// <summary>
        /// Sets this <see cref="Criteria"/> to filter out duplicates.
        /// </summary>
        public Criteria SelectDistinct()
        {
            Distinct = true;
            return this;
        }

        /// <summary>
        /// Sets the <see cref="Criteria"/> filter with <paramref name="filter"/>
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public Criteria Where(Filter filter)
        {
            Guard.ArgumentNotNull(filter, "filter");
            this.Filter = filter;
            return this;
        }

        /// <summary>
        /// Controls the amount of items returned.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public Criteria SetFetchSize(int size)
        {
            this.FetchSize = size;
            return this;
        }


        #endregion
    }
}
