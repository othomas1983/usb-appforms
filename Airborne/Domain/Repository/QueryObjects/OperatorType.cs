﻿
namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Common operator types
    /// </summary>
    public enum OperatorType
    {

        /// <summary>
        /// And operator. 
        /// <remarks>
        /// Evaluates two filters and returns a resultset that must contain both.
        /// </remarks>
        /// </summary>
        And,

        /// <summary>
        /// Or operator. 
        /// <remarks>
        /// Evaluates two filters and returns a resultset that may contain one or the other.
        /// </remarks>
        /// </summary>
        Or
        
      
    }

}


