﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Provides the base class from which the classes that represent filters are derived.
    /// Also provides a static factory methods for the creation of various filters.
    /// </summary>
    public abstract class Filter
    {
        #region Operator Overloads

        #region Static Methods

        /// <summary>
        /// Implements the operator &amp;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator.</returns>
        public static Filter operator &(Filter left, Filter right)
        {

            if (left.IsNotNull() && right.IsNotNull())
            {
                return new OperatorFilter(left, right, OperatorType.And);
            }
            if (left.IsNotNull())
            {
                return left;
            }
            if (right.IsNotNull())
            {
                return right;
            }
            return null;

        }

        /// <summary>
        /// A friendly alternate for &amp; operator.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns></returns>
        public static Filter BitwiseAnd(Filter left, Filter right)
        {
            return left & right;
        }


        /// <summary>
        /// Implements the operator |.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator.</returns>
        public static Filter operator |(Filter left, Filter right)
        {
            if (left.IsNotNull() & right.IsNotNull())
            {
                return new OperatorFilter(left, right, OperatorType.Or);
            }

            if (left.IsNotNull())
            {
                return left;
            }
            if (right.IsNotNull())
            {
                return right;
            }
            return null;

        }

        /// <summary>
        /// A friendly alternate for | operator.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns></returns>
        public static Filter BitwiseOr(Filter left, Filter right)
        {
            return left | right;
        }


        #endregion
        #endregion

        #region Static Methods
        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the Like operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter Like(string property, object value)
        {
            return new SimpleFilter(property, FilterType.Like, value);
        }


        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the Equal operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter Equal(string property, object value)
        {
            return new SimpleFilter(property, FilterType.Equal, value);
        }

        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the NotEqual operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter NotEqual(string property, object value)
        {
            return new SimpleFilter(property, FilterType.NotEqual, value);
        }


        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the GreaterThan operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter GreaterThan(string property, object value)
        {
            return new SimpleFilter(property, FilterType.GreaterThan, value);
        }

        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the GreaterThanOrEqualTo operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter GreaterThanOrEqualTo(string property, object value)
        {
            return new SimpleFilter(property, FilterType.GreaterThanOrEqualTo, value);
        }

        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the GreaterThan operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter LessThan(string property, object value)
        {
            return new SimpleFilter(property, FilterType.LessThan, value);
        }


        /// <summary>
        /// Creates a <see cref="SimpleFilter"/> with the LessThanOrEqualTo operator.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static SimpleFilter LessThanOrEqualTo(string property, object value)
        {
            return new SimpleFilter(property, FilterType.LessThanOrEqualTo, value);
        }

        /// <summary>
        /// Creates a <see cref="InFilter"/>.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static InFilter In(string property, object[] values)
        {
            return new InFilter(property, values);
        }


        /// <summary>
        /// Creates a <see cref="BetweenFilter"/>.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="lower">The lower bound.</param>
        /// <param name="upper">The upper bound.</param>
        /// <returns></returns>
        public static BetweenFilter Between(string property, object lower, object upper)
        {
            return new BetweenFilter(property, lower, upper);
        }

        /// <summary>
        /// Creates a <see cref="BetweenFilter"/>.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="lower">The lower bound.</param>
        /// <param name="upper">The upper bound.</param>
        /// <param name="inclusive">if set to <c>true</c> the upper and lower values will be included in the result.</param>
        /// <returns></returns>
        public static BetweenFilter Between(string property, object lower, object upper, bool inclusive)
        {
            return new BetweenFilter(property, lower, upper, inclusive);
        }



        #endregion


    }
}
