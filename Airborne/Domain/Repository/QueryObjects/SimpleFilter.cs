﻿using System;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Filter that is based on simple equality comparisons
    /// </summary>
    public class SimpleFilter : PropertyFilter
    {
        #region Properties

        /// <summary>
        /// Gets or sets the value that the <see cref="SimpleFilter"/> will be applied to.
        /// </summary>
        /// <value>The value.</value>
        public object Value
        {
            get;
            set;
        }

        #endregion

        #region Overrides
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "{0} {1} {2}".FormatCurrentCulture(Property, Operator, Value);
        }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleFilter"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="operator">The @operator.</param>
        /// <param name="value">The value.</param>
        public SimpleFilter(string property, FilterType @operator, object value)
            : base(property, @operator)
        {
            Value = value;
        }

    }
}
