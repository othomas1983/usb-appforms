﻿
using System;
namespace Airborne.Domain.Repository
{
    /// <summary>
    /// Contract for a transaction
    /// </summary>
    public interface ITransaction : IDisposable 
    {
        /// <summary>
        /// Gets a value indicating whether this transaction is active.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Gets a value indicating whether this transaction was committed.
        /// </summary>
        bool WasCommitted { get; }


        /// <summary>
        /// Gets a value indicating whether this transaction was rolled back.
        /// </summary>
        bool WasRolledBack { get; }

        /// <summary>
        /// Commits this transaction.
        /// </summary>
        void Commit();

        /// <summary>
        /// Performs a rollback on the transaction
        /// </summary>
        void Rollback();
    }
}
