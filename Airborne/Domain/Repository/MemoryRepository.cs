﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Airborne.Domain.Repository.QueryObjects;

namespace Airborne.Domain.Repository
{
    /// <summary>
    /// Cache of objects used in <see cref="MemoryRepository"/>
    /// </summary>
    public class ObjectCache
    {
        private IDictionary<Type, IList<object>> objectCache = new Dictionary<Type, IList<object>>();

        /// <summary>
        /// Adds a new instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void Add(object instance)
        {
            Type type = instance.GetType();
            if (!objectCache.ContainsKey(type))
            {
                objectCache.Add(type, new List<object>());
            }

            if (!objectCache[type].Contains(instance))
            {
                objectCache[type].Add(instance);
            }
        }

        /// <summary>
        /// Removes a instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void Remove(object instance)
        {
            Type type = instance.GetType();
            if (objectCache.ContainsKey(type))
            {
                objectCache[type].Remove(instance);
            }
        }

        /// <summary>
        /// Determines whether an instance exists in the cache.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified instance]; otherwise, <c>false</c>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public bool Contains(object instance)
        {
            Type type = instance.GetType();
            if (objectCache.ContainsKey(type))
            {
                return objectCache[type].Contains(instance);
            }

            return false;
        }

        /// <summary>
        /// Gets the objects in teh cache
        /// </summary>
        /// <value>The objects.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public IDictionary<Type, IList<object>> Objects
        {
            get
            {
                return objectCache;
            }
        }

    }


    /// <summary>
    /// Memory based implementation of the repository.
    /// This is mostly used for testing purposes
    /// </summary>
    public class MemoryRepository : IRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryRepository"/> class.
        /// </summary>
        public MemoryRepository()
        {
            ObjectCache = new ObjectCache();
        }


        #region IRepository Members

        /// <summary>
        /// Starts a transaction
        /// </summary>
        /// <returns></returns>
        public ITransaction BeginTransaction()
        {
            return new AlwaysCommitedTransaction();
        }

        /// <summary>
        /// Gets or sets the object cache.
        /// </summary>
        /// <value>The object cache.</value>
        public ObjectCache ObjectCache
        {
            get;
            set;
        }

        #region IRepository Members

        /// <summary>
        /// Saves the specified entity back to the repository
        /// </summary>
        /// <param name="entity"></param>
        public void Add(IAggregate entity)
        {
            ObjectCache.Add(entity);
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(object entity)
        {
            ObjectCache.Add(entity);
        }


        /// <summary>
        /// Deletes the specified entity in the repository
        /// </summary>
        /// <param name="entity"></param>
        public void Remove(object entity)
        {
            ObjectCache.Remove(entity);
        }

        /// <summary>
        /// This method shows intent to perform update actions on the <paramref name="entity"/>
        /// </summary>
        /// <param name="entity"></param>
        public void Update(object entity)
        {
            if (!ObjectCache.Contains(entity))
            {
                ObjectCache.Add(entity);
            }
        }

        /// <summary>
        /// Refreshes an entity
        /// </summary>
        /// <param name="entity"></param>
        public void Refresh(object entity)
        {
            // Can't put a NotImplementedException else it crashes tests
        }

        /// <summary>
        /// Evicts the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="id">The id.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "type"), 
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "id"), 
        System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public void Evict(Type type, object id)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Tries the find one or returns the default value for the type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <returns>
        /// The first entity which matches the expession or null
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public T First<T>(Expression<Func<T, bool>> predicate)
        {
            if (ObjectCache.Objects.ContainsKey(typeof(T)))
            {
                return ObjectCache.Objects[typeof(T)].Cast<T>().First<T>(predicate.Compile());
            }
            return default(T);
        }

        /// <summary>
        /// Firsts the specified domain criteria.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criteria">The domain criteria.</param>
        /// <returns></returns>
        public T First<T>(Airborne.Domain.Repository.QueryObjects.Criteria criteria)
        {
            if (ObjectCache.Objects.ContainsKey(typeof(T)))
            {
                return ObjectCache.Objects[typeof(T)].Cast<T>().FirstOrDefault<T>(ExpressionBuilder.BuildFunction<T>(criteria));
            }
            return default(T);
        }



        /// <summary>
        /// Finds all entities in the repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>List of <typeparamref name="T"/></returns>
        public IEnumerable<T> FindAll<T>()
        {
            if (ObjectCache.Objects.ContainsKey(typeof(T)))
            {
                return ObjectCache.Objects[typeof(T)].Cast<T>();
            }
            return new List<T>();
        }

        /// <summary>
        /// Finds all entities matching an expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns>List of <typeparamref name="T"/></returns>
        //public IEnumerable<T> FindAll<T>(Func<T, bool> expression)
        //{
        //    return ObjectCache.Objects[typeof(T)].Cast<T>().Where(expression);
        //}

        public IEnumerable<T> FindAll<T>(Expression<Func<T, bool>> expression)
        {
            //return ObjectCache.Objects[typeof(T)].Cast<T>().Where(expression);
            return ObjectCache.Objects[typeof(T)].Cast<T>().Where<T>(expression.Compile());
        }

          



        /// <summary>
        /// Finds all.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criteria">The domain criteria.</param>
        /// <returns></returns>
        public IEnumerable<T> FindAll<T>(Airborne.Domain.Repository.QueryObjects.Criteria criteria)
        {
            if (ObjectCache.Objects.ContainsKey(typeof(T)))
            {
                return ObjectCache.Objects[typeof(T)].OfType<T>().Where(ExpressionBuilder.BuildFunction<T>(criteria));
            }
            else
            {
                return new List<T>();
            }
        }

        /// <summary>
        /// Finds an entity based on a know id or key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById<T>(object id) where T : class
        {
            Type genericType = typeof(T);


            var lists = from a in ObjectCache.Objects
                        where genericType.IsAssignableFrom(a.Key)
                              && a.Value.IsNotNull()
                        select a.Value.OfType<T>().FirstOrDefault();//e => (e is IIdentifiable) && ((IIdentifiable)e).Id == id.ToString());

            return lists.FirstOrDefault(e => e.IsNotNull());
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ObjectCache.Objects.Clear();
            }
        }

        #endregion


        #region IRepository Members

        /// <summary>
        /// Performs a custom initialization routine on the repository instance
        /// </summary>
        public void Init()
        {
           //nothing 2 do ...;
        }

        #endregion
    }



    internal class AlwaysCommitedTransaction : ITransaction
    {

        #region ITransaction Members

        public bool IsActive
        {
            get { return true; }
        }

        public bool WasCommitted
        {
            get { return true; }
        }

        public bool WasRolledBack
        {
            get { return false; }
        }

        public void Commit()
        {
            //do nothing
        }

        public void Rollback()
        {
           //do nothing;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //do nothing
        }

        #endregion
    }
}
