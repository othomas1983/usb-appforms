﻿using System;
using System.Diagnostics;

namespace Airborne.Logging
{
    /// <summary>
    /// ILogger implementation using the System.Diagnostics.Trace as the underlying logger
    /// </summary>
    public sealed class TraceLogger : ILogger
    {
        #region ILogger Members

        /// <summary>
        /// Writes an info message to the underlying trace listener.
        /// </summary>
        public void Info(string message)
        {
            Trace.WriteLine(message, "INFO");
        }

        /// <summary>
        /// Writes a warning message to the underlying trace listener.
        /// </summary>
        public void Warn(string message)
        {
            Trace.WriteLine(message, "WARN");
        }

        /// <summary>
        /// Writes an error message to the underlying trace listener.
        /// </summary>
        public void Error(string message)
        {
            Trace.WriteLine(message, "ERROR");
        }

        /// <summary>
        /// Writes the exception as an error message to the underlying trace listener.
        /// </summary>
        public void Error(Exception ex)
        {
            var msg = ex.Message + Environment.NewLine + ex.StackTrace;
            Trace.WriteLine(msg, "ERROR");
        }

        #endregion
    }
}