﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace Airborne.Logging
{
    /// <summary>
    /// ILogger implementation based on the .Net System.Diagnostics EventLog.
    /// </summary>
     public sealed class WindowsEventLog : ILogger, IDisposable
    {
        #region Locals

        EventLog logger;

        #endregion

        #region Constructors
         
         /// <summary>
        /// Instantiates as instance of the WindowsEventLogger 
         /// </summary>
        public WindowsEventLog()
        {
            logger = new EventLog();
            logger.Source = ResolveSource();
        }

        #endregion

        #region ILogger Members

         /// <summary>
         /// Writes the information message to the windows event log.
         /// </summary>
         /// <param name="message"></param>
        public void Info(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Writes the warning message to the windows event log.
        /// </summary>
        public void Warn(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Warning);
        }

        /// <summary>
        /// Writes the error message to the windows event log.
        /// </summary>
        public void Error(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Error);
        }

        /// <summary>
        /// Writes the exception as a warning message to the windows event log.
        /// </summary>
        public void Error(Exception ex)
        {
            var msg = ex.Message + Environment.NewLine + ex.StackTrace;

            logger.WriteEntry(msg, EventLogEntryType.Error);
        }

        #endregion

        #region Methods

        private static string ResolveSource()
        {
            return ConfigurationManager.AppSettings["EventLogSource"] ?? "Unkown";
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes of the logger instance.
        /// </summary>
        public void Dispose()
        {
            if (logger.IsNotNull())
            {
                logger.Dispose();
            }
        }

        #endregion
    }
}