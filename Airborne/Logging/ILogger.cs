﻿using System;

namespace Airborne.Logging
{
    /// <summary>
    /// Defines the contractual obligation for ILogger implementations
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Writes an information message to the logger
        /// </summary>
        void Info(string message);

        /// <summary>
        /// Writes a warning message to the logger
        /// </summary>
        void Warn(string message);

        /// <summary>
        /// Writes an error message to the logger
        /// </summary>
        void Error(string message);

        /// <summary>
        /// Writes the exception as a message to the logger
        /// </summary>
        void Error(Exception ex);
    }
}