﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.International.Domain.Utilities
{
    public class Enum<T> : IEnum<T>
    {
        protected Enum()
        {
            _flagToIndexMapping = FlagToIndexMappings();
        }

        protected Enum(T input) : this()
        {
            Value = input;
        }

        public Enum(int input) : this()
        {
            Value = this[input];
        }

        public Enum(string input) : this()
        {
            Value = (T)System.Enum.Parse(typeof(T), input);
        }

        public virtual T Value { get; set; }

        public bool HasFlag(T flag)
        {
            var left = (System.Enum)(System.Enum.ToObject(typeof(T), Value));
            var right = (System.Enum)(System.Enum.ToObject(typeof(T), flag));
            return left.HasFlag(right);
        }

        public bool HasFlag(int flag)
        {
            var statusFlag = this[flag];
            var left = (System.Enum)(System.Enum.ToObject(typeof(T), Value));
            var right = (System.Enum)(System.Enum.ToObject(typeof(T), statusFlag));
            return left.HasFlag(right);
        }

        public bool HasAnyFlag(params T[] flag)
        {
            return Values.Any(HasFlag);
        }

        public bool HasAnyFlags(IEnumerable<T> flags)
        {
            return flags.Any(HasFlag);
        }


        public int Code
        {
            get { return Convert.ToInt32(Value); }
        }

        public static string[] Names
        {
            get { return System.Enum.GetNames(typeof(T)); }
        }


        public static IEnumerable<T> Values
        {
            get { return (T[])System.Enum.GetValues(typeof(T)); }
        }

        public string Name
        {
            get { return System.Enum.GetName(typeof(T), Value); }
        }


        public static implicit operator Enum<T>(T value)
        {
            return new Enum<T>(value);
        }

        public static implicit operator Enum<T>(int value)
        {
            return new Enum<T>(value);
        }

        public static implicit operator Enum<T>(string value)
        {
            return new Enum<T>(value);
        }


        private readonly Dictionary<int, int> _flagToIndexMapping;

        public static int[] Codes
        {
            get
            {
                var results = (int[])System.Enum.GetValues(typeof(T));
                return results.Select(Convert.ToInt32).ToArray();
            }
        }

        public static Dictionary<int, string> CodeNameList
        {
            get
            {
                var results = (int[])System.Enum.GetValues(typeof(T));
                return results.ToDictionary(r => r, r => System.Enum.GetName(typeof(T), r));
            }
        }

        private Dictionary<int, int> FlagToIndexMappings()
        {
            var i = 0;
            return Codes.ToDictionary(item => item, item => i++);
        }

        public IEnumerable<int> Indexes()
        {
            return (from statusValue in Codes
                    where HasFlag(statusValue)
                    select _flagToIndexMapping[statusValue]).ToList();
        }

        public int ToIndex()
        {
            return _flagToIndexMapping[Code];
        }

        public T this[int code]
        {
            get { return (T)System.Enum.ToObject(typeof(T), code); }
        }


        public static T FromIndex(int index)
        {
            return Values.ToArray()[index];
        }
    }

    //Cast enums based on string value
    public class Enum<TLeft, TRight> : Enum<TLeft>
    {
        protected Enum(TRight input) : base(((Enum<TLeft>)(((Enum<TRight>)input).Name)).Value)
        {
        }

        public static implicit operator Enum<TLeft, TRight>(TRight value)
        {
            return new Enum<TLeft, TRight>(value);
        }
    }
}
