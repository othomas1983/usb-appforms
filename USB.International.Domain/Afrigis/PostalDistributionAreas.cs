﻿using Newtonsoft.Json;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Mvc;
using SettingsMan = USB.International.Domain.Properties.Settings;

namespace Afrigis
{
    public class AreaList
    {
        public Guid Id { get; set; }
        public string DisplayText { get; set; }
    }


    public class PostalDistributionArea
    {

        #region Properties

        public List<SelectListItem> Areas { get; set; }

        #endregion

        #region Public Methods

        public List<SelectListItem> LoadAreas(string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                var controller = new ApplicationFormApiController();

                var result = controller.AfrigisSearch(query);

                Areas = new List<SelectListItem>();

                if (result.Count > 0)
                {
                    Areas.Add(new SelectListItem { Text = "Results found. Please select...", Value = "0" });
                }

                foreach (var item in result)
                {
                    Areas.Add(new SelectListItem { Text = item.DisplayText, Value = item.Id.AsString() });
                }
            }
            return Areas;
        }

        #endregion
    }
}