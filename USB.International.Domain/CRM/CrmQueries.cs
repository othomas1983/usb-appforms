﻿using Microsoft.Xrm.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Models;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public class CrmQueries
    {
        public static Dictionary<string, int> GetOptionSetValues(string entityName, string fieldName)
        {
            var result = new Dictionary<string, int>();

            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                RetrieveAttributeRequest newAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = fieldName.ToLower(),
                    RetrieveAsIfPublished = true
                };

                RetrieveAttributeResponse picklistResponse = null;
                try
                {
                    picklistResponse = (organizationService.Execute(newAttributeRequest) as RetrieveAttributeResponse);
                }
                finally
                {
                    organizationService.Dispose();
                }

                if (picklistResponse != null)
                {
                    var picklistMetaDetails = (picklistResponse.AttributeMetadata as PicklistAttributeMetadata);

                    if (picklistMetaDetails != null)
                    {
                        var optionsList = picklistMetaDetails.OptionSet.Options.ToArray();

                        if (optionsList != null)
                        {
                            foreach (var option in optionsList)
                            {
                                result.Add(option.Label.UserLocalizedLabel.Label, option.Value.Value);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static Guid GetCurrencyId(string currencyName)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                TransactionCurrency crmCurrency = null;

                try
                {
                    crmCurrency = (from c in crmServiceContext.TransactionCurrencySet
                                   where c.CurrencyName == currencyName
                                   select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCurrency == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCurrency.Id;
                }
            }
        }

        public static string GetHomeLanguageName(Guid languageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_languagehome crmLanguage = null;
                try
                {
                    crmLanguage = (from c in crmServiceContext.usb_languagehomeSet
                                   where c.usb_languagehomeId.GetValueOrDefault() == languageId
                                   select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmLanguage.usb_name;
                }
            }
        }

        public static Guid GetHomeLanguage(Guid languageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_languagehome crmLanguage = null;
                try
                {
                    crmLanguage = (from c in crmServiceContext.usb_languagehomeSet
                                   where c.usb_languagehomeId.GetValueOrDefault() == languageId
                                   select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmLanguage.Id;
                }
            }
        }

        public static string GetNationalityName(Guid nationalityId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_Nationality crmNationality = null;

                try
                {
                    crmNationality = (from c in crmServiceContext.usb_NationalitySet
                                      where c.Id == nationalityId
                                      select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmNationality == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmNationality.usb_Nationality1;
                }
            }
        }

        public static string GetSisCorrespondenceLanguageCode(Guid? crmHomeLanguageId)
        {
            if (crmHomeLanguageId != null && crmHomeLanguageId != Guid.Empty)
            {
                if (GetHomeLanguageName(crmHomeLanguageId.Value).Equals("Afrikaans", StringComparison.InvariantCultureIgnoreCase))
                {
                    return "9";
                }
            }

            return "1";
        }

        public static Guid GetCrmCorrespondenceLanguage(Guid correspondenceLanguageId)
        {
            //string sisCode = GetSisCorrespondenceLanguageCode(correspondenceLanguage);

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_languagecorrespondence crmCorrespondenceLanguage = null;
                try
                {
                    crmCorrespondenceLanguage = (from c in crmServiceContext.usb_languagecorrespondenceSet
                                                 where c.Id == correspondenceLanguageId
                                                 select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCorrespondenceLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCorrespondenceLanguage.Id;
                }

            }
        }

        public static Guid GetCrmCorrespondenceLanguageFromHomeLanguage(Guid crmHomeLanguageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_languagecorrespondence crmCorrespondenceLanguage = null;

                try
                {
                    crmCorrespondenceLanguage = (from c in crmServiceContext.usb_languagecorrespondenceSet
                                                 where c.Id == crmHomeLanguageId
                                                 select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmCorrespondenceLanguage == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCorrespondenceLanguage.Id;
                }
            }
        }

        public static List<EmploymentHistory> GetStudentEmploymentHistory(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_WorkHistory> employmentHistoryCRM = null;
                try
                {
                    employmentHistoryCRM = crmServiceContext.usb_WorkHistorySet.
                                           Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentId);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                var listEmploymentHistory = new List<EmploymentHistory>();
                foreach (var item in employmentHistoryCRM)
                {
                    listEmploymentHistory.Add(CRMEntityMapper.MapToEmploymentHistory(item));
                }

                return listEmploymentHistory;
            }
        }

        public static List<TertiaryEducationHistory> GetStudentEducationHistory(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_HighestQualification> tertiaryEducationHistoryCRM = null;
                try
                {
                    tertiaryEducationHistoryCRM = crmServiceContext.usb_HighestQualificationSet.
                                                  Where(p => p.usb_StudentAcademicRecordLookup.Id == enrolmentId);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }


                var listEducationHistory = new List<TertiaryEducationHistory>();
                foreach (var item in tertiaryEducationHistoryCRM)
                {
                    listEducationHistory.Add(CRMEntityMapper.MapToTertiaryEducationHistory(item));
                }

                return listEducationHistory;
            }
        }

        //public static bool IsWebTokenValid(Guid webTokenId)
        //{
        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {
        //        usb_webtoken token = null;
        //        try
        //        {
        //            token = (from t in crmServiceContext.usb_webtokenSet
        //                     where t.Id == webTokenId
        //                     select t).FirstOrDefault();
        //        }
        //        finally
        //        {
        //            crmServiceContext.Dispose();
        //        }

        //        return token.IsNotNull();
        //    }
        //}

        public static Guid GetCourseOfferingOwner(Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_programmeoffering crmOffering = null;
                try
                {
                    crmOffering = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == offeringId
                                   select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmOffering.OwnerId.Id;
                }
            }
        }

        public static Tuple<string, string, string> GetCourseAdministratorContactDetails(Guid adminId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                SystemUser adminUser = null;

                try
                {
                    adminUser = (from q in crmServiceContext.SystemUserSet where q.Id == adminId select q).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (adminUser == null)
                {
                    return new Tuple<string, string, string>("", "", "");
                }
                else
                {
                    string name = adminUser.FirstName + " " + adminUser.LastName;

                    return new Tuple<string, string, string>(name, adminUser.InternalEMailAddress, adminUser.Address1_Telephone1);
                }
            }
        }

        public static List<Document> GetRequiredDocuments(Guid programmeId, int level = 1)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                List<Document> requiredDocs = null;

                try
                {
                    requiredDocs = (from requiredDoc in crmServiceContext.usb_requireddocumentslistSet
                                    join docType in crmServiceContext.usb_documenttypeSet
                                    on requiredDoc.usb_DocumentTypeId.Id equals docType.usb_documenttypeId
                                    where requiredDoc.usb_ProgrammeId.Id == programmeId &&
                                          requiredDoc.usb_RequirementLevelOptionSet == level
                                    select new Document()
                                    {
                                        DocumentTypeId = docType.usb_documenttypeId,
                                        Description = docType.usb_documentname,
                                        Level = requiredDoc.usb_RequirementLevelOptionSet.GetValueOrDefault()
                                    }).ToList();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (requiredDocs == null)
                {
                    throw new Exception();
                }

                return requiredDocs;
            }
        }

        public static Document GetRequiredDocument(Guid documentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Document document = null;

                try
                {
                    document = (from requiredDoc in crmServiceContext.usb_requireddocumentslistSet
                                join docType in crmServiceContext.usb_documenttypeSet
                                on requiredDoc.usb_DocumentTypeId.Id equals docType.usb_documenttypeId
                                where requiredDoc.Id == documentId
                                select new Document()
                                {
                                    DocumentTypeId = docType.usb_documenttypeId,
                                    Description = docType.usb_documentname,
                                    Level = requiredDoc.usb_RequirementLevelOptionSet.GetValueOrDefault()
                                }).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (document == null)
                {
                    throw new Exception();
                }

                return document;
            }
        }

        public static ApplicationStatus GetApplicationStatus(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                ApplicationStatus appStatus = null;
                try
                {
                    appStatus = (from sar in crmServiceContext.usb_StudentAcademicRecordSet
                                 where sar.usb_StudentAcademicRecordId == enrolmentId
                                 select new ApplicationStatus()
                                 {
                                     PersonalDetailsComplete = sar.usb_PersonalInformationTwoOptions.GetValueOrDefault(),
                                     AddressDetailsComplete = sar.usb_AddressDetailsTwoOptions.GetValueOrDefault(),
                                     WorkStudiesComplete = sar.usb_WorkStudiesTwoOptions.GetValueOrDefault(),
                                     DocumentationComplete = sar.usb_DocumentationTwoOptions.GetValueOrDefault()
                                 }).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (appStatus == null)
                {
                    throw new Exception();
                }

                return appStatus;
            }
        }

        public static ApplicantStatus? GetApplicantStatus(Guid enrolmentId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                int? applicantStatus = null;
                try
                {
                    applicantStatus = (from sar in crmServiceContext.usb_StudentAcademicRecordSet
                                       where sar.usb_StudentAcademicRecordId == enrolmentId
                                       select sar.statuscode).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (applicantStatus == null)
                {
                    return null;
                }

                return (ApplicantStatus)applicantStatus;
            }

        }

        public static bool IsApplicationFormSubmitted(Guid enrolmentId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                bool? submitted = null;
                try
                {
                    submitted = (from sar in crmServiceContext.usb_StudentAcademicRecordSet
                                 where sar.usb_StudentAcademicRecordId == enrolmentId
                                 select sar.usb_StudentStatus.GetValueOrDefault()).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (submitted == null)
                {
                    throw new Exception();
                }

                return submitted ?? false;
            }
        }

        public static usb_programmeoffering GetOffering(Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                usb_programmeoffering crmOffering = null;
                try
                {
                    crmOffering = (from x in crmServiceContext.usb_programmeofferingSet
                                   where x.Id == offeringId
                                   select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmOffering;
                }
            }
        }
    }
}
