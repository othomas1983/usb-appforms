﻿using Microsoft.Xrm.Client;
using SettingsMan = USB.International.Domain.Properties.Settings;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public static class CrmServer
    {
        private static CrmConnection crmConnection = Microsoft.Xrm.Client.CrmConnection.Parse(SettingsMan.Default.CrmConnectionString);

        public static CrmConnection CrmConnection
        {
            get
            {
                return crmConnection;
            }
        }
    }
}
