﻿using Microsoft.Xrm.Client;
using StellenboschUniversity.UsbInternational.Integration.Crm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.International.Domain.Controller.Dto.Enums;
using SettingsMan = USB.International.Domain.Properties.Settings;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public static class Dropdowns
    {
        public static Dictionary<string, Guid> Titles()
        {
            var titles = new Dictionary<string, Guid>();
            var firstTitles = new List<string>() { "Mr", "Mrs", "Miss", "Dr", "Prof" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var crmResults = from q in crmServiceContext.usb_TitleSet orderby q.usb_EngName select q;

                foreach (var crmResult in crmResults)
                {
                    if (titles.ContainsKey(crmResult.usb_EngName) == false)
                    {
                        titles.Add(crmResult.usb_EngName, crmResult.Id);
                    }
                }
            }

            var result = new Dictionary<string, Guid>();

            foreach (var firstTitle in firstTitles)
            {
                if (titles.ContainsKey(firstTitle))
                {
                    result.Add(firstTitle.ToTitleCase(), titles[firstTitle]);
                }
            }

            foreach (var title in titles)
            {
                if (result.ContainsKey(title.Key.ToTitleCase()) == false)
                {
                    result.Add(title.Key.ToTitleCase(), title.Value);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Genders()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_gender> crmResults = null;

                try
                {
                    crmResults = from q in crmServiceContext.usb_genderSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Ethnicities()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_ethnicity> crmResults = null;

                try
                {
                    crmResults = from q in crmServiceContext.usb_ethnicitySet orderby q.usb_EnglishDescription select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_EnglishDescription.ToTitleCase(), crmResult.Id);
                }
            }

            return result;
        }


        public static Dictionary<string, Guid> NonSAEthnicities()
        {
            var result = new Dictionary<string, Guid>();
            var ethnicities = Ethnicities();

            var africanGuid = ethnicities.FirstOrDefault(e => e.Key == "African").Value;
            var asianGuid = ethnicities.FirstOrDefault(e => e.Key == "Indian").Value;
            var caucasianGuid = ethnicities.FirstOrDefault(e => e.Key == "White").Value;
            var mixedGuid = ethnicities.FirstOrDefault(e => e.Key == "Coloured").Value;
            var otherGuid = ethnicities.FirstOrDefault(e => e.Key == "African").Value;

            result.Add("African", africanGuid);
            result.Add("Asian", asianGuid);
            result.Add("Caucasian", caucasianGuid);
            result.Add("Mixed", mixedGuid);
            result.Add("Other", otherGuid);

            return result;
        }


        public static Dictionary<string, Guid> Nationalities()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_Nationality> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_NationalitySet orderby q.usb_Name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (result.ContainsKey(crmResult.usb_Nationality1) == false)
                    {
                        result.Add(crmResult.usb_Nationality1, crmResult.Id);
                    }
                }
            }
            
            return result;
        }

        public static Dictionary<string, Guid> CorrespondenceLanguages()
        {
            var correspondenceLanguages = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_languagecorrespondence> crmResults = null;

                try
                {
                    crmResults = from q in crmServiceContext.usb_languagecorrespondenceSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (correspondenceLanguages.ContainsKey(crmResult.usb_name) == false)
                    {
                        correspondenceLanguages.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            return correspondenceLanguages;
        }

        public static Dictionary<string, Guid> Languages()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_languagehome> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_languagehomeSet orderby q.usb_LanguageCode select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (result.ContainsKey(crmResult.usb_name) == false)
                    {
                        result.Add(crmResult.usb_name, crmResult.Id);
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, int> ForeignIdentificationTypes()
        {
            var result = CrmQueries.GetOptionSetValues(Contact.EntityLogicalName, "usb_IdentificationType");

            if (result.IsNotNull())
            {
                if (result.ContainsKey("ID Number"))
                {
                    result.Remove("ID Number");
                }
            }

            return result;
        }
        
        //public static Dictionary<string, Guid> SouthAfricaIdentificationTypes()
        //{
        //    var result = new Dictionary<string, Guid>();

        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {
        //        IQueryable<usb_idtype> crmResults = null;
        //        try
        //        {
        //            crmResults = from q in crmServiceContext.usb_idtypeSet orderby q.usb_NameEnglish select q;
        //        }
        //        finally
        //        {
        //            crmServiceContext.Dispose();
        //        }

        //        foreach (var crmResult in crmResults)
        //        {
        //            string name = crmResult.usb_NameEnglish;

        //            if (String.Equals(name, "Id Number", StringComparison.InvariantCultureIgnoreCase) == true)
        //            {
        //                result.Add(crmResult.usb_NameEnglish, crmResult.Id);
        //            }
        //        }
        //    }

        //    return result;
        //}

        public static IEnumerable<KeyValuePair<string, Guid>> Disabilities()
        {
            var result = new Dictionary<string, Guid>();
            var resultNoneValueTop = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_disability> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_disabilitySet orderby q.usb_EnglishName select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (crmResult.usb_EnglishName == "None")
                    {
                        resultNoneValueTop.Add(crmResult.usb_EnglishName, crmResult.Id);
                    }
                    else
                    {
                        result.Add(crmResult.usb_EnglishName, crmResult.Id);
                    }
                }
            }

            var n = resultNoneValueTop.Concat(result);

            return n;
        }

        public static Dictionary<string, Guid> AddressCountries()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_country> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_countrySet orderby q.usb_CountryName select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    if (result.ContainsKey(crmResult.usb_CountryName) == false)
                    {
                        result.Add(crmResult.usb_CountryName, crmResult.Id);
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, int> QualificationFields()
        {
            //Demian option set
            //var crmResults = from q in crmServiceContext.usb_FieldOfStudyOptionset orderby q.stb_NameEnglish select q;
            return CrmQueries.GetOptionSetValues("usb_qualifications", "usb_FieldOfStudyOptionset");
        }

        public static Dictionary<string, Guid> QualificationTypes()
        {
            var intermediateResult = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_HighestQualification> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_HighestQualificationSet orderby q.usb_Qualification select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    intermediateResult.Add(crmResult.usb_Qualification, crmResult.Id);
                }
            }

            var result = new Dictionary<string, Guid>();
            string key;
            Guid value;

            key = "Grade 12";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Certificate, Diploma";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "B Degree";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Post Graduate, Honours";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Doctorate, Masters";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            key = "Other";

            if (intermediateResult.TryGetValue(key, out value))
            {
                result.Add(key, value);
            }

            return result;
        }

        public static Dictionary<string, int> Industries()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_industryoptionset");
        }

        public static Dictionary<string, int> WorkAreas()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_workareaoptionset");
        }

        public static Dictionary<string, int> LeesAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_afrikaansleesoptionset");
        }

        public static Dictionary<string, int> PraatAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_afrikaanspraatoptionset");
        }

        public static Dictionary<string, int> SkryfAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_afrikaansskryfoptionset");
        }

        public static Dictionary<string, int> VerstaanAfrikaans()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_afrikaansverstaanoptionset");
        }

        public static Dictionary<string, int> EnglishRead()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_englishreadOptionset");
        }

        public static Dictionary<string, int> EnglishSpeak()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_englishspeakoptionset");
        }

        public static Dictionary<string, int> EnglishUnderstand()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_englishunderstandoptionset");
        }

        public static Dictionary<string, int> EnglishWrite()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_englishwriteoptionset");
        }

        public static Dictionary<string, int> OccupationalCategories()
        {
            return CrmQueries.GetOptionSetValues("usb_employmenthistory", "stb_leadsource");
        }

        public static Dictionary<string, int> MarketingSources()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "stb_leadsource");
        }

        public static Dictionary<string, int> MarketingReasons()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "stb_marketingreason");
        }

        public static Dictionary<string, int> EmployerAssistFinancially()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_employerassistfinanciallyOptionset");
        }

        public static Dictionary<string, int> EmploymentType()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_employmenttype");
        }

        public static Dictionary<string, Guid> PermitTypes()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_permittype> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_permittypeSet orderby q.usb_name select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_name, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> MaritalStatus()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_maritalstatus> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_maritalstatusSet orderby q.usb_EnglishDescription select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_EnglishDescription, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Offerings(bool isMBA, Guid programmeGuid)
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programmeoffering> crmProgrammeOffering = null;

                try
                {
                    if (isMBA)
                    {
                        crmProgrammeOffering = from q in crmServiceContext.usb_programmeofferingSet
                            orderby q.usb_programmeofferingname
                            where q.usb_programmeofferingname.Contains("MBA")
                            select q;
                    }
                    else
                    {
                        crmProgrammeOffering =
                            crmServiceContext.usb_programmeofferingSet.Where(
                                p => p.usb_ProgrammeLookup.Id == programmeGuid);
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmProgrammeOffering)
                {
                    if (crmResult.usb_Year.IsNotNullOrEmpty())
                    {
                        if (!crmResult.usb_Year.Contains(" "))
                        {
                            int defaultDate;
                            int.TryParse(crmResult.usb_Year, out defaultDate);

                            if (defaultDate >= DateTime.Now.Year && crmResult.statuscode == (int)ProgrammeOfferingType.Launched)
                            {
                                result.Add(crmResult.usb_programmeofferingname, crmResult.Id);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, Guid> Programme()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_programme> crmResults = null;

                try
                {
                    crmResults = from q in crmServiceContext.usb_programmeSet orderby q.usb_programmename select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_programmename, crmResult.Id);
                }
            }

            return result;
        }


        public static Dictionary<string, int> MathematicCompetency()
        {
            //return new Dictionary<string, int>() { 
            //{ " No Secondary Schooling Mathematics", 1 }, 
            //{ "South African Grade 9", 2 }, 
            //{ "South African Grade 12 SG", 3 },
            //{ "South African Grade 12 HG", 4 },
            //{ "GCSE", 5 },
            //{ "A Levels", 6 },
            //{ "Tertiary Education", 7 }, 
            //{ "Other", 8 },
            //{ "Chartered Accountant", 9 },
            //{"IEB Grade 12 HG", 10 },
            //{"Engineering Mathematics, Applied Mathematics", 11},
            //{"German Abitur (Higher Secondary Education)", 12},
            //{"Technical University Diploma in Germany", 13},
            //{"First Semester Calcs and Stats, National Diploma",14},
            //{"Statistics Honours", 15},
            //{"German A-Level, Witten Examination; Several Math courses at University level",16},
            //{"German Abitur and all Math courses, Chemistry master with all advanced math courses",17}
            //};

            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_highestlevelmathematicscompetedOptionset");
        }

        //TODO:Mathematics percentage is not in Data dictionary
        public static Dictionary<string, int> MathematicPercentage()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_highestlevelmathematicscompetedoptionset");
        }

        public static Dictionary<string, int> PriorInvolvement()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_whatdidyoudolastyearoptionset");
        }

        public static Dictionary<string, Guid> Institution()
        {
            var result = new Dictionary<string, Guid>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<usb_academicinstitution> crmResults = null;
                try
                {
                    crmResults = from q in crmServiceContext.usb_academicinstitutionSet orderby q.usb_EnglishName select q;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var crmResult in crmResults)
                {
                    result.Add(crmResult.usb_EnglishName, crmResult.Id);
                }
            }

            return result;
        }

        public static Dictionary<string, int> FieldOfStudy()
        {
            return CrmQueries.GetOptionSetValues(usb_HighestQualification.EntityLogicalName, "usb_fieldofstudyoptionset");
        }

        public static Dictionary<string, int> CertificationNumber()
        {
            return CrmQueries.GetOptionSetValues(usb_HighestQualification.EntityLogicalName, "usb_certificationnumber");
        }

        public static Dictionary<string, int> WorkArea()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_workareaoptionset");
        }

        public static Dictionary<string, int> Organization()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_TypeOfOrganisationOptionset");
        }

        public static Dictionary<string, int> Industry()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_industry");
        }

        public static Dictionary<string, int> Degrees()
        {
            return CrmQueries.GetOptionSetValues(usb_WorkHistory.EntityLogicalName, "usb_degreelookup");
        }

        public static Dictionary<string, int> InfoSessions()
        {
            return CrmQueries.GetOptionSetValues(usb_StudentAcademicRecord.EntityLogicalName, "usb_infosessionoptionset");
        }

    }
}