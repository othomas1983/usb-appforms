﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Models;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public static class PersonMaintenanceServiceInteractions
    {
        public static string GetApplicantUSNumber(ApplicationSubmission application, string correspondenceLanguageCode)
        {
            var applicant = new Person();

            if (String.IsNullOrEmpty(application.FirstNames))
            {
                throw new Exception("First names of applicant are missing.");
            }

            if (String.IsNullOrEmpty(application.IdNumber) == false && String.IsNullOrEmpty(application.PassportNumber) == true && String.IsNullOrEmpty(application.ForeignIdNumber) == true)
            {
                applicant.SouthAfricanIdNumber = application.IdNumber.Trim();
            }
            else
            {
                applicant.SouthAfricanIdNumber = "NONE";
            }

            applicant.FirstNames = (String.IsNullOrEmpty(application.GivenName) == false) ? application.GivenName.Trim() : application.FirstNames.Trim();
            applicant.Surname = application.Surname.Trim();
            applicant.FirstNames = application.FirstNames.Trim();
            applicant.Initials = application.Initials.Trim().ToUpper();
            applicant.EmailAddress = application.Email.Trim();
            applicant.DateOfBirth = application.DateOfBirth.GetValueOrDefault();
            applicant.Title = CrmIdToSisCodeMapping.GetTitleSisCode(application.Title.GetValueOrDefault());
            applicant.Gender = CrmIdToSisCodeMapping.GetGenderSisCode(application.Gender.GetValueOrDefault());
            applicant.Ethnicity = CrmIdToSisCodeMapping.GetEthnicitySisCode(application.Ethnicity.GetValueOrDefault());
            applicant.Nationality = CrmIdToSisCodeMapping.GetNationalitySisCode(application.Nationality.GetValueOrDefault());
            applicant.CorrespondenceLanguage = correspondenceLanguageCode;
            applicant.CellphoneNumber = application.MobileNumber;
            applicant.GivenName = application.GivenName;
            applicant.Language = CrmIdToSisCodeMapping.GetLanguageSisCode(application.Language.GetValueOrDefault());
            applicant.MaidenName = application.MaidenName;
            applicant.WorkTelephoneNumber = application.WorkContactTelephone;

            if (String.IsNullOrEmpty(application.PassportNumber) == false)
            {
                applicant.PassportNumber = application.PassportNumber;
                applicant.PassportExpiryDate = application.PassportExpiryDate.HasValue ? application.PassportExpiryDate : null;
            }

            if (String.IsNullOrEmpty(application.ForeignIdNumber) == false)
            {
                applicant.ForeignIdNumber = application.ForeignIdNumber;
                applicant.PassportExpiryDate = application.PassportExpiryDate.HasValue ? application.PassportExpiryDate : null;
            }
            
            var reply = applicant.CreateApplicant(applicant);

            if ((reply.Success == false) || (reply.UsNumber == "0"))
            {
                throw new ApplicationException(String.Format("Could not get student number from SIS for applicant. SIS Error Message: {1}", reply.Message));
            }
            else
            {
                return reply.UsNumber;
            }
        }
    }
}
