﻿using System;
using System.Linq;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Controller.Dto.Enums;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public class ContactMapper
    {
        public static PersonalContactDetails MapToPersonalContactDetails(Contact application)
        {
            var contact = new PersonalContactDetails()
            {
                BusinessPhoneNumber = application.Telephone1,
                DateOfBirth = application.BirthDate.GetValueOrDefault(),
                OtherDisability = application.usb_DisabilityOther,
                GivenName = application.usb_GivenName,
                eMail = application.EMailAddress1,
                FaxNumber = application.Fax,
                FirstNames = application.FirstName,
                ForeignNumber = application.usb_PassportNumber,
                HomePhoneNumber = application.Telephone2,
                IdNumber = application.GovernmentId,
                Initials = application.usb_Initials,
                MaidenName = application.usb_MaidenName,
                MobileNumber = application.MobilePhone,
                PassportNumber = application.usb_PassportNumber,
                SelectedGender = application.usb_GenderLookup.Id,
                SelectedGenderName = application.usb_GenderLookup.Name,
                SelectedNationality =
                    application.usb_NationalityLookup.IsNotNull() ? application.usb_NationalityLookup.Id : new Guid(),
                SelectedCountryOfIssue =
                    application.usb_NationalityLookup.IsNotNull() ? application.usb_NationalityLookup.Id : new Guid(),
                SelectedTitle = application.usb_TitleLookup.IsNotNull() ? application.usb_TitleLookup.Id : new Guid(),
                SelectedLanguage =
                    application.usb_HomeLanguageLookup.IsNotNull() ? application.usb_HomeLanguageLookup.Id : new Guid(),
                SelectedMaritalStatus =
                    application.usb_MaritalStatusLookup.IsNotNull()
                        ? application.usb_MaritalStatusLookup.Id
                        : new Guid(),
                SelectedMaritalStatusName =
                    application.usb_MaritalStatusLookup.IsNotNull() ? application.usb_MaritalStatusLookup.Name : "",
                SelectedEthnicity =
                    application.usb_EthnicityLookup.IsNotNull() ? application.usb_EthnicityLookup.Id : new Guid(),
                SelectedCorrespondanceLanguage =
                    application.usb_CorrespondenceLanguageLookup.IsNotNull()
                        ? application.usb_CorrespondenceLanguageLookup.Id
                        : new Guid(),
                SelectedDisability =
                    application.usb_DisabilityLookup.IsNotNull() ? application.usb_DisabilityLookup.Id : new Guid(),
                UseWheelchair = application.usb_UseWheelchair.GetValueOrDefault(),
                Status = application.StatusCode,
                Surname = application.LastName,
                USNumber = application.usb_StudentNumber,
                SelectedOffering = GetOfferingId(application.Id),
                SARId = GetSARId(application.Id),
                SelectedPermitType =
                    application.usb_PermitTypeLookup.IsNotNull() ? application.usb_PermitTypeLookup.Id : new Guid()
            };

            if (application.usb_PassportExpiryDate.HasValue)
            {
                contact.ForeignIdExpiryDate = application.usb_PassportExpiryDate.GetValueOrDefault();
            }
            else if (application.usb_PassportExpiryDate.HasValue)
            {
                contact.ForeignIdExpiryDate = application.usb_PassportExpiryDate.GetValueOrDefault();
            }

            if (application.usb_IdentificationType.IsNotNull())
            {
                contact.SelectedForeignIdentificationType =
                    (IdentificationType)
                    Enum.ToObject(typeof(IdentificationType), application.usb_IdentificationType.GetValueOrDefault());
            }

            return contact;
        }

        public static AddressDetails MapToAddressDetails(Contact application)
        {
            var contact = new AddressDetails()
            {
                ResidentialCountry =
                    application.usb_address1_CountryLookup.IsNotNull()
                        ? application.usb_address1_CountryLookup.Id
                        : Guid.Empty,
                ResidentialCity = application.usb_address1_City,
                ResidentialStreet1 = application.Address1_Line1,
                ResidentialStreet2 = application.Address1_Line2,
                ResidentialStreet3 = application.Address1_Line3,
                ResidentialPostalCode = application.usb_address1_PostalCode,
                ResidentialValidUntil = application.usb_AddressValidUntil ?? DateTime.MinValue,
                EmergencyContactTitle =
                    application.usb_EContactTitleLookup.IsNotNull()
                        ? application.usb_EContactTitleLookup.Id
                        : Guid.Empty,
                EmergencyContactInitals = application.usb_EContactInitals,
                EmergencyContactName = application.usb_EContactName,
                EmergencyContactSurname = application.usb_EContactSurname,
                EmergencyContactRelationship = application.usb_EContactRelationship,
                EmergencyContactCountry =
                    application.usb_EContactCountryLookup.IsNotNull()
                        ? application.usb_EContactCountryLookup.Id
                        : Guid.Empty,
                EmergencyContactCityTown = application.usb_EContactCityTown,
                EmergencyContactStreet1 = application.usb_EContactStreet1,
                EmergencyContactStreet2 = application.usb_EContactStreet2,
                EmergencyContactStreet3 = application.usb_EContactStreet3,
                EmergencyContactPostalCode = application.usb_EContactPostalCode,
                EmergencyContactTelephone = application.usb_EContactTelephone,
                EmergencyContactEmail = application.usb_EContactEmail
            };

            return contact;
        }

        #region Private Methods

        private static Guid GetOfferingId(Guid contactId)
        {
            var offeringId = new Guid();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sarList = crmServiceContext.usb_StudentAcademicRecordSet.
                    Where(p => p.usb_ContactLookup.Id == contactId).ToList();

                foreach (var item in sarList)
                {
                    if (item.usb_ProgrammeOfferingLookup.IsNotNull())
                    {
                        var crmProgrammeOffering = (from q in crmServiceContext.usb_programmeofferingSet
                            orderby q.usb_programmeofferingname
                            select q).ToList();

                        var programmeOffering =
                            crmProgrammeOffering.Where(x => x.Id == item.usb_ProgrammeOfferingLookup.Id);

                        if (programmeOffering.Count() > 0)
                        {
                            if (programmeOffering.FirstOrDefault().usb_Year.IsNotNullOrEmpty() &&
                                int.Parse(programmeOffering.FirstOrDefault().usb_Year) >= DateTime.Now.Year)
                            {
                                offeringId = programmeOffering.FirstOrDefault().Id;
                            }
                        }
                    }
                }
            }

            return offeringId;
        }

        private static Guid GetSARId(Guid contactId)
        {
            var sarId = new Guid();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var sarList =
                    crmServiceContext.usb_StudentAcademicRecordSet.Where(x => x.usb_ContactLookup.Id == contactId)
                        .ToList();

                foreach (var item in sarList)
                {
                    if (item.usb_ProgrammeOfferingLookup.IsNotNull())
                    {
                        var crmProgrammeOffering = (from a in crmServiceContext.usb_programmeofferingSet
                            orderby a.usb_programmeofferingname
                            select a).ToList();

                        var programmeOffering =
                            crmProgrammeOffering.Where(x => x.Id == item.usb_ProgrammeOfferingLookup.Id);

                        if (programmeOffering.Count() > 0)
                        {
                            if (programmeOffering.FirstOrDefault().usb_Year.IsNotNullOrEmpty() &&
                                int.Parse(programmeOffering.FirstOrDefault().usb_Year) >= DateTime.Now.Year)
                            {
                                sarId = programmeOffering.FirstOrDefault().Id;
                            }
                        }
                    }
                }
            }

            return sarId;
        }

        #endregion

    }
}
