﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {
        public static ApplicationSubmission Map(PersonalContactDetails personalContactDetails)
        {
            var application = new ApplicationSubmission()
            {
                ContactNumber = personalContactDetails.HomePhoneNumber,
                CorrespondanceLanguage = personalContactDetails.SelectedCorrespondanceLanguage,
                DateOfBirth = personalContactDetails.DateOfBirth,
                Disability = personalContactDetails.SelectedDisability,
                OtherDisability = personalContactDetails.OtherDisability,
                Email = personalContactDetails.eMail,
                Ethnicity = personalContactDetails.SelectedEthnicity,
                FaxNumber = personalContactDetails.FaxNumber,
                FirstNames = personalContactDetails.FirstNames,
                ForeignIdExpiryDate = personalContactDetails.ForeignIdExpiryDate,
                ForeignIdNumber = personalContactDetails.ForeignNumber,
                Gender = personalContactDetails.SelectedGender,
                GivenName = personalContactDetails.GivenName,
                IdNumber = personalContactDetails.IdNumber,
                Initials = personalContactDetails.Initials,
                Language = personalContactDetails.SelectedLanguage,
                MaidenName = personalContactDetails.MaidenName,
                MaritalStatus = personalContactDetails.SelectedMaritalStatus,
                MobileNumber = personalContactDetails.MobileNumber.Trim().Replace(" ", "").Replace("(", "").Replace(")", ""),
                Nationality = personalContactDetails.SelectedNationality,
                CountryOfIssue = personalContactDetails.SelectedCountryOfIssue,
                Offering = personalContactDetails.SelectedOffering.GetValueOrDefault(),
                PermitType = (personalContactDetails.SelectedPermitType != Guid.Empty) ? personalContactDetails.SelectedPermitType : null,
                PassportExpiryDate = personalContactDetails.ForeignIdExpiryDate,
                PassportNumber = personalContactDetails.PassportNumber,
                Surname = personalContactDetails.Surname,
                Title = personalContactDetails.SelectedTitle,
                UsesWheelchair = personalContactDetails.UseWheelchair,
                WorkNumber = personalContactDetails.BusinessPhoneNumber,
                IsSouthAfrican = personalContactDetails.IsSouthAfrican,
                ForeignIdType = personalContactDetails.SelectedForeignIdentificationType
            };

            return application;
        }

        public static ApplicationSubmission Map(AddressDetails addressDetails)
        {
            return new ApplicationSubmission()
            {
                ResidentialCity = addressDetails.ResidentialCity,
                ResidentialCountry = addressDetails.ResidentialCountry,
                ResidentialPostalCode = addressDetails.ResidentialPostalCode,
                ResidentialStreet1 = addressDetails.ResidentialStreet1,
                ResidentialStreet2 = addressDetails.ResidentialStreet2,
                ResidentialStreet3 = addressDetails.ResidentialStreet3,
                ResidentialValidUntil = addressDetails.ResidentialValidUntil,
                EmergencyContactTitle = addressDetails.EmergencyContactTitle,
                EmergencyContactInitals = addressDetails.EmergencyContactInitals,
                EmergencyContactName = addressDetails.EmergencyContactName,
                EmergencyContactSurname = addressDetails.EmergencyContactSurname,
                EmergencyContactRelationship = addressDetails.EmergencyContactRelationship,
                EmergencyContactCountry = addressDetails.EmergencyContactCountry,
                EmergencyContactStreet1 = addressDetails.EmergencyContactStreet1,
                EmergencyContactStreet2 = addressDetails.EmergencyContactStreet2,
                EmergencyContactStreet3 = addressDetails.EmergencyContactStreet3,
                EmergencyContactCityTown = addressDetails.EmergencyContactCityTown,
                EmergencyContactPostalCode = addressDetails.EmergencyContactPostalCode,
                EmergencyContactTelephone = addressDetails.EmergencyContactTelephone,
                EmergencyContactEmail = addressDetails.EmergencyContactEmail
            };
        }

        public static ApplicationSubmission Map(Workstudies workStudies)
        {
            var applicationSubmission = new ApplicationSubmission()
            {
                ApplicationSource = USB.International.Domain.Models.ApplicationSource.WorkStudies,
                
                HomeInstitution = workStudies.HomeInstitution,
                PrincipleFieldOfStudy = workStudies.PrincipleFieldOfStudy,
                YearsCompletedAtHomeInstitution = workStudies.YearsCompletedAtHomeInstitution,
                DurationOfExchange = workStudies.DurationOfExchange,
                Qualification = workStudies.TertiaryEducationHistory,
                EmploymentHistory = workStudies.EmploymentHistory,
                YearsOfWorkingExperience = workStudies.YearsOfWorkingExperience,
                MonthsOfWorkingExperience = workStudies.MonthsOfWorkingExperience,
                ContactPersonName = workStudies.ContactPersonName,
                ContactPersonEmailAddress = workStudies.ContactPersonEmailAddress
            };

            return applicationSubmission;

        }

        public static ApplicationSubmission Map(MarketingModel marketing)
        {
            return new ApplicationSubmission()
            {
                usb_advertisementTwoOptions = marketing.checkboxAdvertisement,
                usb_NewsArticleTwoOptions = marketing.checkboxNewsArticle,
                usb_PromotionalEmailsTwoOptions = marketing.checkboxUsbPromotionalEmails,
                usb_ReferralByEmployerTwoOptions = marketing.checkboxReferralByMyEmployer,
                usb_StellenboschUniversityTwoOptions = marketing.checkboxStellenboschUniversity,
                usb_ReferralByAlumnusTwoOptions = marketing.checkboxReferralByAnAlumnus,
                usb_USBWebsiteTwoOptions = marketing.checkboxUsbWebsite,
                usb_ReferralByCurrentStudentTwoOptions = marketing.checkboxReferralByACurrentStudent,
                usb_USBEdTwoOptions = marketing.checkboxUsbEd,
                usb_ReferralByFamilyFriendTwoOptions = marketing.checkboxReferralByAFamilyMember,
                usb_BrochureTwoOptions = marketing.checkboxBrochure,
                usb_SocialMediaTwoOptions = marketing.checkboxSocialMedia,
                usb_ConferenceExpoTwoOptions = marketing.checkboxConferenceOrExpo,
                usb_OnCampusEventTwoOptions = marketing.checkboxOnCampusEvent,

                infoSessionAttended = marketing.infoSessionAttended,
                usb_InfoSessionOptionSet = (marketing.infoSessionOptionSet != 0) ? marketing.infoSessionOptionSet : null,

                usb_SearchEngineTwoOptions = marketing.radWebSearch,

                usb_CareerChangeOptionSet = marketing.radCareerChange,
                usb_ProgressCareerOptionSet = marketing.radCareerProgress,
                usb_AcquireManagementSkillsOptionset = marketing.radManageSkills,

                #region MPhil/PGDip Dev Fin
                usb_AcquireDevFinSkillsOptionset = marketing.radSpecialistSkills,
                usb_AfricaSustainableDevelopmentOptionset = marketing.radSustainableDev,
                usb_AfricaEmergingMarketOptionset = marketing.radEmergingMarket,
                usb_DevelopSAEconomyOptionset = marketing.radDevSAEconomy,
                usb_AfricaMultiNationalEnvironmentOptionset = marketing.radManageMultiNationalBusiness,
                usb_AfricaStrategicDecisionOptionset = marketing.radStrategicDecisions,
                usb_InsightPoliciesProgrammesOptionset = marketing.radInsightPoliciesProgrammes,
                usb_BusinessLeaderOptionset = marketing.radDevelopBusinessLeader,
                #endregion

                #region MPhil/PGDip Future Studies
                usb_ProfessionalFuturistOptionset = marketing.radFuturist,
                usb_BenefitFromUniqueTypeOptionset = marketing.radUniqueProgram,
                usb_ComplexOrgEnvironmentOptionset = marketing.radComplexEnviroment,
                usb_SAGrowthContributionOptionset = marketing.radGrowthSAorContinent,
                usb_TheoryOfFututeStudiesOptionset = marketing.radUnderstandFutures,
                usb_DevelopefutureStudiesOptionset = marketing.radDeveloFutureModel,
                usb_AlignForFuturistOptionset = marketing.radAlignSkills,
                usb_SustainableFutureOptionset = marketing.radSustainFuture,
                usb_ScenarioToolsOptionset = marketing.radScenarioPlanning,
                #endregion

                #region MPhil Coaching Management

                usb_SelfDevelopmentOptionset = marketing.radDevelopMyself,
                usb_EnhanceCoachingSkillsOptionset = marketing.radEnhanceCouchingSkills,
                usb_TheoreticalCoachingOptionset = marketing.radTheoreticalUnderstandingCouching,
                usb_OwnCoachingPracticeOptionset = marketing.radStartOwnCoachingPractice,
                usb_OwnFrameworkModelOptionset = marketing.radPersonalisedCoachingModel,
                usb_DevelopmentOfOthersOptionset = marketing.radDevelopmentOthers,
                usb_ProfessionalCoachesCommunityOptionset = marketing.radCommunityOfProfessionalCoaches,

                #endregion

                #region PGDip BMA

                usb_EntrepreneurialKnowledgeOptionset = marketing.radAcquireEntrKnowledge,
                usb_InnovativeOptionset = marketing.radInnovative,
                usb_DigitalSkillsOptionset = marketing.radDigitalSkills,
                usb_ProgressToMBAOptionset = marketing.radProgressToMBA,

                #endregion

                #region PGDip Leadership

                usb_AuthenticLeadershipOptioset = marketing.radAuthenticLeadershipStyle,
                usb_MakeADifferenceOptionset = marketing.radMakeADifference,
                usb_HowToBeALeaderOptionset = marketing.radHowToBeALeaderInSAContext,
                usb_SustainableTransOptionset = marketing.radSustainableTransformation,

                #endregion

                #region PGDip Project Management

                usb_PMSkillsOptionset = marketing.radProjectManagementSkills,
                usb_ManageProjectsOptionset = marketing.radManageProjects,
                usb_AquireScareSkillsOptionset = marketing.radScarceSkill,

                #endregion

                usb_DevelopAsALeaderOptionset = marketing.radDevelopLeader,
                usb_AcquireBusinessKnowledgeOptionset = marketing.radBusinessKnowledge,
                usb_AquireThinkingSkillsOptionset = marketing.radCriticalThinking,
                usb_ResponsibleLeader = marketing.radEthicalLeader,
                usb_HigherSalary = marketing.radSalary,
                usb_GainKnowledge = marketing.radKnowBusinessStrategy,

                usb_LecturerQaulityOptionset = marketing.radLecturerQuality,
                usb_ElectronicStudyMaterialOptionset = marketing.radElectronicMaterial,
                usb_StudyMaterialOptionset = marketing.radPrintedStudyMaterial,
                usb_ResearchSupervisionOptionset = marketing.radResearchSupervision,
                usb_ICTSupportOptionset = marketing.radICTSupport,
                usb_AvailableCareerOptionset = marketing.radCareerService,
                usb_LibraryServicesOptionset = marketing.radLibraryService,
                usb_AcademicSupportOptionset = marketing.radAcademicSupport,
                usb_MentorsOptionset = marketing.radAvailabilityMentors,
                usb_CampusSecurityOptionset = marketing.radCampusSecurity,
                usb_CateringServicesOptionset = marketing.radCateringServices,
                usb_AdminSupportOptionset = marketing.radAdministrativeSupport,
                usb_OffcampusSupportOptionset = marketing.radOffCampusSupport,
                usb_DiverseClassGroupOptionset = marketing.radDiverseClassGroup,
                usb_DiverseFaultyCompositeOptionset = marketing.radDiverseFacultyComposition,
                usb_BlendedLearningOptionset = marketing.radBlendedLearning,
                usb_ClassroomBasedOptionset = marketing.radClassroomDelivery,

                usb_TripleAccreditationTwoOptions = marketing.chkTripleAccreditation,
                usb_ValueForMoneyTwoOptions = marketing.chkValueForMoney,
                usb_RecommendedByAlumniTwoOptions = marketing.chkRecommendedByAlumni,
                usb_CloseToHomeTwoOptions = marketing.chkGeographicLocation,
                usb_ExcellentReputationTwoOptions = marketing.chkExcellentReputation,
                usb_RecommendedByEmployerTwoOptions = marketing.chkRecommendedByEmplyer,
                usb_HighQualityStudentsTwoOptions = marketing.chkHighQualityStudents,
                usb_ProgrammeFormatTwoOptions = marketing.chkProgrammeFormat,
                usb_USBInternationalTwoOptions = marketing.chkNetworking,
                usb_USBAfricaFocusTwoOptions = marketing.chkAfrica,
                usb_USBGeneralLeadershipFocusTwoOptions = marketing.chkLeadership,
                usb_USBCollaborativeLearningTwoOptions = marketing.chkLearning,
                usb_IAmAMatieTwoOptions = marketing.chkMatie,
                usb_InternationalLecturersInClassTwoOptions = marketing.chkLecturerElectives,
                usb_InternationalStudentsTwoOptions = marketing.chkInternationalStudents,
                usb_DeliveryModeTwoOptions = marketing.chkDeliveryMode
            };
        }

        public static ApplicationSubmission Map(Documentation documentation)
        {
            var docs = documentation.RequiredDocuments.Where(d => d.Status == "Received");

            return new ApplicationSubmission()
            {
                ApplicationSource = USB.International.Domain.Models.ApplicationSource.Documentation,
                DocumentExclusionKeys = documentation.ExcludedDocumentIds ?? new List<int>(),
                DocumentUploadKeys = docs.Select(d => d.DocumentId).ToList(),
                DocumentUploadNames = docs.Select(d => d.Description ?? string.Empty).ToList(),
            };
        }

        public static ApplicationSubmission Map(PaymentDetails paymentDetails)
        {
            return new ApplicationSubmission() { };
        }

    }
}
