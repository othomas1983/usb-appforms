﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.UsbInternational.Integration.Crm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Controller.Dto.Enums;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {
        //Update existing app
        public static ApplicationSubmission MapUpdate(PersonalContactDetails personalContactDetails,
                                               ApplicationSubmission existingApp)
        {
            existingApp.ContactNumber = personalContactDetails.HomePhoneNumber;
            existingApp.CorrespondanceLanguage = personalContactDetails.SelectedCorrespondanceLanguage;
            existingApp.DateOfBirth = personalContactDetails.DateOfBirth;
            existingApp.Disability = personalContactDetails.SelectedDisability;
            existingApp.Ethnicity = personalContactDetails.SelectedEthnicity;
            existingApp.FirstNames = personalContactDetails.FirstNames;
            existingApp.FaxNumber = personalContactDetails.FaxNumber;
            existingApp.ForeignIdExpiryDate = personalContactDetails.ForeignIdExpiryDate;
            existingApp.ForeignIdNumber = personalContactDetails.ForeignNumber;
            existingApp.ForeignIdType = personalContactDetails.SelectedForeignIdentificationType;
            existingApp.Gender = personalContactDetails.SelectedGender;
            existingApp.IdNumber = personalContactDetails.IdNumber;
            existingApp.Initials = personalContactDetails.Initials;
            existingApp.Language = personalContactDetails.SelectedLanguage;
            existingApp.MaritalStatus = personalContactDetails.SelectedMaritalStatus;
            existingApp.MobileNumber = personalContactDetails.MobileNumber;
            existingApp.Nationality = personalContactDetails.SelectedNationality;
            existingApp.CountryOfIssue = personalContactDetails.SelectedCountryOfIssue;
            existingApp.PassportExpiryDate = personalContactDetails.ForeignIdExpiryDate;
            existingApp.PassportNumber = personalContactDetails.PassportNumber;
            existingApp.PermitType = personalContactDetails.SelectedPermitType;
            existingApp.Surname = personalContactDetails.Surname;

            existingApp.Title = personalContactDetails.SelectedTitle;
            existingApp.WorkNumber = personalContactDetails.BusinessPhoneNumber;

            //var dummyDate = new DateTime();

            //if (DateTime.TryParse(personalContactDetails.DateOfBirth, out dummyDate))
            //{
            //    existingApp.DateOfBirth = dummyDate;
            //}
            //if (DateTime.TryParse(personalContactDetails.ForeignIdExpiryDate, out dummyDate))
            //{
            //    existingApp.ForeignIdExpiryDate = dummyDate;
            //    existingApp.PassportExpiryDate = dummyDate;
            //}

            return existingApp;
        }

        public static ApplicationSubmission MapUpdate(AddressDetails addressDetails,
                                               ApplicationSubmission existingApp)
        {
            existingApp.ResidentialCity = addressDetails.ResidentialCity;
            existingApp.ResidentialCountry = addressDetails.ResidentialCountry;
            existingApp.ResidentialPostalCode = addressDetails.ResidentialPostalCode;
            existingApp.ResidentialStreet1 = addressDetails.ResidentialStreet1;
            existingApp.ResidentialStreet2 = addressDetails.ResidentialStreet2;
            existingApp.ResidentialStreet3 = addressDetails.ResidentialStreet3;
            existingApp.ResidentialValidUntil = addressDetails.ResidentialValidUntil;
            existingApp.EmergencyContactTitle = addressDetails.EmergencyContactTitle;
            existingApp.EmergencyContactInitals = addressDetails.EmergencyContactInitals;
            existingApp.EmergencyContactName = addressDetails.EmergencyContactName;
            existingApp.EmergencyContactSurname = addressDetails.EmergencyContactSurname;
            existingApp.EmergencyContactRelationship = addressDetails.EmergencyContactRelationship;
            existingApp.EmergencyContactCountry = addressDetails.EmergencyContactCountry;
            existingApp.EmergencyContactStreet1 = addressDetails.EmergencyContactStreet1;
            existingApp.EmergencyContactStreet2 = addressDetails.EmergencyContactStreet2;
            existingApp.EmergencyContactStreet3 = addressDetails.EmergencyContactStreet3;
            existingApp.EmergencyContactPostalCode = addressDetails.EmergencyContactPostalCode;
            existingApp.EmergencyContactTelephone = addressDetails.EmergencyContactTelephone;
            existingApp.EmergencyContactEmail = addressDetails.EmergencyContactEmail;

            return existingApp;

        }

        public static ApplicationSubmission MapUpdate(Workstudies workStudies,
                                               ApplicationSubmission existingApp)
        {
            existingApp.HomeInstitution = workStudies.HomeInstitution;
            existingApp.PrincipleFieldOfStudy = workStudies.PrincipleFieldOfStudy;
            existingApp.YearsCompletedAtHomeInstitution = workStudies.YearsCompletedAtHomeInstitution;
            existingApp.DurationOfExchange = workStudies.DurationOfExchange;
            existingApp.Qualification = workStudies.TertiaryEducationHistory;
            existingApp.EmploymentHistory = workStudies.EmploymentHistory;
            existingApp.YearsOfWorkingExperience = workStudies.YearsOfWorkingExperience;
            existingApp.ContactPersonName = workStudies.ContactPersonName;
            existingApp.ContactPersonEmailAddress = workStudies.ContactPersonEmailAddress;

            return existingApp;
        }

        public static ApplicationSubmission MapUpdate(MarketingModel marketing,
                                               ApplicationSubmission existingApp)
        {
            existingApp.infoSessionAttended = marketing.infoSessionAttended;
            existingApp.checkboxAdvertisement = marketing.checkboxAdvertisement;
            existingApp.checkboxNewsArticle = marketing.checkboxNewsArticle;
            existingApp.checkboxUsbPromotionalEmails = marketing.checkboxUsbPromotionalEmails;
            existingApp.checkboxConferenceOrExpo = marketing.checkboxConferenceOrExpo;
            existingApp.checkboxReferralByMyEmployer = marketing.checkboxReferralByMyEmployer;
            existingApp.checkboxStellenboschUniversity = marketing.checkboxStellenboschUniversity;
            existingApp.checkboxReferralByAnAlumnus = marketing.checkboxReferralByAnAlumnus;
            existingApp.checkboxUsbWebsite = marketing.checkboxUsbWebsite;
            existingApp.checkboxReferralByACurrentStudent = marketing.checkboxReferralByACurrentStudent;
            existingApp.checkboxUsbEd = marketing.checkboxUsbEd;
            existingApp.checkboxReferralByAFamilyMember = marketing.checkboxReferralByAFamilyMember;
            existingApp.checkboxBrochure = marketing.checkboxBrochure;
            existingApp.checkboxSocialMedia = marketing.checkboxSocialMedia;
            existingApp.checkboxOnCampusEvent = marketing.checkboxOnCampusEvent;

            existingApp.radCareerChange = marketing.radCareerChange;
            existingApp.radCareerProgress = marketing.radCareerProgress;
            existingApp.radManageSkills = marketing.radManageSkills;
            existingApp.radDevelopLeader = marketing.radDevelopLeader;
            existingApp.radBusinessKnowledge = marketing.radBusinessKnowledge;
            existingApp.radCriticalThinking = marketing.radCriticalThinking;
            existingApp.radEthicalLeader = marketing.radEthicalLeader;
            existingApp.radSalary = marketing.radSalary;
            existingApp.radKnowBusinessStrategy = marketing.radKnowBusinessStrategy;
            existingApp.radLecturerQuality = marketing.radLecturerQuality;
            existingApp.radElectronicMaterial = marketing.radElectronicMaterial;
            existingApp.radPrintedStudyMaterial = marketing.radPrintedStudyMaterial;
            existingApp.radICTSupport = marketing.radICTSupport;
            existingApp.radCareerService = marketing.radCareerService;
            existingApp.radLibraryService = marketing.radLibraryService;
            existingApp.radAcademicSupport = marketing.radAcademicSupport;
            existingApp.radAvailabilityMentors = marketing.radAvailabilityMentors;
            existingApp.radCampusSecurity = marketing.radCampusSecurity;
            existingApp.radCateringServices = marketing.radCateringServices;
            existingApp.radAdministrativeSupport = marketing.radAdministrativeSupport;
            existingApp.radOffCampusSupport = marketing.radOffCampusSupport;
            existingApp.radDiverseClassGroup = marketing.radDiverseClassGroup;
            existingApp.radDiverseFacultyComposition = marketing.radDiverseFacultyComposition;
            existingApp.radBlendedLearning = marketing.radBlendedLearning;
            existingApp.radClassroomDelivery = marketing.radClassroomDelivery;
            existingApp.chkTripleAccreditation = marketing.chkTripleAccreditation;
            existingApp.chkValueForMoney = marketing.chkValueForMoney;
            existingApp.chkRecommendedByAlumni = marketing.chkRecommendedByAlumni;
            existingApp.chkGeographicLocation = marketing.chkGeographicLocation;
            existingApp.chkExcellentReputation = marketing.chkExcellentReputation;
            existingApp.chkRecommendedByEmplyer = marketing.chkRecommendedByEmplyer;
            existingApp.chkHighQualityStudents = marketing.chkHighQualityStudents;
            existingApp.chkProgrammeFormat = marketing.chkProgrammeFormat;
            existingApp.chkNetworking = marketing.chkNetworking;
            existingApp.chkAfrica = marketing.chkAfrica;
            existingApp.chkLeadership = marketing.chkLeadership;
            existingApp.chkLearning = marketing.chkLearning;
            existingApp.chkLecturerElectives = marketing.chkLecturerElectives;
            existingApp.chkInternationalStudents = marketing.chkInternationalStudents;
            existingApp.chkDeliveryMode = marketing.chkDeliveryMode;
            return existingApp;
        }

    }
}