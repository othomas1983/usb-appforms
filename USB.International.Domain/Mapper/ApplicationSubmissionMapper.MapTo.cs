﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;

namespace StellenboschUniversity.UsbInternational.Integration.Crm
{
    public partial class ApplicationSubmissionMapper
    {

        #region Enrollment Mapping Region

        public static usb_StudentAcademicRecord MapToEnrollment(ApplicationSubmission application)
        {
            switch (application.ApplicationSource)
            {
                case USB.International.Domain.Models.ApplicationSource.ContactDetails:
                    return MapPersonalDetailsToEnrollment(application);

                case USB.International.Domain.Models.ApplicationSource.AddressDetails:
                    return new usb_StudentAcademicRecord();
                    
                case USB.International.Domain.Models.ApplicationSource.WorkStudies:
                    return MapWorkStudiesToEnrollment(application);

                case USB.International.Domain.Models.ApplicationSource.Documentation:
                    return MapDocumentsToEnrollment(application);

                case USB.International.Domain.Models.ApplicationSource.Payment:
                    return new usb_StudentAcademicRecord();

                case USB.International.Domain.Models.ApplicationSource.Status:
                    return new usb_StudentAcademicRecord();

                default: return null;
            }
        }

        #endregion

        #region Personal Detail Mapping
        private static usb_StudentAcademicRecord MapPersonalDetailsToEnrollment(ApplicationSubmission application)
        {
            return new usb_StudentAcademicRecord()
            {
                //usb_name = application.GivenName + " " + application.Surname,
                //usb_FirstName = application.FirstNames,
                //usb_LastName = application.Surname
            };
        }
        #endregion

        #region Work Studies Mapping
        private static usb_StudentAcademicRecord MapWorkStudiesToEnrollment(ApplicationSubmission application)
        {
            return new usb_StudentAcademicRecord()
            {
                //usb_RPLCandidateTwoOptions = application.IsRplCandidate,

                //usb_AfrikaansLeesOptionset = application.AfrikaansProficiencyRead,
                //usb_AfrikaansPraatOptionset = application.AfrikaansProficiencyTalk,
                //usb_AfrikaansSkryfOptionset = application.AfrikaansProficiencyWrite,
                //usb_AfrikaansVerstaanOptionset = application.AfrikaansProficiencyUnderstand,

                //usb_EnglishReadOptionset = application.EnglishProficiencyRead,
                //usb_EnglishSpeakOptionset = application.EnglishProficiencyTalk,
                //usb_EnglishUnderstandOptionset = application.EnglishProficiencyUnderstand,
                //usb_EnglishWriteOptionset = application.EnglishProficiencyWrite,

                ////Financial Assistance
                //usb_EmployerassistfinanciallyOptionset = application.EmployerAssistFinancially,
                //usb_EmployerName = application.PaymentContactPerson,
                //usb_EmployerTelephone = application.PaymentContactNumber,
                //usb_EmployerEmail = application.PaymentContactEmail,

                //usb_SalaryOptionset = application.AnnualSalary,
                //usb_WhatDidYouDoLastYearOptionset = application.usb_WhatDidYouDoLastYearOptionset,
                //usb_TotalYearsWorkingExperience = application.TotalYearsWorkingExperience,

                //usb_HighestLevelMathematicsCompetedOptionset = application.MathematicsCompetency,
                //usb_SymbolAchieved = application.MathematicsPercentage,

                //usb_Referee1_Cellphone = application.usb_Referee1_Cellphone,
                //usb_Referee1_Email = application.usb_Referee1_Email,
                //usb_Referee1_Name = application.usb_Referee1_Name,
                //usb_Referee1_Position = application.usb_Referee1_Position,
                //usb_Referee1_Telephone = application.usb_Referee1_Telephone,

                //usb_Referee2_Cellphone = application.usb_Referee2_Cellphone,
                //usb_Referee2_Email = application.usb_Referee2_Email,
                //usb_Referee2_Name = application.usb_Referee2_Name,
                //usb_Referee2_Position = application.usb_Referee2_Position,
                //usb_Referee2_Telephone = application.usb_Referee2_Telephone,
            };
        }
        #endregion

        #region Documents Mapping

        private static usb_StudentAcademicRecord MapDocumentsToEnrollment(ApplicationSubmission application)
        {
            return new usb_StudentAcademicRecord()
            {
                //usb_DocumentsCompletedTwoOptions = application.DocumentsCompleteTwoOptions
            };
        }

        #endregion
    }
}
