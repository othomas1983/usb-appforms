﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class MarketingModel
    {
        public bool checkboxAdvertisement { get; set; }
        public bool checkboxNewsArticle { get; set; }
        public bool checkboxUsbPromotionalEmails { get; set; }
        public bool checkboxReferralByMyEmployer { get; set; }
        public bool checkboxReferralByAnAlumnus { get; set; }
        public bool checkboxReferralByACurrentStudent { get; set; }
        public bool checkboxReferralByAFamilyMember { get; set; }
        public bool checkboxSocialMedia { get; set; }
        public bool checkboxConferenceOrExpo { get; set; }
        public bool checkboxStellenboschUniversity { get; set; }
        public bool checkboxUsbWebsite { get; set; }
        public bool checkboxUsbEd { get; set; }
        public bool checkboxBrochure { get; set; }
        public bool checkboxOnCampusEvent { get; set; }

        public bool infoSessionAttended { get; set; }
        public int? infoSessionOptionSet { get; set; }

        public bool radWebSearch { get; set; }

        public int? radCareerChange { get; set; }
        public int? radCareerProgress { get; set; }
        public int? radManageSkills { get; set; }



        public int? radDevelopLeader { get; set; }
        public int? radBusinessKnowledge { get; set; }
        public int? radCriticalThinking { get; set; }
        public int? radEthicalLeader { get; set; }
        public int? radSalary { get; set; }
        public int? radKnowBusinessStrategy { get; set; }

        public int? radLecturerQuality { get; set; }
        public int? radElectronicMaterial { get; set; }
        public int? radPrintedStudyMaterial { get; set; }
        public int? radResearchSupervision { get; set; }
        public int? radICTSupport { get; set; }
        public int? radCareerService { get; set; }
        public int? radLibraryService { get; set; }
        public int? radAcademicSupport { get; set; }
        public int? radAvailabilityMentors { get; set; }
        public int? radCampusSecurity { get; set; }
        public int? radCateringServices { get; set; }
        public int? radAdministrativeSupport { get; set; }
        public int? radOffCampusSupport { get; set; }
        public int? radDiverseClassGroup { get; set; }
        public int? radDiverseFacultyComposition { get; set; }
        public int? radBlendedLearning { get; set; }
        public int? radClassroomDelivery { get; set; }

        public bool chkTripleAccreditation { get; set; }
        public bool chkValueForMoney { get; set; }
        public bool chkRecommendedByAlumni { get; set; }
        public bool chkGeographicLocation { get; set; }
        public bool chkExcellentReputation { get; set; }
        public bool chkRecommendedByEmplyer { get; set; }
        public bool chkHighQualityStudents { get; set; }
        public bool chkProgrammeFormat { get; set; }
        public bool chkNetworking { get; set; }
        public bool chkAfrica { get; set; }
        public bool chkLeadership { get; set; }
        public bool chkLearning { get; set; }
        public bool chkMatie { get; set; }
        public bool chkLecturerElectives { get; set; }
        public bool chkInternationalStudents { get; set; }
        public bool chkDeliveryMode { get; set; }

        #region MPhil/PGDip DevFin
        public int? radSpecialistSkills { get; set; }
        public int? radSustainableDev { get; set; }
        public int? radEmergingMarket { get; set; }
        public int? radDevSAEconomy { get; set; }
        public int? radManageMultiNationalBusiness { get; set; }
        public int? radStrategicDecisions { get; set; }
        public int? radInsightPoliciesProgrammes { get; set; }
        public int? radDevelopBusinessLeader { get; set; }
        #endregion

        #region MPhil/PGDip Future Studies
        public int? radFuturist { get; set; }
        public int? radUniqueProgram { get; set; }
        public int? radComplexEnviroment { get; set; }
        public int? radGrowthSAorContinent { get; set; }
        public int? radUnderstandFutures { get; set; }
        public int? radDeveloFutureModel { get; set; }
        public int? radAlignSkills { get; set; }
        public int? radSustainFuture { get; set; }
        public int? radScenarioPlanning { get; set; }
        #endregion

        #region MPhil Mangement Couching
        public int? radDevelopMyself { get; set; }
        public int? radEnhanceCouchingSkills { get; set; }
        public int? radTheoreticalUnderstandingCouching { get; set; }
        public int? radStartOwnCoachingPractice { get; set; }
        public int? radPersonalisedCoachingModel { get; set; }
        public int? radDevelopmentOthers { get; set; }
        public int? radCommunityOfProfessionalCoaches { get; set; }
        #endregion

        #region PGDip BMA

        public int? radAcquireEntrKnowledge { get; set; }
        public int? radInnovative { get; set; }
        public int? radDigitalSkills { get; set; }
        public int? radProgressToMBA { get; set; }

        #endregion

        #region PGDipLeadership

        public int? radAuthenticLeadershipStyle { get; set; }
        public int? radMakeADifference { get; set; }
        public int? radHowToBeALeaderInSAContext { get; set; }
        public int? radSustainableTransformation { get; set; }

        #endregion

        #region PGDip Project Management

        public int? radProjectManagementSkills { get; set; }
        public int? radManageProjects { get; set; }
        public int? radScarceSkill { get; set; }

        #endregion
    }
}