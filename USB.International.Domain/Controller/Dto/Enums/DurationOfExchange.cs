﻿namespace USB.International.Domain.Controller.Dto.Enums
{
    public enum DurationOfExchange : int
    {
        JanuaryJune = 1,
        JulyNovember = 2
    }
}
