﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public enum IdentificationType : int
    {
        IDNumber = 1,
        PassportNumber = 2,
        ForeignIdentificationNumber = 3

    }
}
