﻿namespace USB.International.Domain.Controller.Dto.Enums
{
    public enum ProgrammeOfferingType
    {
        Proposed = 1,
        Inactive = 2,
        Launched = 864480000,
        Closed = 864480001
    }
}
