﻿namespace USB.International.Domain.Controller.Dto.Enums
{
    public enum PrincipleFieldOfStudy : int
    {
        ArtsHumanities = 1,
        Commerce = 2,
        NaturalPhysicalScience = 3,
        Enigineering = 4,
        HealthSciences = 5,
        AgricultureForestry = 6,
        Law = 7,
        InformationTechnology = 8,
        Education = 9,
        Other = 10
    }
}
