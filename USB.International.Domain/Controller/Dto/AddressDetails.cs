﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto
{
    public class AddressDetails
    {
        public Guid? ResidentialCountry { get; set; }
        public string ResidentialStreet1 { get; set; }
        public string ResidentialStreet2 { get; set; }
        public string ResidentialStreet3 { get; set; }
        public string ResidentialCity { get; set; }
        //public string ResidentialSuburb { get; set; }
        //public string ResidentialStateProvince { get; set; }
        public string ResidentialPostalCode { get; set; }
        public Guid? ResidentialGisId { get; set; }
        public DateTime ResidentialValidUntil { get; set; }

        public Guid? EmergencyContactTitle { get; set; }
        public string EmergencyContactInitals { get; set; }
        public string EmergencyContactSurname { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactRelationship { get; set; }
        public Guid? EmergencyContactCountry { get; set; }
        public string EmergencyContactStreet1 { get; set; }
        public string EmergencyContactStreet2 { get; set; }
        public string EmergencyContactStreet3 { get; set; }
        public string EmergencyContactCityTown { get; set; }
        public string EmergencyContactPostalCode { get; set; }
        public string EmergencyContactTelephone { get; set; }
        public string EmergencyContactEmail { get; set; }
    }
}
