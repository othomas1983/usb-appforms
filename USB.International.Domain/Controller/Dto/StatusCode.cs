﻿using StellenboschUniversity.UsbInternational.Integration.Crm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.International.Domain.Controller.Dto
{
    public class SARStatusCode
    {
        public usb_studentacademicrecord_statuscode GetStatusCode(int code)
        {
            Type type = typeof(usb_studentacademicrecord_statuscode);
            //var value = Enum.GetName(type, (object)code);
            return (usb_studentacademicrecord_statuscode)Enum.Parse(type, "" + code);
        }
    }

}
