﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.SharePoint.Client;
using SettingsMan = USB.International.Domain.Properties.Settings;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;

namespace Usb.International.Domain.Sharepoint
{
    public static class SharepointActions
    {
        private static readonly ICredentials sharepointCredentials = new NetworkCredential(SettingsMan.Default.SharepointUsername, SettingsMan.Default.SharepointPassword, SettingsMan.Default.SharepointDomain);
        private static readonly string sharepointUrl = SettingsMan.Default.SharepointUrl;
        private static readonly string documentLibrary = SettingsMan.Default.SharepointLibrary;

        public static bool DocumentTypeExist(Guid documentTypeId, Guid enrollmentId)
        {
            try
            {
                ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials);

                string folderName = enrollmentId.ToString();

                Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                if (folder.IsNotNull())
                {
                    return fileExistsInFolderMatchingMetaTagValue(folder, clientContext,
                                                                  "DocumentTypeID", documentTypeId.ToString());
                }

                return false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static bool DocumentFileNameExist(Guid enrollmentId, string fileName, Guid documentTypeId)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    if (folder.IsNotNull())
                    {
                        return fileExistsInFolderMatchingMetaTagValue(folder, clientContext,
                                                                      "Title", fileName, documentTypeId);
                    }
                }

                return false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static string GetFullUrlToFile(Guid documentTypeId, Guid enrollmentId)
        {
            using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
            {
                string folderName = enrollmentId.ToString();

                Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                if (folder.IsNotNull())
                {
                    Microsoft.SharePoint.Client.File file =
                        getFileIfExists(folder, "DocumentTypeID", documentTypeId.ToString(), clientContext);

                    if (file.IsNotNull())
                    {
                        Web web = clientContext.Web;
                        Site site = clientContext.Site;

                        clientContext.Load(site, s => s, s => s.Url);
                        clientContext.ExecuteQuery();

                        return site.Url + file.ServerRelativeUrl;
                    }
                }
            }

            return null;
        }

        public static void UploadDocument(string documentTitle, string usNumber, int level,
                                          string programmeName, string documentTypeName,
                                          int documentId, Guid enrollmentId, byte[] documentData, string reviewed)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    documentTitle = USB.International.Domain.Tasks.RemoveSpecialCharacters.RemoveInvalidCharactersFromFileName(documentTitle);


                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    deleteFileFromFolderMatchingMetaTagValue(folder, clientContext, "ID", documentId.ToString());

                    Microsoft.SharePoint.Client.File file = createFile(folder, documentTitle, documentData, clientContext);

                    setFileMetaData(file, documentTitle, level, programmeName,
                                    documentTypeName, documentId.ToString(),
                                    usNumber, reviewed, clientContext);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static Tuple<string, byte[]> DownloadDocument(int documentId, Guid enrollmentId)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    return getFileData(folder, "ID", documentId.ToString(), clientContext);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static IDictionary<string, object> GetItemMetaData(Guid documentTypeId, Guid enrollmentId)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    Microsoft.SharePoint.Client.File file = getFileIfExists(folder, "DocumentTypeID", documentTypeId.ToString(), clientContext);

                    if (file.IsNotNull())
                    {
                        return getFileMetaData(file, documentTypeId.ToString(), clientContext);
                    }
                    else return null;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static IDictionary<string, object> GetItemMetaDataById(int documentId, Guid enrollmentId)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    Microsoft.SharePoint.Client.File file = getFileIfExists(folder, "ID", documentId.ToString(), clientContext);

                    if (file.IsNotNull())
                    {
                        return getFileMetaData(file, documentId.ToString(), clientContext);
                    }
                    else return null;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static string GetFileName(Guid documentTypeId, Guid enrollmentId)
        {
            try
            {
                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    string folderName = enrollmentId.ToString();

                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    Microsoft.SharePoint.Client.File file = getFileIfExists(folder, "DocumentTypeID", documentTypeId.ToString(), clientContext);

                    if (file.IsNotNull())
                    {
                        return file.Name;
                    }
                    else return null;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static void DeleteDocument(int documentId, Guid enrollmentId)
        {
            try
            {
                string folderName = enrollmentId.ToString();

                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    Folder newFolder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    // If exists find first and delete
                    deleteFileFromFolderMatchingMetaTagValue(newFolder, clientContext, "ID", documentId.ToString());
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static void DeleteDocument(Guid documentTypeId, Guid enrollmentId)
        {
            try
            {
                string folderName = enrollmentId.ToString();

                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    Folder newFolder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    // If exists find first and delete
                    deleteFileFromFolderMatchingMetaTagValue(newFolder, clientContext, "DocumentTypeID", documentTypeId.ToString());
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static void SetFileReviewedMetdataTag(int documentId, Guid enrollmentId, bool reviewed)
        {
            try
            {
                string folderName = enrollmentId.ToString();

                using (ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials))
                {
                    Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                    Microsoft.SharePoint.Client.File file =
                        getFileIfExists(folder, "ID", documentId.ToString(), clientContext);

                    setFileReviewedMetaDataTag(file, reviewed ? "yes" : "no", clientContext);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        public static List<Document> GetSharePointUpoadedDocuments(Guid enrolmentId)
        {
            try
            {
                ClientContext clientContext = getClientContext(sharepointUrl, sharepointCredentials);

                string folderName = enrolmentId.ToString();

                Folder folder = getEnrolmentFolder(folderName, documentLibrary, clientContext);

                if (folder.IsNotNull())
                {
                    return GetAllDocumentsInFolder(folder, clientContext);
                }

                return null;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

        private static ClientContext getClientContext(string sharepointUrl, ICredentials credentials)
        {
            ClientContext clientContext = new ClientContext(sharepointUrl);
            clientContext.AuthenticationMode = ClientAuthenticationMode.Default;
            clientContext.Credentials = credentials;

            return clientContext;
        }

        private static Folder getEnrolmentFolder(string folderName, string documentLibrary,
                                         ClientContext clientContext)
        {
            Web web = clientContext.Web;

            var list = web.Lists.GetByTitle(documentLibrary);

            Folder newFolder = list.RootFolder.Folders.Add(folderName);

            if (newFolder.IsNotNull())
            {
                clientContext.Load(newFolder);
                clientContext.ExecuteQuery();
            }

            return newFolder;
        }

        private static void deleteFileFromFolderMatchingMetaTagValue(Folder folder, ClientContext clientContext,
                                                                     string metaTagName, string metaTagValue)
        {
            if (folder.ItemCount > 0)
            {
                FileCollection fileCollection = folder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                Microsoft.SharePoint.Client.File fileTarget = null;

                foreach (Microsoft.SharePoint.Client.File file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields,
                                             f => f.ListItemAllFields[metaTagName]);
                    clientContext.ExecuteQuery();

                    ListItem item = file.ListItemAllFields;

                    if (String.Equals(metaTagValue, item[metaTagName].AsString(), StringComparison.Ordinal))
                    {
                        fileTarget = file;
                        break;
                    }
                }

                if (fileTarget != null)
                {
                    fileTarget.DeleteObject();
                }
            }

            clientContext.ExecuteQuery();
        }

        private static Microsoft.SharePoint.Client.File createFile(Folder parentFolder, string fileName, byte[] documentData, ClientContext clientContext)
        {
            FileCreationInformation fileCreationInformation = new FileCreationInformation();
            fileCreationInformation.Content = documentData;
            fileCreationInformation.Url = fileName;
            fileCreationInformation.Overwrite = true;


            Microsoft.SharePoint.Client.File file = parentFolder.Files.Add(fileCreationInformation);
            clientContext.Load(file);
            clientContext.ExecuteQuery();

            return file;
        }

        private static Tuple<string, byte[]> getFileData(Folder parentFolder,
                                            string metaTagName, string metaTagValue,
                                            ClientContext clientContext)
        {
            Microsoft.SharePoint.Client.File file =
                getFileIfExists(parentFolder, metaTagName, metaTagValue, clientContext);

            if (file.IsNotNull())
            {
                string fileRef = getFileRef(file, clientContext);

                FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, fileRef);

                using (var memory = new MemoryStream())
                {
                    byte[] buffer = new byte[1024 * 64];
                    int nread = 0;
                    while ((nread = fileInfo.Stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        memory.Write(buffer, 0, nread);
                    }
                    memory.Seek(0, SeekOrigin.Begin);

                    return new Tuple<string, byte[]>(file.Name, memory.ToArray());
                }
            }

            return null;
        }

        private static Microsoft.SharePoint.Client.File getFileIfExists(Folder parentFolder,
                                                                        string metaTagName, string metaTagValue,
                                                                        ClientContext clientContext)
        {
            if (parentFolder.ItemCount > 0)
            {
                FileCollection fileCollection = parentFolder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                foreach (Microsoft.SharePoint.Client.File file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields,
                                             f => f.ListItemAllFields[metaTagName]);
                    clientContext.ExecuteQuery();

                    ListItem item = file.ListItemAllFields;

                    if (String.Equals(metaTagValue, item[metaTagName].AsString(), StringComparison.Ordinal))
                    {
                        return file;
                    }
                }
            }

            return null;
        }


        private static string getFileRef(Microsoft.SharePoint.Client.File file, ClientContext clientContext)
        {

            ListItem item = file.ListItemAllFields;

            clientContext.Load(item, i => i, i => i["FileRef"]);
            clientContext.ExecuteQuery();

            return (string)item["FileRef"];
        }

        private static void setFileMetaData(Microsoft.SharePoint.Client.File file, string documentTitle, int level,
                                            string programmeName, string documentTypeName, string documentTypeId,
                                            string usNumber, string reviewedYesNo, ClientContext clientContext)
        {
            ListItem item = file.ListItemAllFields;

            item["Title"] = documentTitle;
            item["Programme"] = programmeName;
            item["DocumentType"] = documentTypeName;
            item["DocumentTypeID"] = documentTypeId.ToString();
            item["StudentNumber"] = usNumber;
            item["Reviewed"] = reviewedYesNo;
            item["Level"] = level;

            item.Update();

            clientContext.ExecuteQuery();
        }

        private static void setFileReviewedMetaDataTag(Microsoft.SharePoint.Client.File file, string reviewedYesNo,
                                                       ClientContext clientContext)
        {
            ListItem item = file.ListItemAllFields;

            item["Reviewed"] = reviewedYesNo;

            item.Update();

            clientContext.ExecuteQuery();
        }

        private static IDictionary<string, object> getFileMetaData(Microsoft.SharePoint.Client.File file, string documentTypeId,
                                            ClientContext clientContext)
        {
            ListItem item = file.ListItemAllFields;

            return item.FieldValues;
        }

        private static bool fileExistsInFolderMatchingMetaTagValue(Folder folder, ClientContext clientContext,
                                                                   string metaTagName, string metaTagValue, Guid? documentTypeId = null)
        {
            if (folder.ItemCount > 0)
            {
                FileCollection fileCollection = folder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                foreach (Microsoft.SharePoint.Client.File file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields,
                                             f => f.ListItemAllFields[metaTagName]);
                    clientContext.ExecuteQuery();

                    ListItem item = file.ListItemAllFields;

                    if (String.Equals(metaTagValue, item[metaTagName].AsString(), StringComparison.Ordinal) &&
                        !((documentTypeId != null) && String.Equals(documentTypeId.AsString(), item["DocumentTypeID"].AsString(), StringComparison.Ordinal)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static List<Document> GetAllDocumentsInFolder(Folder folder, ClientContext clientContext)
        {
            if (folder.ItemCount > 0)
            {
                FileCollection fileCollection = folder.Files;
                clientContext.Load(fileCollection);
                clientContext.ExecuteQuery();

                List<Document> uploadedDocuments = new List<Document>();

                foreach (Microsoft.SharePoint.Client.File file in fileCollection)
                {
                    clientContext.Load(file, f => f, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();

                    ListItem item = file.ListItemAllFields;

                    uploadedDocuments.Add(new Document()
                    {
                        DocumentId = Convert.ToInt32(item["ID"]),
                        DocumentTypeId = Guid.Empty,//item.FieldValues.ContainsKey("DocumentTypeID") ? Guid.Parse(item["DocumentTypeID"].ToString()) : Guid.Empty,
                        Description = item["DocumentType"].AsString(),
                        FileName = item["Title"].AsString(),
                        UploadedDate = ((DateTime)item["Modified"]).ToString("dd/MM/yyyy"),
                        Status = "Received",
                        Reviewed = (item["Reviewed"].AsString().ToLower() == "yes"),
                        Level = item["Level"].IsNull() ? 0 : int.Parse(item["Level"].AsString())
                    });
                }

                return uploadedDocuments;
            }

            return null;
        }
    }
}
