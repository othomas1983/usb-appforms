﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using SettingsMan = USB.International.Domain.Properties.Settings;

namespace StellenboschUniversity.UsbInternational.Integration.Sis.Dto
{
    public class Applicant
    {
        public string Year
        {
            get;
            set;
        }

        public string ExistingStudentNumber
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Surname
        {
            get;
            set;
        }

        public string FirstName1
        {
            get;
            set;
        }

        public string FirstName2
        {
            get;
            set;
        }

        public string FirstName3
        {
            get;
            set;
        }

        public string FirstName4
        {
            get;
            set;
        }

        public string FirstName5
        {
            get;
            set;
        }

        public string Initials
        {
            get;
            set;
        }

        public DateTime DateOfBirth
        {
            get;
            set;
        }

        public string CorrespondenceLanguage
        {
            get;
            set;
        }

        public string Gender
        {
            get;
            set;
        }

        public string Ethnicity
        {
            get;
            set;
        }

        public string Nationality
        {
            get;
            set;
        }

        public string IdNumber
        {
            get;
            set;
        }

        public string EmailAddress
        {
            get;
            set;
        }

        public string CellphoneNumber
        {
            get;
            set;
        }

        public Response CreateApplicant(Applicant applicant)
        {
            string baseUri = SettingsMan.Default.SISEndpoint;

            try
            {
                var json = JsonConvert.SerializeObject(applicant);
                var restClient = new RestClient(baseUri);
                var request = new RestRequest("Application/CreateApplicant", Method.POST);
                request.AddParameter("Application/Json", json, ParameterType.RequestBody);
                var response = restClient.Execute(request).Content;

                var applicantResponse = JsonConvert.DeserializeObject<Response>(response);

                return applicantResponse;

            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
