﻿using Newtonsoft.Json;
using RestSharp;
using StellenboschUniversity.UsbInternational.Integration.Sis.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SettingsMan = USB.International.Domain.Properties.Settings;

namespace USB.International.Domain.Models
{
    public class Person
    {
        public string CellphoneNumber { get; set; }
        public string CorrespondenceLanguage { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Disability { get; set; }
        public string EmailAddress { get; set; }
        public string Ethnicity { get; set; }
        public string FaxNumber { get; set; }
        public string FirstNames { get; set; }
        public int? FirstYearOfRegistration { get; set; }
        public DateTime? ForeignIdExpiryDate { get; set; }
        public string ForeignIdNumber { get; set; }
        public string Gender { get; set; }
        public string GivenName { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string Initials { get; set; }
        public string Language { get; set; }
        public string MaidenName { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string OfficialUsEmailAddress { get; set; }
        public string PassportCountryOfIssue { get; set; }
        public DateTime? PassportExpiryDate { get; set; }
        public string PassportNumber { get; set; }
        public string PostalAddressAfrigisCode { get; set; }
        public string PostalAddressCity { get; set; }
        public string PostalAddressCountry { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public string PostalAddressLine3 { get; set; }
        public string PostalAddressPostalCode { get; set; }
        public string PostalAddressStateOrProvince { get; set; }
        public string PostalAddressSuburb { get; set; }
        public string SouthAfricanIdNumber { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
        public string UsNumber { get; set; }
        public string WorkTelephoneNumber { get; set; }



        public Response CreateApplicant(Person person)
        {
            string baseUri = SettingsMan.Default.SISEndpoint;

            try
            {
                var json = JsonConvert.SerializeObject(person);

                var webClient = new WebClient();
                webClient.Headers.Add("Content-Type", "application/json");
                webClient.Encoding = Encoding.UTF8;

                var response = webClient.UploadString(baseUri, json);

                var applicantResponse = JsonConvert.DeserializeObject<Response>(response);

                return applicantResponse;
            }
            catch (Exception ex)
            {
                return new Response {Success = false, Message = ex.Message};
            }
        }
    }
}