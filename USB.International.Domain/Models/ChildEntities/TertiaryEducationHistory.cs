﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MBA.ApplicationForm.ViewModels
{
    public class TertiaryEducationHistory
    {
        public string Institution { get; set; }
        public string Degree { get; set; }
        public string FieldOfStudy { get; set; }
        public string StudentNumber { get; set; }
        public string Period { get; set; }
        public string Completed { get; set; }
        public string RadioButton { get; set; }
    }
}