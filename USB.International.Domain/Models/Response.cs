﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellenboschUniversity.UsbInternational.Integration.Sis.Dto
{
    public class Response
    {
        public bool Success { get; set; }

        public string UsNumber { get; set; }

        public string Message { get; set; }

        public bool ExistedAlready { get; set; }
    }
}
