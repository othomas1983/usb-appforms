﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;

namespace USB.PGDipFinancialPlanning.ApplicationForm.ViewModels
{
    public class DocumentationViewModel
    {
        public bool UploadGMatScores { get; set; }

        public List<Document> RequiredDocumentsViewModel { get; set; }
    }
}