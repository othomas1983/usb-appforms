﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;

namespace USB.International.ApplicationForm.ViewModels
{
    public class DocumentationViewModel
    {
        public bool UploadGMatScores { get; set; }

        public List<Document> RequiredDocumentsViewModel { get; set; }
    }
}