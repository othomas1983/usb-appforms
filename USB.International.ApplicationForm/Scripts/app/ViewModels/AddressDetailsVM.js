﻿/// <reference path="../../knockout-3.3.0.debug.js" />
/// <reference path="../../knockout.validation.js" />

var knockoutValidationSettings = {
    insertMessages: true,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};

ko.validation.configuration = knockoutValidationSettings;

function AddressDetails(isDisabled) {
    var self = this;

    var rootUrl = "/International/";

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    self.loading = ko.observableArray();

    self.loading.subscribe(function () {
        if (self.loading().length == 0) {
            // Last dropdown population async ajax call has ended

            // Fill in inputs if existing informantion exists
            self.loadExistingDetails();
        }
    });

    self.ShowExpiryDateInput = ko.observable(true);
    self.DisplayResidentialCityValid = ko.observable(true);
    self.DisplayResidentialPostalCodeValid = ko.observable(true);
    self.DisplayResidentialStateValid = ko.observable(true);


    self.ResidentialCountry = ko.observable();
    self.ResidentialStreet1 = ko.observable();
    self.ResidentialStreet2 = ko.observable();
    self.ResidentialStreet3 = ko.observable();
    self.ResidentialCity = ko.observable();
    self.ResidentialPostalCode = ko.observable();
    self.ResidentialValidUntil = ko.observable();

    // Observable arrays for dropdown options
    self.Titles = ko.observableArray();
    self.Countries = ko.observableArray();

    // Observables for input values
    self.EmergencyContactTitle = ko.observable();
    self.EmergencyContactInitals = ko.observable();
    self.EmergencyContactName = ko.observable();
    self.EmergencyContactSurname = ko.observable();
    self.EmergencyContactRelationship = ko.observable();
    self.EmergencyContactCountry = ko.observable();

    self.EmergencyContactStreet1 = ko.observable();

    self.EmergencyContactStreet2 = ko.observable();

    self.EmergencyContactStreet3 = ko.observable();

    self.EmergencyContactCityTown = ko.observable();

    self.EmergencyContactPostalCode = ko.observable();

    self.EmergencyContactTelephone = ko.observable();
    self.EmergencyContactEmail = ko.observable();

    //self.contactNumberLength = ko.computed(function () {
    //    if (self.EmergencyContactTelephone()) {
    //        return self.EmergencyContactTelephone().length;
    //    }
    //});

    self.serverErrorMessage = ko.observable();

    self.setRequiredFields = function () {

        self.ResidentialCountry.extend({ required: true });

        self.ResidentialStreet1.extend({
            required: true,
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            },
            minLength: 5
        });

        self.ResidentialStreet2.extend({
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            }
        });

        self.ResidentialStreet3.extend({
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            }
        });

        self.ResidentialCity.extend({ required: true });

        self.ResidentialPostalCode.extend({
             required: true,
             minLength: 4,
             number: true
        });

        self.ResidentialValidUntil.extend({ required: true });

        self.EmergencyContactTitle.extend({ required: true });
        self.EmergencyContactName.extend({ required: true });
        self.EmergencyContactSurname.extend({ required: true });
        self.EmergencyContactRelationship.extend({ required: true });
        self.EmergencyContactCountry.extend({ required: true });

        self.EmergencyContactStreet1.extend({
            required: true,
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            },
            minLength: 5
        });

        self.EmergencyContactStreet2.extend({
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            }
        });

        self.EmergencyContactStreet3.extend({
            pattern: {
                message: "Invalid address format.",
                params: /^[^<>&#,;]*$/m
            }
        });

        self.EmergencyContactCityTown.extend({ required: true });

        self.EmergencyContactPostalCode.extend({
             required: true,
             minLength: 4,
             number: true
        });

        self.EmergencyContactTelephone.extend({
            required: true,
            pattern: {
                message: "Invalid phone number.",
                params: /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
            }
        });

        self.EmergencyContactTelephone.subscribe(function (data) {

            if (data != undefined) {
                $("#HomePhoneId").intlTelInput("setNumber", data);
                var intlNumber = $("#HomePhoneId").intlTelInput("getNumber");

                self.EmergencyContactTelephone(intlNumber);

                if ($('#HomePhoneId').attr('placeholder') != undefined && intlNumber !== "") {
                    var numberLength = $('#HomePhoneId').attr('placeholder').replace(/\s/g, "").length;
                    if (self.EmergencyContactTelephone().length < numberLength) {
                        self.EmergencyContactTelephone.setError("Please enter at least " + numberLength + " characters.");
                        self.EmergencyContactTelephone.isModified(true);
                    } else if (!$("#HomePhoneId").intlTelInput("isValidNumber")) {
                        self.EmergencyContactTelephone.setError("Invalid number for selected country.");
                        self.EmergencyContactTelephone.isModified(true);
                    } else {
                        self.EmergencyContactTelephone.clearError();
                    }
                }
            }

        });

        $("#HomePhoneId")
            .intlTelInput({
                allowExtensions: true,
                autoHideDialCode: true,
                autoPlaceholder: true,
                nationalMode: false,
                numberType: "FIXED_LINE",
                preferredCountries: [],
                utilsScript: "/International/Scripts/utils.js"
            });

        $("#HomePhoneId")
            .on("countrychange",
                function (e, countryData) {
                    var extension = $("#HomePhoneId").intlTelInput("getExtension");
                    $("#HomePhoneId").intlTelInput("setNumber", "+" + extension);
                });





        self.EmergencyContactEmail.extend({
            required: {
                params: true,
                message: "Invalid email address"
            },
            email: true
        });
    };

    function loadDropDown(type) {

        self.loading.push(true);

        $.ajax({
            url: rootUrl + "AddressDropdown/LoadDropdown?type=" + type,
            type: 'GET',
            contentType: "application/json",
            cache: false,
            success: function (j) {
                populateDropDownList(j.dropdownList, type);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            self.loading.pop();
        });
    };

    function populateDropDownList(list, type) {
        switch (type) {
            case "countries":
                self.Countries(list);
                break;
                case "titles":
                    self.Titles(list);
            default:
        }
    }

    self.loadExistingDetails = function () {
        $.ajax({
            url: rootUrl + "Page/PopulateAddressDetails",
            type: 'POST',
            contentType: "application/json",
            cache: false,
            async: true,
            success: function (data) {

                if (data.EmergencyContactCityTown !== null) {
                    ko.mapping.fromJS(data, {}, self);
                    self.resolveJsonDates(data);
                }
                if (data.EmergencyContactTitle === "00000000-0000-0000-0000-000000000000") {
                    data.EmergencyContactTitle = null;
                }
                if (data.EmergencyContactCountry === "00000000-0000-0000-0000-000000000000") {
                    data.EmergencyContactCountry = null;
                }

            }
        }).always(function () {
            waitingDialog.hide();
        });
    };

    self.resolveJsonDates = function (data) {
        self.ResidentialValidUntil(self.formatDate(data.ResidentialValidUntil));
    }

    self.formatDate = function (jsonDate) {
        try {
            var date = new Date(parseInt(/^\/Date\((.*?)\)\/$/.exec(jsonDate)[1], 10));
            var day = String("00" + date.getDate()).slice(-2);
            var month = String("00" + (date.getMonth() + 1)).slice(-2);
            var year = date.getFullYear();

            var formatedDate = year + "/" + month + "/" + day;

            return formatedDate;
        } catch (e) {
            return jsonDate;
        }
    }

    var AddressDetails = {
        ResidentialCountry: self.ResidentialCountry,
        ResidentialStreet1: self.ResidentialStreet1,
        ResidentialStreet2: self.ResidentialStreet2,
        ResidentialStreet3: self.ResidentialStreet3,
        ResidentialCity: self.ResidentialCity,
        ResidentialStateProvince: self.ResidentialStateProvince,
        ResidentialPostalCode: self.ResidentialPostalCode,
        ResidentialGisId: self.ResidentialGisId,
        ResidentialValidUntil: self.ResidentialValidUntil,

        EmergencyContactTitle: self.EmergencyContactTitle,
        EmergencyContactInitals: self.EmergencyContactInitals,
        EmergencyContactSurname: self.EmergencyContactSurname,
        EmergencyContactName: self.EmergencyContactName,
        EmergencyContactRelationship: self.EmergencyContactRelationship,
        EmergencyContactCountry: self.EmergencyContactCountry,
        EmergencyContactStreet1: self.EmergencyContactStreet1,
        EmergencyContactStreet2: self.EmergencyContactStreet2,
        EmergencyContactStreet3: self.EmergencyContactStreet3,
        EmergencyContactCityTown: self.EmergencyContactCityTown,
        EmergencyContactPostalCode: self.EmergencyContactPostalCode,
        EmergencyContactTelephone: self.EmergencyContactTelephone,
        EmergencyContactEmail: self.EmergencyContactEmail
    }

    AddressDetails.errors = ko.validation.group(AddressDetails);

    //#region Change Warning symbols on text fields for Residental Address
    self.ResidentialCity.subscribe(function () {
        if (self.ResidentialCity() !== '') {
            self.DisplayResidentialCityValid(false);
        }
        else {
            self.DisplayResidentialCityValid(true);
        }
    });

    self.ResidentialPostalCode.subscribe(function () {
        if (self.ResidentialPostalCode() !== '') {
            self.DisplayResidentialPostalCodeValid(false);
        }
        else {
            self.DisplayResidentialPostalCodeValid(true);
        }
    });
    //#endregion

    $(function () {
        waitingDialog.show("Preparing Address Details Form");

        self.setRequiredFields();

        loadDropDown("countries");
        loadDropDown("titles");
    });

    self.StepTwoSaveAndProceed = function () {
        var data = ko.toJSON(AddressDetails);

        if (AddressDetails.errors().length === 0) {
            waitingDialog.show('Saving Address Details');
            $.ajax({
                url: rootUrl + "Page/SubmitAddressDetails",
                contentType: 'application/json; charset=utf-8',
                data: data,
                type: "POST",
                async: true,
                success: function (data) {
                    console.log(data);
                    if (data.Success == false) {
                        self.serverErrorMessage(data.Message);
                        $('#serverValidationAlert').show();
                        window.scroll(0, 0);
                    }
                    else {
                        window.location.href = rootUrl + "Page/WorkStudies";
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            }).done(function (response) {
                console.log(response);
            });
        }
        else {
            $('#requiredFieldsAlert').show();
            AddressDetails.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
    }

    ///Fix this function
    self.afrigisSearchClick = function (containerId) {
        var searchCriteria = $("#" + containerId + " .afrigisSearchBox .afrigisSearchInput").val();

        if ($.trim(searchCriteria).length > 0) {
            $("#" + containerId + " .afrigisSearchResultBox .afrigisSearchButton").attr("disabled", "disabled");

            $("#" + containerId + " .afrigisSearchBox .afrigisSearchSpinner").show();
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: rootUrl + "Afrigis/GetDistributionAreas?query=" + searchCriteria,
                data: AddAntiForgeryToken({ "query": searchCriteria }),
                contentType: 'application/json; charset=utf-8',
                error: function (x, e) {
                    alert("An error occurred while trying to load the data. Please try again later or contact your service administrator.");
                },
                success: function (data) {
                    $("#" + containerId + " .afrigisSearchBox .afrigisSearchButton").removeAttr("disabled");
                    $("#" + containerId + " .afrigisSearchBox .afrigisSearchSpinner").hide();
                    var listItems = data;
                    if (listItems.length > 0) {
                        $("#" + containerId + " .afrigisSearchBox .afrigisResultList").children().remove();

                        $.each(listItems, function (val, text) {
                            $("#" + containerId + " .afrigisSearchBox .afrigisResultList").append(
                                 $("<option></option>").val((text.Value || text.Value == 0) ? text.Value : val).html(text.Text)
                            );
                        });

                        //$("#" + containerId + " .afrigisSearchBox .afrigisResultList").show();
                        $("#" + containerId + " .afrigisSearchBox .afrigisErrorMessage").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").slideDown();
                    }
                    else {
                        $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisErrorMessage").show();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").hide();
                    }
                }
            });
        }
    }

    //Fix this to 
    self.afrigisResultChange = function (containerId) {
        var afrgisId = $("#" + containerId + " .afrigisSearchBox .afrigisResultList option:selected").val();
        var afrigisVal = $("#" + containerId + " .afrigisSearchBox .afrigisResultList option:selected").text();
        if (afrigisVal != "") {
            var afrigisArr = afrigisVal.split(",");

            var suburb = $.trim(afrigisArr[0]);
            var city = $.trim(afrigisArr[1]);
            var postalCode = $.trim(afrigisArr[2]);

            $("#" + containerId + " .afrigisHelpIcon").removeClass('afrigisIncorrectIcon').addClass('afrigisCorrectIcon');
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();
            $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").show();
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchInput").val('');

            switch (containerId) {
                case "residential":
                    {
                        self.ResidentialCity(city);
                        self.ResidentialSuburb(suburb);
                        self.ResidentialPostalCode(postalCode);
                        self.ResidentialGisId(afrgisId);
                        return;
                    }
                case "postal":
                    {
                        self.PostalCity(city);
                        self.PostalSuburb(suburb);
                        self.PostalPostalCode(postalCode);
                        self.PostalGisId(afrgisId);
                        return;
                    }
                case "business":
                    {
                        self.BusinessCity(city);
                        self.BusinessSuburb(suburb);
                        self.BusinessPostalCode(postalCode);
                        self.BusinessGisId(afrgisId);
                        return;
                    }
                case "billing":
                    {
                        self.BillingCity(city);
                        self.BillingSuburb(suburb);
                        self.BillingPostalCode(postalCode);
                        self.BillingGisId(afrgisId);
                        return;
                    }
            }
        }
    }
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });

    $("#addressValidUnit").keypress(function (event) { event.preventDefault(); });
    $(".datepicker").datepicker();

    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);

    var expiryDate = $('#addressValidUnit').datepicker({
        beforeShowDay: function (date) {
            return date.valueOf() >= today.valueOf();
        },
        autoclose: true
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > expiryDate.datepicker("getDate").valueOf() || !expiryDate.datepicker("getDate").valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            expiryDate.datepicker("update", newDate);
        }
    });
});