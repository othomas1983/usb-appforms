﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;

namespace USB.International.ApplicationForm.Controllers
{
    public class WorkStudiesDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "leesAfrikaans" :
                    dropdownList = controller.LeesAfrikaans().ToList();
                    break;
                case "skryfAfrikaans":
                    dropdownList = controller.SkryfAfrikaans().ToList();
                    break;
                case "praatAfrikaans":
                    dropdownList = controller.PraatAfrikaans().ToList();
                    break;
                case "verstaanAfrikaans":
                    dropdownList = controller.VerstaanAfrikaans().ToList();
                    break;
                case "talkEnglish":
                    dropdownList = controller.TalkEnglish().ToList();
                    break;
                case "writeEnglish":
                    dropdownList = controller.WriteEnglish().ToList();
                    break;
                case "readEnglish":
                    dropdownList = controller.ReadEnglish().ToList();
                    break;
                case "understandEnglish":
                    dropdownList = controller.UnderstandEnglish().ToList();
                    break;
                case "mathematicsCompetency":
                    dropdownList = controller.MathematicsCompetency().ToList();
                    break;
                case "mathematicsPercentage":
                    dropdownList = controller.MathematicsPercentage().ToList();
                    break;
                case "priorInvolvement":
                    dropdownList = controller.PriorInvolvement().ToList();
                    break;
                case "institution":
                    dropdownGuidList = controller.Institution().ToList();
                    break;
                case "fieldOfStudy":
                    dropdownList = controller.FieldOfStudy().ToList();
                    break;
                case "workArea":
                    dropdownList = controller.WorkAreas().ToList();
                    break;
                case "organization":
                    dropdownList = controller.Organization().ToList();
                    break;
                case "industry":
                    dropdownList = controller.Industry().ToList();
                    break;
                case "workarea":
                    dropdownList = controller.WorkAreas().ToList();
                    break;
                case "employerAssistFinancially":
                    dropdownList = controller.EmployerAssistFinancially().ToList();
                    break;
                case "employmentType":
                    dropdownList = controller.EmploymentType().ToList();
                    break;
                case "principleFieldOfStudy":
                    dropdownList = controller.FieldOfStudy().ToList();
                    break;
                case "durationOfExchange":
                    dropdownList = controller.DurationOfExchange().ToList();
                    break;
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new { list = dropdownGuidList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { list = dropdownList }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
