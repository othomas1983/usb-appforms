﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using SettingsMan = USB.International.ApplicationForm.Properties.Settings;

namespace USB.International.ApplicationForm.Controllers
{
    public class PersonalDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "gender":
                    dropdownGuidList = controller.Genders().ToList();
                    break;
                case "titles":
                    dropdownGuidList = controller.Titles().ToList();
                    break;
                case "ethnicites":
                    dropdownGuidList = controller.Ethnicities().ToList();
                    break;
                case "nonSAEthnicities":
                    dropdownGuidList = controller.NonSAEthnicities().ToList();
                    break;
                case "nationalities":
                    dropdownGuidList = controller.Nationalities().ToList();
                    break;
                case "countriesIssued":
                    dropdownGuidList = controller.Nationalities().ToList();
                    break;
                case "correspondenceLanguages":
                    dropdownGuidList = controller.CorrespondenceLanguages().ToList();
                    break;
                case "languages":
                    dropdownGuidList = controller.Languages().OrderBy(x => x.Key).ToList();
                    break;
                case "foreignIdentificationTypes":
                    dropdownList = controller.ForeignIdentificationTypes().ToList();
                    break;
                //case "southAfricaIdentificationTypes":
                //    dropdownList = controller.SouthAfricaIdentificationTypes().ToList();
                //    break;
                case "disabilities":
                    dropdownGuidList = controller.Disabilities().ToList();
                    break;
                case "addressCountries":
                    dropdownGuidList = controller.AddressCountries().ToList();
                    break;
                case "permitTypes":
                    dropdownGuidList = controller.PermitTypes().ToList();
                    break;
                case "maritalStatus":
                    dropdownGuidList = controller.MaritalStatus().ToList();
                    break;
                case "offerings":
                    var programmeGuid = SettingsMan.Default.ProgrammeGuid;
                    var isMBA = SettingsMan.Default.IsMBA;
                    dropdownGuidList = controller.Offerings(isMBA, programmeGuid).ToList();
                    break;
                case "programme":
                    dropdownGuidList = controller.Programme().ToList();
                    break;
                default:
                    break;
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new {list = dropdownGuidList}, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new {list = dropdownList}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
