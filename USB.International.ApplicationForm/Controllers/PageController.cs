﻿using NLog;
using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf;
using USB.International.ApplicationForm.Session;
using USB.International.ApplicationForm.ViewModels;
using SettingsMan = USB.International.ApplicationForm.Properties.Settings;
using Newtonsoft.Json;
using StellenboschUniversity.UsbInternational.Integration.Crm;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Models;
using USB.International.Domain.State;

namespace USB.International.ApplicationForm.Controllers
{

    public class PageController : BaseController
    {
        private const string Upload = "Upload";
        private const string EnrolmentGuidString = "EnrollmentGuid";
        private const string EmailString = "email";
        private const string UsNumberString = "UsNumber";
        private const string ContactIdString = "ContactId";
        private const string idParameter = "idParameter";
        private const string reviewIdParameter = "reviewIdParameter";
        private const string ProgrammeGuid = "programmeGuid";
        private const string webTokenGuid = "webTokenId";
        private const string applicantStatusString = "applicantStatus";
        private const string insideCRMString = "insideCRM";
        private const string appFormSubmittedString = "appFormSubmitted";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Home

        [HttpGet]
        public ActionResult Index()
        {
            Session.Clear();

            var iID = string.Empty;

            var id = HttpContext.Request["id"];
            Session[idParameter] = id;

            if (id.IsNotNullOrEmpty())
            {
                var controller = new ApplicationFormApiController();

                var webTokenId = HttpContext.Request[webTokenGuid];
                Session[webTokenGuid] = webTokenId;
                Session[EnrolmentGuidString] = new Guid(id);

                // Comment validation out for testing purposes
                if (webTokenId.IsNotNullOrEmpty()/* && controller.IsWebTokenValid(new Guid(webTokenId))*/)
                {
                    Session[insideCRMString] = true;
                }

                try
                {
                    var status = controller.GetApplicantStatus(new Guid(id));

                    if (status.IsNull())
                    {
                        var msg = "Application status not found." + Environment.NewLine + "Please contact the System Administrator";
                        logger.Error(string.Format(msg));
                        Session[EnrolmentGuidString] = null;

                        ViewBag.Message = msg;

                        return View("Error");
                    }
                    else
                    {
                        Session[applicantStatusString] = status;
                    }

                    Session[appFormSubmittedString] = controller.IsApplicationFormSubmitted(new Guid(id));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);

                    var msg = ex.Message;
                    ViewBag.Message = msg;

                    return View("Error");
                }

                SetViewBagVariables();

                return View("PersonalDetails");
            }


            Session[applicantStatusString] = ApplicantStatus.Applicant;
            return View();
        }

        #endregion

        #region Send Mail

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SendMail(string email)
        {
            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var usNumber = applicationFormApiController.GetStudentNumber(email);

                applicationFormApiController.ExecuteApplicationAcknowledgmentWorkFlow(usNumber);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Submit

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitIndex(string email)
        {
            logger.Info("we here in submit");
            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            if (email == "undefined")
            {
                ModelState.AddModelError("email", "eMail is a required field");
                return View("Index");
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                Session[EnrolmentGuidString] = null;
                Session[EmailString] = email;
                var usNumber = applicationFormApiController.GetStudentNumber(email);
                Session[UsNumberString] = usNumber;

                PersonalContactDetails personDetails = new PersonalContactDetails()
                {
                    eMail = email,
                    Status = null
                };

                if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty())
                {
                    string programmeType = "International";
                    personDetails = applicationFormApiController.GetPersonalDetails(usNumber.ToString());
                    Session[EnrolmentGuidString] = personDetails.SARId;
                }

                return Json(personDetails);

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitPersonalInformation(PersonalContactDetails model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }
            try
            {
                var result = new USB.International.Domain.Validation.ApplicationRules.IdNumberValidation().IsValid(model);

                if (result.IsNotNull())
                {
                    if (result.ErrorMessage.IsNotNullOrEmpty())
                    {
                        ModelState.AddModelError("personalDetails", result.ErrorMessage);
                        return Json(new { Success = false, Message = result.ErrorMessage });
                    }
                }

                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);

                SetUSNumber(applicationSubmission);
                SetEnrolment(applicationFormApiController, model.SelectedOffering);
                
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                var usNumber = Session[UsNumberString]?.AsString();
                applicationSubmission.UsNumber = usNumber;

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.ContactDetails;
                var submission = applicationFormApiController.SubmitPersonalInformation(applicationSubmission);

                if (!submission.Successful)
                {
                    return Json(new { Success = false, Message = submission.Message });
                }

                Session[UsNumberString] = submission.UsNumber ?? applicationSubmission.UsNumber;
                Session[EnrolmentGuidString] = submission.EnrolmentId;
                Session[ContactIdString] = submission.ContactId;

                return View("AddressDetails");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }
        
        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitAddressDetails(AddressDetails model)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.UsNumber = Session[UsNumberString].ToString();

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.AddressDetails;

                var submission = applicationFormApiController.SubmitAddressDetails(applicationSubmission);

                if(!submission.Successful)
                {
                    return Json(new { Success = true, Message = submission.Message });
                }

                return Json(new { Success = true, Message = "" });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        ////[ValidateAntiForgeryToken]
        public ActionResult SubmitWorkStudies(Workstudies model)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();
                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.UsNumber = Session[UsNumberString].ToString();

                applicationSubmission.ApplicationSource = ApplicationSource.WorkStudies;
                var submission = applicationFormApiController.SubmitWorkDetails(applicationSubmission);

                if (!submission.Successful)
                {
                    return Json(new { Success = false, Message = submission.Message });
                }

                return Json(new { Success = true, Message = "" });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitDocumentation(Documentation model, bool SHLTestRequired, string reviewed)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                if (Session[EnrolmentGuidString].IsNull() || Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
                {
                    Session[EnrolmentGuidString] =
                        applicationFormApiController.GetEnrolmentGuid((string)Session[UsNumberString], null);
                }

                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Documentation;

                var result = applicationFormApiController.SubmitDocumentation(applicationSubmission,
                                                                 SettingsMan.Default.ProgrammeGuid,
                                                                 (Guid)Session[EnrolmentGuidString],
                                                                 (string)Session[UsNumberString],
                                                                 (ApplicantStatus)Session[applicantStatusString],
                                                                 SettingsMan.Default.ProgrammeName, SHLTestRequired);

                if (!result.Successful)
                {
                    return Json(new { Success = false, Message = result.Message, ErrorId = result.ErrorId });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            return Json(new { Success = true, Message = "" });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitPayment(PaymentDetails model)
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                if (Session[EnrolmentGuidString].IsNull() || Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
                {
                    Session[EnrolmentGuidString] =
                        applicationFormApiController.GetEnrolmentGuid((string)Session[UsNumberString], null);
                }

                var applicationSubmission = ApplicationSubmissionMapper.Map(model);
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Payment;

                applicationFormApiController.SubmitPaymentDetails(applicationSubmission,
                                                                  (Guid)Session[EnrolmentGuidString],
                                                                  (string)Session[UsNumberString]);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            return Json(new { Success = true, Message = "" });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SubmitStatus()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return Json(new { Success = false, Message = "" });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Success = false, Message = "" });
            }

            try
            {
                var applicationFormApiController = new ApplicationFormApiController();

                if (Session[EnrolmentGuidString].IsNull() || Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
                {
                    Session[EnrolmentGuidString] =
                        applicationFormApiController.GetEnrolmentGuid((string)Session[UsNumberString], null);
                }

                var applicationSubmission = new ApplicationSubmission();
                applicationSubmission.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);
                applicationSubmission.EnrolmentGuid = (Guid)Session[EnrolmentGuidString];

                applicationSubmission.ApplicationSource = Domain.Models.ApplicationSource.Status;

                applicationFormApiController.SubmitStatus(applicationSubmission,
                                                          (Guid)Session[EnrolmentGuidString],
                                                          (string)Session[UsNumberString]);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                var msg = ex.Message;
                ViewBag.Message = msg;

                return View("Error");
            }

            return Json(new { Success = true, Message = "" });
        }

        #endregion

        #region Populate Details

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulatePersonalDetails(PersonalContactDetails model)
        {
            var applicationFormApiController = new ApplicationFormApiController();

            if (Session[idParameter].IsNotNull())
            {
                var id = Session[idParameter];

                try
                {
                    model = applicationFormApiController.GetPersonalDetails(id.AsGuid().GetValueOrDefault());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }


                Session[UsNumberString] = model.USNumber;
                Session.Remove(idParameter);
            }
            else
            {
                var usNumber = Session[UsNumberString];
                model.eMail = Session[EmailString].AsString();
                if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty())
                {
                    try
                    {
                        string programmeType = "International";
                        model = applicationFormApiController.GetPersonalDetails(usNumber.ToString());
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                        logger.Trace(ex.StackTrace);
                    }
                }
            }

            return Json(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulateAddressDetails(AddressDetails model)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            var usNumber = Session[UsNumberString];

            if (Session[UsNumberString].IsNotNull() && Session[UsNumberString].ToString().IsNotNullOrEmpty())
            {
                try
                {
                    model = applicationFormApiController.GetAddressDetails(usNumber.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }

            ViewBag.email = Session["email"];

            return Json(model);

            //return Json(new { AddressDetails = model, USNumber = usNumber }, JsonRequestBehavior.AllowGet);             
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult PopulateWorkStudies(Workstudies model)
        {
            var applicationFormApiController = new ApplicationFormApiController();
            
            SetEnrolment(applicationFormApiController, null);

            var usNumber = Session[UsNumberString];

            if (usNumber.IsNotNull() && usNumber.ToString().IsNotNullOrEmpty())
            {
                try
                {
                    model = applicationFormApiController.GetWorkStudies(usNumber.ToString(), (Guid)Session[EnrolmentGuidString]);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    logger.Trace(ex.StackTrace);
                }
            }

            ViewBag.email = Session["email"];

            return Json(model);
        }

        #endregion

        #region Pages
        [HttpGet]
        [SessionExpire]
        public ActionResult AddressDetails()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Documentation()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false) &&
                 ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted))
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
                ViewBag.ReviewEnabled = true;
                ViewBag.IsAdmittedApplicant = true;
            }
            else if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false))
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
                ViewBag.ReviewEnabled = true;
            }
            else if (Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false) &&
                      ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Admitted))
            {
                ViewBag.IsDeleteEnabled = false;
            }
            else if ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted)
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.IsAdmittedApplicant = true;
            }
            else if ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant)
            {
                ViewBag.IsGmatSelectionEnabled = false;
                ViewBag.DisableView = true;
            }

            Session["programmeGuid"] = SettingsMan.Default.ProgrammeGuid;
            ViewBag.IsInsideCRM = Session[insideCRMString].AsBool();

            var sessionContext = SessionContext.FromSession(Session);
            ViewBag.Context = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sessionContext)));



            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Payment()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            //if ( ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant) ||
            //     (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false)) )
            // {
            //     ViewBag.DisableView = true;
            // }

            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Status()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            var controller = new ApplicationFormApiController();

            if (Session[EnrolmentGuidString].IsNull() || Session[EnrolmentGuidString].ToString().IsNullOrEmpty())
            {
                Session[EnrolmentGuidString] =
                    controller.GetEnrolmentGuid((string)Session["UsNumber"], null);
            }


            ApplicationStatus appStatus = controller.GetApplicationStatus((Guid)Session[EnrolmentGuidString]);

            StatusViewModel model = null;

            if (appStatus.IsNotNull())
            {
                model = new StatusViewModel()
                {
                    PersonalDetailsComplete = appStatus.PersonalDetailsComplete,
                    AddressDetailsComplete = appStatus.AddressDetailsComplete,
                    WorkStudiesComplete = appStatus.WorkStudiesComplete,
                    DocumentationComplete = appStatus.DocumentationComplete,
                };
            }

            SetViewBagVariables();

            return View(model);
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult WorkStudies()
        {
            //if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            //{
            //    return HttpNotFound();
            //}

            //if (((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant) ||
            //     (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false)))
            //{
            //   ViewBag.DisableView = true;
            //}

            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Marketing()
        {
            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                return HttpNotFound();
            }

            //if (((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant) ||
            //     (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false)))
            //{
            //     ViewBag.DisableView = true;
            // }

            SetViewBagVariables();

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult PersonalDetails(string email)
        {
            if (Session[idParameter].IsNull() &&
                (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty()))
            {
                if (email.IsNullOrEmpty())
                {
                    return HttpNotFound();
                }

                ViewBag.IsFirstTimeApplicant = true;
            }

            SetViewBagVariables();

            ViewBag.EmailAddress = email;
            Session["ProgrammeName"] = SettingsMan.Default.ProgrammeName;

            return View();
        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Success()
        {
            //if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            //{
            //    return HttpNotFound();
            //}

            Session.Clear();

            return View();
        }

        [HttpGet]
        public ActionResult Error()
        {
            if(Request.QueryString["message"].IsNotNullOrEmpty())
            {
                ViewBag.Message = Request.QueryString["message"].ToString();
            }

            return View();
        }

        [HttpGet]
        public ActionResult Timeout()
        {
            return View();
        }

        [HttpGet]
        public FileResult DownloadDeclarationFile()
        {
            var usNumber = Session[UsNumberString];
            string pdfTemplate = HttpContext.Server.MapPath(@"~/Content/documents/e-application_postgrad_contract.pdf");

            PdfReader reader = new PdfReader(pdfTemplate);

            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamper = new PdfStamper(reader, ms, '\0', false))
                {
                    AcroFields formFields = stamper.AcroFields;
                    formFields.SetField("UsNumber", usNumber.IsNotNull() ? usNumber.ToString() : string.Empty);
                    stamper.FormFlattening = true;
                }

                return File(ms.ToArray(), MediaTypeNames.Application.Octet, "e-application_postgrad_contract.pdf");
            }
        }

        #endregion

        #region Private Methods
        // Enable both Get and Post so that our jquery call can send data, and get a status
        private string GetBaseUrl(HttpRequestBase request)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            //if (appUrl != "/") appUrl += "/";

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        private void SetViewBagVariables()
        {
            if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      !(Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // All Views Editable
                ViewBag.DisableView = false;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      (Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      (ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted)
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
        }

        private void SetEnrolment(ApplicationFormApiController applicationFormApiController, Guid? offeringId)
        {
            if (Session[EnrolmentGuidString].IsNull() || Guid.Parse(Session[EnrolmentGuidString].ToString()) == Guid.Empty)
            {
                Session[EnrolmentGuidString] = applicationFormApiController.GetEnrolmentGuid((string)Session[UsNumberString], offeringId);
            }
        }

        private void SetUSNumber(ApplicationSubmission applicationSubmission)
        {
            string sisCorrespondenceLanguageCode =
                CrmQueries.GetSisCorrespondenceLanguageCode(applicationSubmission.Language.GetValueOrDefault());

            if (Session[UsNumberString].IsNull() || Session[UsNumberString].ToString().IsNullOrEmpty())
            {
                Session[UsNumberString] =
                    PersonMaintenanceServiceInteractions.GetApplicantUSNumber(applicationSubmission,
                        sisCorrespondenceLanguageCode);
            }
        }

        private SessionContext CheckSessionContext(bool checkSections = true)
        {
            SessionContext context = (SessionContext)Session[Upload] ?? new SessionContext();

            if (context == null || !context.IsValid() || (checkSections && (context.Sections == null || !context.Sections.Any())))
            {
                throw new Exception("Your session has expired or is invalid. Please refresh the page or login again");
            }
            return context;
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            logger.Error(filterContext.Exception.Message);

            ViewBag.Message = filterContext.Exception.Message;

            filterContext.Result = new RedirectResult("~/Error/Index");
            filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
        }
    }
}
