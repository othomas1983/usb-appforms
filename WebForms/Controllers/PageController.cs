﻿using NLog;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.Domain.Models.Enums;

namespace WebForms.Controllers
{
    public class PageController : Controller
    {
        private const string EnrolmentGuidString = "EnrollmentGuid";
        private const string EmailString = "email";
        private const string StudentDetails = "StudentDetails";
        private const string programmeType = "programmeType";
        private const string UsNumberString = "UsNumber";
        private const string ContactIdString = "ContactId";
        private const string idParameter = "idParameter";
        private const string reviewIdParameter = "reviewIdParameter";
        private const string programmeGuid = "programmeGuid";
        private const string webTokenGuid = "webTokenId";
        private const string applicantStatusString = "applicantStatus";
        private const string insideCRMString = "insideCRM";
        private const string appFormSubmittedString = "appFormSubmitted";

        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        public ActionResult MBA ()


        {

            ViewBag.ProgrammeTitle = "MBA";
            Session.Clear();
            Session[programmeType] = ProgrammeType.MBA;

            var iID = string.Empty;

            var id = HttpContext.Request["id"];
            Session[idParameter] = id;

            if (id.IsNotNullOrEmpty())
            {
                var controller = new ApplicationFormApiController();

                var webTokenId = HttpContext.Request[webTokenGuid];
                Session[webTokenGuid] = webTokenId;
                Session[EnrolmentGuidString] = new Guid(id);

                // Comment validation out for testing purposes
                if (webTokenId.IsNotNullOrEmpty()/* && controller.IsWebTokenValid(new Guid(webTokenId))*/)
                {
                    Session[insideCRMString] = true;
                }

                try
                {
                    var status = controller.GetApplicantStatus(new Guid(id));

                    if (status.IsNull())
                    {
                        var msg = "Application status not found." + Environment.NewLine + "Please contact the System Administrator";
                        logger.Error(string.Format(msg));
                        Session[EnrolmentGuidString] = null;
                        ViewBag.Message = msg;

                        return View("Error");
                    }
                    else
                    {
                        Session[applicantStatusString] = status;
                    }

                    Session[appFormSubmittedString] = controller.IsApplicationFormSubmitted(new Guid(id));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);

                    var msg = ex.Message;
                    ViewBag.Message = msg;

                    return View("Error");
                }

                SetViewBagVariables();


                ViewBag.EnrolmentId = new Guid(HttpContext.Request["id"]);
                return View("~/Views/Contact/PersonalDetails.cshtml");
            }


            Session[applicantStatusString] = ApplicantStatus.Applicant;
            return View();
                    

           
        }
        public ActionResult MPhilDevFin()
        {
            ViewBag.ProgrammeTitle = "MPhil Development Finance";
            return View();
        }



        private void SetViewBagVariables()
        {
            if (Session[insideCRMString].IsNotNull() && (Session[insideCRMString].AsBool() ?? false))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      !(Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // All Views Editable
                ViewBag.DisableView = false;
            }
            else if ((Session[applicantStatusString].IsNotNull() && ((ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Applicant)) &&
                      (Session[appFormSubmittedString].IsNotNull() && (Session[appFormSubmittedString].AsBool() ?? false)))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      (ApplicantStatus)Session[applicantStatusString] == ApplicantStatus.Admitted)
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
            else if (Session[applicantStatusString].IsNotNull() &&
                      ((ApplicantStatus)Session[applicantStatusString] != ApplicantStatus.Applicant))
            {
                // Disable Views
                ViewBag.DisableView = true;
            }
        }
    }
}