﻿using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.MBA.ApplicationForm.ViewModels;
using USB.Domain.Models.Enums;

namespace WebForms.Controllers
{
    public class LoadDataController : Controller
    {

        [HttpGet]
        public ActionResult AddressDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, Guid>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "countries":
                    ;
                    dropdownList = controller.AddressCountries().ToList();
                    break;

                default:
                    ;
                    break;
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetDistributionAreasAfrigis()
        {
            var query = HttpContext.Request["query"];

            var postalDistributionArea = new Afrigis.PostalDistributionArea();

            var areas = postalDistributionArea.LoadAreas(query);
            //Session["PostalAreas"] = areas;
            return Json(postalDistributionArea.Areas, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult MarketingDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "infoSessions":
                    ;
                    dropdownList = controller.InfoSessions().ToList();
                    break;

                default:
                    ;
                    break;
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PersonalDropdown(string enrolmentId, string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();

            var tupleList = new List<Tuple<string, Guid, ProgrammeOfferingType>>();

            var controller = new ApplicationFormApiController();

            try
            {
                switch (type)
                {
                    case "gender":
                        ;
                        dropdownGuidList = controller.Genders().ToList();
                        break;
                    case "titles":
                        ;
                        dropdownGuidList = controller.Titles().ToList();
                        break;
                    case "ethnicites":
                        ;
                        dropdownGuidList = controller.Ethnicities().ToList();
                        break;
                    case "nonSAEthnicities":
                        ;
                        dropdownGuidList = controller.NonSAEthnicities().ToList();
                        break;
                    case "nationalities":
                        ;
                        dropdownGuidList = controller.Nationalities().ToList();
                        break;
                    case "countriesIssued":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "correspondenceLanguages":
                        ;
                        dropdownGuidList = controller.CorrespondenceLanguages().ToList();
                        break;
                    case "languages":
                        ;
                        dropdownGuidList = controller.Languages().ToList();
                        break;
                    case "foreignIdentificationTypes":
                        ;
                        dropdownGuidList = controller.ForeignIdentificationTypes().ToList();
                        break;
                    case "SouthAfricaIdentificationTypes":
                        ;
                        dropdownGuidList = controller.SouthAfricaIdentificationTypes().ToList();
                        break;
                    case "disabilities":
                        ;
                        dropdownGuidList = controller.Disabilities().ToList();
                        break;
                    case "addressCountries":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "permitTypes":
                        ;
                        dropdownGuidList = controller.PermitTypes().ToList();
                        break;
                    case "maritalStatus":
                        ;
                        dropdownGuidList = controller.MaritalStatus().ToList();
                        break;
                    case "offerings":
                        ;


                        var programmeGuid = new Guid("fcff9309-d802-e611-80d2-005056b8008e");
                        var isMBA = true;
                                       

                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            tupleList = controller.EnrolmentIdGetOfferings(isMBA, programmeGuid, enrolmentGuid).ToList();
                        }
                        else
                        {
                            tupleList = controller.GetOfferings(isMBA, programmeGuid).ToList();
                        }


                        break;
                    case "programme":
                        ;
                        //dropdownList = controller.Programme().ToList();
                        break;
                    case "blendedAttendance":
                        dropdownList = controller.GetBlendedOptions().ToList();
                        break;
                    case "mbaStreams":
                        dropdownList = controller.GetMBAStreamOptions().ToList();
                        break;
                    default:
                        ;
                        break;
                }
            }
            catch (InvalidOperationException)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new { dropdownGuidList }, JsonRequestBehavior.AllowGet);
            }

            if (tupleList.Any())
            {
                return Json(new { tupleList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult WorkStudiesDropdown(string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();
            var controller = new ApplicationFormApiController();

            switch (type)
            {
                case "leesAfrikaans":
                    ;
                    dropdownList = controller.LeesAfrikaans().ToList();
                    break;
                case "skryfAfrikaans":
                    ;
                    dropdownList = controller.SkryfAfrikaans().ToList();
                    break;
                case "praatAfrikaans":
                    ;
                    dropdownList = controller.PraatAfrikaans().ToList();
                    break;
                case "verstaanAfrikaans":
                    ;
                    dropdownList = controller.VerstaanAfrikaans().ToList();
                    break;
                case "talkEnglish":
                    ;
                    dropdownList = controller.TalkEnglish().ToList();
                    break;
                case "writeEnglish":
                    ;
                    dropdownList = controller.WriteEnglish().ToList();
                    break;
                case "readEnglish":
                    ;
                    dropdownList = controller.ReadEnglish().ToList();
                    break;
                case "understandEnglish":
                    ;
                    dropdownList = controller.UnderstandEnglish().ToList();
                    break;
                case "mathematicsCompetency":
                    ;
                    dropdownList = controller.MathematicsCompetency().ToList();
                    break;
                case "mathematicsPercentage":
                    ;
                    dropdownList = controller.MathematicsPercentage().ToList();
                    break;
                case "priorInvolvement":
                    ;
                    dropdownList = controller.PriorInvolvement().ToList();
                    break;
                case "institution":
                    ;
                    dropdownGuidList = controller.Institution().ToList();
                    break;
                case "fieldOfStudy":
                    ;
                    dropdownList = controller.FieldOfStudy().ToList();
                    break;
                case "workArea":
                    ;
                    dropdownList = controller.WorkAreas().ToList();
                    break;
                case "organization":
                    ;
                    dropdownList = controller.Organization().ToList();
                    break;
                case "industry":
                    ;
                    dropdownList = controller.Industry().ToList();
                    break;
                case "workarea":
                    ;
                    dropdownList = controller.WorkAreas().ToList();
                    break;
                case "grossSalary":
                    ;
                    dropdownList = controller.GrossSalaries().ToList();
                    break;
                case "employerAssistFinancially":
                    ;
                    dropdownList = controller.EmployerAssistFinancially().ToList();
                    break;
                case "employmentType":
                    ;
                    dropdownList = controller.EmploymentType().ToList();
                    break;
                case "employerWorkAreas":
                    ;
                    dropdownList = controller.EmployerWorkAreas().ToList();
                    break;
                default:
                    ;
                    break;
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new { list = dropdownGuidList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { list = dropdownList }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
