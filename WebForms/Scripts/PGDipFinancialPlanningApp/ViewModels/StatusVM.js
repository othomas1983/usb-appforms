﻿function Status(model, isDisabled) {
    var self = this;

    var rootUrl = "/PGDipFinPlanning/";

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    self.personalDetailsComplete = ko.observable(model.PersonalDetailsComplete? model.PersonalDetailsComplete: false);
    self.addressDetailsComplete = ko.observable(model.AddressDetailsComplete? model.AddressDetailsComplete: false);
    self.workStudiesComplete = ko.observable(model.WorkStudiesComplete? model.WorkStudiesComplete: false);
    self.marketingComplete = ko.observable(model.MarketingComplete? model.MarketingComplete: false);
    self.documentationComplete = ko.observable(model.DocumentationComplete? model.DocumentationComplete: false);
    self.paymentComplete = ko.observable(model.PaymentComplete? model.PaymentComplete: false);
    self.AllSectionsCompleted = ko.observable(self.personalDetailsComplete()
                                        && self.addressDetailsComplete()
                                        && self.workStudiesComplete() && self.marketingComplete()
                                        && self.documentationComplete() && self.paymentComplete());

    var Status = {
        PersonalDetailsComplete :   self.personalDetailsComplete,
        AddressDetailsComplete  :   self.addressDetailsComplete,
        WorkStudiesComplete     :   self.workStudiesComplete,
        MarketingComplete       :   self.marketingComplete,
        DocumentationComplete   :   self.documentationComplete,
        PaymentComplete         :   self.paymentComplete
    };

    self.Submit = function () {

        waitingDialog.show('Loading Submission Details');

        $.ajax({
            url: rootUrl + "Page/SubmitStatus",
            contentType: 'application/json; charset=utf-8',
            //data: AddAntiForgeryToken({}),
            type: "POST",
            success: function (j) {
                    
                window.location.href = rootUrl + "Page/Success";
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).done(function (response) {
            waitingDialog.hide();
        });
    }
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});



