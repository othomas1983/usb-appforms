﻿function Documentation(isGMATSelectionEnabled, isDisabled, isDeleteEnabled,
                       isReviewEnabled, isAdmittedApplicant, isBackOffice) {
    var self = this;

    var rootUrl = "/PGDipFutureStudies/";

    self.isBuzyLoading = ko.observable(true);

    self.errorId = ko.observable();

    self.isDeleteEnabled = ko.observable((isDeleteEnabled !== null) ? isDeleteEnabled : true);
    self.isGMATSelectionEnabled = ko.observable((isGMATSelectionEnabled !== null) ? isGMATSelectionEnabled : true);
    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);
    self.isReviewEnabled = ko.observable((isReviewEnabled !== null) ? isReviewEnabled : false);
    self.isAdmittedApplicant = ko.observable((isAdmittedApplicant !== null) ? isAdmittedApplicant : false);

    self.isBackOffice = ko.observable(false);

    self.uploadGMatScores = ko.observable();
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});