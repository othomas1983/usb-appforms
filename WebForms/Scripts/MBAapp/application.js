﻿var Application = function() {
    var instance = this;

    instance.personalDetails_Url = "/Contact/PersonalDetails";
    instance.addressDetails_Url = "/Contact/AddressDetails";
    instance.work_Study_Url = "/Contact/WorkStudies";
    instance.marketing_Url = "/Contact/Marketing";
    instance.documentation_Url = "/Contact/Documentation";
    instance.payment_Url = "/Contact/Payment";
    instance.status_Url = "/Contact/Status";

    var activeTab = null;

    instance.canClickOnPersonalDetails = ko.observable(false);
    instance.canClickOnAddressDetails = ko.observable(false);
    instance.canClickOnWorkAndStudies = ko.observable(false);
    instance.canClickOnMarketingDetails = ko.observable(false);
    instance.canClickOnDocumentationDetails = ko.observable(false);
    instance.canClickOnPaymentDetails = ko.observable(false);
    instance.canClickOnStatusDetails = ko.observable(false);

    this.load = function() {
        instance.setActiveTab();
        instance.registerEvents();
    }

    this.registerEvents = function () {

        $('.navPersonalDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnPersonalDetails()) {
                    window.location.href = instance.personalDetails_Url;
                }
            });
        $('.navAddressDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnAddressDetails()) {
                    window.location.href = instance.addressDetails_Url;
                }
            });
        $('.navWorkAndStudyDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnWorkAndStudies()) {
                    window.location.href = instance.work_Study_Url;
                }
            });
        $('.navMarketingDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnMarketingDetails()) {
                    window.location.href = instance.marketing_Url;
                }
            });
        $('.navDocumentationDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                //if (instance.canClickOnDocumentationDetails()) {
                if (instance.canClickOnPersonalDetails() &&
                    instance.canClickOnAddressDetails() &&
                    instance.canClickOnWorkAndStudies()) {
                    window.location.href = instance.documentation_Url;
                }
                //}
            });
        $('.navPaymentDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnPaymentDetails()) {
                    window.location.href = instance.payment_Url;
                }
            });
        $('.navStatusDetails').on('click',
            function () {
                //instance.ValidateCurrentStep();
                if (instance.canClickOnStatusDetails()) {
                    window.location.href = instance.status_Url;
                }
            });
    };

    this.recreatePopoverDialog = function (elId) {
        $(elId).popover({
            container: "body",
            placement: "bottom",
            trigger: "hover"
        });
    };

    this.setActiveTab = function() {
        $.ajax({
            url: "../Contact/GetApplicationStatus",
            type: 'POST',
            contentType: "application/json",
            cache: false,
            success: function(data) {
                if (data != undefined) {
                    if (data.PersonalDetailsComplete != undefined) {

                        instance.recreatePopoverDialog("#tab1");

                        if (data.PersonalDetailsComplete) {
                            $("#tab1").popover("destroy");
                            $("#navPersonalDetails").removeClass("glyphicon-remove");
                            $("#navPersonalDetails").addClass("glyphicon-ok");
                            instance.canClickOnPersonalDetails(true);
                        } else {
                            $("#navPersonalDetails").addClass("glyphicon-remove");
                            $("#navPersonalDetails").removeClass("glyphicon-ok");
                            instance.canClickOnPersonalDetails(false);
                        }
                    }
                    if (data.AddressDetailsComplete != undefined) {

                        instance.recreatePopoverDialog("#tab2");

                        if (data.AddressDetailsComplete) {
                            $("#tab2").popover("destroy");
                            $("#navAddressDetails").removeClass("glyphicon-remove");
                            $("#navAddressDetails").addClass("glyphicon-ok");
                            instance.canClickOnAddressDetails(true);
                        } else {
                            $("#navAddressDetails").addClass("glyphicon-remove");
                            $("#navAddressDetails").removeClass("glyphicon-ok");
                            instance.canClickOnAddressDetails(false);
                        }
                    }
                    if (data.WorkStudiesComplete != undefined) {

                        instance.recreatePopoverDialog("#tab3");

                        if (data.WorkStudiesComplete) {
                            $("#tab3").popover("destroy");
                            $("#navWorkAndStudyDetails").removeClass("glyphicon-remove");
                            $("#navWorkAndStudyDetails").addClass("glyphicon-ok");
                            instance.canClickOnWorkAndStudies(true);
                        } else {
                            $("#navWorkAndStudyDetails").addClass("glyphicon-remove");
                            $("#navWorkAndStudyDetails").removeClass("glyphicon-ok");
                            instance.canClickOnWorkAndStudies(false);
                        }
                    }
                    if (data.MarketingComplete != undefined) {

                        instance.recreatePopoverDialog("#tab4");

                        if (data.MarketingComplete) {
                            $("#tab4").popover("destroy");
                            $("#navMarketingDetails").removeClass("glyphicon-remove");
                            $("#navMarketingDetails").addClass("glyphicon-ok");
                            instance.canClickOnMarketingDetails(true);
                        } else {
                            $("#navMarketingDetails").addClass("glyphicon-remove");
                            $("#navMarketingDetails").removeClass("glyphicon-ok");
                            instance.canClickOnMarketingDetails(false);
                        }
                    }
                    if (data.DocumentationComplete != undefined) {

                        instance.recreatePopoverDialog("#tab5");

                        if (data.DocumentationComplete) {
                            $("#tab5").popover("destroy");
                            $("#navDocumentationDetails").removeClass("glyphicon-remove");
                            $("#navDocumentationDetails").addClass("glyphicon-ok");
                            instance.canClickOnDocumentationDetails(true);
                        } else {
                            $("#navDocumentationDetails").addClass("glyphicon-remove");
                            $("#navDocumentationDetails").removeClass("glyphicon-ok");
                            instance.canClickOnDocumentationDetails(false);
                        }
                    }
                    if (data.PaymentComplete != undefined) {

                        instance.recreatePopoverDialog("#tab6");

                        if (data.PaymentComplete) {
                            $("#tab6").popover("destroy");
                            $("#navPaymentDetails").removeClass("glyphicon-remove");
                            $("#navPaymentDetails").addClass("glyphicon-ok");
                            instance.canClickOnPaymentDetails(true);
                        } else {
                            $("#navPaymentDetails").addClass("glyphicon-remove");
                            $("#navPaymentDetails").removeClass("glyphicon-ok");
                            instance.canClickOnPaymentDetails(false);
                        }
                    }
                }
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });

        var url = window.location.pathname;
        if (url == instance.personalDetails_Url) {
            $("#tab1").addClass('active');
            $('#currentPage').text('Page 1 of 7');
        }
        if (url == instance.addressDetails_Url) {
            $("#tab2").addClass('active');
            $('#currentPage').text('Page 2 of 7');
        }
        if (url == instance.work_Study_Url) {
            $("#tab3").addClass('active');
            $('#currentPage').text('Page 3 of 7');
        }
        if (url == instance.marketing_Url) {
            $("#tab4").addClass('active');
            $('#currentPage').text('Page 4 of 7');
        }
        if (url == instance.documentation_Url) {
            $("#tab5").addClass('active');
            $('#currentPage').text('Page 5 of 7');
        }
        if (url == instance.payment_Url) {
            $("#tab6").addClass('active');
            $('#currentPage').text('Page 6 of 7');
        }
        if (url == instance.status_Url) {
            $("#tab7").addClass('active');
            $('#currentPage').text('Page 7 of 7');
        }
    }
    
    AddAntiForgeryToken = function(data) {
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        return data;
    };
}
