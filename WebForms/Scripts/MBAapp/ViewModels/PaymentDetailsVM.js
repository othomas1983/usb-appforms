﻿function PaymentDetails(isDisabled) {
    var self = this;

    var rootUrl = "/MBA/";
   // self.NationalitiesApi_url = rootUrl + "http://bpcappprod.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Nationalities/";

    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);

    self.bank = ko.observable();
    self.typeOfAccount = ko.observableArray();
    self.accountNumber = ko.observableArray();
    self.branchCode = ko.observableArray();
    self.branchName = ko.observableArray();
    self.beneficiaryName = ko.observableArray();
    self.chequesMadeOutTo = ko.observableArray();

    var PaymentDetails = {
        Bank : self.bank,
        TypeOfAccount : self.typeOfAccount,
        AccountNumber : self.accountNumber,
        BranchCode : self.branchCode,
        BranchName : self.branchName,
        BeneficiaryName : self.beneficiaryName,
        ChequesMadeOutTo : self.chequesMadeOutTo 
    }

    self.StepSevenProceed = function () {

        $.ajax({
            url: rootUrl + "Contact/SubmitPayment",
            contentType: 'application/json; charset=utf-8',
            //data: AddAntiForgeryToken(ko.mapping.toJSON(PaymentDetails)),
            data: ko.mapping.toJSON(PaymentDetails),
            type: "POST",
            success: function (data) {
                console.log(data);
                window.location.href = rootUrl + "Contact/Status";
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).done(function (response) {
            console.log(response);
        });
    }

    // $.getJSON(self.NationalitiesApi_url, function (data) {
   //     LoadKeyValuePairJSONObject(data, self.nationalities);
    // });

}

function LoadKeyValuePairJSONObject(data, observableArray) {
    var jsonResult = data;
    var list = observableArray;
    for (key in data) {
        var item = {
            name: key,         // Push the key on the array
            value: jsonResult[key] // Push the key's value on the array
        };
        list.push(item);
    };
    return list;
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});



