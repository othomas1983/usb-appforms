﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebForms.Sessions
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check  sessions here
            if (HttpContext.Current.Session["UsNumber"] == null)
            {
                filterContext.Result = new RedirectResult("~/Contact/Index");

                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}