﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using USB.Domain.Models;

namespace USB.MPhilDevFin.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMPhileDevFinService" in both code and config file together.
    [ServiceContract]
    public interface IMPhileDevFinService
    {
        [OperationContract]
        ApplicationSubmissionResult SavePersonalDetails(string personalDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveAddressDetails(string addressDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveWorkDetails(string workDetails);

        [OperationContract]
        ApplicationSubmissionResult SaveDocument(DocumentType documentType, byte[] binary);
    }
}
