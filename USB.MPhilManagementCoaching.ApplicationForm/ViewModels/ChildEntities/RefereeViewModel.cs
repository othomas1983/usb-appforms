﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.MPhilManagementCoaching.ApplicationForm.ViewModels
{
    public class RefereeViewModel
    {
        public string RefereeName { get; set; }
        public string RefereePosition { get; set; }
        public string RefereeTelephone { get; set; }
        public string RefereeCellPhone { get; set; }
        public string RefereeEmail { get; set; }
    }
}