﻿function Documentation(isGMATSelectionEnabled,
    isDisabled,
    isDeleteEnabled,
    isReviewEnabled,
    isAdmittedApplicant,
    context,
    isInsideCRM,
    isSHLTestRequired) {

    var self = this;

    var rootUrl = "/PGDipBMA/";

    self.isBuzyLoading = ko.observable(true);

    self.errorId = ko.observable();

    self.isDeleteEnabled = ko.observable((isDeleteEnabled !== null) ? isDeleteEnabled : true);
    self.isGMATSelectionEnabled = ko.observable((isGMATSelectionEnabled !== null) ? isGMATSelectionEnabled : true);
    self.isDisabled = ko.observable((isDisabled !== null) ? isDisabled : false);
    self.isReviewEnabled = ko.observable((isReviewEnabled !== null) ? isReviewEnabled : false);
    self.isAdmittedApplicant = ko.observable((isAdmittedApplicant !== null) ? isAdmittedApplicant : false);
    self.context = ko.observable((context !== null) ? context : "");
    self.canSelectSHLOrGMAT = ko.observable(true);
    self.canDisplayDocuments = ko.observable(false);

    self.isBackOffice = ko.observable(false);

    self.isSHLTestRequired = ko.observable(isSHLTestRequired);

    self.isSHLTestRequired.subscribe(function (value) {
        self.canDisplayDocuments(false);

        if (value != undefined) {
            self.canDisplayDocuments(true);
            document.getElementById("documentFrame")
            .setAttribute("src", "/Shared/Upload?data=" + self.context() + "&includeGMAT=" + !self.isSHLTestRequired());
        }
    });

    self.checkSHLTestRequired = new function () {
        self.canDisplayDocuments(false);

        if (self.isSHLTestRequired() != undefined || self.isSHLTestRequired() != null) {
            self.canDisplayDocuments(true);
            document.getElementById("documentFrame")
            .setAttribute("src", "/Shared/Upload?data=" + self.context() + "&includeGMAT=" + !self.isSHLTestRequired());
        }
    };

    self.isInsideCRM = ko.observable((isInsideCRM !== null) ? isInsideCRM : false);

    self.isInsideCRM = new function () {
        if (self.isInsideCRM()) {
            self.canSelectSHLOrGMAT(false);
            self.canDisplayDocuments(true);
            document.getElementById("documentFrame")
            .setAttribute("src", "/Shared/Upload?data=" + self.context() + "&includeGMAT=" + !self.isSHLTestRequired());
        }
    };

    self.uploadGMatScores = ko.observable();

    self.fileUploadViewer = ko.observable();
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});