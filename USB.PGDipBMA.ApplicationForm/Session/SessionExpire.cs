﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace USB.PGDipBMA.ApplicationForm.Session
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check  sessions here
            if (HttpContext.Current.Session["UsNumber"] == null)
            {
                filterContext.Result = new RedirectResult("~/Page/Index");

                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}