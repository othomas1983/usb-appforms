﻿/// <reference path="../../knockout-3.3.0.debug.js" />
/// <reference path="../../knockout.validation.js" />

var knockoutValidationSettings = {
    insertMessages: false,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};

ko.validation.configuration = knockoutValidationSettings;

function AddressDetails(isDisabled) {
    var self = this;
    
    var rootUrl = "/PGDipFutureStudies/";
    
    self.isDisabled = ko.observable((isDisabled !== null)? isDisabled : false);

    self.loading = ko.observableArray();

    self.loading.subscribe(function () {
        if (self.loading().length == 0) {
            // Last dropdown population async ajax call has ended

            // Fill in inputs if existing informantion exists
            self.loadExistingDetails();
        }
    });

    self.AfrigisSearch = ko.observable();

    self.DisplayResidentialSuburbValid = ko.observable(true);
    self.DisplayResidentialCityValid = ko.observable(true);
    self.DisplayResidentialPostalAddressValid = ko.observable(true);
    self.DisplayResidentialStateValid = ko.observable(true);

    self.DisplayPostalSuburbValid = ko.observable(true);
    self.DisplayPostalCityValid = ko.observable(true);
    self.DisplayPostalPostalAddressValid = ko.observable(true);
    self.DisplayPostalStateValid = ko.observable(true);

    self.DisplayBusinessSuburbValid = ko.observable(true);
    self.DisplayBusinessCityValid = ko.observable(true);
    self.DisplayBusinessPostalAddressValid = ko.observable(true);
    self.DisplayBusinessStateValid = ko.observable(true);

    self.DisplayBillingSuburbValid = ko.observable(true);
    self.DisplayBillingCityValid = ko.observable(true);
    self.DisplayBillingPostalAddressValid = ko.observable(true);
    self.DisplayBillingStateValid = ko.observable(true);
    
    self.IsResidentialAddressSelected = ko.observable(false);
    self.IsSAResidentialAddress = ko.observable(false);
    self.IsNonSAResidentialAddress = ko.observable(false);

    self.IsPostalAddressSelected = ko.observable(false);
    self.IsSAPostalAddress = ko.observable(false);
    self.IsNonSAPostalAddress = ko.observable(false);

    self.IsBusinessAddressSelected = ko.observable(false);
    self.IsSABusinessAddress = ko.observable(false);
    self.IsNonSABusinessAddress = ko.observable(false);

    self.IsBillingAddressSelected = ko.observable(false);
    self.IsSABillingAddress = ko.observable(false);
    self.IsNonSABillingAddress = ko.observable(false);

    self.Countries = ko.observableArray();
    self.ResidentialCountry = ko.observable();
    self.ResidentialStreet1 = ko.observable();
    self.ResidentialStreet2 = ko.observable();
    self.ResidentialStreet3 = ko.observable();
    self.ResidentialCity = ko.observable();
    self.ResidentialSuburb = ko.observable();
    self.ResidentialStateProvince = ko.observable();
    self.ResidentialPostalCode = ko.observable();
    self.ResidentialGisId = ko.observable();

    self.PostalCountry = ko.observable();
    self.PostalStreet1 = ko.observable();
    self.PostalStreet2 = ko.observable();
    self.PostalStreet3 = ko.observable();
    self.PostalCity = ko.observable();
    self.PostalSuburb = ko.observable();
    self.PostalStateProvince = ko.observable();
    self.PostalPostalCode = ko.observable();
    self.PostalGisId = ko.observable();

    self.BusinessCountry = ko.observable();
    self.BusinessStreet1 = ko.observable();
    self.BusinessStreet2 = ko.observable();
    self.BusinessStreet3 = ko.observable();
    self.BusinessCity = ko.observable();
    self.BusinessSuburb = ko.observable();
    self.BusinessPostalCode = ko.observable();
    self.BusinessGisId = ko.observable();

    self.BillingTo = ko.observable();
    self.BillingCountry = ko.observable();
    self.BillingStreet1 = ko.observable();
    self.BillingStreet2 = ko.observable();
    self.BillingStreet3 = ko.observable();
    self.BillingCity = ko.observable();
    self.BillingSuburb = ko.observable();
    self.BillingPostalCode = ko.observable();
    self.BillingGisId = ko.observable();

    self.serverErrorMessage = ko.observable();

    self.setRequiredFields = function () {

        self.ResidentialCountry.extend({ required: true });
        self.ResidentialStreet1.extend({ required: true });
        self.ResidentialCity.extend({ required: true });
        self.ResidentialSuburb.extend({ required: { onlyIf: function () { return self.IsSAResidentialAddress() === true; } } });
        self.ResidentialStateProvince.extend({ required: { onlyIf: function () { return self.IsNonSAResidentialAddress() === true; } } });
        self.ResidentialPostalCode.extend({ required: true });

        self.PostalCountry.extend({ required: true });
        self.PostalStreet1.extend({ required: true });
        self.PostalCity.extend({ required: true });
        self.PostalSuburb.extend({ required: { onlyIf: function () { return self.IsSAPostalAddress() === true; } } });
        self.PostalStateProvince.extend({ required: { onlyIf: function () { return self.IsNonSAResidentialAddress() === true; } } });
        self.PostalPostalCode.extend({ required: true });

        self.BusinessCountry.extend({ required: true });
        self.BusinessStreet1.extend({ required: true });
        self.BusinessCity.extend({ required: true });
        self.BusinessSuburb.extend({ required: { onlyIf: function () { return self.IsSABusinessAddress() === true; } } });
        self.BusinessPostalCode.extend({ required: true });

        self.BillingTo.extend({ required: true });
        self.BillingCountry.extend({ required: true });
        self.BillingStreet1.extend({ required: true });
        self.BillingCity.extend({ required: true });
        self.BillingSuburb.extend({ required: { onlyIf: function () { return self.IsSABillingAddress() === true; } } });
        self.BillingPostalCode.extend({ required: true });
    };

    function LoadDropDown(type) {

        self.loading.push(true);

        $.ajax({
            url: rootUrl + "AddressDropdown/LoadDropdown?type=" + type,
            type: 'GET',
            contentType: "application/json",
            cache: false,
            success: function (j) {
                populateDropDownList(j.dropdownList, type);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        }).always(function () {
            self.loading.pop();
        });
    };

    function populateDropDownList(list, type) {
        switch (type) {
            case "countries":;
                self.Countries(list);
                break;
            default:;
        }
    }

    self.loadExistingDetails = function () {
        $.ajax({
            url: rootUrl + "Page/PopulateAddressDetails",
            type: 'POST',
            contentType: "application/json",
            cache: false,
            async: true,
            success: function (data) {
                if (data.ResidentialCountry !== null) {
                    ko.mapping.fromJS(data, {}, self);
                }
            }
        }).always(function () {
            waitingDialog.hide();
        });
    };

    var AddressDetails = {

        ResidentialCountry: self.ResidentialCountry,
        ResidentialStreet1: self.ResidentialStreet1,
        ResidentialStreet2: self.ResidentialStreet2,
        ResidentialStreet3: self.ResidentialStreet3,
        ResidentialCity: self.ResidentialCity,
        ResidentialSuburb: self.ResidentialSuburb,
        ResidentialStateProvince: self.ResidentialStateProvince,
        ResidentialPostalCode: self.ResidentialPostalCode,
        ResidentialGisId: self.ResidentialGisId,

        PostalCountry: self.PostalCountry,
        PostalStreet1: self.PostalStreet1,
        PostalStreet2: self.PostalStreet2,
        PostalStreet3: self.PostalStreet3,
        PostalCity: self.PostalCity,
        PostalSuburb: self.PostalSuburb,
        PostalStateProvince: self.PostalStateProvince,
        PostalPostalCode: self.PostalPostalCode,
        PostalGisId: self.PostalGisId,

        BusinessCountry: self.BusinessCountry,
        BusinessStreet1: self.BusinessStreet1,
        BusinessStreet2: self.BusinessStreet2,
        BusinessStreet3: self.BusinessStreet3,
        BusinessCity: self.BusinessCity,
        BusinessSuburb: self.BusinessSuburb,
        BusinessPostalCode: self.BusinessPostalCode,
        BusinessGisId: self.BusinessGisId,

        BillingTo: self.BillingTo,
        BillingCountry: self.BillingCountry,
        BillingStreet1: self.BillingStreet1,
        BillingStreet2: self.BillingStreet2,
        BillingStreet3: self.BillingStreet3,
        BillingCity: self.BillingCity,
        BillingSuburb: self.BillingSuburb,
        BillingPostalCode: self.BillingPostalCode,
        BillingGisId: self.BillingGisId,
    }

    AddressDetails.errors = ko.validation.group(AddressDetails);

    self.ResidentialCountry.subscribe(function () {
        var selectedValue = self.ResidentialCountry();

        selectSouthAfrica = $('#resAddress option').filter(function () { return $(this).html() == "South Africa"; }).val();

        if (selectedValue === selectSouthAfrica) {
            self.IsResidentialAddressSelected(true);
            self.IsSAResidentialAddress(true);
            self.IsNonSAResidentialAddress(false);
        }
        else if ( (selectedValue !== undefined) && (selectedValue !== null) ) {
            self.IsResidentialAddressSelected(true);
            self.IsSAResidentialAddress(false);
            self.IsNonSAResidentialAddress(true);
        }
        else {
            self.IsResidentialAddressSelected(false);
            self.IsSAResidentialAddress(false);
            self.IsNonSAResidentialAddress(false);
        }
    });



    self.PostalCountry.subscribe(function () {
        var selectedValue = self.PostalCountry();

        selectSouthAfrica = $('#postalAddress option').filter(function () { return $(this).html() == "South Africa"; }).val();

        if (selectedValue === selectSouthAfrica) {
            self.IsPostalAddressSelected(true);
            self.IsSAPostalAddress(true);
            self.IsNonSAPostalAddress(false);
        }
        else if ((selectedValue !== undefined) && (selectedValue !== null)) {
            self.IsPostalAddressSelected(true);
            self.IsSAPostalAddress(false);
            self.IsNonSAPostalAddress(true);
        }
        else {
            self.IsPostalAddressSelected(false);
            self.IsSAPostalAddress(false);
            self.IsNonSAPostalAddress(false);
        }
    });

    self.BusinessCountry.subscribe(function () {
        var selectedValue = self.BusinessCountry();

        selectSouthAfrica = $('#businessAddress option').filter(function () { return $(this).html() == "South Africa"; }).val();

        if (selectedValue === selectSouthAfrica) {
            self.IsBusinessAddressSelected(true);
            self.IsSABusinessAddress(true);
            self.IsNonSABusinessAddress(false);
        }
        else if ((selectedValue !== undefined) && (selectedValue !== null)) {
            self.IsBusinessAddressSelected(true);
            self.IsSABusinessAddress(false);
            self.IsNonSABusinessAddress(true);
        }
        else {
            self.IsBusinessAddressSelected(false);
            self.IsSABusinessAddress(false);
            self.IsNonSABusinessAddress(false);
        }
    });

    self.BillingCountry.subscribe(function () {
        var selectedValue = self.BillingCountry();

        selectSouthAfrica = $('#billingAddress option').filter(function () { return $(this).html() == "South Africa"; }).val();

        if (selectedValue === selectSouthAfrica) {
            self.IsBillingAddressSelected(true);
            self.IsSABillingAddress(true);
            self.IsNonSABillingAddress(false);
        }
        else if ((selectedValue !== undefined) && (selectedValue !== null)) {
            self.IsBillingAddressSelected(true);
            self.IsSABillingAddress(false);
            self.IsNonSABillingAddress(true);
        }
        else {
            self.IsBillingAddressSelected(false);
            self.IsSABillingAddress(false);
            self.IsNonSABillingAddress(false);
        }
    });

    //#region Change Warning symbols on text fields for Residental Address
    self.ResidentialSuburb.subscribe(function () {
        if(self.ResidentialSuburb() !== '')
        {
            self.DisplayResidentialSuburbValid(false);
        }
        else
        {
            self.DisplayResidentialSuburbValid(true);
        }
    });

    self.ResidentialCity.subscribe(function () {
        if (self.ResidentialCity() !== '') {
            self.DisplayResidentialCityValid(false);
        }
        else {
            self.DisplayResidentialCityValid(true);
        }
    });

    self.ResidentialPostalCode.subscribe(function () {
        if (self.ResidentialPostalCode() !== '') {
            self.DisplayResidentialPostalAddressValid(false);
        }
        else {
            self.DisplayResidentialPostalAddressValid(true);
        }
    });

    self.ResidentialStateProvince.subscribe(function () {
        if (self.ResidentialStateProvince() !== '') {
            self.DisplayResidentialStateValid(false);
        }
        else {
            self.DisplayResidentialStateValid(true);
        }
    });

    //#endregion

    //#region Change symbols on text fiels for Postal Address
    self.PostalSuburb.subscribe(function () {
        if (self.PostalSuburb() !== '') {
            self.DisplayPostalSuburbValid(false);
        }
        else {
            self.DisplayPostalSuburbValid(true);
        }
    });

    self.PostalCity.subscribe(function () {
        if (self.PostalCity() !== '') {
            self.DisplayPostalCityValid(false);
        }
        else {
            self.DisplayPostalCityValid(true);
        }
    });

    self.PostalPostalCode.subscribe(function () {
        if (self.PostalPostalCode() !== '') {
            self.DisplayPostalPostalAddressValid(false);
        }
        else {
            self.DisplayPostalPostalAddressValid(true);
        }
    });

    self.PostalStateProvince.subscribe(function () {
        if (self.PostalStateProvince() !== '') {
            self.DisplayPostalStateValid(false);
        }
        else {
            self.DisplayPostalStateValid(true);
        }
    });
    //#endregion

    //#region Change symbols on text fields for Business Address

    self.BusinessSuburb.subscribe(function () {
        if (self.BusinessSuburb() !== '') {
            self.DisplayBusinessSuburbValid(false);
        }
        else {
            self.DisplayBusinessSuburbValid(true);
        }
    });

    self.BusinessCity.subscribe(function () {
        if (self.BusinessCity() !== '') {
            self.DisplayBusinessCityValid(false);
        }
        else {
            self.DisplayBusinessCityValid(true);
        }
    });

    self.BusinessPostalCode.subscribe(function () {
        if (self.BusinessPostalCode() !== '') {
            self.DisplayBusinessPostalAddressValid(false);
        }
        else {
            self.DisplayBusinessPostalAddressValid(true);
        }
    });

    //#endregion

    //#region Change Symbols on text fields for Billing Address

    self.BillingSuburb.subscribe(function () {
        if (self.BillingSuburb() !== '') {
            self.DisplayBillingSuburbValid(false);
        }
        else {
            self.DisplayBillingSuburbValid(true);
        }
    });

    self.BillingCity.subscribe(function () {
        if (self.BillingCity() !== '') {
            self.DisplayBillingCityValid(false);
        }
        else {
            self.DisplayBillingCityValid(true);
        }
    });

    self.BillingPostalCode.subscribe(function () {
        if (self.BillingPostalCode() !== '') {
            self.DisplayBillingPostalAddressValid(false);
        }
        else {
            self.DisplayBillingPostalAddressValid(true);
        }
    });

    //#endregion

    //#region Copy To Postal Address
    self.CopyResidentialToPostal = function () {
        self.PostalCity(self.ResidentialCity());
        self.PostalCountry (self.ResidentialCountry());
        self.PostalPostalCode(self.ResidentialPostalCode());
        self.PostalStreet1(self.ResidentialStreet1());
        self.PostalStreet2(self.ResidentialStreet2());
        self.PostalStreet3(self.ResidentialStreet3());
        self.PostalStateProvince(self.ResidentialStateProvince());
        self.PostalSuburb(self.ResidentialSuburb());
        self.PostalGisId(self.ResidentialGisId());
    }

    self.CopyBusinessToPostal = function () {
        self.PostalCity(self.BusinessCity());
        self.PostalCountry(self.BusinessCountry());
        self.PostalPostalCode(self.BusinessPostalCode());
        self.PostalStreet1(self.BusinessStreet1());
        self.PostalStreet2(self.BusinessStreet2());
        self.PostalStreet3(self.BusinessStreet3());
        self.PostalStateProvince('');
        self.PostalSuburb(self.BusinessSuburb());
        self.PostalGisId(self.BusinessGisId());
    }

    self.CopyBillingToPostal = function () {
        self.PostalCity(self.BillingCity());
        self.PostalCountry(self.BillingCountry());
        self.PostalPostalCode(self.BillingPostalCode());
        self.PostalStreet1(self.BillingStreet1());
        self.PostalStreet2(self.BillingStreet2());
        self.PostalStreet3(self.BillingStreet3());
        self.PostalStateProvince('');
        self.PostalSuburb(self.BillingSuburb());
        self.PostalGisId(self.BillingGisId());
    }
    //#endregion

    //#region Copy To Business Address
    self.CopyResidentialToBusiness = function () {
        self.BusinessCity(self.ResidentialCity());
        self.BusinessCountry(self.ResidentialCountry());
        self.BusinessPostalCode(self.ResidentialPostalCode());
        self.BusinessStreet1(self.ResidentialStreet1());
        self.BusinessStreet2(self.ResidentialStreet2());
        self.BusinessStreet3(self.ResidentialStreet3());
        self.BusinessSuburb(self.ResidentialSuburb());
        self.BusinessGisId(self.ResidentialGisId());
    }

    self.CopyPostalToBusiness = function () {
        self.BusinessCity(self.PostalCity());
        self.BusinessCountry(self.PostalCountry());
        self.BusinessPostalCode(self.PostalPostalCode());
        self.BusinessStreet1(self.PostalStreet1());
        self.BusinessStreet2(self.PostalStreet2());
        self.BusinessStreet3(self.PostalStreet3());
        self.BusinessSuburb(self.PostalSuburb());
        self.BusinessGisId(self.PostalGisId());
    }

    self.CopyBillingToBusiness = function () {
        self.BusinessCity(self.BillingCity());
        self.BusinessCountry(self.BillingCountry());
        self.BusinessPostalCode(self.BillingPostalCode());
        self.BusinessStreet1(self.BillingStreet1());
        self.BusinessStreet2(self.BillingStreet2());
        self.BusinessStreet3(self.BillingStreet3());
        self.BusinessSuburb(self.BillingSuburb());
        self.BusinessGisId(self.BillingGisId());
    }
    //#endregion

    //#region Copy To Billing Address
    self.CopyResidentialToBilling = function () {
        self.BillingCity(self.ResidentialCity());
        self.BillingCountry(self.ResidentialCountry());
        self.BillingPostalCode(self.ResidentialPostalCode());
        self.BillingStreet1(self.ResidentialStreet1());
        self.BillingStreet2(self.ResidentialStreet2());
        self.BillingStreet3(self.ResidentialStreet3());
        self.BillingSuburb(self.ResidentialSuburb());
        self.BillingGisId(self.ResidentialGisId());
    }

    self.CopyPostalToBilling = function () {
        self.BillingCity(self.PostalCity());
        self.BillingCountry(self.PostalCountry());
        self.BillingPostalCode(self.PostalPostalCode());
        self.BillingStreet1(self.PostalStreet1());
        self.BillingStreet2(self.PostalStreet2());
        self.BillingStreet3(self.PostalStreet3());
        self.BillingSuburb(self.PostalSuburb());
        self.BillingGisId(self.PostalGisId());
    }

    self.CopyBusinessToBilling = function () {
        self.BillingCity(self.BusinessCity());
        self.BillingCountry(self.BusinessCountry());
        self.BillingPostalCode(self.BusinessPostalCode());
        self.BillingStreet1(self.BusinessStreet1());
        self.BillingStreet2(self.BusinessStreet2());
        self.BillingStreet3(self.BusinessStreet3());
        self.BillingSuburb(self.BusinessSuburb());
        self.BillingGisId(self.BusinessGisId());
    }
    //#endregion

    $(function () {
        waitingDialog.show("Preparing Address Details Form");

        self.setRequiredFields();

        LoadDropDown("countries");
    });

    self.StepTwoSaveAndProceed = function () {
        var data = ko.toJSON(AddressDetails);

        if (AddressDetails.errors().length === 0) {
            waitingDialog.show('Saving Address Details');
            $.ajax({
                url: rootUrl + "Page/SubmitAddressDetails",
                contentType: 'application/json; charset=utf-8',
                data: data,
                type: "POST",
                async: true,
                success: function (data) {
                    console.log(data);
                    if (data.Success == false) {
                        self.serverErrorMessage(data.Message);
                        $('#serverValidationAlert').show();
                        window.scroll(0, 0);
                    }
                    else {
                        window.location.href = rootUrl + "Page/WorkStudies";
                    }
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            }).done(function (response) {
                console.log(response);
            });
        }
        else {
            $('#requiredFieldsAlert').show();
            AddressDetails.errors.showAllMessages();
            window.scrollTo(0, 0);
        }
    }

    ///Fix this function
    self.afrigisSearchClick = function(containerId) {
        var searchCriteria = $("#" + containerId + " .afrigisSearchBox .afrigisSearchInput").val();        

        if ($.trim(searchCriteria).length > 0) {
            $("#" + containerId + " .afrigisSearchResultBox .afrigisSearchButton").attr("disabled", "disabled");
            
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchSpinner").show();
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: rootUrl + "Afrigis/GetDistributionAreas?query=" + searchCriteria,
                data: AddAntiForgeryToken({ "query": searchCriteria }),
                contentType: 'application/json; charset=utf-8',
                error: function (x, e) {
                    alert("An error occurred while trying to load the data. Please try again later or contact your service administrator.");
                },
                success: function (data) {
                    $("#" + containerId + " .afrigisSearchBox .afrigisSearchButton").removeAttr("disabled");
                    $("#" + containerId + " .afrigisSearchBox .afrigisSearchSpinner").hide();
                    var listItems = data;
                    if (listItems.length > 0) {
                        $("#" + containerId + " .afrigisSearchBox .afrigisResultList").children().remove();

                        $.each(listItems, function (val, text) {
                            $("#" + containerId + " .afrigisSearchBox .afrigisResultList").append(
                                 $("<option></option>").val((text.Value || text.Value == 0)? text.Value: val).html(text.Text)
                            );
                        });

                        //$("#" + containerId + " .afrigisSearchBox .afrigisResultList").show();
                        $("#" + containerId + " .afrigisSearchBox .afrigisErrorMessage").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").slideDown();
                    }
                    else {
                        $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();
                        $("#" + containerId + " .afrigisSearchBox .afrigisErrorMessage").show();
                        $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").hide();
                    }
                }
            });
        }
    }
    
    //Fix this to 
    self.afrigisResultChange = function (containerId) {
        var afrgisId = $("#" + containerId + " .afrigisSearchBox .afrigisResultList option:selected").val();
        var afrigisVal = $("#" + containerId + " .afrigisSearchBox .afrigisResultList option:selected").text();
        if (afrigisVal != "") {
            var afrigisArr = afrigisVal.split(",");
            
            var suburb = $.trim(afrigisArr[0]);
            var city = $.trim(afrigisArr[1]);
            var postalCode = $.trim(afrigisArr[2]);

            $("#" + containerId + " .afrigisHelpIcon").removeClass('afrigisIncorrectIcon').addClass('afrigisCorrectIcon');
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchResultBox").hide();
            $("#" + containerId + " .afrigisSearchBox .afrigisSuccessMessage").show();
            $("#" + containerId + " .afrigisSearchBox .afrigisSearchInput").val('');

            switch (containerId) {
                case "residential" :
                    {                        
                        self.ResidentialCity(city);
                        self.ResidentialSuburb(suburb);
                        self.ResidentialPostalCode(postalCode);
                        self.ResidentialGisId(afrgisId);
                        return;
                    }
                case "postal":
                    {
                        self.PostalCity(city);
                        self.PostalSuburb(suburb);
                        self.PostalPostalCode(postalCode);
                        self.PostalGisId(afrgisId);
                        return;
                    }
                case "business":
                    {
                        self.BusinessCity(city);
                        self.BusinessSuburb(suburb);
                        self.BusinessPostalCode(postalCode);
                        self.BusinessGisId(afrgisId);
                        return;
                    }
                case "billing":
                    {
                        self.BillingCity(city);
                        self.BillingSuburb(suburb);
                        self.BillingPostalCode(postalCode);
                        self.BillingGisId(afrgisId);
                        return;
                    }
            }
        }
    }
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });
});