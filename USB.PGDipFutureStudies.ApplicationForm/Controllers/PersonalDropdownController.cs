﻿using Newtonsoft.Json;
using StellenboschUniversity.Usb.Integration.Crm;
using StellenboschUniversity.Usb.Integration.Crm.Controllers;
using StellenboschUniversity.Usb.Integration.Crm.Controllers.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.Domain.Models.Enums;
using USB.PGDipFutureStudies.ApplicationForm.ViewModels;
using SettingsMan = USB.PGDipFutureStudies.ApplicationForm.Properties.Settings;

namespace USB.PGDipFutureStudies.ApplicationForm.Controllers
{
    public class PersonalDropdownController : BaseController
    {
        [HttpGet]
        public ActionResult LoadDropdown(string enrolmentId, string type)
        {
            var dropdownList = new List<KeyValuePair<string, int>>();
            var dropdownGuidList = new List<KeyValuePair<string, Guid>>();

            var tupleList = new List<Tuple<string, Guid, ProgrammeOfferingType>>();

            var controller = new ApplicationFormApiController();

            try
            {
                switch (type)
                {
                    case "gender":
                        ;
                        dropdownGuidList = controller.Genders().ToList();
                        break;
                    case "titles":
                        ;
                        dropdownGuidList = controller.Titles().ToList();
                        break;
                    case "ethnicites":
                        ;
                        dropdownGuidList = controller.Ethnicities().ToList();
                        break;
                    case "nonSAEthnicities":
                        ;
                        dropdownGuidList = controller.NonSAEthnicities().ToList();
                        break;
                    case "nationalities":
                        ;
                        dropdownGuidList = controller.Nationalities().ToList();
                        break;
                    case "countriesIssued":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "correspondenceLanguages":
                        ;
                        dropdownGuidList = controller.CorrespondenceLanguages().ToList();
                        break;
                    case "languages":
                        ;
                        dropdownGuidList = controller.Languages().ToList();
                        break;
                    case "foreignIdentificationTypes":
                        ;
                        dropdownGuidList = controller.ForeignIdentificationTypes().ToList();
                        break;
                    case "SouthAfricaIdentificationTypes":
                        ;
                        dropdownGuidList = controller.SouthAfricaIdentificationTypes().ToList();
                        break;
                    case "disabilities":
                        ;
                        dropdownGuidList = controller.Disabilities().ToList();
                        break;
                    case "addressCountries":
                        ;
                        dropdownGuidList = controller.AddressCountries().ToList();
                        break;
                    case "permitTypes":
                        ;
                        dropdownGuidList = controller.PermitTypes().ToList();
                        break;
                    case "maritalStatus":
                        ;
                        dropdownGuidList = controller.MaritalStatus().ToList();
                        break;
                    case "offerings":
                        ;
                        var programmeGuid = SettingsMan.Default.ProgrammeGuid;
                        var isMBA = SettingsMan.Default.IsMBA;
                        //dropdownGuidList = controller.Offerings(isMBA, programmeGuid).ToList();


                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            tupleList = controller.EnrolmentIdGetOfferings(isMBA, programmeGuid, enrolmentGuid).ToList();
                        }
                        else
                        {
                            tupleList = controller.GetOfferings(isMBA, programmeGuid).ToList();
                        }


                        break;
                    case "programme":
                        ;
                        //dropdownList = controller.Programme().ToList();
                        break;
                    case "blendedAttendance":
                        if (enrolmentId.IsNotNullOrEmpty() || enrolmentId != "")
                        {
                            Guid enrolmentGuid = Guid.Parse(enrolmentId);
                            dropdownList = controller.EnrolmentIdGetBlendedOptions(enrolmentGuid).ToList();
                        }
                        else
                        {
                            dropdownList = controller.GetBlendedOptions().ToList();

                        }
                        break;
                    default:
                        ;
                        break;
                }
            }
            catch (InvalidOperationException)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            if (dropdownGuidList.Count > 0)
            {
                return Json(new { dropdownGuidList }, JsonRequestBehavior.AllowGet);
            }

            if (tupleList.Any())
            {
                return Json(new { tupleList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { dropdownList }, JsonRequestBehavior.AllowGet);
        }
    }
}
