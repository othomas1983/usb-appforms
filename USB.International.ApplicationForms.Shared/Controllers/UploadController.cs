﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using NLog;
using StellenboschUniversity.UsbInternational.Integration.Crm;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using StellenboschUniversity.UsbInternational.Integration.Crm.Crm;
using Usb.International.Domain.Sharepoint;
using USB.International.Domain.Models;
using USB.International.Domain.State;
using ApplicationSubmissionResult =
    StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto.ApplicationSubmissionResult;
using System.Linq;
using System.Net.Mime;
using System.Web;
using iTextSharp.text.pdf;

namespace USB.International.ApplicationForms.Shared.Controllers
{
    public class UploadController : Controller
    {

        private const string Upload = "Upload";

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [System.Web.Http.HttpPost]
        public JsonResult SetState(SessionContext context)
        {
            if (!context.IsValid())
                return Json(new {Success = false});
            Session[Upload] = context;
            return Json(new {Success = true});
        }

        private void Setflags(MultiFileUploadViewModel model, SessionContext context)
        {
            bool? insideCRM = context.InsideCRM;

            if (context.InsideCRM)
            {
                model.DisableView = true;
                model.ReviewEnabled = true;
            }
            else if (context.ApplicantStatusEnumValue != ApplicantStatus.Applicant)
            {
                model.DisableView = true;
            }

            foreach (var document in model.Documents)
            {
                document.CanReview = document.Status == "Received" && document.Level < 3 && model.ReviewEnabled;
            }
        }


        [System.Web.Http.HttpGet]
        public ActionResult Index(string data)
        {
            if (Session[Upload] == null && string.IsNullOrEmpty(data))
            {
                return
                    Json(
                        new
                        {
                            Success = false,
                            ErrorMessage =
                            "Your session is invalid or has expired. Please refresh the page or login again. "
                        });
            }
            if (data != null)
            {
                var json = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(data));
                Session[Upload] = JsonConvert.DeserializeObject<SessionContext>(json);
            }
            var context = CheckSessionContext(false);

            context.RootUrl = ViewBag.RootUrl;

            MultiFileUploadViewModel model = context.Sections != null
                ? RestoreExistingModel(context)
                : GetNewModel(context);

            Setflags(model, context);

            Session[Upload] = context;
            ViewBag.Model = JsonConvert.SerializeObject(model);

            return View();

        }

        private static MultiFileUploadViewModel GetNewModel(SessionContext context)
        {
            var controller = new ApplicationFormApiController();

            try
            {
                var enrolmentId = context.EnrolmentId;

                List<Document> requiredDocs = null;
                List<Document> existingDocs = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId) ??
                                              new List<Document>();

                requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId,
                    context.InsideCRM);
                requiredDocs.AddRange(controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId,
                    context.InsideCRM, 2));

                if (context.InsideCRM)
                {
                    var notUploaded = (from required in requiredDocs
                        join document in existingDocs
                        on required.Description equals document.Description
                        into temp
                        from left in temp.DefaultIfEmpty()
                        select new {Required = required, Existing = left}).Where(i => i.Existing == null);

                    existingDocs.AddRange(notUploaded.Select(i => i.Required));

                    var hasSHLDocsAlready = existingDocs.Where(x => x.Description.Contains("SHL")).ToList();

                    if (hasSHLDocsAlready.Count == 0)
                    {
                        List<Document> shlDocumentationResult =
                            controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId, enrolmentId,
                                context.InsideCRM, 2);
                        var shlDocumentation = shlDocumentationResult.Where(x => x.Description.Contains("SHL")).ToList();

                        existingDocs.AddRange(shlDocumentation);
                    }

                    existingDocs = (existingDocs ?? new List<Document>()).OrderBy(x => x.Level).ToList();
                }
                else
                {
                    var applicantStatus = context.ApplicantStatusEnumValue;
                    switch (applicantStatus)
                    {
                        case ApplicantStatus.Applicant:
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, context.InsideCRM);
                            requiredDocs.AddRange(controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, context.InsideCRM, 2));
                            break;
                        case ApplicantStatus.Admitted:
                            requiredDocs = controller.GetRequiredDocumentsInformationDirect(context.ProgrammeId,
                                enrolmentId, context.InsideCRM, 3);
                            break;
                    }
                    ;

                    var notUploaded = (from required in requiredDocs
                        join document in existingDocs
                        on required.Description equals document.Description
                        into temp
                        from left in temp.DefaultIfEmpty()
                        select new {Required = required, Existing = left}).Where(i => i.Existing == null);

                    existingDocs.AddRange(notUploaded.Select(i => i.Required));

                    if (
                        existingDocs.FirstOrDefault(d => d.Description.Contains("Copy of Medical Insurance"))
                            .IsNotNull())
                    {
                        existingDocs.RemoveAll(x => x.Description.Contains("Copy of Medical Insurance"));
                    }

                    if (existingDocs.FirstOrDefault(d => d.Description.Contains("Subject Choices")).IsNotNull())
                    {
                        existingDocs.RemoveAll(x => x.Description.Contains("Subject Choices"));
                    }
                }

                MultiFileUploadViewModel model =
                    new MultiFileUploadViewModel(existingDocs.Where(d => !d.IsExcluded).ToList());
                context.Sections =
                    model.Sections.Select(s => new FileUploadSection(s.Name, s.Level, s.DocumentType, null)).ToList();
                model.Sections = model.Sections.OrderBy(s => s.Name).ToList();
                model.EnrolmentId = context.EnrolmentId;

                var declarationDoc = model.Sections.FirstOrDefault(x => x.Name.Contains("Signed Declaration Form"));

                if (declarationDoc.IsNotNull())
                {
                    model.Sections.Remove(declarationDoc);
                    model.Sections.Insert(0, declarationDoc);
                }

                return model;
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to load Documents: " + ex.Message);
            }

            return null;
        }

        private static MultiFileUploadViewModel RestoreExistingModel(SessionContext context)
        {
            var controller = new ApplicationFormApiController();
            var enrolmentId = context.EnrolmentId;
            List<Document> existingDocs = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId) ?? new List<Document>();
            
            var model = new MultiFileUploadViewModel(existingDocs.Where(d => !d.IsExcluded).ToList());
            var emptysections = (from contextSection in context.Sections
                join dbSection in model.Sections
                    on contextSection.Name equals dbSection.Name
                    into temp
                from left in temp.DefaultIfEmpty()
                select new {Existing = contextSection, Document = left})
                .Where(i => i.Document == null)
                .ToList();

            model.Sections.AddRange(emptysections.Select(s => s.Existing).ToList());
            model.Sections = model.Sections.OrderBy(s => s.Name).ToList();
            model.EnrolmentId = context.EnrolmentId;

            return model;
        }

        [System.Web.Http.HttpGet]
        public FileResult DownloadFile(int documentId)
        {

            var context = CheckSessionContext();

            var controller = new ApplicationFormApiController();

            Tuple<string, byte[]> documentData = controller.GetDocument(documentId,context.EnrolmentId);

            return File(documentData.Item2, System.Net.Mime.MediaTypeNames.Application.Octet, documentData.Item1);
        }


        private string GetBaseUrl(HttpRequestBase request)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
            return baseUrl;
        }

        //[ValidateAntiForgeryToken]
        [System.Web.Http.HttpPost]
        public JsonResult UploadFiles(IEnumerable<string> data, IEnumerable<int> documentIdsToDelete)
        {
            var context = CheckSessionContext();

            List<Document> documents = new List<Document>();

            foreach (var largeDocumentString in data ?? new string[] {})
            {
                documents.Add(JsonConvert.DeserializeObject<Document>(largeDocumentString));
            }

            var model = documents.Any() ? new MultiFileUploadViewModel(documents) : RestoreExistingModel(context);

            try
            {
                var receivedDocuments = model.Documents.Where(d => d.DocumentId == 0).ToList();

                var applicationSubmission = new ApplicationSubmission()
                {
                    ApplicationSource = Domain.Models.ApplicationSource.Documentation,
                    DocumentExclusionKeys = new List<int>(),
                    DocumentUploadKeys = receivedDocuments.Select(d => d.DocumentId).ToList(),
                    DocumentUploadNames = receivedDocuments.Select(d => d.FileName ?? string.Empty).ToList(),
                };

                applicationSubmission.ApplicationSource = ApplicationSource.Documentation;

                var result = SubmitDocumentation(applicationSubmission,
                    receivedDocuments,
                    documentIdsToDelete,
                    context.ProgrammeId,
                    context.EnrolmentId,
                    context.UsNumber,
                    context.ApplicantStatusEnumValue.Value,
                    "",
                    context.Sections);

                if (!result.Successful)
                {
                    return Json(new {Success = false, ErrorMessage = result.Message, ErrorId = result.ErrorId});
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return Json(new {Success = false, ErrorMessage = ex.Message});

            }

            return Json(new {Success = true});
        }

        private SessionContext CheckSessionContext(bool checkSections = true)
        {
            SessionContext context = (SessionContext) Session[Upload] ?? new SessionContext();

            if (context == null || !context.IsValid() || (checkSections && (context.Sections == null || !context.Sections.Any())))
            {
                throw new Exception("Your session has expired or is invalid. Please refresh the page or login again");
            }
            return context;
        }


        public ApplicationSubmissionResult SubmitDocumentation(ApplicationSubmission applicationSubmission, IEnumerable<Document> documentsReceived, IEnumerable<int> documentIdsToDelete,
            Guid programmeId, Guid enrolmentId, string usNumber,
            ApplicantStatus status, string programmeName, IList<FileUploadSection> sections)
        {
            if (!CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrolmentId))
            {
                CrmFileUpload.CreateDocumentLocationForEnrollment(enrolmentId);
            }


            //Add
            var validDocs = (from d in documentsReceived
                             join s in sections
                                 on d.Description equals s.Name
                             select d).ToList();

            foreach (var document in validDocs)
            {
                var bytes = Convert.FromBase64String(document.Data.Substring(document.Data.LastIndexOf(',') + 1));
                SharepointActions.UploadDocument(document.FileName, usNumber, document.Level, programmeName, document.Description, document.DocumentId, enrolmentId, bytes, "No");
                
                applicationSubmission.DocumentUploadKeys.Add(document.DocumentId);
            }
            //Delete
            foreach (var documentId in (documentIdsToDelete ?? new int[] { }).Distinct().ToList())
            {
                SharepointActions.DeleteDocument(documentId, enrolmentId);
                applicationSubmission.DocumentUploadKeys.Remove(documentId);
            }
            
            applicationSubmission.EnrolmentGuid = enrolmentId;

            var controller = new ApplicationFormApiController();
            var uploadedDocuments = controller.GetSharePointUpoadedDocumentsInformation(enrolmentId);

            if (uploadedDocuments.IsNull())
            {
                applicationSubmission.DocumentsOutstandingTwoOptions = true;
                applicationSubmission.DocumentsCompleteTwoOptions = false;
            }
            else
            {
                applicationSubmission.DocumentsOutstandingTwoOptions = uploadedDocuments.IsNotNull() &&
                                                                       sections.Count > uploadedDocuments.Count;
                applicationSubmission.DocumentsCompleteTwoOptions = uploadedDocuments.IsNotNull() &&
                                                                    sections.Count <= uploadedDocuments.Count;
            }
            
            CrmActions.CreateOrUpdateEnrollment(usNumber, applicationSubmission);

            return new ApplicationSubmissionResult() { Successful = true };
        }
    }
}