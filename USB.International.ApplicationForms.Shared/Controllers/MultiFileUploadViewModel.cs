﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;
using StellenboschUniversity.UsbInternational.Integration.Crm.Controllers.Dto;
using USB.International.Domain.Models;

namespace USB.International.ApplicationForms.Shared.Controllers
{
    public class MultiFileUploadViewModel
    {
        private IEnumerable<Document> _documents;
        private IList<FileUploadSection> _sections;

        public MultiFileUploadViewModel(IEnumerable<Document> documents)
        {
            Documents = documents;
        }

        public MultiFileUploadViewModel()
        {
        }

       
        [JsonIgnore]
        public IEnumerable<Document> Documents {
            get
            {
                if (_documents == null && _sections != null && _sections.Any())
                {
                    _documents = _sections.SelectMany(s => (s.Documents ?? new List<Document>()).ToList()).ToList();
                } 
                return _documents;
            }
            set
            {
                _documents = value;
                _sections = 
                    Documents.GroupBy(d => d.Description)
                        .Select(i => new FileUploadSection(i.Key,  Documents.First().Level, Documents.First().DocumentType, Documents.Where(d => d.FileName != null && d.Description == i.Key).ToList())).ToList();


            }
        }

       

        public IList<FileUploadSection> Sections {
            get { return _sections; } 
            set { _sections = value; } 
        }

        public Guid EnrolmentId { get; set; }
        public bool DisableView { get; internal set; }
        public bool ReviewEnabled { get; internal set; }

    }
}