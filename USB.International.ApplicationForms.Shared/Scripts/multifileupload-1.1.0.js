﻿
var documentIdsToDelete = [];

Upload = function (obj) {

    var self = {
        Load: function(model) {

            var result = ProcessResult(model, true);
            self = $.extend(self, result);
           
            var multiFileUpload = new MultiFileUpload(self.Sections);

            ko.applyBindings(multiFileUpload, $("#Upload")[0]);

            GetForm($("#Upload")).validate($.extend(new Validate(), {
                submitHandler: function () {

                    var returnHandler = function (result, success) {
                        waitingDialog.hide();
                        if (success) {
                            var documentationPath = window.parent.location.pathname;
                            var lastIndex = documentationPath.lastIndexOf('/') + 1;

                            var domainPath = documentationPath.substring(0, lastIndex);
                            window.parent.location.href = domainPath + 'Status';
                        } else {
                            ShowDialog(result);
                        };
                    }
                  
                    var files = [];

                    $.each(self.Sections(), function (index, section) {

                        //if (section.Name() == "Signed Declaration Form") {
                        //    if (section.Documents().length == 0) {
                        //        isDeclartionFormProvided = false;
                        //    }
                        //}

                        $.each(section.Documents(), function (index, item) {

                            if (item.DocumentId() == 0 || (item.ReviewStateChanged())) {
                                files.push(ko.toJSON(item));
                            }
                        });

                    });

                    //if (isDeclartionFormProvided) {
                        waitingDialog.show('Saving and uploading documents, please wait..');
                        PostAsync("Upload", "UploadFiles", { data: files, documentIdsToDelete: documentIdsToDelete }, returnHandler, false);
                    //} else {
                    //    alert("Please upload your Declaration Form");
                    //}


                    return false;
                }
            }));
        },
        Saving: ko.observable(),
       
    };

    return self;


}

MultiFileUpload = function (sections) {


    var uploadFileControls = $.map(sections(), function (section) {
        return { Name: section.Name(), FileUpload: new FileUpload(section.Name(), true, section) }
    });
    var self = {
        Sections: ko.observableArray(uploadFileControls),
        GetDownloadLink: function (item) {
            return "Upload/DownloadFile?documentId=" + item.DocumentId() + "&documentTypeName=" + item.Description();
        },
    }

    return self;

}

FileUpload = function (caption, required, parent) {

    var self = {
        Caption: ko.observable(IsDefinedAndNotNull(caption) ? caption : "Upload File"),
        DummyFile: parent.Dummy,
        Files: parent.Documents,
        Upload: function () {
            var maxFiles = 5;
            if (self.Files.length > maxFiles) {
                ShowDialog("A maximum of " + maxFiles + " files are allowed per category");
                return;
            }
            var item = this;
            var existing = $.grep(self.Files, function (candidate) {
                return candidate.FileName() == item.FileName();
            });
            if (existing.length > 0) {
                var index = item.FileName().lastIndexOf(".");
                var fileName = index != -1 ? item.FileName().substr(0, index) : item.FileName();
                var ext = index != -1 ? item.FileName().substr(index) : "";
                item.FileName(fileName + "_" + moment().format("YYYYMMDDhhmmss") + ext);

            };

            var newFile = item.Clone();
            newFile.Description(parent.Name());
            newFile.Level(parent.Level());
            newFile.DocumentType(parent.DocumentType());
            newFile.Status("Received");
            newFile.UploadedDate(moment().format("YYYY/MM/DD"));

            self.Files.push(newFile);

        },
        RemoveFile: function (document) {
            self.Files.remove(document);
            if (document.DocumentId() != 0) {
                documentIdsToDelete.push(document.DocumentId())
            }
        },
        FileChosen: function () {
            $(this).parent().find('.selectedFile')[0].click();
        },
        Required: ko.observable(IsDefinedAndNotNull(required) ? required : false),
        ReviewSelected: function (element) {
            this.ReviewStateChanged(true);
            return true;
        }
    }

    //$.each(self.Files(), function (index, section) {
    //    if (section.Reviewed()) {
    //        debugger;
    //        self.IsDisabled(true);
    //        //$('.reviewed *').prop('disabled', true);
    //    }
    //});

    return self;

}

ko.bindingHandlers['file'] = {
    init: function (element, valueAccessor, allBindings) {

        var fileContents, fileName,  reader, size;

        if ((typeof valueAccessor()) === "function") {
            fileContents = valueAccessor();
        }
        else {
            fileContents = valueAccessor()['data'];
            size = valueAccessor()['size'];
            fileName = valueAccessor()['name'];
            
        }

        reader = new FileReader();
        reader.onloadend = function () {
            fileContents(reader.result);
            allBindings().change(ko.dataFor(element));
        }

        var changeHandler = function () {

            var file = element.files[0];
        
            if (file === undefined) {
                fileContents(null);
                return;
            }

            var acceptedFormats = [".doc", ".docx", ".pdf", ".jpg", ".tiff", ".png"];
            var found = false;
            $.each(acceptedFormats, function (index, item) {
                if (found) return;
                if ((file.name.toUpperCase().endsWith(item.toUpperCase()))) {
                    found = true;
                    return;
                }
            });

            if (!found) {
                alert("A file with this extension is not allowed. Please refer to the top of the page for allowed file types.")
                return;
            }
               

            if (file.size > 2*1024*1024) {
                alert("Maximum file upload file size is 2 MB. Please scan or convert the documents in a lower resolution and upload a smaller file / several smaller files.");
                fileContents(null);
                return;
            }

            size(file.size);

            reader.readAsDataURL(file); // A callback (above) will set fileContents

            if (typeof fileName === "function") {
                fileName(file.name);
            }

        }

        ko.utils.registerEventHandler(element, 'change', changeHandler);

    }
};

ko.bindingHandlers.isReviewed = {
    update: function (elem, valueAccessor) {
        var disabled = ko.utils.unwrapObservable(valueAccessor());

        ko.utils.arrayForEach(elem.getElementsByTagName('input'), function (i) {
            i.disabled = disabled;
        });
    }
};