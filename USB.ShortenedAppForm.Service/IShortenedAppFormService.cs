﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using USB.Domain;
using USB.Domain.Models;

namespace USB.ShortenedAppForm.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IShortenedAppFormService" in both code and config file together.
    [ServiceContract]
    public interface IShortenedAppFormService
    {
        [OperationContract]
        void DoWork();
    }
}
